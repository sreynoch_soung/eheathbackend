
package ehealth.domain;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

/**
 */

@Entity
@NamedQueries({
		@NamedQuery(name = "findAllDoctors", query = "select myDoctor from Doctor myDoctor"),
		@NamedQuery(name = "findDoctorByCodeKey", query = "select myDoctor from Doctor myDoctor where myDoctor.codeKey = ?1"),
		@NamedQuery(name = "findDoctorByCodeKeyContaining", query = "select myDoctor from Doctor myDoctor where myDoctor.codeKey like ?1"),
		@NamedQuery(name = "findDoctorByDoctorId", query = "select myDoctor from Doctor myDoctor where myDoctor.doctorId = ?1"),
		@NamedQuery(name = "findDoctorByHospital", query = "select myDoctor from Doctor myDoctor where myDoctor.hospital = ?1"),
		@NamedQuery(name = "findDoctorByHospitalContaining", query = "select myDoctor from Doctor myDoctor where myDoctor.hospital like ?1"),
		@NamedQuery(name = "findDoctorByPrimaryKey", query = "select myDoctor from Doctor myDoctor where myDoctor.doctorId = ?1") })

@Table(catalog = "ehealthdb", name = "doctor")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(namespace = "eHealthBackend/ehealth/domain", name = "Doctor")
@XmlRootElement(namespace = "eHealthBackend/ehealth/domain")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "doctorId")

public class Doctor implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 */

	@Column(name = "doctor_id", nullable = false)
	@Basic(fetch = FetchType.EAGER)

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@XmlElement
	Integer doctorId;
	/**
	 */

	@Column(name = "hospital", length = 45, nullable = false)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String hospital;
	/**
	 */

	@Column(name = "code_key", length = 45, nullable = false, unique = true)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String codeKey;

	/**
	 */
	@OneToOne(cascade = CascadeType.ALL,orphanRemoval = true,fetch = FetchType.EAGER)
	@JoinColumns({ @JoinColumn(name = "user_id", referencedColumnName = "user_id", nullable = false) })
	@XmlTransient
	User user;
	/**
	 */
	@OneToMany(mappedBy = "doctor", cascade = { CascadeType.REMOVE }, fetch = FetchType.LAZY)

	@XmlElement(name = "", namespace = "")
	java.util.Set<ehealth.domain.ExerciseTarget> exerciseTargets;
	/**
	 */
	@OneToMany(mappedBy = "doctor", cascade = { CascadeType.REMOVE }, fetch = FetchType.LAZY)

	@XmlElement(name = "", namespace = "")
	java.util.Set<ehealth.domain.NutritionTarget> nutritionTargets;
	/**
	 */
	@OneToMany(mappedBy = "doctor", cascade = { CascadeType.REMOVE }, fetch = FetchType.EAGER)

	@XmlElement(name = "", namespace = "")
	java.util.Set<ehealth.domain.Kid> kids;

	/**
	 */
	public void setDoctorId(Integer doctorId) {
		this.doctorId = doctorId;
	}

	/**
	 */
	public Integer getDoctorId() {
		return this.doctorId;
	}

	/**
	 */
	public void setHospital(String hospital) {
		this.hospital = hospital;
	}

	/**
	 */
	public String getHospital() {
		return this.hospital;
	}

	/**
	 */
	public void setCodeKey(String codeKey) {
		this.codeKey = codeKey;
	}

	/**
	 */
	public String getCodeKey() {
		return this.codeKey;
	}

	/**
	 */
	public void setUser(User user) {
		this.user = user;
	}

	/**
	 */
	@JsonIgnore
	public User getUser() {
		return user;
	}

	/**
	 */
	public void setExerciseTargets(Set<ExerciseTarget> exerciseTargets) {
		this.exerciseTargets = exerciseTargets;
	}

	/**
	 */
	@JsonIgnore
	public Set<ExerciseTarget> getExerciseTargets() {
		if (exerciseTargets == null) {
			exerciseTargets = new java.util.LinkedHashSet<ehealth.domain.ExerciseTarget>();
		}
		return exerciseTargets;
	}

	/**
	 */
	public void setNutritionTargets(Set<NutritionTarget> nutritionTargets) {
		this.nutritionTargets = nutritionTargets;
	}

	/**
	 */
	@JsonIgnore
	public Set<NutritionTarget> getNutritionTargets() {
		if (nutritionTargets == null) {
			nutritionTargets = new java.util.LinkedHashSet<ehealth.domain.NutritionTarget>();
		}
		return nutritionTargets;
	}

	/**
	 */
	public void setKids(Set<Kid> kids) {
		this.kids = kids;
	}

	/**
	 */
	@JsonIgnore
	public Set<Kid> getKids() {
		if (kids == null) {
			kids = new java.util.LinkedHashSet<ehealth.domain.Kid>();
		}
		return kids;
	}

	/**
	 */
	public Doctor() {
	}

	/**
	 * Copies the contents of the specified bean into this bean.
	 *
	 */
	public void copy(Doctor that) {
		setDoctorId(that.getDoctorId());
		setHospital(that.getHospital());
		setCodeKey(that.getCodeKey());
		setUser(that.getUser());
		setExerciseTargets(new java.util.LinkedHashSet<ehealth.domain.ExerciseTarget>(that.getExerciseTargets()));
		setNutritionTargets(new java.util.LinkedHashSet<ehealth.domain.NutritionTarget>(that.getNutritionTargets()));
		setKids(new java.util.LinkedHashSet<ehealth.domain.Kid>(that.getKids()));
	}

	/**
	 * Returns a textual representation of a bean.
	 *
	 */
	public String toString() {

		StringBuilder buffer = new StringBuilder();

		buffer.append("doctorId=[").append(doctorId).append("] ");
		buffer.append("hospital=[").append(hospital).append("] ");
		buffer.append("codeKey=[").append(codeKey).append("] ");

		return buffer.toString();
	}

	/**
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = (int) (prime * result + ((doctorId == null) ? 0 : doctorId.hashCode()));
		return result;
	}

	/**
	 */
	public boolean equals(Object obj) {
		if (obj == this)
			return true;
		if (!(obj instanceof Doctor))
			return false;
		Doctor equalCheck = (Doctor) obj;
		if ((doctorId == null && equalCheck.doctorId != null) || (doctorId != null && equalCheck.doctorId == null))
			return false;
		if (doctorId != null && !doctorId.equals(equalCheck.doctorId))
			return false;
		return true;
	}
}
