
package ehealth.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Set;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

/**
 */

@Entity
@NamedQueries({
		@NamedQuery(name = "findAllFoodTageTypes", query = "select myFoodTageType from FoodTageType myFoodTageType"),
		@NamedQuery(name = "findFoodTageTypeByFoodTageTypeId", query = "select myFoodTageType from FoodTageType myFoodTageType where myFoodTageType.foodTageTypeId = ?1"),
		@NamedQuery(name = "findFoodTageTypeByPrimaryKey", query = "select myFoodTageType from FoodTageType myFoodTageType where myFoodTageType.foodTageTypeId = ?1"),
		@NamedQuery(name = "findFoodTageTypeByTypeName", query = "select myFoodTageType from FoodTageType myFoodTageType where myFoodTageType.typeName = ?1"),
		@NamedQuery(name = "findFoodTageTypeByTypeNameContaining", query = "select myFoodTageType from FoodTageType myFoodTageType where myFoodTageType.typeName like ?1") })

@Table(catalog = "ehealthdb", name = "food_tage_type")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(namespace = "eHealthBackend/ehealth/domain", name = "FoodTageType")
@XmlRootElement(namespace = "eHealthBackend/ehealth/domain")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "foodTageTypeId")

public class FoodTageType implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 */

	@Column(name = "food_tage_type_id", nullable = false)
	@Basic(fetch = FetchType.EAGER)

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@XmlElement
	Integer foodTageTypeId;
	/**
	 */

	@Column(name = "type_name", length = 45)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String typeName;
	/**
	 */

	@Column(name = "value", precision = 12, nullable = false)
	@Basic(fetch = FetchType.EAGER)

	BigDecimal value;

	/**
	 */
	@OneToMany(mappedBy = "foodTageType", cascade = { CascadeType.REMOVE }, fetch = FetchType.LAZY)

	@XmlElement(name = "", namespace = "")
	java.util.Set<ehealth.domain.NutritionTarget> nutritionTargets;
	/**
	 */
	@OneToMany(mappedBy = "foodTageType", cascade = { CascadeType.REMOVE }, fetch = FetchType.LAZY)

	@XmlElement(name = "", namespace = "")
	java.util.Set<ehealth.domain.FoodTageInput> foodTageInputs;

	/**
	 */
	public void setFoodTageTypeId(Integer foodTageTypeId) {
		this.foodTageTypeId = foodTageTypeId;
	}

	/**
	 */
	public Integer getFoodTageTypeId() {
		return this.foodTageTypeId;
	}

	/**
	 */
	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}

	/**
	 */
	public String getTypeName() {
		return this.typeName;
	}

	/**
	 */
	public void setNutritionTargets(Set<NutritionTarget> nutritionTargets) {
		this.nutritionTargets = nutritionTargets;
	}

	/**
	 */
	@JsonIgnore
	public Set<NutritionTarget> getNutritionTargets() {
		if (nutritionTargets == null) {
			nutritionTargets = new java.util.LinkedHashSet<ehealth.domain.NutritionTarget>();
		}
		return nutritionTargets;
	}

	/**
	 */
	public void setFoodTageInputs(Set<FoodTageInput> foodTageInputs) {
		this.foodTageInputs = foodTageInputs;
	}

	/**
	 */
	@JsonIgnore
	public Set<FoodTageInput> getFoodTageInputs() {
		if (foodTageInputs == null) {
			foodTageInputs = new java.util.LinkedHashSet<ehealth.domain.FoodTageInput>();
		}
		return foodTageInputs;
	}

	public BigDecimal getValue() {
		return value;
	}

	public void setValue(BigDecimal value) {
		this.value = value;
	}

	/**
	 */
	public FoodTageType() {
	}

	/**
	 * Copies the contents of the specified bean into this bean.
	 *
	 */
	public void copy(FoodTageType that) {
		setFoodTageTypeId(that.getFoodTageTypeId());
		setTypeName(that.getTypeName());
		setNutritionTargets(new java.util.LinkedHashSet<ehealth.domain.NutritionTarget>(that.getNutritionTargets()));
		setFoodTageInputs(new java.util.LinkedHashSet<ehealth.domain.FoodTageInput>(that.getFoodTageInputs()));
	}

	/**
	 * Returns a textual representation of a bean.
	 *
	 */
	public String toString() {

		StringBuilder buffer = new StringBuilder();

		buffer.append("foodTageTypeId=[").append(foodTageTypeId).append("] ");
		buffer.append("typeName=[").append(typeName).append("] ");

		return buffer.toString();
	}

	/**
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = (int) (prime * result + ((foodTageTypeId == null) ? 0 : foodTageTypeId.hashCode()));
		return result;
	}

	/**
	 */
	public boolean equals(Object obj) {
		if (obj == this)
			return true;
		if (!(obj instanceof FoodTageType))
			return false;
		FoodTageType equalCheck = (FoodTageType) obj;
		if ((foodTageTypeId == null && equalCheck.foodTageTypeId != null) || (foodTageTypeId != null && equalCheck.foodTageTypeId == null))
			return false;
		if (foodTageTypeId != null && !foodTageTypeId.equals(equalCheck.foodTageTypeId))
			return false;
		return true;
	}
}
