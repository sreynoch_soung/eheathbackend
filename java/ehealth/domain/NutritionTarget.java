
package ehealth.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Calendar;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

/**
 */

@Entity
@NamedQueries({
		@NamedQuery(name = "findAllNutritionTargets", query = "select myNutritionTarget from NutritionTarget myNutritionTarget"),
		@NamedQuery(name = "findNutritionTargetByDate", query = "select myNutritionTarget from NutritionTarget myNutritionTarget where myNutritionTarget.date = ?1"),
		@NamedQuery(name = "findNutritionTargetByNutritionTargetId", query = "select myNutritionTarget from NutritionTarget myNutritionTarget where myNutritionTarget.nutritionTargetId = ?1"),
		@NamedQuery(name = "findNutritionTargetByPrimaryKey", query = "select myNutritionTarget from NutritionTarget myNutritionTarget where myNutritionTarget.nutritionTargetId = ?1") })

@Table(catalog = "ehealthdb", name = "nutrition_target")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(namespace = "eHealthBackend/ehealth/domain", name = "NutritionTarget")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "nutritionTargetId")

public class NutritionTarget implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 */

	@Column(name = "nutrition_target_id", nullable = false)
	@Basic(fetch = FetchType.EAGER)

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@XmlElement
	Integer nutritionTargetId;
	/**
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "date", nullable = false)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	Calendar date;

	/**
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "end_date", nullable = false)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	Calendar endDate;
	/**
	 */

	@Column(name = "value", precision = 12, nullable = false)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal value;
	
	/**
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumns({ @JoinColumn(name = "doctor_id", referencedColumnName = "doctor_id", nullable = false) })
	@XmlTransient
	Doctor doctor;
	/**
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumns({ @JoinColumn(name = "kid_id", referencedColumnName = "kid_id", nullable = false) })
	@XmlTransient
	Kid kid;
	/**
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumns({ @JoinColumn(name = "food_tage_id", referencedColumnName = "food_tage_type_id", nullable = false) })
	@XmlTransient
	FoodTageType foodTageType;

	/**
	 */
	public void setNutritionTargetId(Integer nutritionTargetId) {
		this.nutritionTargetId = nutritionTargetId;
	}

	/**
	 */
	public Integer getNutritionTargetId() {
		return this.nutritionTargetId;
	}

	/**
	 */
	public void setDate(Calendar date) {
		this.date = date;
	}

	/**
	 */
	public Calendar getDate() {
		return this.date;
	}

	public BigDecimal getValue() {
		return value;
	}

	public void setValue(BigDecimal value) {
		this.value = value;
	}

	public Calendar getEndDate() {
		return endDate;
	}

	public void setEndDate(Calendar endDate) {
		this.endDate = endDate;
	}

	/**
	 */
	public void setDoctor(Doctor doctor) {
		this.doctor = doctor;
	}

	/**
	 */
	@JsonIgnore
	public Doctor getDoctor() {
		return doctor;
	}

	/**
	 */
	public void setKid(Kid kid) {
		this.kid = kid;
	}

	/**
	 */
	@JsonIgnore
	public Kid getKid() {
		return kid;
	}

	/**
	 */
	public void setFoodTageType(FoodTageType foodTageType) {
		this.foodTageType = foodTageType;
	}

	/**
	 */
	@JsonIgnore
	public FoodTageType getFoodTageType() {
		return foodTageType;
	}

	/**
	 */
	public NutritionTarget() {
	}

	/**
	 * Copies the contents of the specified bean into this bean.
	 *
	 */
	public void copy(NutritionTarget that) {
		setNutritionTargetId(that.getNutritionTargetId());
		setValue(that.getValue());
		setDate(that.getDate());
		setDoctor(that.getDoctor());
		setKid(that.getKid());
		setFoodTageType(that.getFoodTageType());
	}

	/**
	 * Returns a textual representation of a bean.
	 *
	 */
	public String toString() {

		StringBuilder buffer = new StringBuilder();

		buffer.append("nutritionTargetId=[").append(nutritionTargetId).append("] ");
		buffer.append("value=[").append(value).append("] ");
		buffer.append("date=[").append(date).append("] ");

		return buffer.toString();
	}

	/**
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = (int) (prime * result + ((nutritionTargetId == null) ? 0 : nutritionTargetId.hashCode()));
		return result;
	}

	/**
	 */
	public boolean equals(Object obj) {
		if (obj == this)
			return true;
		if (!(obj instanceof NutritionTarget))
			return false;
		NutritionTarget equalCheck = (NutritionTarget) obj;
		if ((nutritionTargetId == null && equalCheck.nutritionTargetId != null) || (nutritionTargetId != null && equalCheck.nutritionTargetId == null))
			return false;
		if (nutritionTargetId != null && !nutritionTargetId.equals(equalCheck.nutritionTargetId))
			return false;
		return true;
	}
}
