package ehealth.domain;

import java.util.Set;

public class ExerciseTargetInput {
	Set<EffortType> efforttypes;
	ExerciseTarget exercisetarget;
	public Set<EffortType> getEfforttypes() {
		return efforttypes;
	}
	public void setEfforttypes(Set<EffortType> efforttypes) {
		this.efforttypes = efforttypes;
	}
	public ExerciseTarget getExercisetarget() {
		return exercisetarget;
	}
	public void setExercisetarget(ExerciseTarget exercisetarget) {
		this.exercisetarget = exercisetarget;
	}
	
	
	
}
