
package ehealth.domain;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

/**
 */

@Entity
@NamedQueries({
		@NamedQuery(name = "findAllCountrys", query = "select myCountry from Country myCountry"),
		@NamedQuery(name = "findCountryByCountryId", query = "select myCountry from Country myCountry where myCountry.countryId = ?1"),
		@NamedQuery(name = "findCountryByName", query = "select myCountry from Country myCountry where myCountry.name = ?1"),
		@NamedQuery(name = "findCountryByNameContaining", query = "select myCountry from Country myCountry where myCountry.name like ?1"),
		@NamedQuery(name = "findCountryByPrimaryKey", query = "select myCountry from Country myCountry where myCountry.countryId = ?1") })

@Table(catalog = "ehealthdb", name = "country")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(namespace = "eHealthBackend/ehealth/domain", name = "Country")
@XmlRootElement(namespace = "eHealthBackend/ehealth/domain")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "countryId")

public class Country implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 */

	@Column(name = "country_id", nullable = false)
	@Basic(fetch = FetchType.EAGER)

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@XmlElement
	Integer countryId;
	/**
	 */

	@Column(name = "name", length = 45)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String name;

	/**
	 */
	@OneToMany(mappedBy = "country", cascade = { CascadeType.REMOVE }, fetch = FetchType.LAZY)

	@XmlElement(name = "", namespace = "")
	java.util.Set<ehealth.domain.City> cities;

	/**
	 */
	public void setCountryId(Integer countryId) {
		this.countryId = countryId;
	}

	/**
	 */
	public Integer getCountryId() {
		return this.countryId;
	}

	/**
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 */
	public String getName() {
		return this.name;
	}

	/**
	 */
	public void setCities(Set<City> cities) {
		this.cities = cities;
	}

	/**
	 */
	@JsonIgnore
	public Set<City> getCities() {
		if (cities == null) {
			cities = new java.util.LinkedHashSet<ehealth.domain.City>();
		}
		return cities;
	}

	/**
	 */
	public Country() {
	}

	/**
	 * Copies the contents of the specified bean into this bean.
	 *
	 */
	public void copy(Country that) {
		setCountryId(that.getCountryId());
		setName(that.getName());
		setCities(new java.util.LinkedHashSet<ehealth.domain.City>(that.getCities()));
	}

	/**
	 * Returns a textual representation of a bean.
	 *
	 */
	public String toString() {

		StringBuilder buffer = new StringBuilder();

		buffer.append("countryId=[").append(countryId).append("] ");
		buffer.append("name=[").append(name).append("] ");

		return buffer.toString();
	}

	/**
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = (int) (prime * result + ((countryId == null) ? 0 : countryId.hashCode()));
		return result;
	}

	/**
	 */
	public boolean equals(Object obj) {
		if (obj == this)
			return true;
		if (!(obj instanceof Country))
			return false;
		Country equalCheck = (Country) obj;
		if ((countryId == null && equalCheck.countryId != null) || (countryId != null && equalCheck.countryId == null))
			return false;
		if (countryId != null && !countryId.equals(equalCheck.countryId))
			return false;
		return true;
	}
}
