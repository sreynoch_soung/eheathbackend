
package ehealth.domain;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Set;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

/**
 */

@Entity
@NamedQueries({
		@NamedQuery(name = "findAllChatRecords", query = "select myChatRecord from ChatRecord myChatRecord"),
		@NamedQuery(name = "findChatRecordByChatRecordId", query = "select myChatRecord from ChatRecord myChatRecord where myChatRecord.chatRecordId = ?1"),
		@NamedQuery(name = "findChatRecordByDate", query = "select myChatRecord from ChatRecord myChatRecord where myChatRecord.date = ?1"),
		@NamedQuery(name = "findChatRecordByMessage", query = "select myChatRecord from ChatRecord myChatRecord where myChatRecord.message = ?1"),
		@NamedQuery(name = "findChatRecordByMessageContaining", query = "select myChatRecord from ChatRecord myChatRecord where myChatRecord.message like ?1"),
		@NamedQuery(name = "findChatRecordByPrimaryKey", query = "select myChatRecord from ChatRecord myChatRecord where myChatRecord.chatRecordId = ?1") })

@Table(catalog = "ehealthdb", name = "chat_record")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(namespace = "eHealthBackend/ehealth/domain", name = "ChatRecord")
@XmlRootElement(namespace = "eHealthBackend/ehealth/domain")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "chatRecordId")

public class ChatRecord implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 */

	@Column(name = "chat_record_id", nullable = false)
	@Basic(fetch = FetchType.EAGER)

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@XmlElement
	Integer chatRecordId;
	/**
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "date", nullable = false)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	Calendar date;
	/**
	 */

	@Column(name = "message", length = 400, nullable = false)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String message;

	/**
	 */
	@OneToMany(mappedBy = "chatRecord", cascade = { CascadeType.REMOVE }, fetch = FetchType.LAZY)

	@XmlElement(name = "", namespace = "")
	java.util.Set<ehealth.domain.KidChat> kidChats;

	/**
	 */
	public void setChatRecordId(Integer chatRecordId) {
		this.chatRecordId = chatRecordId;
	}

	/**
	 */
	public Integer getChatRecordId() {
		return this.chatRecordId;
	}

	/**
	 */
	public void setDate(Calendar date) {
		this.date = date;
	}

	/**
	 */
	public Calendar getDate() {
		return this.date;
	}

	/**
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 */
	public String getMessage() {
		return this.message;
	}

	/**
	 */
	public void setKidChats(Set<KidChat> kidChats) {
		this.kidChats = kidChats;
	}

	/**
	 */
	@JsonIgnore
	public Set<KidChat> getKidChats() {
		if (kidChats == null) {
			kidChats = new java.util.LinkedHashSet<ehealth.domain.KidChat>();
		}
		return kidChats;
	}

	/**
	 */
	public ChatRecord() {
	}

	/**
	 * Copies the contents of the specified bean into this bean.
	 *
	 */
	public void copy(ChatRecord that) {
		setChatRecordId(that.getChatRecordId());
		setDate(that.getDate());
		setMessage(that.getMessage());
		setKidChats(new java.util.LinkedHashSet<ehealth.domain.KidChat>(that.getKidChats()));
	}

	/**
	 * Returns a textual representation of a bean.
	 *
	 */
	public String toString() {

		StringBuilder buffer = new StringBuilder();

		buffer.append("chatRecordId=[").append(chatRecordId).append("] ");
		buffer.append("date=[").append(date).append("] ");
		buffer.append("message=[").append(message).append("] ");

		return buffer.toString();
	}

	/**
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = (int) (prime * result + ((chatRecordId == null) ? 0 : chatRecordId.hashCode()));
		return result;
	}

	/**
	 */
	public boolean equals(Object obj) {
		if (obj == this)
			return true;
		if (!(obj instanceof ChatRecord))
			return false;
		ChatRecord equalCheck = (ChatRecord) obj;
		if ((chatRecordId == null && equalCheck.chatRecordId != null) || (chatRecordId != null && equalCheck.chatRecordId == null))
			return false;
		if (chatRecordId != null && !chatRecordId.equals(equalCheck.chatRecordId))
			return false;
		return true;
	}
}
