
package ehealth.domain;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

/**
 */

@Entity
@NamedQueries({
		@NamedQuery(name = "findAllFoodTageInputs", query = "select myFoodTageInput from FoodTageInput myFoodTageInput"),
		@NamedQuery(name = "findFoodTageInputByFoodTageInputId", query = "select myFoodTageInput from FoodTageInput myFoodTageInput where myFoodTageInput.foodTageInputId = ?1"),
		@NamedQuery(name = "findFoodTageInputByPrimaryKey", query = "select myFoodTageInput from FoodTageInput myFoodTageInput where myFoodTageInput.foodTageInputId = ?1"),
		@NamedQuery(name = "findFoodTageInputByValue", query = "select myFoodTageInput from FoodTageInput myFoodTageInput where myFoodTageInput.value = ?1") })

@Table(catalog = "ehealthdb", name = "food_tage_input")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(namespace = "eHealthBackend/ehealth/domain", name = "FoodTageInput")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "foodTageInputId")

public class FoodTageInput implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 */

	@Column(name = "food_tage_input_id", nullable = false)
	@Basic(fetch = FetchType.EAGER)

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@XmlElement
	Integer foodTageInputId;
	/**
	 */

	@Column(name = "value", nullable = false)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	Integer value;

	/**
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumns({
			@JoinColumn(name = "food_tage_type_id", referencedColumnName = "food_tage_type_id", nullable = false) })
	@XmlTransient
	FoodTageType foodTageType;
	/**
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumns({ @JoinColumn(name = "food_photo_id", referencedColumnName = "food_photo_id", nullable = false) })
	@XmlTransient
	FoodPhoto foodPhoto;

	/**
	 */
	public void setFoodTageInputId(Integer foodTageInputId) {
		this.foodTageInputId = foodTageInputId;
	}

	/**
	 */
	public Integer getFoodTageInputId() {
		return this.foodTageInputId;
	}

	/**
	 */
	public void setValue(Integer value) {
		this.value = value;
	}

	/**
	 */
	public Integer getValue() {
		return this.value;
	}

	/**
	 */
	public void setFoodTageType(FoodTageType foodTageType) {
		this.foodTageType = foodTageType;
	}

	/**
	 */
	@JsonIgnore
	public FoodTageType getFoodTageType() {
		return foodTageType;
	}

	/**
	 */
	public void setFoodPhoto(FoodPhoto foodPhoto) {
		this.foodPhoto = foodPhoto;
	}

	/**
	 */
	@JsonIgnore
	public FoodPhoto getFoodPhoto() {
		return foodPhoto;
	}

	/**
	 */
	public FoodTageInput() {
	}

	/**
	 * Copies the contents of the specified bean into this bean.
	 *
	 */
	public void copy(FoodTageInput that) {
		setFoodTageInputId(that.getFoodTageInputId());
		setValue(that.getValue());
		setFoodTageType(that.getFoodTageType());
		setFoodPhoto(that.getFoodPhoto());
	}

	/**
	 * Returns a textual representation of a bean.
	 *
	 */
	public String toString() {

		StringBuilder buffer = new StringBuilder();

		buffer.append("foodTageInputId=[").append(foodTageInputId).append("] ");
		buffer.append("value=[").append(value).append("] ");

		return buffer.toString();
	}

	/**
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = (int) (prime * result + ((foodTageInputId == null) ? 0 : foodTageInputId.hashCode()));
		return result;
	}

	/**
	 */
	public boolean equals(Object obj) {
		if (obj == this)
			return true;
		if (!(obj instanceof FoodTageInput))
			return false;
		FoodTageInput equalCheck = (FoodTageInput) obj;
		if ((foodTageInputId == null && equalCheck.foodTageInputId != null) || (foodTageInputId != null && equalCheck.foodTageInputId == null))
			return false;
		if (foodTageInputId != null && !foodTageInputId.equals(equalCheck.foodTageInputId))
			return false;
		return true;
	}
}
