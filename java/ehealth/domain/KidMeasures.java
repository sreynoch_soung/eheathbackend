
package ehealth.domain;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

/**
 */

@Entity
@NamedQueries({
		@NamedQuery(name = "findAllKidMeasuress", query = "select myKidMeasures from KidMeasures myKidMeasures"),
		@NamedQuery(name = "findKidMeasuresByBloodSugarLevel", query = "select myKidMeasures from KidMeasures myKidMeasures where myKidMeasures.bloodSugarLevel = ?1"),
		@NamedQuery(name = "findKidMeasuresByGlucoseLevel", query = "select myKidMeasures from KidMeasures myKidMeasures where myKidMeasures.glucoseLevel = ?1"),
		@NamedQuery(name = "findKidMeasuresByHabit", query = "select myKidMeasures from KidMeasures myKidMeasures where myKidMeasures.habit = ?1"),
		@NamedQuery(name = "findKidMeasuresByHabitContaining", query = "select myKidMeasures from KidMeasures myKidMeasures where myKidMeasures.habit like ?1"),
		@NamedQuery(name = "findKidMeasuresByKidMeasuresId", query = "select myKidMeasures from KidMeasures myKidMeasures where myKidMeasures.kidMeasuresId = ?1"),
		@NamedQuery(name = "findKidMeasuresByMets", query = "select myKidMeasures from KidMeasures myKidMeasures where myKidMeasures.mets = ?1"),
		@NamedQuery(name = "findKidMeasuresByPrimaryKey", query = "select myKidMeasures from KidMeasures myKidMeasures where myKidMeasures.kidMeasuresId = ?1") })

@Table(catalog = "ehealthdb", name = "kid_measures")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(namespace = "eHealthBackend/ehealth/domain", name = "KidMeasures")
@XmlRootElement(namespace = "eHealthBackend/ehealth/domain")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "kidMeasuresId")

public class KidMeasures implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 */

	@Column(name = "kid_measures_id", nullable = false)
	@Basic(fetch = FetchType.EAGER)

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@XmlElement
	Integer kidMeasuresId;
	/**
	 */

	@Column(name = "glucose_level")
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	Integer glucoseLevel;
	/**
	 */

	@Column(name = "blood_sugar_level")
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	Integer bloodSugarLevel;
	/**
	 */

	@Column(name = "habit", length = 200)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String habit;
	/**
	 */

	@Column(name = "mets")
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	Integer mets;

	/**
	 */
	@OneToMany(mappedBy = "kidMeasures", cascade = { CascadeType.REMOVE }, fetch = FetchType.LAZY)

	@XmlElement(name = "", namespace = "")
	java.util.Set<ehealth.domain.Kid> kids;

	/**
	 */
	public void setKidMeasuresId(Integer kidMeasuresId) {
		this.kidMeasuresId = kidMeasuresId;
	}

	/**
	 */
	public Integer getKidMeasuresId() {
		return this.kidMeasuresId;
	}

	/**
	 */
	public void setGlucoseLevel(Integer glucoseLevel) {
		this.glucoseLevel = glucoseLevel;
	}

	/**
	 */
	public Integer getGlucoseLevel() {
		return this.glucoseLevel;
	}

	/**
	 */
	public void setBloodSugarLevel(Integer bloodSugarLevel) {
		this.bloodSugarLevel = bloodSugarLevel;
	}

	/**
	 */
	public Integer getBloodSugarLevel() {
		return this.bloodSugarLevel;
	}

	/**
	 */
	public void setHabit(String habit) {
		this.habit = habit;
	}

	/**
	 */
	public String getHabit() {
		return this.habit;
	}

	/**
	 */
	public void setMets(Integer mets) {
		this.mets = mets;
	}

	/**
	 */
	public Integer getMets() {
		return this.mets;
	}

	/**
	 */
	public void setKids(Set<Kid> kids) {
		this.kids = kids;
	}

	/**
	 */
	@JsonIgnore
	public Set<Kid> getKids() {
		if (kids == null) {
			kids = new java.util.LinkedHashSet<ehealth.domain.Kid>();
		}
		return kids;
	}

	/**
	 */
	public KidMeasures() {
	}

	/**
	 * Copies the contents of the specified bean into this bean.
	 *
	 */
	public void copy(KidMeasures that) {
		setKidMeasuresId(that.getKidMeasuresId());
		setGlucoseLevel(that.getGlucoseLevel());
		setBloodSugarLevel(that.getBloodSugarLevel());
		setHabit(that.getHabit());
		setMets(that.getMets());
		setKids(new java.util.LinkedHashSet<ehealth.domain.Kid>(that.getKids()));
	}

	/**
	 * Returns a textual representation of a bean.
	 *
	 */
	public String toString() {

		StringBuilder buffer = new StringBuilder();

		buffer.append("kidMeasuresId=[").append(kidMeasuresId).append("] ");
		buffer.append("glucoseLevel=[").append(glucoseLevel).append("] ");
		buffer.append("bloodSugarLevel=[").append(bloodSugarLevel).append("] ");
		buffer.append("habit=[").append(habit).append("] ");
		buffer.append("mets=[").append(mets).append("] ");

		return buffer.toString();
	}

	/**
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = (int) (prime * result + ((kidMeasuresId == null) ? 0 : kidMeasuresId.hashCode()));
		return result;
	}

	/**
	 */
	public boolean equals(Object obj) {
		if (obj == this)
			return true;
		if (!(obj instanceof KidMeasures))
			return false;
		KidMeasures equalCheck = (KidMeasures) obj;
		if ((kidMeasuresId == null && equalCheck.kidMeasuresId != null) || (kidMeasuresId != null && equalCheck.kidMeasuresId == null))
			return false;
		if (kidMeasuresId != null && !kidMeasuresId.equals(equalCheck.kidMeasuresId))
			return false;
		return true;
	}
}
