
package ehealth.domain;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

/**
 */

@Entity
@NamedQueries({
		@NamedQuery(name = "findAllKidChats", query = "select myKidChat from KidChat myKidChat"),
		@NamedQuery(name = "findKidChatByKidChatId", query = "select myKidChat from KidChat myKidChat where myKidChat.kidChatId = ?1"),
		@NamedQuery(name = "findKidChatByPrimaryKey", query = "select myKidChat from KidChat myKidChat where myKidChat.kidChatId = ?1") })

@Table(catalog = "ehealthdb", name = "kid_chat")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(namespace = "eHealthBackend/ehealth/domain", name = "KidChat")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "kidChatId")

public class KidChat implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 */

	@Column(name = "kid_chat_id", nullable = false)
	@Basic(fetch = FetchType.EAGER)

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@XmlElement
	Integer kidChatId;

	/**
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumns({ @JoinColumn(name = "kid_id_2", referencedColumnName = "kid_id", nullable = false) })
	@XmlTransient
	Kid kidByKidId2;
	/**
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumns({ @JoinColumn(name = "chat_record_id", referencedColumnName = "chat_record_id", nullable = false) })
	@XmlTransient
	ChatRecord chatRecord;
	/**
	 */
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumns({ @JoinColumn(name = "kid_id_1", referencedColumnName = "kid_id", nullable = false) })
	@XmlTransient
	Kid kidByKidId1;

	/**
	 */
	public void setKidChatId(Integer kidChatId) {
		this.kidChatId = kidChatId;
	}

	/**
	 */
	public Integer getKidChatId() {
		return this.kidChatId;
	}

	/**
	 */
	public void setKidByKidId2(Kid kidByKidId2) {
		this.kidByKidId2 = kidByKidId2;
	}

	/**
	 */
	@JsonIgnore
	public Kid getKidByKidId2() {
		return kidByKidId2;
	}

	/**
	 */
	public void setChatRecord(ChatRecord chatRecord) {
		this.chatRecord = chatRecord;
	}

	/**
	 */
	@JsonIgnore
	public ChatRecord getChatRecord() {
		return chatRecord;
	}

	/**
	 */
	public void setKidByKidId1(Kid kidByKidId1) {
		this.kidByKidId1 = kidByKidId1;
	}

	/**
	 */
	@JsonIgnore
	public Kid getKidByKidId1() {
		return kidByKidId1;
	}

	/**
	 */
	public KidChat() {
	}

	/**
	 * Copies the contents of the specified bean into this bean.
	 *
	 */
	public void copy(KidChat that) {
		setKidChatId(that.getKidChatId());
		setKidByKidId2(that.getKidByKidId2());
		setChatRecord(that.getChatRecord());
		setKidByKidId1(that.getKidByKidId1());
	}

	/**
	 * Returns a textual representation of a bean.
	 *
	 */
	public String toString() {

		StringBuilder buffer = new StringBuilder();

		buffer.append("kidChatId=[").append(kidChatId).append("] ");

		return buffer.toString();
	}

	/**
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = (int) (prime * result + ((kidChatId == null) ? 0 : kidChatId.hashCode()));
		return result;
	}

	/**
	 */
	public boolean equals(Object obj) {
		if (obj == this)
			return true;
		if (!(obj instanceof KidChat))
			return false;
		KidChat equalCheck = (KidChat) obj;
		if ((kidChatId == null && equalCheck.kidChatId != null) || (kidChatId != null && equalCheck.kidChatId == null))
			return false;
		if (kidChatId != null && !kidChatId.equals(equalCheck.kidChatId))
			return false;
		return true;
	}
}
