
package ehealth.domain;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

/**
 */

@Entity
@NamedQueries({
		@NamedQuery(name = "findAllCitys", query = "select myCity from City myCity"),
		@NamedQuery(name = "findCityByCityId", query = "select myCity from City myCity where myCity.cityId = ?1"),
		@NamedQuery(name = "findCityByName", query = "select myCity from City myCity where myCity.name = ?1"),
		@NamedQuery(name = "findCityByNameContaining", query = "select myCity from City myCity where myCity.name like ?1"),
		@NamedQuery(name = "findCityByPrimaryKey", query = "select myCity from City myCity where myCity.cityId = ?1") })

@Table(catalog = "ehealthdb", name = "city")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(namespace = "eHealthBackend/ehealth/domain", name = "City")
@XmlRootElement(namespace = "eHealthBackend/ehealth/domain")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "cityId")

public class City implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 */

	@Column(name = "city_id", nullable = false)
	@Basic(fetch = FetchType.EAGER)

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@XmlElement
	Integer cityId;
	/**
	 */

	@Column(name = "name", length = 45)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String name;

	/**
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumns({ @JoinColumn(name = "country_id", referencedColumnName = "country_id", nullable = false) })
	@XmlTransient
	Country country;
	/**
	 */
	@OneToMany(mappedBy = "city", cascade = { CascadeType.REMOVE }, fetch = FetchType.LAZY)

	@XmlElement(name = "", namespace = "")
	java.util.Set<ehealth.domain.Parent> parents;
	/**
	 */
	@OneToMany(mappedBy = "city", cascade = { CascadeType.REMOVE }, fetch = FetchType.LAZY)

	@XmlElement(name = "", namespace = "")
	java.util.Set<ehealth.domain.Kid> kids;

	/**
	 */
	public void setCityId(Integer cityId) {
		this.cityId = cityId;
	}

	/**
	 */
	public Integer getCityId() {
		return this.cityId;
	}

	/**
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 */
	public String getName() {
		return this.name;
	}

	/**
	 */
	public void setCountry(Country country) {
		this.country = country;
	}

	/**
	 */
	@JsonIgnore
	public Country getCountry() {
		return country;
	}

	/**
	 */
	public void setParents(Set<Parent> parents) {
		this.parents = parents;
	}

	/**
	 */
	@JsonIgnore
	public Set<Parent> getParents() {
		if (parents == null) {
			parents = new java.util.LinkedHashSet<ehealth.domain.Parent>();
		}
		return parents;
	}

	/**
	 */
	public void setKids(Set<Kid> kids) {
		this.kids = kids;
	}

	/**
	 */
	@JsonIgnore
	public Set<Kid> getKids() {
		if (kids == null) {
			kids = new java.util.LinkedHashSet<ehealth.domain.Kid>();
		}
		return kids;
	}

	/**
	 */
	public City() {
	}

	/**
	 * Copies the contents of the specified bean into this bean.
	 *
	 */
	public void copy(City that) {
		setCityId(that.getCityId());
		setName(that.getName());
		setCountry(that.getCountry());
		setParents(new java.util.LinkedHashSet<ehealth.domain.Parent>(that.getParents()));
		setKids(new java.util.LinkedHashSet<ehealth.domain.Kid>(that.getKids()));
	}

	/**
	 * Returns a textual representation of a bean.
	 *
	 */
	public String toString() {

		StringBuilder buffer = new StringBuilder();

		buffer.append("cityId=[").append(cityId).append("] ");
		buffer.append("name=[").append(name).append("] ");

		return buffer.toString();
	}

	/**
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = (int) (prime * result + ((cityId == null) ? 0 : cityId.hashCode()));
		return result;
	}

	/**
	 */
	public boolean equals(Object obj) {
		if (obj == this)
			return true;
		if (!(obj instanceof City))
			return false;
		City equalCheck = (City) obj;
		if ((cityId == null && equalCheck.cityId != null) || (cityId != null && equalCheck.cityId == null))
			return false;
		if (cityId != null && !cityId.equals(equalCheck.cityId))
			return false;
		return true;
	}
}
