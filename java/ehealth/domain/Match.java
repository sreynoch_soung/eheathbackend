
package ehealth.domain;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Set;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

/**
 */

@Entity
@NamedQueries({
		@NamedQuery(name = "findAllMatchs", query = "select myMatch from Match myMatch"),
		@NamedQuery(name = "findMatchByDate", query = "select myMatch from Match myMatch where myMatch.date = ?1"),
		@NamedQuery(name = "findMatchByMatchId", query = "select myMatch from Match myMatch where myMatch.matchId = ?1"),
		@NamedQuery(name = "findMatchByPrimaryKey", query = "select myMatch from Match myMatch where myMatch.matchId = ?1"),
		@NamedQuery(name = "findMatchByWinScore", query = "select myMatch from Match myMatch where myMatch.winScore = ?1"),
		@NamedQuery(name = "findMatchByWinScoreContaining", query = "select myMatch from Match myMatch where myMatch.winScore like ?1") })

@Table(catalog = "ehealthdb", name = "match")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(namespace = "eHealthBackend/ehealth/domain", name = "Match")
@XmlRootElement(namespace = "eHealthBackend/ehealth/domain")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "matchId")

public class Match implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 */

	@Column(name = "match_id", nullable = false)
	@Basic(fetch = FetchType.EAGER)

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@XmlElement
	Integer matchId;
	/**
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "date", nullable = false)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	Calendar date;
	/**
	 */

	@Column(name = "win_score", length = 3)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String winScore;

	/**
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumns({ @JoinColumn(name = "team_id2", referencedColumnName = "team_id", nullable = false) })
	@XmlTransient
	Team teamByTeamId2;
	/**
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumns({ @JoinColumn(name = "team_id1", referencedColumnName = "team_id", nullable = false) })
	@XmlTransient
	Team teamByTeamId1;
	/**
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumns({ @JoinColumn(name = "team_id_win", referencedColumnName = "team_id") })
	@XmlTransient
	Team teamByTeamIdWin;
	/**
	 */
	@OneToMany(mappedBy = "match", cascade = { CascadeType.REMOVE }, fetch = FetchType.LAZY)

	@XmlElement(name = "", namespace = "")
	java.util.Set<ehealth.domain.League> leagues;

	/**
	 */
	public void setMatchId(Integer matchId) {
		this.matchId = matchId;
	}

	/**
	 */
	public Integer getMatchId() {
		return this.matchId;
	}

	/**
	 */
	public void setDate(Calendar date) {
		this.date = date;
	}

	/**
	 */
	public Calendar getDate() {
		return this.date;
	}

	/**
	 */
	public void setWinScore(String winScore) {
		this.winScore = winScore;
	}

	/**
	 */
	public String getWinScore() {
		return this.winScore;
	}

	/**
	 */
	public void setTeamByTeamId2(Team teamByTeamId2) {
		this.teamByTeamId2 = teamByTeamId2;
	}

	/**
	 */
	@JsonIgnore
	public Team getTeamByTeamId2() {
		return teamByTeamId2;
	}

	/**
	 */
	public void setTeamByTeamId1(Team teamByTeamId1) {
		this.teamByTeamId1 = teamByTeamId1;
	}

	/**
	 */
	@JsonIgnore
	public Team getTeamByTeamId1() {
		return teamByTeamId1;
	}

	/**
	 */
	public void setTeamByTeamIdWin(Team teamByTeamIdWin) {
		this.teamByTeamIdWin = teamByTeamIdWin;
	}

	/**
	 */
	@JsonIgnore
	public Team getTeamByTeamIdWin() {
		return teamByTeamIdWin;
	}

	/**
	 */
	public void setLeagues(Set<League> leagues) {
		this.leagues = leagues;
	}

	/**
	 */
	@JsonIgnore
	public Set<League> getLeagues() {
		if (leagues == null) {
			leagues = new java.util.LinkedHashSet<ehealth.domain.League>();
		}
		return leagues;
	}

	/**
	 */
	public Match() {
	}

	/**
	 * Copies the contents of the specified bean into this bean.
	 *
	 */
	public void copy(Match that) {
		setMatchId(that.getMatchId());
		setDate(that.getDate());
		setWinScore(that.getWinScore());
		setTeamByTeamId2(that.getTeamByTeamId2());
		setTeamByTeamId1(that.getTeamByTeamId1());
		setTeamByTeamIdWin(that.getTeamByTeamIdWin());
		setLeagues(new java.util.LinkedHashSet<ehealth.domain.League>(that.getLeagues()));
	}

	/**
	 * Returns a textual representation of a bean.
	 *
	 */
	public String toString() {

		StringBuilder buffer = new StringBuilder();

		buffer.append("matchId=[").append(matchId).append("] ");
		buffer.append("date=[").append(date).append("] ");
		buffer.append("winScore=[").append(winScore).append("] ");

		return buffer.toString();
	}

	/**
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = (int) (prime * result + ((matchId == null) ? 0 : matchId.hashCode()));
		return result;
	}

	/**
	 */
	public boolean equals(Object obj) {
		if (obj == this)
			return true;
		if (!(obj instanceof Match))
			return false;
		Match equalCheck = (Match) obj;
		if ((matchId == null && equalCheck.matchId != null) || (matchId != null && equalCheck.matchId == null))
			return false;
		if (matchId != null && !matchId.equals(equalCheck.matchId))
			return false;
		return true;
	}
}
