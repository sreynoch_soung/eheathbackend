package ehealth.domain;

import java.io.Serializable;

public class UserAccountKid implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	User user;
	int kidCityId;
	int doctorId;
	
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public int getKidCityId() {
		return kidCityId;
	}
	public void setKidCityId(int kidCityId) {
		this.kidCityId = kidCityId;
	}
	public int getDoctorId() {
		return doctorId;
	}
	public void setDoctorId(int doctorId) {
		this.doctorId = doctorId;
	}
}
