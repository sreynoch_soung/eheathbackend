
package ehealth.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Set;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

/**
 */

@Entity
@NamedQueries({
		@NamedQuery(name = "findAllKids", query = "select myKid from Kid myKid"),
		@NamedQuery(name = "findKidByAddress", query = "select myKid from Kid myKid where myKid.address = ?1"),
		@NamedQuery(name = "findKidByAddressContaining", query = "select myKid from Kid myKid where myKid.address like ?1"),
		@NamedQuery(name = "findKidByCodeKey", query = "select myKid from Kid myKid where myKid.codeKey = ?1"),
		@NamedQuery(name = "findKidByCodeKeyContaining", query = "select myKid from Kid myKid where myKid.codeKey like ?1"),
		@NamedQuery(name = "findKidByHigh", query = "select myKid from Kid myKid where myKid.high = ?1"),
		@NamedQuery(name = "findKidByKidId", query = "select myKid from Kid myKid where myKid.kidId = ?1"),
		@NamedQuery(name = "findKidByPrimaryKey", query = "select myKid from Kid myKid where myKid.kidId = ?1"),
		@NamedQuery(name = "findKidByWeight", query = "select myKid from Kid myKid where myKid.weight = ?1")})

@Table(catalog = "ehealthdb", name = "kid")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(namespace = "eHealthBackend/ehealth/domain", name = "Kid")
@XmlRootElement(namespace = "eHealthBackend/ehealth/domain")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "kidId")

public class Kid implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 */

	@Column(name = "kid_id", nullable = false)
	@Basic(fetch = FetchType.EAGER)

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@XmlElement
	Integer kidId;
	/**
	 */

	@Column(name = "high", precision = 12)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal high;
	/**
	 */

	@Column(name = "weight", precision = 12)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal weight;
	/**
	 */

	@Column(name = "address", length = 150)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String address;
	/**
	 */

	@Column(name = "code_key", length = 45, nullable = false, unique = true)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String codeKey;

	/**
	 */

	@Column(name = "connected", nullable = false)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	Boolean connected;
	
	/**
	 */
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumns({ @JoinColumn(name = "doctor_id", referencedColumnName = "doctor_id") })
	@XmlTransient
	Doctor doctor;
	/**
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumns({ @JoinColumn(name = "team_id", referencedColumnName = "team_id") })
	@XmlTransient
	Team team;
	/**
	 */
	@OneToOne(cascade = CascadeType.ALL,orphanRemoval = true,fetch = FetchType.EAGER)
	@JoinColumns({ @JoinColumn(name = "user_id", referencedColumnName = "user_id", nullable = false) })
	@XmlTransient
	User user;
	/**
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumns({ @JoinColumn(name = "account_setting_id", referencedColumnName = "account_setting_id") })
	@XmlTransient
	AccountSetting accountSetting;
	/**
	 */
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumns({ @JoinColumn(name = "city_id", referencedColumnName = "city_id", nullable = false) })
	@XmlTransient
	City city;
	/**
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumns({ @JoinColumn(name = "kid_measures_id", referencedColumnName = "kid_measures_id") })
	@XmlTransient
	KidMeasures kidMeasures;
	/**
	 */
	@OneToMany(mappedBy = "kid", cascade = { CascadeType.REMOVE }, fetch = FetchType.LAZY)

	@XmlElement(name = "", namespace = "")
	java.util.Set<ehealth.domain.NutritionTarget> nutritionTargets;
	/**
	 */
	@OneToMany(mappedBy = "kid", cascade = { CascadeType.REMOVE }, fetch = FetchType.LAZY)

	@XmlElement(name = "", namespace = "")
	java.util.Set<ehealth.domain.ExerciseInput> exerciseInputs;
	/**
	 */
	@OneToMany(mappedBy = "kid", cascade = { CascadeType.REMOVE }, fetch = FetchType.LAZY)

	@XmlElement(name = "", namespace = "")
	java.util.Set<ehealth.domain.Meal> meals;
	/**
	 */
	@OneToMany(mappedBy = "kid", cascade = { CascadeType.REMOVE }, fetch = FetchType.LAZY)

	@XmlElement(name = "", namespace = "")
	java.util.Set<ehealth.domain.ExerciseTarget> exerciseTargets;
	/**
	 */
	@OneToMany(mappedBy = "kid", cascade = { CascadeType.REMOVE }, fetch = FetchType.LAZY)

	@XmlElement(name = "", namespace = "")
	java.util.Set<ehealth.domain.ParentKid> parentKids;
	/**
	 */
	@OneToMany(mappedBy = "kidByKidId2", cascade = { CascadeType.REMOVE }, fetch = FetchType.LAZY)

	@XmlElement(name = "", namespace = "")
	java.util.Set<ehealth.domain.KidChat> kidChatsForKidId2;
	/**
	 */
	@OneToMany(mappedBy = "kid", cascade = { CascadeType.REMOVE }, fetch = FetchType.LAZY)

	@XmlElement(name = "", namespace = "")
	java.util.Set<ehealth.domain.Bonus> bonuses;
	/**
	 */
	@OneToMany(mappedBy = "kidByKidId2", cascade = { CascadeType.REMOVE }, fetch = FetchType.LAZY)

	@XmlElement(name = "", namespace = "")
	java.util.Set<ehealth.domain.KidChat> kidChatsForKidId1;
	/**
	 */
	@OneToMany(mappedBy = "kid", cascade = { CascadeType.REMOVE }, fetch = FetchType.LAZY)

	@XmlElement(name = "", namespace = "")
	java.util.Set<ehealth.domain.Grad> grads;

	/**
	 */
	public void setKidId(Integer kidId) {
		this.kidId = kidId;
	}

	/**
	 */
	public Integer getKidId() {
		return this.kidId;
	}

	/**
	 */
	public void setHigh(BigDecimal high) {
		this.high = high;
	}

	/**
	 */
	public BigDecimal getHigh() {
		return this.high;
	}

	/**
	 */
	public void setWeight(BigDecimal weight) {
		this.weight = weight;
	}

	/**
	 */
	public BigDecimal getWeight() {
		return this.weight;
	}

	/**
	 */
	public void setAddress(String address) {
		this.address = address;
	}

	/**
	 */
	public String getAddress() {
		return this.address;
	}

	/**
	 */
	public void setCodeKey(String codeKey) {
		this.codeKey = codeKey;
	}

	/**
	 */
	public String getCodeKey() {
		return this.codeKey;
	}
	
	/**
	 */
	public Boolean getConnected() {
		return connected;
	}

	/**
	 */
	public void setConnected(Boolean connected) {
		this.connected = connected;
	}

	/**
	 */
	public void setDoctor(Doctor doctor) {
		this.doctor = doctor;
	}

	/**
	 */
	@JsonIgnore
	public Doctor getDoctor() {
		return doctor;
	}

	/**
	 */
	public void setTeam(Team team) {
		this.team = team;
	}

	/**
	 */
	@JsonIgnore
	public Team getTeam() {
		return team;
	}

	/**
	 */
	public void setUser(User user) {
		this.user = user;
	}

	/**
	 */
//	@JsonIgnore
	public User getUser() {
		return user;
	}

	/**
	 */
	public void setAccountSetting(AccountSetting accountSetting) {
		this.accountSetting = accountSetting;
	}

	/**
	 */
	@JsonIgnore
	public AccountSetting getAccountSetting() {
		return accountSetting;
	}

	/**
	 */
	public void setCity(City city) {
		this.city = city;
	}

	/**
	 */
	@JsonIgnore
	public City getCity() {
		return city;
	}

	/**
	 */
	public void setKidMeasures(KidMeasures kidMeasures) {
		this.kidMeasures = kidMeasures;
	}

	/**
	 */
	@JsonIgnore
	public KidMeasures getKidMeasures() {
		return kidMeasures;
	}

	/**
	 */
	public void setNutritionTargets(Set<NutritionTarget> nutritionTargets) {
		this.nutritionTargets = nutritionTargets;
	}

	/**
	 */
	@JsonIgnore
	public Set<NutritionTarget> getNutritionTargets() {
		if (nutritionTargets == null) {
			nutritionTargets = new java.util.LinkedHashSet<ehealth.domain.NutritionTarget>();
		}
		return nutritionTargets;
	}

	/**
	 */
	public void setExerciseInputs(Set<ExerciseInput> exerciseInputs) {
		this.exerciseInputs = exerciseInputs;
	}

	/**
	 */
	@JsonIgnore
	public Set<ExerciseInput> getExerciseInputs() {
		if (exerciseInputs == null) {
			exerciseInputs = new java.util.LinkedHashSet<ehealth.domain.ExerciseInput>();
		}
		return exerciseInputs;
	}

	/**
	 */
	public void setMeals(Set<Meal> meals) {
		this.meals = meals;
	}

	/**
	 */
	@JsonIgnore
	public Set<Meal> getMeals() {
		if (meals == null) {
			meals = new java.util.LinkedHashSet<ehealth.domain.Meal>();
		}
		return meals;
	}

	/**
	 */
	public void setExerciseTargets(Set<ExerciseTarget> exerciseTargets) {
		this.exerciseTargets = exerciseTargets;
	}

	/**
	 */
	@JsonIgnore
	public Set<ExerciseTarget> getExerciseTargets() {
		if (exerciseTargets == null) {
			exerciseTargets = new java.util.LinkedHashSet<ehealth.domain.ExerciseTarget>();
		}
		return exerciseTargets;
	}

	/**
	 */
	public void setParentKids(Set<ParentKid> parentKids) {
		this.parentKids = parentKids;
	}

	/**
	 */
	@JsonIgnore
	public Set<ParentKid> getParentKids() {
		if (parentKids == null) {
			parentKids = new java.util.LinkedHashSet<ehealth.domain.ParentKid>();
		}
		return parentKids;
	}

	/**
	 */
	public void setKidChatsForKidId2(Set<KidChat> kidChatsForKidId2) {
		this.kidChatsForKidId2 = kidChatsForKidId2;
	}

	/**
	 */
	@JsonIgnore
	public Set<KidChat> getKidChatsForKidId2() {
		if (kidChatsForKidId2 == null) {
			kidChatsForKidId2 = new java.util.LinkedHashSet<ehealth.domain.KidChat>();
		}
		return kidChatsForKidId2;
	}

	/**
	 */
	public void setBonuses(Set<Bonus> bonuses) {
		this.bonuses = bonuses;
	}

	/**
	 */
	@JsonIgnore
	public Set<Bonus> getBonuses() {
		if (bonuses == null) {
			bonuses = new java.util.LinkedHashSet<ehealth.domain.Bonus>();
		}
		return bonuses;
	}

	/**
	 */
	public void setKidChatsForKidId1(Set<KidChat> kidChatsForKidId1) {
		this.kidChatsForKidId1 = kidChatsForKidId1;
	}

	/**
	 */
	@JsonIgnore
	public Set<KidChat> getKidChatsForKidId1() {
		if (kidChatsForKidId1 == null) {
			kidChatsForKidId1 = new java.util.LinkedHashSet<ehealth.domain.KidChat>();
		}
		return kidChatsForKidId1;
	}

	/**
	 */
	public void setGrads(Set<Grad> grads) {
		this.grads = grads;
	}

	/**
	 */
	@JsonIgnore
	public Set<Grad> getGrads() {
		if (grads == null) {
			grads = new java.util.LinkedHashSet<ehealth.domain.Grad>();
		}
		return grads;
	}

	/**
	 */
	public Kid() {
	}

	/**
	 * Copies the contents of the specified bean into this bean.
	 *
	 */
	public void copy(Kid that) {
		setKidId(that.getKidId());
		setHigh(that.getHigh());
		setWeight(that.getWeight());
		setAddress(that.getAddress());
		setConnected(that.getConnected());
		setCodeKey(that.getCodeKey());
		setDoctor(that.getDoctor());
		setTeam(that.getTeam());
		setUser(that.getUser());
		setAccountSetting(that.getAccountSetting());
		setCity(that.getCity());
		setKidMeasures(that.getKidMeasures());
		setNutritionTargets(new java.util.LinkedHashSet<ehealth.domain.NutritionTarget>(that.getNutritionTargets()));
		setExerciseInputs(new java.util.LinkedHashSet<ehealth.domain.ExerciseInput>(that.getExerciseInputs()));
		setMeals(new java.util.LinkedHashSet<ehealth.domain.Meal>(that.getMeals()));
		setExerciseTargets(new java.util.LinkedHashSet<ehealth.domain.ExerciseTarget>(that.getExerciseTargets()));
		setParentKids(new java.util.LinkedHashSet<ehealth.domain.ParentKid>(that.getParentKids()));
		setKidChatsForKidId2(new java.util.LinkedHashSet<ehealth.domain.KidChat>(that.getKidChatsForKidId2()));
		setBonuses(new java.util.LinkedHashSet<ehealth.domain.Bonus>(that.getBonuses()));
		setKidChatsForKidId1(new java.util.LinkedHashSet<ehealth.domain.KidChat>(that.getKidChatsForKidId1()));
		setGrads(new java.util.LinkedHashSet<ehealth.domain.Grad>(that.getGrads()));
	}

	/**
	 * Returns a textual representation of a bean.
	 *
	 */
	public String toString() {

		StringBuilder buffer = new StringBuilder();

		buffer.append("kidId=[").append(kidId).append("] ");
		buffer.append("high=[").append(high).append("] ");
		buffer.append("weight=[").append(weight).append("] ");
		buffer.append("address=[").append(address).append("] ");
		buffer.append("codeKey=[").append(codeKey).append("] ");
		buffer.append("connected=[").append(connected).append("] ");
		return buffer.toString();
	}

	/**
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = (int) (prime * result + ((kidId == null) ? 0 : kidId.hashCode()));
		return result;
	}

	/**
	 */
	public boolean equals(Object obj) {
		if (obj == this)
			return true;
		if (!(obj instanceof Kid))
			return false;
		Kid equalCheck = (Kid) obj;
		if ((kidId == null && equalCheck.kidId != null) || (kidId != null && equalCheck.kidId == null))
			return false;
		if (kidId != null && !kidId.equals(equalCheck.kidId))
			return false;
		return true;
	}
}
