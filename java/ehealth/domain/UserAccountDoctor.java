package ehealth.domain;

import java.io.Serializable;

public class UserAccountDoctor implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	User user;
	String hospitalName;
	
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public String getHospitalName() {
		return hospitalName;
	}
	public void setHospitalName(String hospitalName) {
		this.hospitalName = hospitalName;
	}

	

}
