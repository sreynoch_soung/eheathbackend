
package ehealth.domain;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

/**
 */

@Entity
@NamedQueries({
		@NamedQuery(name = "findAllBonuss", query = "select myBonus from Bonus myBonus"),
		@NamedQuery(name = "findBonusByActivityType", query = "select myBonus from Bonus myBonus where myBonus.activityType = ?1"),
		@NamedQuery(name = "findBonusByActivityTypeContaining", query = "select myBonus from Bonus myBonus where myBonus.activityType like ?1"),
		@NamedQuery(name = "findBonusByBonusId", query = "select myBonus from Bonus myBonus where myBonus.bonusId = ?1"),
		@NamedQuery(name = "findBonusByPoint", query = "select myBonus from Bonus myBonus where myBonus.point = ?1"),
		@NamedQuery(name = "findBonusByPrimaryKey", query = "select myBonus from Bonus myBonus where myBonus.bonusId = ?1") })

@Table(catalog = "ehealthdb", name = "bonus")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(namespace = "eHealthBackend/ehealth/domain", name = "Bonus")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "bonusId")

public class Bonus implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 */

	@Column(name = "bonus_id", nullable = false)
	@Basic(fetch = FetchType.EAGER)

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@XmlElement
	Integer bonusId;
	/**
	 */

	@Column(name = "point", nullable = false)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	Integer point;
	/**
	 */

	@Column(name = "activity_type", length = 45)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String activityType;

	/**
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumns({ @JoinColumn(name = "kid_id", referencedColumnName = "kid_id", nullable = false) })
	@XmlTransient
	Kid kid;

	/**
	 */
	public void setBonusId(Integer bonusId) {
		this.bonusId = bonusId;
	}

	/**
	 */
	public Integer getBonusId() {
		return this.bonusId;
	}

	/**
	 */
	public void setPoint(Integer point) {
		this.point = point;
	}

	/**
	 */
	public Integer getPoint() {
		return this.point;
	}

	/**
	 */
	public void setActivityType(String activityType) {
		this.activityType = activityType;
	}

	/**
	 */
	public String getActivityType() {
		return this.activityType;
	}

	/**
	 */
	public void setKid(Kid kid) {
		this.kid = kid;
	}

	/**
	 */
	@JsonIgnore
	public Kid getKid() {
		return kid;
	}

	/**
	 */
	public Bonus() {
	}

	/**
	 * Copies the contents of the specified bean into this bean.
	 *
	 */
	public void copy(Bonus that) {
		setBonusId(that.getBonusId());
		setPoint(that.getPoint());
		setActivityType(that.getActivityType());
		setKid(that.getKid());
	}

	/**
	 * Returns a textual representation of a bean.
	 *
	 */
	public String toString() {

		StringBuilder buffer = new StringBuilder();

		buffer.append("bonusId=[").append(bonusId).append("] ");
		buffer.append("point=[").append(point).append("] ");
		buffer.append("activityType=[").append(activityType).append("] ");

		return buffer.toString();
	}

	/**
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = (int) (prime * result + ((bonusId == null) ? 0 : bonusId.hashCode()));
		return result;
	}

	/**
	 */
	public boolean equals(Object obj) {
		if (obj == this)
			return true;
		if (!(obj instanceof Bonus))
			return false;
		Bonus equalCheck = (Bonus) obj;
		if ((bonusId == null && equalCheck.bonusId != null) || (bonusId != null && equalCheck.bonusId == null))
			return false;
		if (bonusId != null && !bonusId.equals(equalCheck.bonusId))
			return false;
		return true;
	}
}
