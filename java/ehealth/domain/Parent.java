
package ehealth.domain;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

/**
 */

@Entity
@NamedQueries({
		@NamedQuery(name = "findAllParents", query = "select myParent from Parent myParent"),
		@NamedQuery(name = "findParentByAddress", query = "select myParent from Parent myParent where myParent.address = ?1"),
		@NamedQuery(name = "findParentByAddressContaining", query = "select myParent from Parent myParent where myParent.address like ?1"),
		@NamedQuery(name = "findParentByOccupation", query = "select myParent from Parent myParent where myParent.occupation = ?1"),
		@NamedQuery(name = "findParentByOccupationContaining", query = "select myParent from Parent myParent where myParent.occupation like ?1"),
		@NamedQuery(name = "findParentByParentId", query = "select myParent from Parent myParent where myParent.parentId = ?1"),
		@NamedQuery(name = "findParentByPrimaryKey", query = "select myParent from Parent myParent where myParent.parentId = ?1") })

@Table(catalog = "ehealthdb", name = "parent")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(namespace = "eHealthBackend/ehealth/domain", name = "Parent")
@XmlRootElement(namespace = "eHealthBackend/ehealth/domain")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "parentId")

public class Parent implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 */

	@Column(name = "parent_id", nullable = false)
	@Basic(fetch = FetchType.EAGER)

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@XmlElement
	Integer parentId;
	/**
	 */

	@Column(name = "occupation", length = 45)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String occupation;
	/**
	 */

	@Column(name = "address", length = 150)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String address;

	/**
	 */
	@OneToOne(cascade = CascadeType.ALL,orphanRemoval = true,fetch = FetchType.LAZY)
	@JoinColumns({ @JoinColumn(name = "user_id", referencedColumnName = "user_id", nullable = false) })
	@XmlTransient
	User user;
	/**
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumns({ @JoinColumn(name = "account_setting_id", referencedColumnName = "account_setting_id") })
	@XmlTransient
	AccountSetting accountSetting;
	/**
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumns({ @JoinColumn(name = "city_id", referencedColumnName = "city_id", nullable = false) })
	@XmlTransient
	City city;
	/**
	 */
	@OneToMany(mappedBy = "parent", cascade = { CascadeType.REMOVE }, fetch = FetchType.LAZY)

	@XmlElement(name = "", namespace = "")
	java.util.Set<ehealth.domain.ParentKid> parentKids;

	/**
	 */
	public void setParentId(Integer parentId) {
		this.parentId = parentId;
	}

	/**
	 */
	public Integer getParentId() {
		return this.parentId;
	}

	/**
	 */
	public void setOccupation(String occupation) {
		this.occupation = occupation;
	}

	/**
	 */
	public String getOccupation() {
		return this.occupation;
	}

	/**
	 */
	public void setAddress(String address) {
		this.address = address;
	}

	/**
	 */
	public String getAddress() {
		return this.address;
	}

	/**
	 */
	public void setUser(User user) {
		this.user = user;
	}

	/**
	 */
	@JsonIgnore
	public User getUser() {
		return user;
	}

	/**
	 */
	public void setAccountSetting(AccountSetting accountSetting) {
		this.accountSetting = accountSetting;
	}

	/**
	 */
	@JsonIgnore
	public AccountSetting getAccountSetting() {
		return accountSetting;
	}

	/**
	 */
	public void setCity(City city) {
		this.city = city;
	}

	/**
	 */
	@JsonIgnore
	public City getCity() {
		return city;
	}

	/**
	 */
	public void setParentKids(Set<ParentKid> parentKids) {
		this.parentKids = parentKids;
	}

	/**
	 */
	@JsonIgnore
	public Set<ParentKid> getParentKids() {
		if (parentKids == null) {
			parentKids = new java.util.LinkedHashSet<ehealth.domain.ParentKid>();
		}
		return parentKids;
	}

	/**
	 */
	public Parent() {
	}

	/**
	 * Copies the contents of the specified bean into this bean.
	 *
	 */
	public void copy(Parent that) {
		setParentId(that.getParentId());
		setOccupation(that.getOccupation());
		setAddress(that.getAddress());
		setUser(that.getUser());
		setAccountSetting(that.getAccountSetting());
		setCity(that.getCity());
		setParentKids(new java.util.LinkedHashSet<ehealth.domain.ParentKid>(that.getParentKids()));
	}

	/**
	 * Returns a textual representation of a bean.
	 *
	 */
	public String toString() {

		StringBuilder buffer = new StringBuilder();

		buffer.append("parentId=[").append(parentId).append("] ");
		buffer.append("occupation=[").append(occupation).append("] ");
		buffer.append("address=[").append(address).append("] ");

		return buffer.toString();
	}

	/**
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = (int) (prime * result + ((parentId == null) ? 0 : parentId.hashCode()));
		return result;
	}

	/**
//	 */
//	public boolean equals(Object obj) {
//		if (obj == this)
//			return true;
//		if (!(obj instanceof Parent))
//			return false;
//		Parent equalCheck = (Parent) obj;
//		if ((parentId == null && equalCheck.parentId != null) || (parentId != null && equalCheck.parentId == null))
//			return false;
//		if (parentId != null && !parentId.equals(equalCheck.parentId))
//			return false;
//		return true;
//	}
}
