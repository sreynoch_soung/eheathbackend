
package ehealth.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Calendar;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

/**
 */

@Entity
@NamedQueries({
		@NamedQuery(name = "findAllExerciseTargets", query = "select myExerciseTarget from ExerciseTarget myExerciseTarget"),
		@NamedQuery(name = "findExerciseTargetByDate", query = "select myExerciseTarget from ExerciseTarget myExerciseTarget where myExerciseTarget.date = ?1"),
		@NamedQuery(name = "findExerciseTargetByExerciseTargetId", query = "select myExerciseTarget from ExerciseTarget myExerciseTarget where myExerciseTarget.exerciseTargetId = ?1"),
		@NamedQuery(name = "findExerciseTargetByPrimaryKey", query = "select myExerciseTarget from ExerciseTarget myExerciseTarget where myExerciseTarget.exerciseTargetId = ?1"),
		@NamedQuery(name = "findExerciseTargetByValue", query = "select myExerciseTarget from ExerciseTarget myExerciseTarget where myExerciseTarget.value = ?1") })

@Table(catalog = "ehealthdb", name = "exercise_target")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(namespace = "eHealthBackend/ehealth/domain", name = "ExerciseTarget")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "exerciseTargetId")

public class ExerciseTarget implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 */

	@Column(name = "exercise_target_id", nullable = false)
	@Basic(fetch = FetchType.EAGER)

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@XmlElement
	Integer exerciseTargetId;
	/**
	 */

	@Column(name = "value", precision = 12, nullable = false)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal value;
	/**
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "date", nullable = false)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	Calendar date;
	
	/**
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "end_date", nullable = false)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	Calendar endDate;

	/**
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumns({ @JoinColumn(name = "doctor_id", referencedColumnName = "doctor_id", nullable = false) })
	@XmlTransient
	Doctor doctor;
	/**
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumns({ @JoinColumn(name = "kid_id", referencedColumnName = "kid_id", nullable = false) })
	@XmlTransient
	Kid kid;
	/**
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumns({ @JoinColumn(name = "effort_type_id", referencedColumnName = "effort_type_id", nullable = false) })
	@XmlTransient
	EffortType effortType;

	/**
	 */
	public void setExerciseTargetId(Integer exerciseTargetId) {
		this.exerciseTargetId = exerciseTargetId;
	}

	/**
	 */
	public Integer getExerciseTargetId() {
		return this.exerciseTargetId;
	}

	/**
	 */
	public void setValue(BigDecimal value) {
		this.value = value;
	}

	/**
	 */
	public BigDecimal getValue() {
		return this.value;
	}

	/**
	 */
	public void setDate(Calendar date) {
		this.date = date;
	}

	/**
	 */
	public Calendar getDate() {
		return this.date;
	}

	public Calendar getEndDate() {
		return endDate;
	}

	public void setEndDate(Calendar endDate) {
		this.endDate = endDate;
	}

	/**
	 */
	public void setDoctor(Doctor doctor) {
		this.doctor = doctor;
	}

	/**
	 */
	@JsonIgnore
	public Doctor getDoctor() {
		return doctor;
	}

	/**
	 */
	public void setKid(Kid kid) {
		this.kid = kid;
	}

	/**
	 */
	@JsonIgnore
	public Kid getKid() {
		return kid;
	}

	public EffortType getEffortType() {
		return effortType;
	}

	public void setEffortType(EffortType effortType) {
		this.effortType = effortType;
	}

	/**
	 */
	public ExerciseTarget() {
	}

	/**
	 * Copies the contents of the specified bean into this bean.
	 *
	 */
	public void copy(ExerciseTarget that) {
		setExerciseTargetId(that.getExerciseTargetId());
		setValue(that.getValue());
		setDate(that.getDate());
		setDoctor(that.getDoctor());
		setKid(that.getKid());
	}

	/**
	 * Returns a textual representation of a bean.
	 *
	 */
	public String toString() {

		StringBuilder buffer = new StringBuilder();

		buffer.append("exerciseTargetId=[").append(exerciseTargetId).append("] ");
		buffer.append("value=[").append(value).append("] ");
		buffer.append("date=[").append(date).append("] ");

		return buffer.toString();
	}

	/**
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = (int) (prime * result + ((exerciseTargetId == null) ? 0 : exerciseTargetId.hashCode()));
		return result;
	}

	/**
	 */
	public boolean equals(Object obj) {
		if (obj == this)
			return true;
		if (!(obj instanceof ExerciseTarget))
			return false;
		ExerciseTarget equalCheck = (ExerciseTarget) obj;
		if ((exerciseTargetId == null && equalCheck.exerciseTargetId != null) || (exerciseTargetId != null && equalCheck.exerciseTargetId == null))
			return false;
		if (exerciseTargetId != null && !exerciseTargetId.equals(equalCheck.exerciseTargetId))
			return false;
		return true;
	}
}
