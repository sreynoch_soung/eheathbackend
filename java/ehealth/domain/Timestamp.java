
package ehealth.domain;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Set;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

/**
 */

@Entity
@NamedQueries({
		@NamedQuery(name = "findAllTimestamps", query = "select myTimestamp from Timestamp myTimestamp"),
		@NamedQuery(name = "findTimestampByCreateDate", query = "select myTimestamp from Timestamp myTimestamp where myTimestamp.createDate = ?1"),
		@NamedQuery(name = "findTimestampByIsActive", query = "select myTimestamp from Timestamp myTimestamp where myTimestamp.isActive = ?1"),
		@NamedQuery(name = "findTimestampByLoginDate", query = "select myTimestamp from Timestamp myTimestamp where myTimestamp.loginDate = ?1"),
		@NamedQuery(name = "findTimestampByLogoutDate", query = "select myTimestamp from Timestamp myTimestamp where myTimestamp.logoutDate = ?1"),
		@NamedQuery(name = "findTimestampByPrimaryKey", query = "select myTimestamp from Timestamp myTimestamp where myTimestamp.timestampId = ?1"),
		@NamedQuery(name = "findTimestampByTimestampId", query = "select myTimestamp from Timestamp myTimestamp where myTimestamp.timestampId = ?1"),
		@NamedQuery(name = "findTimestampByUpdateDate", query = "select myTimestamp from Timestamp myTimestamp where myTimestamp.updateDate = ?1") })

@Table(catalog = "ehealthdb", name = "timestamp")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(namespace = "eHealthBackend/ehealth/domain", name = "Timestamp")
@XmlRootElement(namespace = "eHealthBackend/ehealth/domain")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "timestampId")

public class Timestamp implements Serializable {
//	 private final static Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
	 

	
	private static final long serialVersionUID = 1L;

	/**
	 */

	@Column(name = "timestamp_id", nullable = false)
	@Basic(fetch = FetchType.EAGER)

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@XmlElement
	Integer timestampId;
	/**
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "login_date")
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	Calendar loginDate;
	/**
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "logout_date")
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	Calendar logoutDate;
	/**
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "create_date", nullable = false)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	Calendar createDate;
	/**
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "update_date")
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	Calendar updateDate;
	/**
	 */

	@Column(name = "isActive")
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	Boolean isActive;

	/**
	 */
	@OneToMany(mappedBy = "timestamp", cascade = { CascadeType.REMOVE }, fetch = FetchType.LAZY)

	@XmlElement(name = "", namespace = "")
	java.util.Set<ehealth.domain.User> users;

	/**
	 */
	public void setTimestampId(Integer timestampId) {
//		LOGGER.setLevel(Level.SEVERE);
//
//		 LOGGER.info("Info Log");
		this.timestampId = timestampId;
	}

	/**
	 */
	public Integer getTimestampId() {
		return this.timestampId;
	}

	/**
	 */
	public void setLoginDate(Calendar loginDate) {
		this.loginDate = loginDate;
	}

	/**
	 */
	public Calendar getLoginDate() {
		return this.loginDate;
	}

	/**
	 */
	public void setLogoutDate(Calendar logoutDate) {
		this.logoutDate = logoutDate;
	}

	/**
	 */
	public Calendar getLogoutDate() {
		return this.logoutDate;
	}

	/**
	 */
	public void setCreateDate(Calendar createDate) {
		this.createDate = createDate;
	}

	/**
	 */
	public Calendar getCreateDate() {
		return this.createDate;
	}

	/**
	 */
	public void setUpdateDate(Calendar updateDate) {
		this.updateDate = updateDate;
	}

	/**
	 */
	public Calendar getUpdateDate() {
		return this.updateDate;
	}

	/**
	 */
	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	/**
	 */
	public Boolean getIsActive() {
		return this.isActive;
	}

	/**
	 */
	public void setUsers(Set<User> users) {
		this.users = users;
	}

	/**
	 */
	@JsonIgnore
	public Set<User> getUsers() {
		if (users == null) {
			users = new java.util.LinkedHashSet<ehealth.domain.User>();
		}
		return users;
	}

	/**
	 */
	public Timestamp() {
	}

	/**
	 * Copies the contents of the specified bean into this bean.
	 *
	 */
	public void copy(Timestamp that) {
		setTimestampId(that.getTimestampId());
		setLoginDate(that.getLoginDate());
		setLogoutDate(that.getLogoutDate());
		setCreateDate(that.getCreateDate());
		setUpdateDate(that.getUpdateDate());
		setIsActive(that.getIsActive());
		setUsers(new java.util.LinkedHashSet<ehealth.domain.User>(that.getUsers()));
	}

	/**
	 * Returns a textual representation of a bean.
	 *
	 */
	public String toString() {

		StringBuilder buffer = new StringBuilder();

		buffer.append("timestampId=[").append(timestampId).append("] ");
		buffer.append("loginDate=[").append(loginDate).append("] ");
		buffer.append("logoutDate=[").append(logoutDate).append("] ");
		buffer.append("createDate=[").append(createDate).append("] ");
		buffer.append("updateDate=[").append(updateDate).append("] ");
		buffer.append("isActive=[").append(isActive).append("] ");

		return buffer.toString();
	}

	/**
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = (int) (prime * result + ((timestampId == null) ? 0 : timestampId.hashCode()));
		return result;
	}

	/**
	 */
//	public boolean equals(Object obj) {
//		if (obj == this)
//			return true;
//		if (!(obj instanceof Timestamp))
//			return false;
//		Timestamp equalCheck = (Timestamp) obj;
//		if ((timestampId == null && equalCheck.timestampId != null) || (timestampId != null && equalCheck.timestampId == null))
//			return false;
//		if (timestampId != null && !timestampId.equals(equalCheck.timestampId))
//			return false;
//		return true;
//	}
}
