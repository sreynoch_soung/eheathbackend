
package ehealth.domain;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Set;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

/**
 */

@Entity
@NamedQueries({
		@NamedQuery(name = "findAllMeals", query = "select myMeal from Meal myMeal"),
		@NamedQuery(name = "findMealByDate", query = "select myMeal from Meal myMeal where myMeal.date = ?1"),
		@NamedQuery(name = "findMealByMealId", query = "select myMeal from Meal myMeal where myMeal.mealId = ?1"),
		@NamedQuery(name = "findMealByPrimaryKey", query = "select myMeal from Meal myMeal where myMeal.mealId = ?1") })

@Table(catalog = "ehealthdb", name = "meal")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(namespace = "eHealthBackend/ehealth/domain", name = "Meal")
@XmlRootElement(namespace = "eHealthBackend/ehealth/domain")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "mealId")

public class Meal implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 */

	@Column(name = "meal_id", nullable = false)
	@Basic(fetch = FetchType.EAGER)

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@XmlElement
	Integer mealId;
	/**
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "date", nullable = false)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	Calendar date;

	/**
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumns({ @JoinColumn(name = "kid_id", referencedColumnName = "kid_id", nullable = false) })
	@XmlTransient
	Kid kid;
	/**
	 */
	@OneToMany(mappedBy = "meal", cascade = { CascadeType.REMOVE }, fetch = FetchType.LAZY)

	@XmlElement(name = "", namespace = "")
	java.util.Set<ehealth.domain.FoodPhoto> foodPhotos;

	/**
	 */
	public void setMealId(Integer mealId) {
		this.mealId = mealId;
	}

	/**
	 */
	public Integer getMealId() {
		return this.mealId;
	}

	/**
	 */
	public void setDate(Calendar date) {
		this.date = date;
	}

	/**
	 */
	public Calendar getDate() {
		return this.date;
	}

	/**
	 */
	public void setKid(Kid kid) {
		this.kid = kid;
	}

	/**
	 */
	@JsonIgnore
	public Kid getKid() {
		return kid;
	}

	/**
	 */
	public void setFoodPhotos(Set<FoodPhoto> foodPhotos) {
		this.foodPhotos = foodPhotos;
	}

	/**
	 */
	@JsonIgnore
	public Set<FoodPhoto> getFoodPhotos() {
		if (foodPhotos == null) {
			foodPhotos = new java.util.LinkedHashSet<ehealth.domain.FoodPhoto>();
		}
		return foodPhotos;
	}

	/**
	 */
	public Meal() {
	}

	/**
	 * Copies the contents of the specified bean into this bean.
	 *
	 */
	public void copy(Meal that) {
		setMealId(that.getMealId());
		setDate(that.getDate());
		setKid(that.getKid());
		setFoodPhotos(new java.util.LinkedHashSet<ehealth.domain.FoodPhoto>(that.getFoodPhotos()));
	}

	/**
	 * Returns a textual representation of a bean.
	 *
	 */
	public String toString() {

		StringBuilder buffer = new StringBuilder();

		buffer.append("mealId=[").append(mealId).append("] ");
		buffer.append("date=[").append(date).append("] ");

		return buffer.toString();
	}

	/**
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = (int) (prime * result + ((mealId == null) ? 0 : mealId.hashCode()));
		return result;
	}

	/**
	 */
	public boolean equals(Object obj) {
		if (obj == this)
			return true;
		if (!(obj instanceof Meal))
			return false;
		Meal equalCheck = (Meal) obj;
		if ((mealId == null && equalCheck.mealId != null) || (mealId != null && equalCheck.mealId == null))
			return false;
		if (mealId != null && !mealId.equals(equalCheck.mealId))
			return false;
		return true;
	}
}
