
package ehealth.domain;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

/**
 */

@Entity
@NamedQueries({
		@NamedQuery(name = "findAllExerciseTypes", query = "select myExerciseType from ExerciseType myExerciseType"),
		@NamedQuery(name = "findExerciseTypeByExerciseTypeId", query = "select myExerciseType from ExerciseType myExerciseType where myExerciseType.exerciseTypeId = ?1"),
		@NamedQuery(name = "findExerciseTypeByPrimaryKey", query = "select myExerciseType from ExerciseType myExerciseType where myExerciseType.exerciseTypeId = ?1"),
		@NamedQuery(name = "findExerciseTypeByTypeName", query = "select myExerciseType from ExerciseType myExerciseType where myExerciseType.typeName = ?1"),
		@NamedQuery(name = "findExerciseTypeByTypeNameContaining", query = "select myExerciseType from ExerciseType myExerciseType where myExerciseType.typeName like ?1") })

@Table(catalog = "ehealthdb", name = "exercise_type")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(namespace = "eHealthBackend/ehealth/domain", name = "ExerciseType")
@XmlRootElement(namespace = "eHealthBackend/ehealth/domain")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "exerciseTypeId")

public class ExerciseType implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 */

	@Column(name = "exercise_type_id", nullable = false)
	@Basic(fetch = FetchType.EAGER)

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@XmlElement
	Integer exerciseTypeId;
	/**
	 */

	@Column(name = "type_name", length = 45)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String typeName;

	/**
	 */
	@OneToMany(mappedBy = "exerciseType", cascade = { CascadeType.REMOVE }, fetch = FetchType.LAZY)

	@XmlElement(name = "", namespace = "")
	java.util.Set<ehealth.domain.ExerciseInput> exerciseInputs;

	/**
	 */
	public void setExerciseTypeId(Integer exerciseTypeId) {
		this.exerciseTypeId = exerciseTypeId;
	}

	/**
	 */
	public Integer getExerciseTypeId() {
		return this.exerciseTypeId;
	}

	/**
	 */
	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}

	/**
	 */
	public String getTypeName() {
		return this.typeName;
	}

	/**
	 */
	public void setExerciseInputs(Set<ExerciseInput> exerciseInputs) {
		this.exerciseInputs = exerciseInputs;
	}

	/**
	 */
	@JsonIgnore
	public Set<ExerciseInput> getExerciseInputs() {
		if (exerciseInputs == null) {
			exerciseInputs = new java.util.LinkedHashSet<ehealth.domain.ExerciseInput>();
		}
		return exerciseInputs;
	}

	/**
	 */
	public ExerciseType() {
	}

	/**
	 * Copies the contents of the specified bean into this bean.
	 *
	 */
	public void copy(ExerciseType that) {
		setExerciseTypeId(that.getExerciseTypeId());
		setTypeName(that.getTypeName());
		setExerciseInputs(new java.util.LinkedHashSet<ehealth.domain.ExerciseInput>(that.getExerciseInputs()));
	}

	/**
	 * Returns a textual representation of a bean.
	 *
	 */
	public String toString() {

		StringBuilder buffer = new StringBuilder();

		buffer.append("exerciseTypeId=[").append(exerciseTypeId).append("] ");
		buffer.append("typeName=[").append(typeName).append("] ");

		return buffer.toString();
	}

	/**
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = (int) (prime * result + ((exerciseTypeId == null) ? 0 : exerciseTypeId.hashCode()));
		return result;
	}

	/**
	 */
	public boolean equals(Object obj) {
		if (obj == this)
			return true;
		if (!(obj instanceof ExerciseType))
			return false;
		ExerciseType equalCheck = (ExerciseType) obj;
		if ((exerciseTypeId == null && equalCheck.exerciseTypeId != null) || (exerciseTypeId != null && equalCheck.exerciseTypeId == null))
			return false;
		if (exerciseTypeId != null && !exerciseTypeId.equals(equalCheck.exerciseTypeId))
			return false;
		return true;
	}
}
