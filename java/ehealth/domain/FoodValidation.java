
package ehealth.domain;

import java.io.Serializable;
import java.util.Calendar;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

/**
 */

@Entity
@NamedQueries({
		@NamedQuery(name = "findAllFoodValidations", query = "select myFoodValidation from FoodValidation myFoodValidation"),
		@NamedQuery(name = "findFoodValidationByFoodValidationId", query = "select myFoodValidation from FoodValidation myFoodValidation where myFoodValidation.foodValidationId = ?1"),
		@NamedQuery(name = "findFoodValidationByPrimaryKey", query = "select myFoodValidation from FoodValidation myFoodValidation where myFoodValidation.foodValidationId = ?1"),
		@NamedQuery(name = "findFoodValidationByTime", query = "select myFoodValidation from FoodValidation myFoodValidation where myFoodValidation.time = ?1"),
		@NamedQuery(name = "findFoodValidationByValue", query = "select myFoodValidation from FoodValidation myFoodValidation where myFoodValidation.value = ?1") })

@Table(catalog = "ehealthdb", name = "food_validation")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(namespace = "eHealthBackend/ehealth/domain", name = "FoodValidation")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "foodValidationId")

public class FoodValidation implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 */

	@Column(name = "`food_validation _id`", nullable = false)
	@Basic(fetch = FetchType.EAGER)

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@XmlElement
	Integer foodValidationId;
	/**
	 */

	@Column(name = "value", nullable = false)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	Boolean value;
	/**
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "time", nullable = false)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	Calendar time;

	/**
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumns({ @JoinColumn(name = "user_id", referencedColumnName = "user_id", nullable = false) })
	@XmlTransient
	User user;
	/**
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumns({ @JoinColumn(name = "food_photo_id", referencedColumnName = "food_photo_id", nullable = false) })
	@XmlTransient
	FoodPhoto foodPhoto;

	/**
	 */
	public void setFoodValidationId(Integer foodValidationId) {
		this.foodValidationId = foodValidationId;
	}

	/**
	 */
	public Integer getFoodValidationId() {
		return this.foodValidationId;
	}

	/**
	 */
	public void setValue(Boolean value) {
		this.value = value;
	}

	/**
	 */
	public Boolean getValue() {
		return this.value;
	}

	/**
	 */
	public void setTime(Calendar time) {
		this.time = time;
	}

	/**
	 */
	public Calendar getTime() {
		return this.time;
	}

	/**
	 */
	public void setUser(User user) {
		this.user = user;
	}

	/**
	 */
	@JsonIgnore
	public User getUser() {
		return user;
	}

	/**
	 */
	public void setFoodPhoto(FoodPhoto foodPhoto) {
		this.foodPhoto = foodPhoto;
	}

	/**
	 */
	@JsonIgnore
	public FoodPhoto getFoodPhoto() {
		return foodPhoto;
	}

	/**
	 */
	public FoodValidation() {
	}

	/**
	 * Copies the contents of the specified bean into this bean.
	 *
	 */
	public void copy(FoodValidation that) {
		setFoodValidationId(that.getFoodValidationId());
		setValue(that.getValue());
		setTime(that.getTime());
		setUser(that.getUser());
		setFoodPhoto(that.getFoodPhoto());
	}

	/**
	 * Returns a textual representation of a bean.
	 *
	 */
	public String toString() {

		StringBuilder buffer = new StringBuilder();

		buffer.append("foodValidationId=[").append(foodValidationId).append("] ");
		buffer.append("value=[").append(value).append("] ");
		buffer.append("time=[").append(time).append("] ");

		return buffer.toString();
	}

	/**
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = (int) (prime * result + ((foodValidationId == null) ? 0 : foodValidationId.hashCode()));
		return result;
	}

	/**
	 */
	public boolean equals(Object obj) {
		if (obj == this)
			return true;
		if (!(obj instanceof FoodValidation))
			return false;
		FoodValidation equalCheck = (FoodValidation) obj;
		if ((foodValidationId == null && equalCheck.foodValidationId != null) || (foodValidationId != null && equalCheck.foodValidationId == null))
			return false;
		if (foodValidationId != null && !foodValidationId.equals(equalCheck.foodValidationId))
			return false;
		return true;
	}
}
