	
package ehealth.domain;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

/**
 */

@Entity
@NamedQueries({
		@NamedQuery(name = "findAccountSettingByAccountSettingId", query = "select myAccountSetting from AccountSetting myAccountSetting where myAccountSetting.accountSettingId = ?1"),
		@NamedQuery(name = "findAccountSettingByNumPhotoValidateGroup", query = "select myAccountSetting from AccountSetting myAccountSetting where myAccountSetting.numPhotoValidateGroup = ?1"),
		@NamedQuery(name = "findAccountSettingByPrimaryKey", query = "select myAccountSetting from AccountSetting myAccountSetting where myAccountSetting.accountSettingId = ?1"),
		@NamedQuery(name = "findAllAccountSettings", query = "select myAccountSetting from AccountSetting myAccountSetting") })

@Table(catalog = "ehealthdb", name = "account_setting")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(namespace = "eHealthBackend/ehealth/domain", name = "AccountSetting")
@XmlRootElement(namespace = "eHealthBackend/ehealth/domain")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "accountSettingId")

public class AccountSetting implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 */

	@Column(name = "account_setting_id", nullable = false)
	@Basic(fetch = FetchType.EAGER)

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@XmlElement
	Integer accountSettingId;
	/**
	 */

	@Column(name = "num_photo_validate_group")
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	Integer numPhotoValidateGroup;

	/**
	 */
	@OneToMany(mappedBy = "accountSetting", cascade = { CascadeType.REMOVE }, fetch = FetchType.LAZY)

	@XmlElement(name = "", namespace = "")
	java.util.Set<ehealth.domain.Kid> kids;
	/**
	 */
	@OneToMany(mappedBy = "accountSetting", cascade = { CascadeType.REMOVE }, fetch = FetchType.LAZY)

	@XmlElement(name = "", namespace = "")
	java.util.Set<ehealth.domain.Parent> parents;

	/**
	 */
	public void setAccountSettingId(Integer accountSettingId) {
		this.accountSettingId = accountSettingId;
	}

	/**
	 */
	public Integer getAccountSettingId() {
		return this.accountSettingId;
	}

	/**
	 */
	public void setNumPhotoValidateGroup(Integer numPhotoValidateGroup) {
		this.numPhotoValidateGroup = numPhotoValidateGroup;
	}

	/**
	 */
	public Integer getNumPhotoValidateGroup() {
		return this.numPhotoValidateGroup;
	}

	/**
	 */
	public void setKids(Set<Kid> kids) {
		this.kids = kids;
	}

	/**
	 */
	@JsonIgnore
	public Set<Kid> getKids() {
		if (kids == null) {
			kids = new java.util.LinkedHashSet<ehealth.domain.Kid>();
		}
		return kids;
	}

	/**
	 */
	public void setParents(Set<Parent> parents) {
		this.parents = parents;
	}

	/**
	 */
	@JsonIgnore
	public Set<Parent> getParents() {
		if (parents == null) {
			parents = new java.util.LinkedHashSet<ehealth.domain.Parent>();
		}
		return parents;
	}

	/**
	 */
	public AccountSetting() {
	}

	/**
	 * Copies the contents of the specified bean into this bean.
	 *
	 */
	public void copy(AccountSetting that) {
		setAccountSettingId(that.getAccountSettingId());
		setNumPhotoValidateGroup(that.getNumPhotoValidateGroup());
		setKids(new java.util.LinkedHashSet<ehealth.domain.Kid>(that.getKids()));
		setParents(new java.util.LinkedHashSet<ehealth.domain.Parent>(that.getParents()));
	}

	/**
	 * Returns a textual representation of a bean.
	 *
	 */
	public String toString() {

		StringBuilder buffer = new StringBuilder();

		buffer.append("accountSettingId=[").append(accountSettingId).append("] ");
		buffer.append("numPhotoValidateGroup=[").append(numPhotoValidateGroup).append("] ");

		return buffer.toString();
	}

	/**
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = (int) (prime * result + ((accountSettingId == null) ? 0 : accountSettingId.hashCode()));
		return result;
	}

	/**
	 */
	public boolean equals(Object obj) {
		if (obj == this)
			return true;
		if (!(obj instanceof AccountSetting))
			return false;
		AccountSetting equalCheck = (AccountSetting) obj;
		if ((accountSettingId == null && equalCheck.accountSettingId != null) || (accountSettingId != null && equalCheck.accountSettingId == null))
			return false;
		if (accountSettingId != null && !accountSettingId.equals(equalCheck.accountSettingId))
			return false;
		return true;
	}
}
