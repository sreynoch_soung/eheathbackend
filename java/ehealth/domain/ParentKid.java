
package ehealth.domain;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

/**
 */

@Entity
@NamedQueries({
		@NamedQuery(name = "findAllParentKids", query = "select myParentKid from ParentKid myParentKid"),
		@NamedQuery(name = "findParentKidByParentKidId", query = "select myParentKid from ParentKid myParentKid where myParentKid.parentKidId = ?1"),
		@NamedQuery(name = "findParentKidByPrimaryKey", query = "select myParentKid from ParentKid myParentKid where myParentKid.parentKidId = ?1") })

@Table(catalog = "ehealthdb", name = "parent_kid")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(namespace = "eHealthBackend/ehealth/domain", name = "ParentKid")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "parentKidId")

public class ParentKid implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 */

	@Column(name = "parent_kid_id", nullable = false)
	@Basic(fetch = FetchType.EAGER)

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@XmlElement
	Integer parentKidId;

	/**
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumns({ @JoinColumn(name = "parent_id", referencedColumnName = "parent_id", nullable = false) })
	@XmlTransient
	Parent parent;
	/**
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumns({ @JoinColumn(name = "kid_id", referencedColumnName = "kid_id", nullable = false) })
	@XmlTransient
	Kid kid;

	/**
	 */
	public void setParentKidId(Integer parentKidId) {
		this.parentKidId = parentKidId;
	}

	/**
	 */
	public Integer getParentKidId() {
		return this.parentKidId;
	}

	/**
	 */
	public void setParent(Parent parent) {
		this.parent = parent;
	}

	/**
	 */
	@JsonIgnore
	public Parent getParent() {
		return parent;
	}

	/**
	 */
	public void setKid(Kid kid) {
		this.kid = kid;
	}

	/**
	 */
	@JsonIgnore
	public Kid getKid() {
		return kid;
	}

	/**
	 */
	public ParentKid() {
	}

	/**
	 * Copies the contents of the specified bean into this bean.
	 *
	 */
	public void copy(ParentKid that) {
		setParentKidId(that.getParentKidId());
		setParent(that.getParent());
		setKid(that.getKid());
	}

	/**
	 * Returns a textual representation of a bean.
	 *
	 */
	public String toString() {

		StringBuilder buffer = new StringBuilder();

		buffer.append("parentKidId=[").append(parentKidId).append("] ");

		return buffer.toString();
	}

	/**
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = (int) (prime * result + ((parentKidId == null) ? 0 : parentKidId.hashCode()));
		return result;
	}

	/**
	 */
	public boolean equals(Object obj) {
		if (obj == this)
			return true;
		if (!(obj instanceof ParentKid))
			return false;
		ParentKid equalCheck = (ParentKid) obj;
		if ((parentKidId == null && equalCheck.parentKidId != null) || (parentKidId != null && equalCheck.parentKidId == null))
			return false;
		if (parentKidId != null && !parentKidId.equals(equalCheck.parentKidId))
			return false;
		return true;
	}
}
