
package ehealth.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Calendar;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

/**
 */

@Entity
@NamedQueries({
		@NamedQuery(name = "findAllExerciseInputs", query = "select myExerciseInput from ExerciseInput myExerciseInput"),
		@NamedQuery(name = "findExerciseInputByDate", query = "select myExerciseInput from ExerciseInput myExerciseInput where myExerciseInput.date = ?1"),
		@NamedQuery(name = "findExerciseInputByExerciseInputId", query = "select myExerciseInput from ExerciseInput myExerciseInput where myExerciseInput.exerciseInputId = ?1"),
		@NamedQuery(name = "findExerciseInputByFinalValidationValue", query = "select myExerciseInput from ExerciseInput myExerciseInput where myExerciseInput.finalValidationValue = ?1"),
		@NamedQuery(name = "findExerciseInputByPrimaryKey", query = "select myExerciseInput from ExerciseInput myExerciseInput where myExerciseInput.exerciseInputId = ?1"),
		@NamedQuery(name = "findExerciseInputByValue", query = "select myExerciseInput from ExerciseInput myExerciseInput where myExerciseInput.value = ?1") })

@Table(catalog = "ehealthdb", name = "exercise_input")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(namespace = "eHealthBackend/ehealth/domain", name = "ExerciseInput")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "exerciseInputId")

public class ExerciseInput implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 */

	@Column(name = "exercise_input_id", nullable = false)
	@Basic(fetch = FetchType.EAGER)

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@XmlElement
	Integer exerciseInputId;
	/**
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "date", nullable = false)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	Calendar date;
	/**
	 */

	@Column(name = "value", precision = 12, nullable = false)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal value;
	/**
	 */

	@Column(name = "final_validation_value", precision = 12)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	BigDecimal finalValidationValue;

	/**
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumns({
			@JoinColumn(name = "exercise_type_id", referencedColumnName = "exercise_type_id", nullable = false) })
	@XmlTransient
	ExerciseType exerciseType;
	/**
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumns({ @JoinColumn(name = "kid_id", referencedColumnName = "kid_id", nullable = false) })
	@XmlTransient
	Kid kid;
	/**
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumns({ @JoinColumn(name = "effort_type_id", referencedColumnName = "effort_type_id", nullable = false) })
	@XmlTransient
	EffortType effortType;

	/**
	 */
	public void setExerciseInputId(Integer exerciseInputId) {
		this.exerciseInputId = exerciseInputId;
	}

	/**
	 */
	public Integer getExerciseInputId() {
		return this.exerciseInputId;
	}

	/**
	 */
	public void setDate(Calendar date) {
		this.date = date;
	}

	/**
	 */
	public Calendar getDate() {
		return this.date;
	}

	/**
	 */
	public void setValue(BigDecimal value) {
		this.value = value;
	}

	/**
	 */
	public BigDecimal getValue() {
		return this.value;
	}

	/**
	 */
	public void setFinalValidationValue(BigDecimal finalValidationValue) {
		this.finalValidationValue = finalValidationValue;
	}

	/**
	 */
	public BigDecimal getFinalValidationValue() {
		return this.finalValidationValue;
	}

	/**
	 */
	public void setExerciseType(ExerciseType exerciseType) {
		this.exerciseType = exerciseType;
	}

	/**
	 */
	@JsonIgnore
	public ExerciseType getExerciseType() {
		return exerciseType;
	}

	/**
	 */
	public void setKid(Kid kid) {
		this.kid = kid;
	}

	/**
	 */
	@JsonIgnore
	public Kid getKid() {
		return kid;
	}

	/**
	 */
	public void setEffortType(EffortType effortType) {
		this.effortType = effortType;
	}

	/**
	 */
	@JsonIgnore
	public EffortType getEffortType() {
		return effortType;
	}

	/**
	 */
	public ExerciseInput() {
	}

	/**
	 * Copies the contents of the specified bean into this bean.
	 *
	 */
	public void copy(ExerciseInput that) {
		setExerciseInputId(that.getExerciseInputId());
		setDate(that.getDate());
		setValue(that.getValue());
		setFinalValidationValue(that.getFinalValidationValue());
		setExerciseType(that.getExerciseType());
		setKid(that.getKid());
		setEffortType(that.getEffortType());
	}

	/**
	 * Returns a textual representation of a bean.
	 *
	 */
	public String toString() {

		StringBuilder buffer = new StringBuilder();

		buffer.append("exerciseInputId=[").append(exerciseInputId).append("] ");
		buffer.append("date=[").append(date).append("] ");
		buffer.append("value=[").append(value).append("] ");
		buffer.append("finalValidationValue=[").append(finalValidationValue).append("] ");

		return buffer.toString();
	}

	/**
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = (int) (prime * result + ((exerciseInputId == null) ? 0 : exerciseInputId.hashCode()));
		return result;
	}

	/**
	 */
	public boolean equals(Object obj) {
		if (obj == this)
			return true;
		if (!(obj instanceof ExerciseInput))
			return false;
		ExerciseInput equalCheck = (ExerciseInput) obj;
		if ((exerciseInputId == null && equalCheck.exerciseInputId != null) || (exerciseInputId != null && equalCheck.exerciseInputId == null))
			return false;
		if (exerciseInputId != null && !exerciseInputId.equals(equalCheck.exerciseInputId))
			return false;
		return true;
	}
}
