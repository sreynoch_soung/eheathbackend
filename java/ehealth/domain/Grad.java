
package ehealth.domain;

import java.io.Serializable;
import java.util.Calendar;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

/**
 */

@Entity
@NamedQueries({
		@NamedQuery(name = "findAllGrads", query = "select myGrad from Grad myGrad"),
		@NamedQuery(name = "findGradByDate", query = "select myGrad from Grad myGrad where myGrad.date = ?1"),
		@NamedQuery(name = "findGradByGradId", query = "select myGrad from Grad myGrad where myGrad.gradId = ?1"),
		@NamedQuery(name = "findGradByPrimaryKey", query = "select myGrad from Grad myGrad where myGrad.gradId = ?1"),
		@NamedQuery(name = "findGradByValue", query = "select myGrad from Grad myGrad where myGrad.value = ?1"),
		@NamedQuery(name = "findGradByValueContaining", query = "select myGrad from Grad myGrad where myGrad.value like ?1") })

@Table(catalog = "ehealthdb", name = "grad")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(namespace = "eHealthBackend/ehealth/domain", name = "Grad")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "gradId")

public class Grad implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 */

	@Column(name = "grad_id", nullable = false)
	@Basic(fetch = FetchType.EAGER)

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@XmlElement
	Integer gradId;
	/**
	 */

	@Column(name = "value", length = 3, nullable = false)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String value;
	/**
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "date", nullable = false)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	Calendar date;

	/**
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumns({ @JoinColumn(name = "kid_id", referencedColumnName = "kid_id", nullable = false) })
	@XmlTransient
	Kid kid;

	/**
	 */
	public void setGradId(Integer gradId) {
		this.gradId = gradId;
	}

	/**
	 */
	public Integer getGradId() {
		return this.gradId;
	}

	/**
	 */
	public void setValue(String value) {
		this.value = value;
	}

	/**
	 */
	public String getValue() {
		return this.value;
	}

	/**
	 */
	public void setDate(Calendar date) {
		this.date = date;
	}

	/**
	 */
	public Calendar getDate() {
		return this.date;
	}

	/**
	 */
	public void setKid(Kid kid) {
		this.kid = kid;
	}

	/**
	 */
	@JsonIgnore
	public Kid getKid() {
		return kid;
	}

	/**
	 */
	public Grad() {
	}

	/**
	 * Copies the contents of the specified bean into this bean.
	 *
	 */
	public void copy(Grad that) {
		setGradId(that.getGradId());
		setValue(that.getValue());
		setDate(that.getDate());
		setKid(that.getKid());
	}

	/**
	 * Returns a textual representation of a bean.
	 *
	 */
	public String toString() {

		StringBuilder buffer = new StringBuilder();

		buffer.append("gradId=[").append(gradId).append("] ");
		buffer.append("value=[").append(value).append("] ");
		buffer.append("date=[").append(date).append("] ");

		return buffer.toString();
	}

	/**
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = (int) (prime * result + ((gradId == null) ? 0 : gradId.hashCode()));
		return result;
	}

	/**
	 */
	public boolean equals(Object obj) {
		if (obj == this)
			return true;
		if (!(obj instanceof Grad))
			return false;
		Grad equalCheck = (Grad) obj;
		if ((gradId == null && equalCheck.gradId != null) || (gradId != null && equalCheck.gradId == null))
			return false;
		if (gradId != null && !gradId.equals(equalCheck.gradId))
			return false;
		return true;
	}
}
