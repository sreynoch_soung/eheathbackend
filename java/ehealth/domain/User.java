
package ehealth.domain;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

/**
 */

@Entity
@NamedQueries({
		@NamedQuery(name = "findAllUsers", query = "select myUser from User myUser"),
		@NamedQuery(name = "findUserByAge", query = "select myUser from User myUser where myUser.age = ?1"),
		@NamedQuery(name = "findUserByEmail", query = "select myUser from User myUser where myUser.email = ?1"),
		@NamedQuery(name = "findUserByEmailContaining", query = "select myUser from User myUser where myUser.email like ?1"),
		@NamedQuery(name = "findUserByEnabled", query = "select myUser from User myUser where myUser.enabled = ?1"),
		@NamedQuery(name = "findUserByName", query = "select myUser from User myUser where myUser.name = ?1"),
		@NamedQuery(name = "findUserByNameContaining", query = "select myUser from User myUser where myUser.name like ?1"),
		@NamedQuery(name = "findUserByPassword", query = "select myUser from User myUser where myUser.password = ?1"),
		@NamedQuery(name = "findUserByPasswordContaining", query = "select myUser from User myUser where myUser.password like ?1"),
		@NamedQuery(name = "findUserByPrimaryKey", query = "select myUser from User myUser where myUser.userId = ?1"),
		@NamedQuery(name = "findUserBySex", query = "select myUser from User myUser where myUser.sex = ?1"),
		@NamedQuery(name = "findUserBySexContaining", query = "select myUser from User myUser where myUser.sex like ?1"),
		@NamedQuery(name = "findUserBySurename", query = "select myUser from User myUser where myUser.surename = ?1"),
		@NamedQuery(name = "findUserBySurenameContaining", query = "select myUser from User myUser where myUser.surename like ?1"),
		@NamedQuery(name = "findUserByTelephone", query = "select myUser from User myUser where myUser.telephone = ?1"),
		@NamedQuery(name = "findUserByTelephoneContaining", query = "select myUser from User myUser where myUser.telephone like ?1"),
		@NamedQuery(name = "findUserByUserId", query = "select myUser from User myUser where myUser.userId = ?1"),
		@NamedQuery(name = "findUserByUsername", query = "select myUser from User myUser where myUser.username = ?1"),
		@NamedQuery(name = "findUserByUsernameContaining", query = "select myUser from User myUser where myUser.username like ?1") })

@Table(catalog = "ehealthdb", name = "user")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(namespace = "TestProject/ehealth/domain", name = "User")
@XmlRootElement(namespace = "TestProject/ehealth/domain")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "userId")

public class User implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 */

	@Column(name = "user_id", nullable = false)
	@Basic(fetch = FetchType.EAGER)

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@XmlElement
	Integer userId;
	/**
	 */

	@Column(name = "username", length = 45, nullable = false, unique = true)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String username;
	/**
	 */

	@Column(name = "name", length = 45, nullable = false)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String name;
	/**
	 */

	@Column(name = "surename", length = 45, nullable = false)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String surename;
	/**
	 */

	@Column(name = "password", length = 45, nullable = false)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String password;
	/**
	 */

	@Column(name = "email", length = 100, nullable = true, unique=true)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String email;
	/**
	 */

	@Column(name = "telephone", length = 20, nullable = false, unique=true)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String telephone;
	/**
	 */

	@Column(name = "age")
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	Integer age;
	/**
	 */

	@Column(name = "sex", length = 10, nullable = false)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String sex;
	/**
	 */

	@Column(name = "enabled", nullable = false)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	Boolean enabled;

	/**
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumns({ @JoinColumn(name = "user_role_id", referencedColumnName = "user_role_id", nullable = false) })
	@XmlTransient
	UserRole userRole;
	/**
	 */
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumns({ @JoinColumn(name = "timestamp_id", referencedColumnName = "timestamp_id", nullable = false) })
	@XmlTransient
	Timestamp timestamp;
	/**
	 */
	@OneToMany(mappedBy = "user", cascade = { CascadeType.REMOVE }, fetch = FetchType.EAGER)

	@XmlElement(name = "", namespace = "")
	java.util.Set<ehealth.domain.Doctor> doctors;
	/**
	 */
	@OneToMany(mappedBy = "user", cascade = { CascadeType.REMOVE }, fetch = FetchType.LAZY)

	@XmlElement(name = "", namespace = "")
	java.util.Set<ehealth.domain.Parent> parents;
	/**
	 */
	@OneToMany(mappedBy = "user", cascade = { CascadeType.REMOVE }, fetch = FetchType.LAZY)

	@XmlElement(name = "", namespace = "")
	java.util.Set<ehealth.domain.Kid> kids;
	/**
	 */
	@OneToMany(mappedBy = "user", cascade = { CascadeType.REMOVE }, fetch = FetchType.LAZY)

	@XmlElement(name = "", namespace = "")
	java.util.Set<ehealth.domain.FoodValidation> foodValidations;

	/**
	 */
	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	/**
	 */
	public Integer getUserId() {
		return this.userId;
	}

	/**
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 */
	public String getUsername() {
		return this.username;
	}

	/**
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 */
	public String getName() {
		return this.name;
	}

	/**
	 */
	public void setSurename(String surename) {
		this.surename = surename;
	}

	/**
	 */
	public String getSurename() {
		return this.surename;
	}

	/**
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 */
	public String getPassword() {
		return this.password;
	}

	/**
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 */
	public String getEmail() {
		return this.email;
	}

	/**
	 */
	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	/**
	 */
	public String getTelephone() {
		return this.telephone;
	}

	/**
	 */
	public void setAge(Integer age) {
		this.age = age;
	}

	/**
	 */
	public Integer getAge() {
		return this.age;
	}

	/**
	 */
	public void setSex(String sex) {
		this.sex = sex;
	}

	/**
	 */
	public String getSex() {
		return this.sex;
	}

	/**
	 */
	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}

	/**
	 */
	public Boolean getEnabled() {
		return this.enabled;
	}

	/**
	 */
	public void setUserRole(UserRole userRole) {
		this.userRole = userRole;
	}

	/**
	 */
	@JsonIgnore
	public UserRole getUserRole() {
		return userRole;
	}

	/**
	 */
	public void setTimestamp(Timestamp timestamp) {
		this.timestamp = timestamp;
	}

	/**
	 */
	@JsonIgnore
	public Timestamp getTimestamp() {
		return timestamp;
	}

	/**
	 */
	public void setDoctors(Set<Doctor> doctors) {
		this.doctors = doctors;
	}

	/**
	 */
	@JsonIgnore
	public Set<Doctor> getDoctors() {
		if (doctors == null) {
			doctors = new java.util.LinkedHashSet<ehealth.domain.Doctor>();
		}
		return doctors;
	}

	/**
	 */
	public void setParents(Set<Parent> parents) {
		this.parents = parents;
	}

	/**
	 */
	@JsonIgnore
	public Set<Parent> getParents() {
		if (parents == null) {
			parents = new java.util.LinkedHashSet<ehealth.domain.Parent>();
		}
		return parents;
	}

	/**
	 */
	public void setKids(Set<Kid> kids) {
		this.kids = kids;
	}

	/**
	 */
	@JsonIgnore
	public Set<Kid> getKids() {
		if (kids == null) {
			kids = new java.util.LinkedHashSet<ehealth.domain.Kid>();
		}
		return kids;
	}

	/**
	 */
	public void setFoodValidations(Set<FoodValidation> foodValidations) {
		this.foodValidations = foodValidations;
	}

	/**
	 */
	@JsonIgnore
	public Set<FoodValidation> getFoodValidations() {
		if (foodValidations == null) {
			foodValidations = new java.util.LinkedHashSet<ehealth.domain.FoodValidation>();
		}
		return foodValidations;
	}

	/**
	 */
	public User() {
	}

	/**
	 * Copies the contents of the specified bean into this bean.
	 *
	 */
	public void copy(User that) {
		setUserId(that.getUserId());
		setUsername(that.getUsername());
		setName(that.getName());
		setSurename(that.getSurename());
		setPassword(that.getPassword());
		setEmail(that.getEmail());
		setTelephone(that.getTelephone());
		setAge(that.getAge());
		setSex(that.getSex());
		setEnabled(that.getEnabled());
		setUserRole(that.getUserRole());
		setTimestamp(that.getTimestamp());
		setDoctors(new java.util.LinkedHashSet<ehealth.domain.Doctor>(that.getDoctors()));
		setParents(new java.util.LinkedHashSet<ehealth.domain.Parent>(that.getParents()));
		setKids(new java.util.LinkedHashSet<ehealth.domain.Kid>(that.getKids()));
		setFoodValidations(new java.util.LinkedHashSet<ehealth.domain.FoodValidation>(that.getFoodValidations()));
	}

	/**
	 * Returns a textual representation of a bean.
	 *
	 */
	public String toString() {

		StringBuilder buffer = new StringBuilder();

		buffer.append("userId=[").append(userId).append("] ");
		buffer.append("username=[").append(username).append("] ");
		buffer.append("name=[").append(name).append("] ");
		buffer.append("surename=[").append(surename).append("] ");
		buffer.append("password=[").append(password).append("] ");
		buffer.append("email=[").append(email).append("] ");
		buffer.append("telephone=[").append(telephone).append("] ");
		buffer.append("age=[").append(age).append("] ");
		buffer.append("sex=[").append(sex).append("] ");
		buffer.append("enabled=[").append(enabled).append("] ");

		return buffer.toString();
	}

	/**
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = (int) (prime * result + ((userId == null) ? 0 : userId.hashCode()));
		return result;
	}

	/**
	 */
	public boolean equals(Object obj) {
		if (obj == this)
			return true;
		if (!(obj instanceof User))
			return false;
		User equalCheck = (User) obj;
		if ((userId == null && equalCheck.userId != null) || (userId != null && equalCheck.userId == null))
			return false;
		if (userId != null && !userId.equals(equalCheck.userId))
			return false;
		return true;
	}
}
