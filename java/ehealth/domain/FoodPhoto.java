
package ehealth.domain;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

/**
 */

@Entity
@NamedQueries({
		@NamedQuery(name = "findAllFoodPhotos", query = "select myFoodPhoto from FoodPhoto myFoodPhoto"),
		@NamedQuery(name = "findFoodPhotoByFinalValidationValue", query = "select myFoodPhoto from FoodPhoto myFoodPhoto where myFoodPhoto.finalValidationValue = ?1"),
		@NamedQuery(name = "findFoodPhotoByFoodPhotoId", query = "select myFoodPhoto from FoodPhoto myFoodPhoto where myFoodPhoto.foodPhotoId = ?1"),
		@NamedQuery(name = "findFoodPhotoByPrimaryKey", query = "select myFoodPhoto from FoodPhoto myFoodPhoto where myFoodPhoto.foodPhotoId = ?1"),
		@NamedQuery(name = "findFoodPhotoByUrl", query = "select myFoodPhoto from FoodPhoto myFoodPhoto where myFoodPhoto.url = ?1"),
		@NamedQuery(name = "findFoodPhotoByUrlContaining", query = "select myFoodPhoto from FoodPhoto myFoodPhoto where myFoodPhoto.url like ?1") })

@Table(catalog = "ehealthdb", name = "food_photo")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(namespace = "eHealthBackend/ehealth/domain", name = "FoodPhoto")
@XmlRootElement(namespace = "eHealthBackend/ehealth/domain")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "foodPhotoId")

public class FoodPhoto implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 */

	@Column(name = "food_photo_id", nullable = false)
	@Basic(fetch = FetchType.EAGER)

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@XmlElement
	Integer foodPhotoId;
	/**
	 */

	@Column(name = "url", length = 300, nullable = false)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String url;
	/**
	 */

	@Column(name = "final_validation_value")
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	Integer finalValidationValue;

	/**
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumns({ @JoinColumn(name = "meal_id", referencedColumnName = "meal_id", nullable = false) })
	@XmlTransient
	Meal meal;
	/**
	 */
	@OneToMany(mappedBy = "foodPhoto", cascade = { CascadeType.REMOVE }, fetch = FetchType.LAZY)

	@XmlElement(name = "", namespace = "")
	java.util.Set<ehealth.domain.FoodTageInput> foodTageInputs;
	/**
	 */
	@OneToMany(mappedBy = "foodPhoto", cascade = { CascadeType.REMOVE }, fetch = FetchType.LAZY)

	@XmlElement(name = "", namespace = "")
	java.util.Set<ehealth.domain.FoodValidation> foodValidations;

	/**
	 */
	public void setFoodPhotoId(Integer foodPhotoId) {
		this.foodPhotoId = foodPhotoId;
	}

	/**
	 */
	public Integer getFoodPhotoId() {
		return this.foodPhotoId;
	}

	/**
	 */
	public void setUrl(String url) {
		this.url = url;
	}

	/**
	 */
	public String getUrl() {
		return this.url;
	}

	/**
	 */
	public void setFinalValidationValue(Integer finalValidationValue) {
		this.finalValidationValue = finalValidationValue;
	}

	/**
	 */
	public Integer getFinalValidationValue() {
		return this.finalValidationValue;
	}

	/**
	 */
	public void setMeal(Meal meal) {
		this.meal = meal;
	}

	/**
	 */
	@JsonIgnore
	public Meal getMeal() {
		return meal;
	}

	/**
	 */
	public void setFoodTageInputs(Set<FoodTageInput> foodTageInputs) {
		this.foodTageInputs = foodTageInputs;
	}

	/**
	 */
	@JsonIgnore
	public Set<FoodTageInput> getFoodTageInputs() {
		if (foodTageInputs == null) {
			foodTageInputs = new java.util.LinkedHashSet<ehealth.domain.FoodTageInput>();
		}
		return foodTageInputs;
	}

	/**
	 */
	public void setFoodValidations(Set<FoodValidation> foodValidations) {
		this.foodValidations = foodValidations;
	}

	/**
	 */
	@JsonIgnore
	public Set<FoodValidation> getFoodValidations() {
		if (foodValidations == null) {
			foodValidations = new java.util.LinkedHashSet<ehealth.domain.FoodValidation>();
		}
		return foodValidations;
	}

	/**
	 */
	public FoodPhoto() {
	}

	/**
	 * Copies the contents of the specified bean into this bean.
	 *
	 */
	public void copy(FoodPhoto that) {
		setFoodPhotoId(that.getFoodPhotoId());
		setUrl(that.getUrl());
		setFinalValidationValue(that.getFinalValidationValue());
		setMeal(that.getMeal());
		setFoodTageInputs(new java.util.LinkedHashSet<ehealth.domain.FoodTageInput>(that.getFoodTageInputs()));
		setFoodValidations(new java.util.LinkedHashSet<ehealth.domain.FoodValidation>(that.getFoodValidations()));
	}

	/**
	 * Returns a textual representation of a bean.
	 *
	 */
	public String toString() {

		StringBuilder buffer = new StringBuilder();

		buffer.append("foodPhotoId=[").append(foodPhotoId).append("] ");
		buffer.append("url=[").append(url).append("] ");
		buffer.append("finalValidationValue=[").append(finalValidationValue).append("] ");

		return buffer.toString();
	}

	/**
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = (int) (prime * result + ((foodPhotoId == null) ? 0 : foodPhotoId.hashCode()));
		return result;
	}

	/**
	 */
	public boolean equals(Object obj) {
		if (obj == this)
			return true;
		if (!(obj instanceof FoodPhoto))
			return false;
		FoodPhoto equalCheck = (FoodPhoto) obj;
		if ((foodPhotoId == null && equalCheck.foodPhotoId != null) || (foodPhotoId != null && equalCheck.foodPhotoId == null))
			return false;
		if (foodPhotoId != null && !foodPhotoId.equals(equalCheck.foodPhotoId))
			return false;
		return true;
	}
}
