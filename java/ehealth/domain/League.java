
package ehealth.domain;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

/**
 */

@Entity
@NamedQueries({
		@NamedQuery(name = "findAllLeagues", query = "select myLeague from League myLeague"),
		@NamedQuery(name = "findLeagueByLeagueId", query = "select myLeague from League myLeague where myLeague.leagueId = ?1"),
		@NamedQuery(name = "findLeagueByLeagueName", query = "select myLeague from League myLeague where myLeague.leagueName = ?1"),
		@NamedQuery(name = "findLeagueByLeagueNameContaining", query = "select myLeague from League myLeague where myLeague.leagueName like ?1"),
		@NamedQuery(name = "findLeagueByMatchDate", query = "select myLeague from League myLeague where myLeague.matchDate = ?1"),
		@NamedQuery(name = "findLeagueByMatchDateContaining", query = "select myLeague from League myLeague where myLeague.matchDate like ?1"),
		@NamedQuery(name = "findLeagueByPoint", query = "select myLeague from League myLeague where myLeague.point = ?1"),
		@NamedQuery(name = "findLeagueByPointContaining", query = "select myLeague from League myLeague where myLeague.point like ?1"),
		@NamedQuery(name = "findLeagueByPrimaryKey", query = "select myLeague from League myLeague where myLeague.leagueId = ?1") })

@Table(catalog = "ehealthdb", name = "league")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(namespace = "eHealthBackend/ehealth/domain", name = "League")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "leagueId")

public class League implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 */

	@Column(name = "league_id", nullable = false)
	@Basic(fetch = FetchType.EAGER)

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@XmlElement
	Integer leagueId;
	/**
	 */

	@Column(name = "league_name", length = 45, nullable = false)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String leagueName;
	/**
	 */

	@Column(name = "match_date", length = 45, nullable = false)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String matchDate;
	/**
	 */

	@Column(name = "point", length = 45)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String point;

	/**
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumns({ @JoinColumn(name = "team_id", referencedColumnName = "team_id", nullable = false) })
	@XmlTransient
	Team team;
	/**
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumns({ @JoinColumn(name = "match_id", referencedColumnName = "match_id", nullable = false) })
	@XmlTransient
	Match match;

	/**
	 */
	public void setLeagueId(Integer leagueId) {
		this.leagueId = leagueId;
	}

	/**
	 */
	public Integer getLeagueId() {
		return this.leagueId;
	}

	/**
	 */
	public void setLeagueName(String leagueName) {
		this.leagueName = leagueName;
	}

	/**
	 */
	public String getLeagueName() {
		return this.leagueName;
	}

	/**
	 */
	public void setMatchDate(String matchDate) {
		this.matchDate = matchDate;
	}

	/**
	 */
	public String getMatchDate() {
		return this.matchDate;
	}

	/**
	 */
	public void setPoint(String point) {
		this.point = point;
	}

	/**
	 */
	public String getPoint() {
		return this.point;
	}

	/**
	 */
	public void setTeam(Team team) {
		this.team = team;
	}

	/**
	 */
	@JsonIgnore
	public Team getTeam() {
		return team;
	}

	/**
	 */
	public void setMatch(Match match) {
		this.match = match;
	}

	/**
	 */
	@JsonIgnore
	public Match getMatch() {
		return match;
	}

	/**
	 */
	public League() {
	}

	/**
	 * Copies the contents of the specified bean into this bean.
	 *
	 */
	public void copy(League that) {
		setLeagueId(that.getLeagueId());
		setLeagueName(that.getLeagueName());
		setMatchDate(that.getMatchDate());
		setPoint(that.getPoint());
		setTeam(that.getTeam());
		setMatch(that.getMatch());
	}

	/**
	 * Returns a textual representation of a bean.
	 *
	 */
	public String toString() {

		StringBuilder buffer = new StringBuilder();

		buffer.append("leagueId=[").append(leagueId).append("] ");
		buffer.append("leagueName=[").append(leagueName).append("] ");
		buffer.append("matchDate=[").append(matchDate).append("] ");
		buffer.append("point=[").append(point).append("] ");

		return buffer.toString();
	}

	/**
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = (int) (prime * result + ((leagueId == null) ? 0 : leagueId.hashCode()));
		return result;
	}

	/**
	 */
	public boolean equals(Object obj) {
		if (obj == this)
			return true;
		if (!(obj instanceof League))
			return false;
		League equalCheck = (League) obj;
		if ((leagueId == null && equalCheck.leagueId != null) || (leagueId != null && equalCheck.leagueId == null))
			return false;
		if (leagueId != null && !leagueId.equals(equalCheck.leagueId))
			return false;
		return true;
	}
}
