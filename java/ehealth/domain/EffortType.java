
package ehealth.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Set;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

/**
 */

@Entity
@NamedQueries({
		@NamedQuery(name = "findAllEffortTypes", query = "select myEffortType from EffortType myEffortType"),
		@NamedQuery(name = "findEffortTypeByEffortTypeId", query = "select myEffortType from EffortType myEffortType where myEffortType.effortTypeId = ?1"),
		@NamedQuery(name = "findEffortTypeByPrimaryKey", query = "select myEffortType from EffortType myEffortType where myEffortType.effortTypeId = ?1"),
		@NamedQuery(name = "findEffortTypeByTitle", query = "select myEffortType from EffortType myEffortType where myEffortType.title = ?1"),
		@NamedQuery(name = "findEffortTypeByTitleContaining", query = "select myEffortType from EffortType myEffortType where myEffortType.title like ?1") })

@Table(catalog = "ehealthdb", name = "effort_type")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(namespace = "eHealthBackend/ehealth/domain", name = "EffortType")
@XmlRootElement(namespace = "eHealthBackend/ehealth/domain")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "effortTypeId")

public class EffortType implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 */

	@Column(name = "effort_type_id", nullable = false)
	@Basic(fetch = FetchType.EAGER)

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@XmlElement
	Integer effortTypeId;
	/**
	 */

	@Column(name = "title", length = 45)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String title;
	/**
	 */

	@Column(name = "value", precision = 12, nullable = false)
	@Basic(fetch = FetchType.EAGER)

	BigDecimal value;
	/**
	 */
	@OneToMany(mappedBy = "effortType", cascade = { CascadeType.REMOVE }, fetch = FetchType.LAZY)

	@XmlElement(name = "", namespace = "")
	java.util.Set<ehealth.domain.ExerciseInput> exerciseInputs;

	/**
	 */
	public void setEffortTypeId(Integer effortTypeId) {
		this.effortTypeId = effortTypeId;
	}

	/**
	 */
	public Integer getEffortTypeId() {
		return this.effortTypeId;
	}

	/**
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 */
	public String getTitle() {
		return this.title;
	}

	/**
	 */
	public void setExerciseInputs(Set<ExerciseInput> exerciseInputs) {
		this.exerciseInputs = exerciseInputs;
	}

	/**
	 */
	@JsonIgnore
	public Set<ExerciseInput> getExerciseInputs() {
		if (exerciseInputs == null) {
			exerciseInputs = new java.util.LinkedHashSet<ehealth.domain.ExerciseInput>();
		}
		return exerciseInputs;
	}

	public BigDecimal getValue() {
		return value;
	}

	public void setValue(BigDecimal value) {
		this.value = value;
	}

	/**
	 */
	public EffortType() {
	}

	/**
	 * Copies the contents of the specified bean into this bean.
	 *
	 */
	public void copy(EffortType that) {
		setEffortTypeId(that.getEffortTypeId());
		setTitle(that.getTitle());
		setExerciseInputs(new java.util.LinkedHashSet<ehealth.domain.ExerciseInput>(that.getExerciseInputs()));
	}

	/**
	 * Returns a textual representation of a bean.
	 *
	 */
	public String toString() {

		StringBuilder buffer = new StringBuilder();

		buffer.append("effortTypeId=[").append(effortTypeId).append("] ");
		buffer.append("title=[").append(title).append("] ");

		return buffer.toString();
	}

	/**
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = (int) (prime * result + ((effortTypeId == null) ? 0 : effortTypeId.hashCode()));
		return result;
	}

	/**
	 */
	public boolean equals(Object obj) {
		if (obj == this)
			return true;
		if (!(obj instanceof EffortType))
			return false;
		EffortType equalCheck = (EffortType) obj;
		if ((effortTypeId == null && equalCheck.effortTypeId != null) || (effortTypeId != null && equalCheck.effortTypeId == null))
			return false;
		if (effortTypeId != null && !effortTypeId.equals(equalCheck.effortTypeId))
			return false;
		return true;
	}
}
