
package ehealth.domain;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

/**
 */

@Entity
@NamedQueries({
		@NamedQuery(name = "findAllTeams", query = "select myTeam from Team myTeam"),
		@NamedQuery(name = "findTeamByName", query = "select myTeam from Team myTeam where myTeam.name = ?1"),
		@NamedQuery(name = "findTeamByNameContaining", query = "select myTeam from Team myTeam where myTeam.name like ?1"),
		@NamedQuery(name = "findTeamByNumMember", query = "select myTeam from Team myTeam where myTeam.numMember = ?1"),
		@NamedQuery(name = "findTeamByPrimaryKey", query = "select myTeam from Team myTeam where myTeam.teamId = ?1"),
		@NamedQuery(name = "findTeamByTeamId", query = "select myTeam from Team myTeam where myTeam.teamId = ?1") })

@Table(catalog = "ehealthdb", name = "team")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(namespace = "eHealthBackend/ehealth/domain", name = "Team")
@XmlRootElement(namespace = "eHealthBackend/ehealth/domain")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "teamId")

public class Team implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 */

	@Column(name = "team_id", nullable = false)
	@Basic(fetch = FetchType.EAGER)

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@XmlElement
	Integer teamId;
	/**
	 */

	@Column(name = "name", length = 45, nullable = false)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String name;
	/**
	 */

	@Column(name = "numMember")
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	Integer numMember;

	/**
	 */
	@OneToMany(mappedBy = "team", cascade = { CascadeType.REMOVE }, fetch = FetchType.LAZY)

	@XmlElement(name = "", namespace = "")
	java.util.Set<ehealth.domain.Kid> kids;
	/**
	 */
	@OneToMany(mappedBy = "team", cascade = { CascadeType.REMOVE }, fetch = FetchType.LAZY)

	@XmlElement(name = "", namespace = "")
	java.util.Set<ehealth.domain.League> leagues;
	/**
	 */
	@OneToMany(mappedBy = "teamByTeamId2", cascade = { CascadeType.REMOVE }, fetch = FetchType.LAZY)

	@XmlElement(name = "", namespace = "")
	java.util.Set<ehealth.domain.Match> matchsForTeamId2;
	/**
	 */
	@OneToMany(mappedBy = "teamByTeamId2", cascade = { CascadeType.REMOVE }, fetch = FetchType.LAZY)

	@XmlElement(name = "", namespace = "")
	java.util.Set<ehealth.domain.Match> matchsForTeamId1;
	/**
	 */
	@OneToMany(mappedBy = "teamByTeamId2", cascade = { CascadeType.REMOVE }, fetch = FetchType.LAZY)

	@XmlElement(name = "", namespace = "")
	java.util.Set<ehealth.domain.Match> matchsForTeamIdWin;

	/**
	 */
	public void setTeamId(Integer teamId) {
		this.teamId = teamId;
	}

	/**
	 */
	public Integer getTeamId() {
		return this.teamId;
	}

	/**
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 */
	public String getName() {
		return this.name;
	}

	/**
	 */
	public void setNumMember(Integer numMember) {
		this.numMember = numMember;
	}

	/**
	 */
	public Integer getNumMember() {
		return this.numMember;
	}

	/**
	 */
	public void setKids(Set<Kid> kids) {
		this.kids = kids;
	}

	/**
	 */
	@JsonIgnore
	public Set<Kid> getKids() {
		if (kids == null) {
			kids = new java.util.LinkedHashSet<ehealth.domain.Kid>();
		}
		return kids;
	}

	/**
	 */
	public void setLeagues(Set<League> leagues) {
		this.leagues = leagues;
	}

	/**
	 */
	@JsonIgnore
	public Set<League> getLeagues() {
		if (leagues == null) {
			leagues = new java.util.LinkedHashSet<ehealth.domain.League>();
		}
		return leagues;
	}

	/**
	 */
	public void setMatchsForTeamId2(Set<Match> matchsForTeamId2) {
		this.matchsForTeamId2 = matchsForTeamId2;
	}

	/**
	 */
	@JsonIgnore
	public Set<Match> getMatchsForTeamId2() {
		if (matchsForTeamId2 == null) {
			matchsForTeamId2 = new java.util.LinkedHashSet<ehealth.domain.Match>();
		}
		return matchsForTeamId2;
	}

	/**
	 */
	public void setMatchsForTeamId1(Set<Match> matchsForTeamId1) {
		this.matchsForTeamId1 = matchsForTeamId1;
	}

	/**
	 */
	@JsonIgnore
	public Set<Match> getMatchsForTeamId1() {
		if (matchsForTeamId1 == null) {
			matchsForTeamId1 = new java.util.LinkedHashSet<ehealth.domain.Match>();
		}
		return matchsForTeamId1;
	}

	/**
	 */
	public void setMatchsForTeamIdWin(Set<Match> matchsForTeamIdWin) {
		this.matchsForTeamIdWin = matchsForTeamIdWin;
	}

	/**
	 */
	@JsonIgnore
	public Set<Match> getMatchsForTeamIdWin() {
		if (matchsForTeamIdWin == null) {
			matchsForTeamIdWin = new java.util.LinkedHashSet<ehealth.domain.Match>();
		}
		return matchsForTeamIdWin;
	}

	/**
	 */
	public Team() {
	}

	/**
	 * Copies the contents of the specified bean into this bean.
	 *
	 */
	public void copy(Team that) {
		setTeamId(that.getTeamId());
		setName(that.getName());
		setNumMember(that.getNumMember());
		setKids(new java.util.LinkedHashSet<ehealth.domain.Kid>(that.getKids()));
		setLeagues(new java.util.LinkedHashSet<ehealth.domain.League>(that.getLeagues()));
		setMatchsForTeamId2(new java.util.LinkedHashSet<ehealth.domain.Match>(that.getMatchsForTeamId2()));
		setMatchsForTeamId1(new java.util.LinkedHashSet<ehealth.domain.Match>(that.getMatchsForTeamId1()));
		setMatchsForTeamIdWin(new java.util.LinkedHashSet<ehealth.domain.Match>(that.getMatchsForTeamIdWin()));
	}

	/**
	 * Returns a textual representation of a bean.
	 *
	 */
	public String toString() {

		StringBuilder buffer = new StringBuilder();

		buffer.append("teamId=[").append(teamId).append("] ");
		buffer.append("name=[").append(name).append("] ");
		buffer.append("numMember=[").append(numMember).append("] ");

		return buffer.toString();
	}

	/**
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = (int) (prime * result + ((teamId == null) ? 0 : teamId.hashCode()));
		return result;
	}

	/**
	 */
	public boolean equals(Object obj) {
		if (obj == this)
			return true;
		if (!(obj instanceof Team))
			return false;
		Team equalCheck = (Team) obj;
		if ((teamId == null && equalCheck.teamId != null) || (teamId != null && equalCheck.teamId == null))
			return false;
		if (teamId != null && !teamId.equals(equalCheck.teamId))
			return false;
		return true;
	}
}
