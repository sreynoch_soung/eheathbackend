package ehealth.domain;

import java.io.Serializable;
import java.util.Set;

public class UserAccountParent implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	User user;
	Parent parent;
	int parentCityId;
	Set<Kid> kid;

	public Set<Kid> getKid() {
		return kid;
	}

	public void setKid(Set<Kid> kid) {
		this.kid = kid;
	}


	public User getUser() {
		return user;
	}

	public Parent getParent() {
		return parent;
	}

	public int getParentCityId() {
		return parentCityId;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public void setParent(Parent parent) {
		this.parent = parent;
	}

	public void setParentCityId(int parentCityId) {
		this.parentCityId = parentCityId;
	}

}
