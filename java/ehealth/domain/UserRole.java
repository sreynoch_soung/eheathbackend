
package ehealth.domain;
import java.io.Serializable;
import java.util.Set;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

/**
 */

@Entity
@NamedQueries({
		@NamedQuery(name = "findAllUserRoles", query = "select myUserRole from UserRole myUserRole"),
		@NamedQuery(name = "findUserRoleByPrimaryKey", query = "select myUserRole from UserRole myUserRole where myUserRole.userRoleId = ?1"),
		@NamedQuery(name = "findUserRoleByRoleTitle", query = "select myUserRole from UserRole myUserRole where myUserRole.roleTitle = ?1"),
		@NamedQuery(name = "findUserRoleByRoleTitleContaining", query = "select myUserRole from UserRole myUserRole where myUserRole.roleTitle like ?1"),
		@NamedQuery(name = "findUserRoleByUserRoleId", query = "select myUserRole from UserRole myUserRole where myUserRole.userRoleId = ?1") })

@Table(catalog = "ehealthdb", name = "user_role")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(namespace = "TestProject/ehealth/domain", name = "UserRole")
@XmlRootElement(namespace = "TestProject/ehealth/domain")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "userRoleId")

public class UserRole implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 */

	@Column(name = "user_role_id", nullable = false)
	@Basic(fetch = FetchType.EAGER)

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@XmlElement
	Integer userRoleId;
	/**
	 */

	@Column(name = "role_title", length = 20, nullable = false)
	@Basic(fetch = FetchType.EAGER)

	@XmlElement
	String roleTitle;

	/**
	 */
	@OneToMany(mappedBy = "userRole", cascade = { CascadeType.REMOVE }, fetch = FetchType.LAZY)

	@XmlElement(name = "", namespace = "")
	java.util.Set<ehealth.domain.User> users;

	/**
	 */
	public void setUserRoleId(Integer userRoleId) {
		this.userRoleId = userRoleId;
	}

	/**
	 */
	public Integer getUserRoleId() {
		return this.userRoleId;
	}

	/**
	 */
	public void setRoleTitle(String roleTitle) {
		this.roleTitle = roleTitle;
	}

	/**
	 */
	public String getRoleTitle() {
		return this.roleTitle;
	}

	/**
	 */
	public void setUsers(Set<User> users) {
		this.users = users;
	}

	/**
	 */
	@JsonIgnore
	public Set<User> getUsers() {
		if (users == null) {
			users = new java.util.LinkedHashSet<ehealth.domain.User>();
		}
		return users;
	}

	/**
	 */
	public UserRole() {
	}

	/**
	 * Copies the contents of the specified bean into this bean.
	 *
	 */
	public void copy(UserRole that) {
		setUserRoleId(that.getUserRoleId());
		setRoleTitle(that.getRoleTitle());
		setUsers(new java.util.LinkedHashSet<ehealth.domain.User>(that.getUsers()));
	}

	/**
	 * Returns a textual representation of a bean.
	 *
	 */
	public String toString() {

		StringBuilder buffer = new StringBuilder();

		buffer.append("userRoleId=[").append(userRoleId).append("] ");
		buffer.append("roleTitle=[").append(roleTitle).append("] ");

		return buffer.toString();
	}

	/**
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = (int) (prime * result + ((userRoleId == null) ? 0 : userRoleId.hashCode()));
		return result;
	}

	/**
	 */
	public boolean equals(Object obj) {
		if (obj == this)
			return true;
		if (!(obj instanceof UserRole))
			return false;
		UserRole equalCheck = (UserRole) obj;
		if ((userRoleId == null && equalCheck.userRoleId != null) || (userRoleId != null && equalCheck.userRoleId == null))
			return false;
		if (userRoleId != null && !userRoleId.equals(equalCheck.userRoleId))
			return false;
		return true;
	}
}
