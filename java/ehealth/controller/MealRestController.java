package ehealth.controller;

import ehealth.dao.FoodPhotoDAO;
import ehealth.dao.KidDAO;
import ehealth.dao.MealDAO;

import ehealth.domain.FoodPhoto;
import ehealth.domain.Kid;
import ehealth.domain.Meal;

import ehealth.service.MealService;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Controller;

import org.springframework.web.bind.WebDataBinder;

import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Spring Rest controller that handles CRUD requests for Meal entities
 * 
 */

@Controller("MealRestController")

public class MealRestController {

	/**
	 * DAO injected by Spring that manages FoodPhoto entities
	 * 
	 */
	@Autowired
	private FoodPhotoDAO foodPhotoDAO;

	/**
	 * DAO injected by Spring that manages Kid entities
	 * 
	 */
	@Autowired
	private KidDAO kidDAO;

	/**
	 * DAO injected by Spring that manages Meal entities
	 * 
	 */
	@Autowired
	private MealDAO mealDAO;

	/**
	 * Service injected by Spring that provides CRUD operations for Meal entities
	 * 
	 */
	@Autowired
	private MealService mealService;

	/**
	 * Register custom, context-specific property editors
	 * 
	 */
	@InitBinder
	public void initBinder(WebDataBinder binder, HttpServletRequest request) { // Register static property editors.
		binder.registerCustomEditor(java.util.Calendar.class, new org.skyway.spring.util.databinding.CustomCalendarEditor());
		binder.registerCustomEditor(byte[].class, new org.springframework.web.multipart.support.ByteArrayMultipartFileEditor());
		binder.registerCustomEditor(boolean.class, new org.skyway.spring.util.databinding.EnhancedBooleanEditor(false));
		binder.registerCustomEditor(Boolean.class, new org.skyway.spring.util.databinding.EnhancedBooleanEditor(true));
		binder.registerCustomEditor(java.math.BigDecimal.class, new org.skyway.spring.util.databinding.NaNHandlingNumberEditor(java.math.BigDecimal.class, true));
		binder.registerCustomEditor(Integer.class, new org.skyway.spring.util.databinding.NaNHandlingNumberEditor(Integer.class, true));
		binder.registerCustomEditor(java.util.Date.class, new org.skyway.spring.util.databinding.CustomDateEditor());
		binder.registerCustomEditor(String.class, new org.skyway.spring.util.databinding.StringEditor());
		binder.registerCustomEditor(Long.class, new org.skyway.spring.util.databinding.NaNHandlingNumberEditor(Long.class, true));
		binder.registerCustomEditor(Double.class, new org.skyway.spring.util.databinding.NaNHandlingNumberEditor(Double.class, true));
	}

	/**
	* Save an existing Meal entity
	* 
	*/
	@RequestMapping(value = "/Meal", method = RequestMethod.PUT)
	@ResponseBody
	public Meal saveMeal(@RequestBody Meal meal) {
		mealService.saveMeal(meal);
		return mealDAO.findMealByPrimaryKey(meal.getMealId());
	}

	/**
	* Create a new FoodPhoto entity
	* 
	*/
	@RequestMapping(value = "/Meal/{meal_mealId}/foodPhotos", method = RequestMethod.POST)
	@ResponseBody
	public FoodPhoto newMealFoodPhotos(@PathVariable Integer meal_mealId, @RequestBody FoodPhoto foodphoto) {
		mealService.saveMealFoodPhotos(meal_mealId, foodphoto);
		return foodPhotoDAO.findFoodPhotoByPrimaryKey(foodphoto.getFoodPhotoId());
	}

	/**
	* Show all FoodPhoto entities by Meal
	* 
	*/
	@RequestMapping(value = "/Meal/{meal_mealId}/foodPhotos", method = RequestMethod.GET)
	@ResponseBody
	public List<FoodPhoto> getMealFoodPhotos(@PathVariable Integer meal_mealId) {
		return new java.util.ArrayList<FoodPhoto>(mealDAO.findMealByPrimaryKey(meal_mealId).getFoodPhotos());
	}

	/**
	* Select an existing Meal entity
	* 
	*/
	@RequestMapping(value = "/Meal/{meal_mealId}", method = RequestMethod.GET)
	@ResponseBody
	public Meal loadMeal(@PathVariable Integer meal_mealId) {
		return mealDAO.findMealByPrimaryKey(meal_mealId);
	}

	/**
	* View an existing Kid entity
	* 
	*/
	@RequestMapping(value = "/Meal/{meal_mealId}/kid/{kid_kidId}", method = RequestMethod.GET)
	@ResponseBody
	public Kid loadMealKid(@PathVariable Integer meal_mealId, @PathVariable Integer related_kid_kidId) {
		Kid kid = kidDAO.findKidByPrimaryKey(related_kid_kidId, -1, -1);

		return kid;
	}

	/**
	* Create a new Kid entity
	* 
	*/
	@RequestMapping(value = "/Meal/{meal_mealId}/kid", method = RequestMethod.POST)
	@ResponseBody
	public Kid newMealKid(@PathVariable Integer meal_mealId, @RequestBody Kid kid) {
		mealService.saveMealKid(meal_mealId, kid);
		return kidDAO.findKidByPrimaryKey(kid.getKidId());
	}

	/**
	* Delete an existing FoodPhoto entity
	* 
	*/
	@RequestMapping(value = "/Meal/{meal_mealId}/foodPhotos/{foodphoto_foodPhotoId}", method = RequestMethod.DELETE)
	@ResponseBody
	public void deleteMealFoodPhotos(@PathVariable Integer meal_mealId, @PathVariable Integer related_foodphotos_foodPhotoId) {
		mealService.deleteMealFoodPhotos(meal_mealId, related_foodphotos_foodPhotoId);
	}

	/**
	* Delete an existing Meal entity
	* 
	*/
	@RequestMapping(value = "/Meal/{meal_mealId}", method = RequestMethod.DELETE)
	@ResponseBody
	public void deleteMeal(@PathVariable Integer meal_mealId) {
		Meal meal = mealDAO.findMealByPrimaryKey(meal_mealId);
		mealService.deleteMeal(meal);
	}

	/**
	* View an existing FoodPhoto entity
	* 
	*/
	@RequestMapping(value = "/Meal/{meal_mealId}/foodPhotos/{foodphoto_foodPhotoId}", method = RequestMethod.GET)
	@ResponseBody
	public FoodPhoto loadMealFoodPhotos(@PathVariable Integer meal_mealId, @PathVariable Integer related_foodphotos_foodPhotoId) {
		FoodPhoto foodphoto = foodPhotoDAO.findFoodPhotoByPrimaryKey(related_foodphotos_foodPhotoId, -1, -1);

		return foodphoto;
	}

	/**
	* Save an existing Kid entity
	* 
	*/
	@RequestMapping(value = "/Meal/{meal_mealId}/kid", method = RequestMethod.PUT)
	@ResponseBody
	public Kid saveMealKid(@PathVariable Integer meal_mealId, @RequestBody Kid kid) {
		mealService.saveMealKid(meal_mealId, kid);
		return kidDAO.findKidByPrimaryKey(kid.getKidId());
	}

	/**
	* Show all Meal entities
	* 
	*/
	@RequestMapping(value = "/Meal", method = RequestMethod.GET)
	@ResponseBody
	public List<Meal> listMeals() {
		return new java.util.ArrayList<Meal>(mealService.loadMeals());
	}

	/**
	* Create a new Meal entity
	* 
	*/
	@RequestMapping(value = "/Meal", method = RequestMethod.POST)
	@ResponseBody
	public Meal newMeal(@RequestBody Meal meal) {
		mealService.saveMeal(meal);
		return mealDAO.findMealByPrimaryKey(meal.getMealId());
	}

	/**
	* Get Kid entity by Meal
	* 
	*/
	@RequestMapping(value = "/Meal/{meal_mealId}/kid", method = RequestMethod.GET)
	@ResponseBody
	public Kid getMealKid(@PathVariable Integer meal_mealId) {
		return mealDAO.findMealByPrimaryKey(meal_mealId).getKid();
	}

	/**
	* Delete an existing Kid entity
	* 
	*/
	@RequestMapping(value = "/Meal/{meal_mealId}/kid/{kid_kidId}", method = RequestMethod.DELETE)
	@ResponseBody
	public void deleteMealKid(@PathVariable Integer meal_mealId, @PathVariable Integer related_kid_kidId) {
		mealService.deleteMealKid(meal_mealId, related_kid_kidId);
	}

	/**
	* Save an existing FoodPhoto entity
	* 
	*/
	@RequestMapping(value = "/Meal/{meal_mealId}/foodPhotos", method = RequestMethod.PUT)
	@ResponseBody
	public FoodPhoto saveMealFoodPhotos(@PathVariable Integer meal_mealId, @RequestBody FoodPhoto foodphotos) {
		mealService.saveMealFoodPhotos(meal_mealId, foodphotos);
		return foodPhotoDAO.findFoodPhotoByPrimaryKey(foodphotos.getFoodPhotoId());
	}
}