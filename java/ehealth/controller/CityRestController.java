package ehealth.controller;

import ehealth.dao.CityDAO;
import ehealth.dao.CountryDAO;
import ehealth.dao.KidDAO;
import ehealth.dao.ParentDAO;

import ehealth.domain.City;
import ehealth.domain.Country;
import ehealth.domain.Kid;
import ehealth.domain.Parent;

import ehealth.service.CityService;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Controller;

import org.springframework.web.bind.WebDataBinder;

import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Spring Rest controller that handles CRUD requests for City entities
 * 
 */

@Controller("CityRestController")

public class CityRestController {

	/**
	 * DAO injected by Spring that manages City entities
	 * 
	 */
	@Autowired
	private CityDAO cityDAO;

	/**
	 * DAO injected by Spring that manages Country entities
	 * 
	 */
	@Autowired
	private CountryDAO countryDAO;

	/**
	 * DAO injected by Spring that manages Kid entities
	 * 
	 */
	@Autowired
	private KidDAO kidDAO;

	/**
	 * DAO injected by Spring that manages Parent entities
	 * 
	 */
	@Autowired
	private ParentDAO parentDAO;

	/**
	 * Service injected by Spring that provides CRUD operations for City entities
	 * 
	 */
	@Autowired
	private CityService cityService;

	/**
	 * Select an existing City entity
	 * 
	 */
	@RequestMapping(value = "/City/{city_cityId}", method = RequestMethod.GET)
	@ResponseBody
	public City loadCity(@PathVariable Integer city_cityId) {
		return cityDAO.findCityByPrimaryKey(city_cityId);
	}

	/**
	* Delete an existing Kid entity
	* 
	*/
	@RequestMapping(value = "/City/{city_cityId}/kids/{kid_kidId}", method = RequestMethod.DELETE)
	@ResponseBody
	public void deleteCityKids(@PathVariable Integer city_cityId, @PathVariable Integer related_kids_kidId) {
		cityService.deleteCityKids(city_cityId, related_kids_kidId);
	}

	/**
	* Create a new City entity
	* 
	*/
	@RequestMapping(value = "/City", method = RequestMethod.POST)
	@ResponseBody
	public City newCity(@RequestBody City city) {
		cityService.saveCity(city);
		return cityDAO.findCityByPrimaryKey(city.getCityId());
	}

	/**
	* Register custom, context-specific property editors
	* 
	*/
	@InitBinder
	public void initBinder(WebDataBinder binder, HttpServletRequest request) { // Register static property editors.
		binder.registerCustomEditor(java.util.Calendar.class, new org.skyway.spring.util.databinding.CustomCalendarEditor());
		binder.registerCustomEditor(byte[].class, new org.springframework.web.multipart.support.ByteArrayMultipartFileEditor());
		binder.registerCustomEditor(boolean.class, new org.skyway.spring.util.databinding.EnhancedBooleanEditor(false));
		binder.registerCustomEditor(Boolean.class, new org.skyway.spring.util.databinding.EnhancedBooleanEditor(true));
		binder.registerCustomEditor(java.math.BigDecimal.class, new org.skyway.spring.util.databinding.NaNHandlingNumberEditor(java.math.BigDecimal.class, true));
		binder.registerCustomEditor(Integer.class, new org.skyway.spring.util.databinding.NaNHandlingNumberEditor(Integer.class, true));
		binder.registerCustomEditor(java.util.Date.class, new org.skyway.spring.util.databinding.CustomDateEditor());
		binder.registerCustomEditor(String.class, new org.skyway.spring.util.databinding.StringEditor());
		binder.registerCustomEditor(Long.class, new org.skyway.spring.util.databinding.NaNHandlingNumberEditor(Long.class, true));
		binder.registerCustomEditor(Double.class, new org.skyway.spring.util.databinding.NaNHandlingNumberEditor(Double.class, true));
	}

	/**
	* Save an existing Parent entity
	* 
	*/
	@RequestMapping(value = "/City/{city_cityId}/parents", method = RequestMethod.PUT)
	@ResponseBody
	public Parent saveCityParents(@PathVariable Integer city_cityId, @RequestBody Parent parents) {
		cityService.saveCityParents(city_cityId, parents);
		return parentDAO.findParentByPrimaryKey(parents.getParentId());
	}

	/**
	* Create a new Kid entity
	* 
	*/
	@RequestMapping(value = "/City/{city_cityId}/kids", method = RequestMethod.POST)
	@ResponseBody
	public Kid newCityKids(@PathVariable Integer city_cityId, @RequestBody Kid kid) {
		cityService.saveCityKids(city_cityId, kid);
		return kidDAO.findKidByPrimaryKey(kid.getKidId());
	}

	/**
	* Delete an existing Country entity
	* 
	*/
	@RequestMapping(value = "/City/{city_cityId}/country/{country_countryId}", method = RequestMethod.DELETE)
	@ResponseBody
	public void deleteCityCountry(@PathVariable Integer city_cityId, @PathVariable Integer related_country_countryId) {
		cityService.deleteCityCountry(city_cityId, related_country_countryId);
	}

	/**
	* Delete an existing City entity
	* 
	*/
	@RequestMapping(value = "/City/{city_cityId}", method = RequestMethod.DELETE)
	@ResponseBody
	public void deleteCity(@PathVariable Integer city_cityId) {
		City city = cityDAO.findCityByPrimaryKey(city_cityId);
		cityService.deleteCity(city);
	}

	/**
	* Create a new Country entity
	* 
	*/
	@RequestMapping(value = "/City/{city_cityId}/country", method = RequestMethod.POST)
	@ResponseBody
	public Country newCityCountry(@PathVariable Integer city_cityId, @RequestBody Country country) {
		cityService.saveCityCountry(city_cityId, country);
		return countryDAO.findCountryByPrimaryKey(country.getCountryId());
	}

	/**
	* Get Country entity by City
	* 
	*/
	@RequestMapping(value = "/City/{city_cityId}/country", method = RequestMethod.GET)
	@ResponseBody
	public Country getCityCountry(@PathVariable Integer city_cityId) {
		return cityDAO.findCityByPrimaryKey(city_cityId).getCountry();
	}

	/**
	* Save an existing Kid entity
	* 
	*/
	@RequestMapping(value = "/City/{city_cityId}/kids", method = RequestMethod.PUT)
	@ResponseBody
	public Kid saveCityKids(@PathVariable Integer city_cityId, @RequestBody Kid kids) {
		cityService.saveCityKids(city_cityId, kids);
		return kidDAO.findKidByPrimaryKey(kids.getKidId());
	}

	/**
	* View an existing Kid entity
	* 
	*/
	@RequestMapping(value = "/City/{city_cityId}/kids/{kid_kidId}", method = RequestMethod.GET)
	@ResponseBody
	public Kid loadCityKids(@PathVariable Integer city_cityId, @PathVariable Integer related_kids_kidId) {
		Kid kid = kidDAO.findKidByPrimaryKey(related_kids_kidId, -1, -1);

		return kid;
	}

	/**
	* Delete an existing Parent entity
	* 
	*/
	@RequestMapping(value = "/City/{city_cityId}/parents/{parent_parentId}", method = RequestMethod.DELETE)
	@ResponseBody
	public void deleteCityParents(@PathVariable Integer city_cityId, @PathVariable Integer related_parents_parentId) {
		cityService.deleteCityParents(city_cityId, related_parents_parentId);
	}

	/**
	* Save an existing Country entity
	* 
	*/
	@RequestMapping(value = "/City/{city_cityId}/country", method = RequestMethod.PUT)
	@ResponseBody
	public Country saveCityCountry(@PathVariable Integer city_cityId, @RequestBody Country country) {
		cityService.saveCityCountry(city_cityId, country);
		return countryDAO.findCountryByPrimaryKey(country.getCountryId());
	}

	/**
	* Show all Parent entities by City
	* 
	*/
	@RequestMapping(value = "/City/{city_cityId}/parents", method = RequestMethod.GET)
	@ResponseBody
	public List<Parent> getCityParents(@PathVariable Integer city_cityId) {
		return new java.util.ArrayList<Parent>(cityDAO.findCityByPrimaryKey(city_cityId).getParents());
	}

	/**
	* Show all Kid entities by City
	* 
	*/
	@RequestMapping(value = "/City/{city_cityId}/kids", method = RequestMethod.GET)
	@ResponseBody
	public List<Kid> getCityKids(@PathVariable Integer city_cityId) {
		return new java.util.ArrayList<Kid>(cityDAO.findCityByPrimaryKey(city_cityId).getKids());
	}

	/**
	* Create a new Parent entity
	* 
	*/
	@RequestMapping(value = "/City/{city_cityId}/parents", method = RequestMethod.POST)
	@ResponseBody
	public Parent newCityParents(@PathVariable Integer city_cityId, @RequestBody Parent parent) {
		cityService.saveCityParents(city_cityId, parent);
		return parentDAO.findParentByPrimaryKey(parent.getParentId());
	}

	/**
	* Show all City entities
	* 
	*/
	@RequestMapping(value = "/City", method = RequestMethod.GET)
	@ResponseBody
	public List<City> listCitys() {
		return new java.util.ArrayList<City>(cityService.loadCitys());
	}

	/**
	* Save an existing City entity
	* 
	*/
	@RequestMapping(value = "/City", method = RequestMethod.PUT)
	@ResponseBody
	public City saveCity(@RequestBody City city) {
		cityService.saveCity(city);
		return cityDAO.findCityByPrimaryKey(city.getCityId());
	}

	/**
	* View an existing Parent entity
	* 
	*/
	@RequestMapping(value = "/City/{city_cityId}/parents/{parent_parentId}", method = RequestMethod.GET)
	@ResponseBody
	public Parent loadCityParents(@PathVariable Integer city_cityId, @PathVariable Integer related_parents_parentId) {
		Parent parent = parentDAO.findParentByPrimaryKey(related_parents_parentId, -1, -1);

		return parent;
	}

	/**
	* View an existing Country entity
	* 
	*/
	@RequestMapping(value = "/City/{city_cityId}/country/{country_countryId}", method = RequestMethod.GET)
	@ResponseBody
	public Country loadCityCountry(@PathVariable Integer city_cityId, @PathVariable Integer related_country_countryId) {
		Country country = countryDAO.findCountryByPrimaryKey(related_country_countryId, -1, -1);

		return country;
	}
}