package ehealth.controller;

import ehealth.dao.ChatRecordDAO;
import ehealth.dao.KidChatDAO;

import ehealth.domain.ChatRecord;
import ehealth.domain.KidChat;

import ehealth.service.ChatRecordService;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Controller;

import org.springframework.web.bind.WebDataBinder;

import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Spring Rest controller that handles CRUD requests for ChatRecord entities
 * 
 */

@Controller("ChatRecordRestController")

public class ChatRecordRestController {

	/**
	 * DAO injected by Spring that manages ChatRecord entities
	 * 
	 */
	@Autowired
	private ChatRecordDAO chatRecordDAO;

	/**
	 * DAO injected by Spring that manages KidChat entities
	 * 
	 */
	@Autowired
	private KidChatDAO kidChatDAO;

	/**
	 * Service injected by Spring that provides CRUD operations for ChatRecord entities
	 * 
	 */
	@Autowired
	private ChatRecordService chatRecordService;

	/**
	 * Create a new KidChat entity
	 * 
	 */
	@RequestMapping(value = "/ChatRecord/{chatrecord_chatRecordId}/kidChats", method = RequestMethod.POST)
	@ResponseBody
	public KidChat newChatRecordKidChats(@PathVariable Integer chatrecord_chatRecordId, @RequestBody KidChat kidchat) {
		chatRecordService.saveChatRecordKidChats(chatrecord_chatRecordId, kidchat);
		return kidChatDAO.findKidChatByPrimaryKey(kidchat.getKidChatId());
	}

	/**
	* Select an existing ChatRecord entity
	* 
	*/
	@RequestMapping(value = "/ChatRecord/{chatrecord_chatRecordId}", method = RequestMethod.GET)
	@ResponseBody
	public ChatRecord loadChatRecord(@PathVariable Integer chatrecord_chatRecordId) {
		return chatRecordDAO.findChatRecordByPrimaryKey(chatrecord_chatRecordId);
	}

	/**
	* Save an existing ChatRecord entity
	* 
	*/
	@RequestMapping(value = "/ChatRecord", method = RequestMethod.PUT)
	@ResponseBody
	public ChatRecord saveChatRecord(@RequestBody ChatRecord chatrecord) {
		chatRecordService.saveChatRecord(chatrecord);
		return chatRecordDAO.findChatRecordByPrimaryKey(chatrecord.getChatRecordId());
	}

	/**
	* Delete an existing KidChat entity
	* 
	*/
	@RequestMapping(value = "/ChatRecord/{chatrecord_chatRecordId}/kidChats/{kidchat_kidChatId}", method = RequestMethod.DELETE)
	@ResponseBody
	public void deleteChatRecordKidChats(@PathVariable Integer chatrecord_chatRecordId, @PathVariable Integer related_kidchats_kidChatId) {
		chatRecordService.deleteChatRecordKidChats(chatrecord_chatRecordId, related_kidchats_kidChatId);
	}

	/**
	* Show all ChatRecord entities
	* 
	*/
	@RequestMapping(value = "/ChatRecord", method = RequestMethod.GET)
	@ResponseBody
	public List<ChatRecord> listChatRecords() {
		return new java.util.ArrayList<ChatRecord>(chatRecordService.loadChatRecords());
	}

	/**
	* Save an existing KidChat entity
	* 
	*/
	@RequestMapping(value = "/ChatRecord/{chatrecord_chatRecordId}/kidChats", method = RequestMethod.PUT)
	@ResponseBody
	public KidChat saveChatRecordKidChats(@PathVariable Integer chatrecord_chatRecordId, @RequestBody KidChat kidchats) {
		chatRecordService.saveChatRecordKidChats(chatrecord_chatRecordId, kidchats);
		return kidChatDAO.findKidChatByPrimaryKey(kidchats.getKidChatId());
	}

	/**
	* Register custom, context-specific property editors
	* 
	*/
	@InitBinder
	public void initBinder(WebDataBinder binder, HttpServletRequest request) { // Register static property editors.
		binder.registerCustomEditor(java.util.Calendar.class, new org.skyway.spring.util.databinding.CustomCalendarEditor());
		binder.registerCustomEditor(byte[].class, new org.springframework.web.multipart.support.ByteArrayMultipartFileEditor());
		binder.registerCustomEditor(boolean.class, new org.skyway.spring.util.databinding.EnhancedBooleanEditor(false));
		binder.registerCustomEditor(Boolean.class, new org.skyway.spring.util.databinding.EnhancedBooleanEditor(true));
		binder.registerCustomEditor(java.math.BigDecimal.class, new org.skyway.spring.util.databinding.NaNHandlingNumberEditor(java.math.BigDecimal.class, true));
		binder.registerCustomEditor(Integer.class, new org.skyway.spring.util.databinding.NaNHandlingNumberEditor(Integer.class, true));
		binder.registerCustomEditor(java.util.Date.class, new org.skyway.spring.util.databinding.CustomDateEditor());
		binder.registerCustomEditor(String.class, new org.skyway.spring.util.databinding.StringEditor());
		binder.registerCustomEditor(Long.class, new org.skyway.spring.util.databinding.NaNHandlingNumberEditor(Long.class, true));
		binder.registerCustomEditor(Double.class, new org.skyway.spring.util.databinding.NaNHandlingNumberEditor(Double.class, true));
	}

	/**
	* Show all KidChat entities by ChatRecord
	* 
	*/
	@RequestMapping(value = "/ChatRecord/{chatrecord_chatRecordId}/kidChats", method = RequestMethod.GET)
	@ResponseBody
	public List<KidChat> getChatRecordKidChats(@PathVariable Integer chatrecord_chatRecordId) {
		return new java.util.ArrayList<KidChat>(chatRecordDAO.findChatRecordByPrimaryKey(chatrecord_chatRecordId).getKidChats());
	}

	/**
	* Create a new ChatRecord entity
	* 
	*/
	@RequestMapping(value = "/ChatRecord", method = RequestMethod.POST)
	@ResponseBody
	public ChatRecord newChatRecord(@RequestBody ChatRecord chatrecord) {
		chatRecordService.saveChatRecord(chatrecord);
		return chatRecordDAO.findChatRecordByPrimaryKey(chatrecord.getChatRecordId());
	}

	/**
	* Delete an existing ChatRecord entity
	* 
	*/
	@RequestMapping(value = "/ChatRecord/{chatrecord_chatRecordId}", method = RequestMethod.DELETE)
	@ResponseBody
	public void deleteChatRecord(@PathVariable Integer chatrecord_chatRecordId) {
		ChatRecord chatrecord = chatRecordDAO.findChatRecordByPrimaryKey(chatrecord_chatRecordId);
		chatRecordService.deleteChatRecord(chatrecord);
	}

	/**
	* View an existing KidChat entity
	* 
	*/
	@RequestMapping(value = "/ChatRecord/{chatrecord_chatRecordId}/kidChats/{kidchat_kidChatId}", method = RequestMethod.GET)
	@ResponseBody
	public KidChat loadChatRecordKidChats(@PathVariable Integer chatrecord_chatRecordId, @PathVariable Integer related_kidchats_kidChatId) {
		KidChat kidchat = kidChatDAO.findKidChatByPrimaryKey(related_kidchats_kidChatId, -1, -1);

		return kidchat;
	}
}