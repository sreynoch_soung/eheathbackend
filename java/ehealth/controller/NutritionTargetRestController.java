package ehealth.controller;

import ehealth.dao.DoctorDAO;
import ehealth.dao.FoodTageTypeDAO;
import ehealth.dao.KidDAO;
import ehealth.dao.NutritionTargetDAO;

import ehealth.domain.Doctor;
import ehealth.domain.FoodTageType;
import ehealth.domain.Kid;
import ehealth.domain.NutritionTarget;

import ehealth.service.NutritionTargetService;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Controller;

import org.springframework.web.bind.WebDataBinder;

import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Spring Rest controller that handles CRUD requests for NutritionTarget entities
 * 
 */

@Controller("NutritionTargetRestController")

public class NutritionTargetRestController {

	/**
	 * DAO injected by Spring that manages Doctor entities
	 * 
	 */
	@Autowired
	private DoctorDAO doctorDAO;

	/**
	 * DAO injected by Spring that manages FoodTageType entities
	 * 
	 */
	@Autowired
	private FoodTageTypeDAO foodTageTypeDAO;

	/**
	 * DAO injected by Spring that manages Kid entities
	 * 
	 */
	@Autowired
	private KidDAO kidDAO;

	/**
	 * DAO injected by Spring that manages NutritionTarget entities
	 * 
	 */
	@Autowired
	private NutritionTargetDAO nutritionTargetDAO;

	/**
	 * Service injected by Spring that provides CRUD operations for NutritionTarget entities
	 * 
	 */
	@Autowired
	private NutritionTargetService nutritionTargetService;

	/**
	 * Create a new FoodTageType entity
	 * 
	 */
	@RequestMapping(value = "/NutritionTarget/{nutritiontarget_nutritionTargetId}/foodTageType", method = RequestMethod.POST)
	@ResponseBody
	public FoodTageType newNutritionTargetFoodTageType(@PathVariable Integer nutritiontarget_nutritionTargetId, @RequestBody FoodTageType foodtagetype) {
		nutritionTargetService.saveNutritionTargetFoodTageType(nutritiontarget_nutritionTargetId, foodtagetype);
		return foodTageTypeDAO.findFoodTageTypeByPrimaryKey(foodtagetype.getFoodTageTypeId());
	}

	/**
	* Get FoodTageType entity by NutritionTarget
	* 
	*/
	@RequestMapping(value = "/NutritionTarget/{nutritiontarget_nutritionTargetId}/foodTageType", method = RequestMethod.GET)
	@ResponseBody
	public FoodTageType getNutritionTargetFoodTageType(@PathVariable Integer nutritiontarget_nutritionTargetId) {
		return nutritionTargetDAO.findNutritionTargetByPrimaryKey(nutritiontarget_nutritionTargetId).getFoodTageType();
	}

	/**
	* View an existing FoodTageType entity
	* 
	*/
	@RequestMapping(value = "/NutritionTarget/{nutritiontarget_nutritionTargetId}/foodTageType/{foodtagetype_foodTageTypeId}", method = RequestMethod.GET)
	@ResponseBody
	public FoodTageType loadNutritionTargetFoodTageType(@PathVariable Integer nutritiontarget_nutritionTargetId, @PathVariable Integer related_foodtagetype_foodTageTypeId) {
		FoodTageType foodtagetype = foodTageTypeDAO.findFoodTageTypeByPrimaryKey(related_foodtagetype_foodTageTypeId, -1, -1);

		return foodtagetype;
	}

	/**
	* Delete an existing FoodTageType entity
	* 
	*/
	@RequestMapping(value = "/NutritionTarget/{nutritiontarget_nutritionTargetId}/foodTageType/{foodtagetype_foodTageTypeId}", method = RequestMethod.DELETE)
	@ResponseBody
	public void deleteNutritionTargetFoodTageType(@PathVariable Integer nutritiontarget_nutritionTargetId, @PathVariable Integer related_foodtagetype_foodTageTypeId) {
		nutritionTargetService.deleteNutritionTargetFoodTageType(nutritiontarget_nutritionTargetId, related_foodtagetype_foodTageTypeId);
	}

	/**
	* Register custom, context-specific property editors
	* 
	*/
	@InitBinder
	public void initBinder(WebDataBinder binder, HttpServletRequest request) { // Register static property editors.
		binder.registerCustomEditor(java.util.Calendar.class, new org.skyway.spring.util.databinding.CustomCalendarEditor());
		binder.registerCustomEditor(byte[].class, new org.springframework.web.multipart.support.ByteArrayMultipartFileEditor());
		binder.registerCustomEditor(boolean.class, new org.skyway.spring.util.databinding.EnhancedBooleanEditor(false));
		binder.registerCustomEditor(Boolean.class, new org.skyway.spring.util.databinding.EnhancedBooleanEditor(true));
		binder.registerCustomEditor(java.math.BigDecimal.class, new org.skyway.spring.util.databinding.NaNHandlingNumberEditor(java.math.BigDecimal.class, true));
		binder.registerCustomEditor(Integer.class, new org.skyway.spring.util.databinding.NaNHandlingNumberEditor(Integer.class, true));
		binder.registerCustomEditor(java.util.Date.class, new org.skyway.spring.util.databinding.CustomDateEditor());
		binder.registerCustomEditor(String.class, new org.skyway.spring.util.databinding.StringEditor());
		binder.registerCustomEditor(Long.class, new org.skyway.spring.util.databinding.NaNHandlingNumberEditor(Long.class, true));
		binder.registerCustomEditor(Double.class, new org.skyway.spring.util.databinding.NaNHandlingNumberEditor(Double.class, true));
	}

	/**
	* View an existing Doctor entity
	* 
	*/
	@RequestMapping(value = "/NutritionTarget/{nutritiontarget_nutritionTargetId}/doctor/{doctor_doctorId}", method = RequestMethod.GET)
	@ResponseBody
	public Doctor loadNutritionTargetDoctor(@PathVariable Integer nutritiontarget_nutritionTargetId, @PathVariable Integer related_doctor_doctorId) {
		Doctor doctor = doctorDAO.findDoctorByPrimaryKey(related_doctor_doctorId, -1, -1);

		return doctor;
	}

	/**
	* Select an existing NutritionTarget entity
	* 
	*/
	@RequestMapping(value = "/NutritionTarget/{nutritiontarget_nutritionTargetId}", method = RequestMethod.GET)
	@ResponseBody
	public NutritionTarget loadNutritionTarget(@PathVariable Integer nutritiontarget_nutritionTargetId) {
		return nutritionTargetDAO.findNutritionTargetByPrimaryKey(nutritiontarget_nutritionTargetId);
	}

	/**
	* Save an existing NutritionTarget entity
	* 
	*/
	@RequestMapping(value = "/NutritionTarget", method = RequestMethod.PUT)
	@ResponseBody
	public NutritionTarget saveNutritionTarget(@RequestBody NutritionTarget nutritiontarget) {
		nutritionTargetService.saveNutritionTarget(nutritiontarget);
		return nutritionTargetDAO.findNutritionTargetByPrimaryKey(nutritiontarget.getNutritionTargetId());
	}

	/**
	* Delete an existing NutritionTarget entity
	* 
	*/
	@RequestMapping(value = "/NutritionTarget/{nutritiontarget_nutritionTargetId}", method = RequestMethod.DELETE)
	@ResponseBody
	public void deleteNutritionTarget(@PathVariable Integer nutritiontarget_nutritionTargetId) {
		NutritionTarget nutritiontarget = nutritionTargetDAO.findNutritionTargetByPrimaryKey(nutritiontarget_nutritionTargetId);
		nutritionTargetService.deleteNutritionTarget(nutritiontarget);
	}

	/**
	* Get Kid entity by NutritionTarget
	* 
	*/
	@RequestMapping(value = "/NutritionTarget/{nutritiontarget_nutritionTargetId}/kid", method = RequestMethod.GET)
	@ResponseBody
	public Kid getNutritionTargetKid(@PathVariable Integer nutritiontarget_nutritionTargetId) {
		return nutritionTargetDAO.findNutritionTargetByPrimaryKey(nutritiontarget_nutritionTargetId).getKid();
	}

	/**
	* Save an existing Doctor entity
	* 
	*/
	@RequestMapping(value = "/NutritionTarget/{nutritiontarget_nutritionTargetId}/doctor", method = RequestMethod.PUT)
	@ResponseBody
	public Doctor saveNutritionTargetDoctor(@PathVariable Integer nutritiontarget_nutritionTargetId, @RequestBody Doctor doctor) {
		nutritionTargetService.saveNutritionTargetDoctor(nutritiontarget_nutritionTargetId, doctor);
		return doctorDAO.findDoctorByPrimaryKey(doctor.getDoctorId());
	}

	/**
	* View an existing Kid entity
	* 
	*/
	@RequestMapping(value = "/NutritionTarget/{nutritiontarget_nutritionTargetId}/kid/{kid_kidId}", method = RequestMethod.GET)
	@ResponseBody
	public Kid loadNutritionTargetKid(@PathVariable Integer nutritiontarget_nutritionTargetId, @PathVariable Integer related_kid_kidId) {
		Kid kid = kidDAO.findKidByPrimaryKey(related_kid_kidId, -1, -1);

		return kid;
	}

	/**
	* Create a new Doctor entity
	* 
	*/
	@RequestMapping(value = "/NutritionTarget/{nutritiontarget_nutritionTargetId}/doctor", method = RequestMethod.POST)
	@ResponseBody
	public Doctor newNutritionTargetDoctor(@PathVariable Integer nutritiontarget_nutritionTargetId, @RequestBody Doctor doctor) {
		nutritionTargetService.saveNutritionTargetDoctor(nutritiontarget_nutritionTargetId, doctor);
		return doctorDAO.findDoctorByPrimaryKey(doctor.getDoctorId());
	}

	/**
	* Delete an existing Doctor entity
	* 
	*/
	@RequestMapping(value = "/NutritionTarget/{nutritiontarget_nutritionTargetId}/doctor/{doctor_doctorId}", method = RequestMethod.DELETE)
	@ResponseBody
	public void deleteNutritionTargetDoctor(@PathVariable Integer nutritiontarget_nutritionTargetId, @PathVariable Integer related_doctor_doctorId) {
		nutritionTargetService.deleteNutritionTargetDoctor(nutritiontarget_nutritionTargetId, related_doctor_doctorId);
	}

	/**
	* Save an existing Kid entity
	* 
	*/
	@RequestMapping(value = "/NutritionTarget/{nutritiontarget_nutritionTargetId}/kid", method = RequestMethod.PUT)
	@ResponseBody
	public Kid saveNutritionTargetKid(@PathVariable Integer nutritiontarget_nutritionTargetId, @RequestBody Kid kid) {
		nutritionTargetService.saveNutritionTargetKid(nutritiontarget_nutritionTargetId, kid);
		return kidDAO.findKidByPrimaryKey(kid.getKidId());
	}

	/**
	* Delete an existing Kid entity
	* 
	*/
	@RequestMapping(value = "/NutritionTarget/{nutritiontarget_nutritionTargetId}/kid/{kid_kidId}", method = RequestMethod.DELETE)
	@ResponseBody
	public void deleteNutritionTargetKid(@PathVariable Integer nutritiontarget_nutritionTargetId, @PathVariable Integer related_kid_kidId) {
		nutritionTargetService.deleteNutritionTargetKid(nutritiontarget_nutritionTargetId, related_kid_kidId);
	}

	/**
	* Create a new NutritionTarget entity
	* 
	*/
	@RequestMapping(value = "/NutritionTarget", method = RequestMethod.POST)
	@ResponseBody
	public NutritionTarget newNutritionTarget(@RequestBody NutritionTarget nutritiontarget) {
		nutritionTargetService.saveNutritionTarget(nutritiontarget);
		return nutritionTargetDAO.findNutritionTargetByPrimaryKey(nutritiontarget.getNutritionTargetId());
	}

	/**
	* Get Doctor entity by NutritionTarget
	* 
	*/
	@RequestMapping(value = "/NutritionTarget/{nutritiontarget_nutritionTargetId}/doctor", method = RequestMethod.GET)
	@ResponseBody
	public Doctor getNutritionTargetDoctor(@PathVariable Integer nutritiontarget_nutritionTargetId) {
		return nutritionTargetDAO.findNutritionTargetByPrimaryKey(nutritiontarget_nutritionTargetId).getDoctor();
	}

	/**
	* Save an existing FoodTageType entity
	* 
	*/
	@RequestMapping(value = "/NutritionTarget/{nutritiontarget_nutritionTargetId}/foodTageType", method = RequestMethod.PUT)
	@ResponseBody
	public FoodTageType saveNutritionTargetFoodTageType(@PathVariable Integer nutritiontarget_nutritionTargetId, @RequestBody FoodTageType foodtagetype) {
		nutritionTargetService.saveNutritionTargetFoodTageType(nutritiontarget_nutritionTargetId, foodtagetype);
		return foodTageTypeDAO.findFoodTageTypeByPrimaryKey(foodtagetype.getFoodTageTypeId());
	}

	/**
	* Create a new Kid entity
	* 
	*/
	@RequestMapping(value = "/NutritionTarget/{nutritiontarget_nutritionTargetId}/kid", method = RequestMethod.POST)
	@ResponseBody
	public Kid newNutritionTargetKid(@PathVariable Integer nutritiontarget_nutritionTargetId, @RequestBody Kid kid) {
		nutritionTargetService.saveNutritionTargetKid(nutritiontarget_nutritionTargetId, kid);
		return kidDAO.findKidByPrimaryKey(kid.getKidId());
	}

	/**
	* Show all NutritionTarget entities
	* 
	*/
	@RequestMapping(value = "/NutritionTarget", method = RequestMethod.GET)
	@ResponseBody
	public List<NutritionTarget> listNutritionTargets() {
		return new java.util.ArrayList<NutritionTarget>(nutritionTargetService.loadNutritionTargets());
	}
}