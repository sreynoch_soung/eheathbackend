package ehealth.controller;

import ehealth.dao.AccountSettingDAO;
import ehealth.dao.KidDAO;
import ehealth.dao.ParentDAO;

import ehealth.domain.AccountSetting;
import ehealth.domain.Kid;
import ehealth.domain.Parent;

import ehealth.service.AccountSettingService;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Controller;

import org.springframework.web.bind.WebDataBinder;

import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Spring Rest controller that handles CRUD requests for AccountSetting entities
 * 
 */

@Controller("AccountSettingRestController")

public class AccountSettingRestController {

	/**
	 * DAO injected by Spring that manages AccountSetting entities
	 * 
	 */
	@Autowired
	private AccountSettingDAO accountSettingDAO;

	/**
	 * DAO injected by Spring that manages Kid entities
	 * 
	 */
	@Autowired
	private KidDAO kidDAO;

	/**
	 * DAO injected by Spring that manages Parent entities
	 * 
	 */
	@Autowired
	private ParentDAO parentDAO;

	/**
	 * Service injected by Spring that provides CRUD operations for AccountSetting entities
	 * 
	 */
	@Autowired
	private AccountSettingService accountSettingService;

	/**
	 * Select an existing AccountSetting entity
	 * 
	 */
	@RequestMapping(value = "/AccountSetting/{accountsetting_accountSettingId}", method = RequestMethod.GET)
	@ResponseBody
	public AccountSetting loadAccountSetting(@PathVariable Integer accountsetting_accountSettingId) {
		return accountSettingDAO.findAccountSettingByPrimaryKey(accountsetting_accountSettingId);
	}

	/**
	* Save an existing AccountSetting entity
	* 
	*/
	@RequestMapping(value = "/AccountSetting", method = RequestMethod.PUT)
	@ResponseBody
	public AccountSetting saveAccountSetting(@RequestBody AccountSetting accountsetting) {
		accountSettingService.saveAccountSetting(accountsetting);
		return accountSettingDAO.findAccountSettingByPrimaryKey(accountsetting.getAccountSettingId());
	}

	/**
	* Delete an existing Parent entity
	* 
	*/
	@RequestMapping(value = "/AccountSetting/{accountsetting_accountSettingId}/parents/{parent_parentId}", method = RequestMethod.DELETE)
	@ResponseBody
	public void deleteAccountSettingParents(@PathVariable Integer accountsetting_accountSettingId, @PathVariable Integer related_parents_parentId) {
		accountSettingService.deleteAccountSettingParents(accountsetting_accountSettingId, related_parents_parentId);
	}

	/**
	* Register custom, context-specific property editors
	* 
	*/
	@InitBinder
	public void initBinder(WebDataBinder binder, HttpServletRequest request) { // Register static property editors.
		binder.registerCustomEditor(java.util.Calendar.class, new org.skyway.spring.util.databinding.CustomCalendarEditor());
		binder.registerCustomEditor(byte[].class, new org.springframework.web.multipart.support.ByteArrayMultipartFileEditor());
		binder.registerCustomEditor(boolean.class, new org.skyway.spring.util.databinding.EnhancedBooleanEditor(false));
		binder.registerCustomEditor(Boolean.class, new org.skyway.spring.util.databinding.EnhancedBooleanEditor(true));
		binder.registerCustomEditor(java.math.BigDecimal.class, new org.skyway.spring.util.databinding.NaNHandlingNumberEditor(java.math.BigDecimal.class, true));
		binder.registerCustomEditor(Integer.class, new org.skyway.spring.util.databinding.NaNHandlingNumberEditor(Integer.class, true));
		binder.registerCustomEditor(java.util.Date.class, new org.skyway.spring.util.databinding.CustomDateEditor());
		binder.registerCustomEditor(String.class, new org.skyway.spring.util.databinding.StringEditor());
		binder.registerCustomEditor(Long.class, new org.skyway.spring.util.databinding.NaNHandlingNumberEditor(Long.class, true));
		binder.registerCustomEditor(Double.class, new org.skyway.spring.util.databinding.NaNHandlingNumberEditor(Double.class, true));
	}

	/**
	* Save an existing Kid entity
	* 
	*/
	@RequestMapping(value = "/AccountSetting/{accountsetting_accountSettingId}/kids", method = RequestMethod.PUT)
	@ResponseBody
	public Kid saveAccountSettingKids(@PathVariable Integer accountsetting_accountSettingId, @RequestBody Kid kids) {
		accountSettingService.saveAccountSettingKids(accountsetting_accountSettingId, kids);
		return kidDAO.findKidByPrimaryKey(kids.getKidId());
	}

	/**
	* Show all Parent entities by AccountSetting
	* 
	*/
	@RequestMapping(value = "/AccountSetting/{accountsetting_accountSettingId}/parents", method = RequestMethod.GET)
	@ResponseBody
	public List<Parent> getAccountSettingParents(@PathVariable Integer accountsetting_accountSettingId) {
		return new java.util.ArrayList<Parent>(accountSettingDAO.findAccountSettingByPrimaryKey(accountsetting_accountSettingId).getParents());
	}

	/**
	* Create a new Parent entity
	* 
	*/
	@RequestMapping(value = "/AccountSetting/{accountsetting_accountSettingId}/parents", method = RequestMethod.POST)
	@ResponseBody
	public Parent newAccountSettingParents(@PathVariable Integer accountsetting_accountSettingId, @RequestBody Parent parent) {
		accountSettingService.saveAccountSettingParents(accountsetting_accountSettingId, parent);
		return parentDAO.findParentByPrimaryKey(parent.getParentId());
	}

	/**
	* Delete an existing AccountSetting entity
	* 
	*/
	@RequestMapping(value = "/AccountSetting/{accountsetting_accountSettingId}", method = RequestMethod.DELETE)
	@ResponseBody
	public void deleteAccountSetting(@PathVariable Integer accountsetting_accountSettingId) {
		AccountSetting accountsetting = accountSettingDAO.findAccountSettingByPrimaryKey(accountsetting_accountSettingId);
		accountSettingService.deleteAccountSetting(accountsetting);
	}

	/**
	* Save an existing Parent entity
	* 
	*/
	@RequestMapping(value = "/AccountSetting/{accountsetting_accountSettingId}/parents", method = RequestMethod.PUT)
	@ResponseBody
	public Parent saveAccountSettingParents(@PathVariable Integer accountsetting_accountSettingId, @RequestBody Parent parents) {
		accountSettingService.saveAccountSettingParents(accountsetting_accountSettingId, parents);
		return parentDAO.findParentByPrimaryKey(parents.getParentId());
	}

	/**
	* View an existing Kid entity
	* 
	*/
	@RequestMapping(value = "/AccountSetting/{accountsetting_accountSettingId}/kids/{kid_kidId}", method = RequestMethod.GET)
	@ResponseBody
	public Kid loadAccountSettingKids(@PathVariable Integer accountsetting_accountSettingId, @PathVariable Integer related_kids_kidId) {
		Kid kid = kidDAO.findKidByPrimaryKey(related_kids_kidId, -1, -1);

		return kid;
	}

	/**
	* Show all AccountSetting entities
	* 
	*/
	@RequestMapping(value = "/AccountSetting", method = RequestMethod.GET)
	@ResponseBody
	public List<AccountSetting> listAccountSettings() {
		return new java.util.ArrayList<AccountSetting>(accountSettingService.loadAccountSettings());
	}

	/**
	* Create a new AccountSetting entity
	* 
	*/
	@RequestMapping(value = "/AccountSetting", method = RequestMethod.POST)
	@ResponseBody
	public AccountSetting newAccountSetting(@RequestBody AccountSetting accountsetting) {
		accountSettingService.saveAccountSetting(accountsetting);
		return accountSettingDAO.findAccountSettingByPrimaryKey(accountsetting.getAccountSettingId());
	}

	/**
	* Create a new Kid entity
	* 
	*/
	@RequestMapping(value = "/AccountSetting/{accountsetting_accountSettingId}/kids", method = RequestMethod.POST)
	@ResponseBody
	public Kid newAccountSettingKids(@PathVariable Integer accountsetting_accountSettingId, @RequestBody Kid kid) {
		accountSettingService.saveAccountSettingKids(accountsetting_accountSettingId, kid);
		return kidDAO.findKidByPrimaryKey(kid.getKidId());
	}

	/**
	* Show all Kid entities by AccountSetting
	* 
	*/
	@RequestMapping(value = "/AccountSetting/{accountsetting_accountSettingId}/kids", method = RequestMethod.GET)
	@ResponseBody
	public List<Kid> getAccountSettingKids(@PathVariable Integer accountsetting_accountSettingId) {
		return new java.util.ArrayList<Kid>(accountSettingDAO.findAccountSettingByPrimaryKey(accountsetting_accountSettingId).getKids());
	}

	/**
	* Delete an existing Kid entity
	* 
	*/
	@RequestMapping(value = "/AccountSetting/{accountsetting_accountSettingId}/kids/{kid_kidId}", method = RequestMethod.DELETE)
	@ResponseBody
	public void deleteAccountSettingKids(@PathVariable Integer accountsetting_accountSettingId, @PathVariable Integer related_kids_kidId) {
		accountSettingService.deleteAccountSettingKids(accountsetting_accountSettingId, related_kids_kidId);
	}

	/**
	* View an existing Parent entity
	* 
	*/
	@RequestMapping(value = "/AccountSetting/{accountsetting_accountSettingId}/parents/{parent_parentId}", method = RequestMethod.GET)
	@ResponseBody
	public Parent loadAccountSettingParents(@PathVariable Integer accountsetting_accountSettingId, @PathVariable Integer related_parents_parentId) {
		Parent parent = parentDAO.findParentByPrimaryKey(related_parents_parentId, -1, -1);

		return parent;
	}
}