package ehealth.controller;

import ehealth.dao.KidDAO;
import ehealth.dao.LeagueDAO;
import ehealth.dao.MatchDAO;
import ehealth.dao.TeamDAO;

import ehealth.domain.Kid;
import ehealth.domain.League;
import ehealth.domain.Match;
import ehealth.domain.Team;

import ehealth.service.TeamService;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Controller;

import org.springframework.web.bind.WebDataBinder;

import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Spring Rest controller that handles CRUD requests for Team entities
 * 
 */

@Controller("TeamRestController")

public class TeamRestController {

	/**
	 * DAO injected by Spring that manages Kid entities
	 * 
	 */
	@Autowired
	private KidDAO kidDAO;

	/**
	 * DAO injected by Spring that manages League entities
	 * 
	 */
	@Autowired
	private LeagueDAO leagueDAO;

	/**
	 * DAO injected by Spring that manages Match entities
	 * 
	 */
	@Autowired
	private MatchDAO matchDAO;

	/**
	 * DAO injected by Spring that manages Team entities
	 * 
	 */
	@Autowired
	private TeamDAO teamDAO;

	/**
	 * Service injected by Spring that provides CRUD operations for Team entities
	 * 
	 */
	@Autowired
	private TeamService teamService;

	/**
	 * Show all Kid entities by Team
	 * 
	 */
	@RequestMapping(value = "/Team/{team_teamId}/kids", method = RequestMethod.GET)
	@ResponseBody
	public List<Kid> getTeamKids(@PathVariable Integer team_teamId) {
		return new java.util.ArrayList<Kid>(teamDAO.findTeamByPrimaryKey(team_teamId).getKids());
	}

	/**
	* Delete an existing Team entity
	* 
	*/
	@RequestMapping(value = "/Team/{team_teamId}", method = RequestMethod.DELETE)
	@ResponseBody
	public void deleteTeam(@PathVariable Integer team_teamId) {
		Team team = teamDAO.findTeamByPrimaryKey(team_teamId);
		teamService.deleteTeam(team);
	}

	/**
	* Delete an existing Match entity
	* 
	*/
	@RequestMapping(value = "/Team/{team_teamId}/matchsForTeamId1/{match_matchId}", method = RequestMethod.DELETE)
	@ResponseBody
	public void deleteTeamMatchsForTeamId1(@PathVariable Integer team_teamId, @PathVariable Integer related_matchsforteamid1_matchId) {
		teamService.deleteTeamMatchsForTeamId1(team_teamId, related_matchsforteamid1_matchId);
	}

	/**
	* Delete an existing Match entity
	* 
	*/
	@RequestMapping(value = "/Team/{team_teamId}/matchsForTeamIdWin/{match_matchId}", method = RequestMethod.DELETE)
	@ResponseBody
	public void deleteTeamMatchsForTeamIdWin(@PathVariable Integer team_teamId, @PathVariable Integer related_matchsforteamidwin_matchId) {
		teamService.deleteTeamMatchsForTeamIdWin(team_teamId, related_matchsforteamidwin_matchId);
	}

	/**
	* Delete an existing League entity
	* 
	*/
	@RequestMapping(value = "/Team/{team_teamId}/leagues/{league_leagueId}", method = RequestMethod.DELETE)
	@ResponseBody
	public void deleteTeamLeagues(@PathVariable Integer team_teamId, @PathVariable Integer related_leagues_leagueId) {
		teamService.deleteTeamLeagues(team_teamId, related_leagues_leagueId);
	}

	/**
	* View an existing Match entity
	* 
	*/
	@RequestMapping(value = "/Team/{team_teamId}/matchsForTeamId2/{match_matchId}", method = RequestMethod.GET)
	@ResponseBody
	public Match loadTeamMatchsForTeamId2(@PathVariable Integer team_teamId, @PathVariable Integer related_matchsforteamid2_matchId) {
		Match match = matchDAO.findMatchByPrimaryKey(related_matchsforteamid2_matchId, -1, -1);

		return match;
	}

	/**
	* Save an existing League entity
	* 
	*/
	@RequestMapping(value = "/Team/{team_teamId}/leagues", method = RequestMethod.PUT)
	@ResponseBody
	public League saveTeamLeagues(@PathVariable Integer team_teamId, @RequestBody League leagues) {
		teamService.saveTeamLeagues(team_teamId, leagues);
		return leagueDAO.findLeagueByPrimaryKey(leagues.getLeagueId());
	}

	/**
	* Show all League entities by Team
	* 
	*/
	@RequestMapping(value = "/Team/{team_teamId}/leagues", method = RequestMethod.GET)
	@ResponseBody
	public List<League> getTeamLeagues(@PathVariable Integer team_teamId) {
		return new java.util.ArrayList<League>(teamDAO.findTeamByPrimaryKey(team_teamId).getLeagues());
	}

	/**
	* Create a new Team entity
	* 
	*/
	@RequestMapping(value = "/Team", method = RequestMethod.POST)
	@ResponseBody
	public Team newTeam(@RequestBody Team team) {
		teamService.saveTeam(team);
		return teamDAO.findTeamByPrimaryKey(team.getTeamId());
	}

	/**
	* Create a new Match entity
	* 
	*/
	@RequestMapping(value = "/Team/{team_teamId}/matchsForTeamId2", method = RequestMethod.POST)
	@ResponseBody
	public Match newTeamMatchsForTeamId2(@PathVariable Integer team_teamId, @RequestBody Match match) {
		teamService.saveTeamMatchsForTeamId2(team_teamId, match);
		return matchDAO.findMatchByPrimaryKey(match.getMatchId());
	}

	/**
	* Show all Match entities by Team
	* 
	*/
	@RequestMapping(value = "/Team/{team_teamId}/matchsForTeamIdWin", method = RequestMethod.GET)
	@ResponseBody
	public List<Match> getTeamMatchsForTeamIdWin(@PathVariable Integer team_teamId) {
		return new java.util.ArrayList<Match>(teamDAO.findTeamByPrimaryKey(team_teamId).getMatchsForTeamIdWin());
	}

	/**
	* View an existing Kid entity
	* 
	*/
	@RequestMapping(value = "/Team/{team_teamId}/kids/{kid_kidId}", method = RequestMethod.GET)
	@ResponseBody
	public Kid loadTeamKids(@PathVariable Integer team_teamId, @PathVariable Integer related_kids_kidId) {
		Kid kid = kidDAO.findKidByPrimaryKey(related_kids_kidId, -1, -1);

		return kid;
	}

	/**
	* Create a new Match entity
	* 
	*/
	@RequestMapping(value = "/Team/{team_teamId}/matchsForTeamIdWin", method = RequestMethod.POST)
	@ResponseBody
	public Match newTeamMatchsForTeamIdWin(@PathVariable Integer team_teamId, @RequestBody Match match) {
		teamService.saveTeamMatchsForTeamIdWin(team_teamId, match);
		return matchDAO.findMatchByPrimaryKey(match.getMatchId());
	}

	/**
	* View an existing Match entity
	* 
	*/
	@RequestMapping(value = "/Team/{team_teamId}/matchsForTeamId1/{match_matchId}", method = RequestMethod.GET)
	@ResponseBody
	public Match loadTeamMatchsForTeamId1(@PathVariable Integer team_teamId, @PathVariable Integer related_matchsforteamid1_matchId) {
		Match match = matchDAO.findMatchByPrimaryKey(related_matchsforteamid1_matchId, -1, -1);

		return match;
	}

	/**
	* Show all Match entities by Team
	* 
	*/
	@RequestMapping(value = "/Team/{team_teamId}/matchsForTeamId2", method = RequestMethod.GET)
	@ResponseBody
	public List<Match> getTeamMatchsForTeamId2(@PathVariable Integer team_teamId) {
		return new java.util.ArrayList<Match>(teamDAO.findTeamByPrimaryKey(team_teamId).getMatchsForTeamId2());
	}

	/**
	* Show all Team entities
	* 
	*/
	@RequestMapping(value = "/Team", method = RequestMethod.GET)
	@ResponseBody
	public List<Team> listTeams() {
		return new java.util.ArrayList<Team>(teamService.loadTeams());
	}

	/**
	* Save an existing Team entity
	* 
	*/
	@RequestMapping(value = "/Team", method = RequestMethod.PUT)
	@ResponseBody
	public Team saveTeam(@RequestBody Team team) {
		teamService.saveTeam(team);
		return teamDAO.findTeamByPrimaryKey(team.getTeamId());
	}

	/**
	* Create a new Match entity
	* 
	*/
	@RequestMapping(value = "/Team/{team_teamId}/matchsForTeamId1", method = RequestMethod.POST)
	@ResponseBody
	public Match newTeamMatchsForTeamId1(@PathVariable Integer team_teamId, @RequestBody Match match) {
		teamService.saveTeamMatchsForTeamId1(team_teamId, match);
		return matchDAO.findMatchByPrimaryKey(match.getMatchId());
	}

	/**
	* Save an existing Kid entity
	* 
	*/
	@RequestMapping(value = "/Team/{team_teamId}/kids", method = RequestMethod.PUT)
	@ResponseBody
	public Kid saveTeamKids(@PathVariable Integer team_teamId, @RequestBody Kid kids) {
		teamService.saveTeamKids(team_teamId, kids);
		return kidDAO.findKidByPrimaryKey(kids.getKidId());
	}

	/**
	* Create a new Kid entity
	* 
	*/
	@RequestMapping(value = "/Team/{team_teamId}/kids", method = RequestMethod.POST)
	@ResponseBody
	public Kid newTeamKids(@PathVariable Integer team_teamId, @RequestBody Kid kid) {
		teamService.saveTeamKids(team_teamId, kid);
		return kidDAO.findKidByPrimaryKey(kid.getKidId());
	}

	/**
	* Show all Match entities by Team
	* 
	*/
	@RequestMapping(value = "/Team/{team_teamId}/matchsForTeamId1", method = RequestMethod.GET)
	@ResponseBody
	public List<Match> getTeamMatchsForTeamId1(@PathVariable Integer team_teamId) {
		return new java.util.ArrayList<Match>(teamDAO.findTeamByPrimaryKey(team_teamId).getMatchsForTeamId1());
	}

	/**
	* Create a new League entity
	* 
	*/
	@RequestMapping(value = "/Team/{team_teamId}/leagues", method = RequestMethod.POST)
	@ResponseBody
	public League newTeamLeagues(@PathVariable Integer team_teamId, @RequestBody League league) {
		teamService.saveTeamLeagues(team_teamId, league);
		return leagueDAO.findLeagueByPrimaryKey(league.getLeagueId());
	}

	/**
	* Delete an existing Kid entity
	* 
	*/
	@RequestMapping(value = "/Team/{team_teamId}/kids/{kid_kidId}", method = RequestMethod.DELETE)
	@ResponseBody
	public void deleteTeamKids(@PathVariable Integer team_teamId, @PathVariable Integer related_kids_kidId) {
		teamService.deleteTeamKids(team_teamId, related_kids_kidId);
	}

	/**
	* View an existing Match entity
	* 
	*/
	@RequestMapping(value = "/Team/{team_teamId}/matchsForTeamIdWin/{match_matchId}", method = RequestMethod.GET)
	@ResponseBody
	public Match loadTeamMatchsForTeamIdWin(@PathVariable Integer team_teamId, @PathVariable Integer related_matchsforteamidwin_matchId) {
		Match match = matchDAO.findMatchByPrimaryKey(related_matchsforteamidwin_matchId, -1, -1);

		return match;
	}

	/**
	* Save an existing Match entity
	* 
	*/
	@RequestMapping(value = "/Team/{team_teamId}/matchsForTeamId1", method = RequestMethod.PUT)
	@ResponseBody
	public Match saveTeamMatchsForTeamId1(@PathVariable Integer team_teamId, @RequestBody Match matchsforteamid1) {
		teamService.saveTeamMatchsForTeamId1(team_teamId, matchsforteamid1);
		return matchDAO.findMatchByPrimaryKey(matchsforteamid1.getMatchId());
	}

	/**
	* Register custom, context-specific property editors
	* 
	*/
	@InitBinder
	public void initBinder(WebDataBinder binder, HttpServletRequest request) { // Register static property editors.
		binder.registerCustomEditor(java.util.Calendar.class, new org.skyway.spring.util.databinding.CustomCalendarEditor());
		binder.registerCustomEditor(byte[].class, new org.springframework.web.multipart.support.ByteArrayMultipartFileEditor());
		binder.registerCustomEditor(boolean.class, new org.skyway.spring.util.databinding.EnhancedBooleanEditor(false));
		binder.registerCustomEditor(Boolean.class, new org.skyway.spring.util.databinding.EnhancedBooleanEditor(true));
		binder.registerCustomEditor(java.math.BigDecimal.class, new org.skyway.spring.util.databinding.NaNHandlingNumberEditor(java.math.BigDecimal.class, true));
		binder.registerCustomEditor(Integer.class, new org.skyway.spring.util.databinding.NaNHandlingNumberEditor(Integer.class, true));
		binder.registerCustomEditor(java.util.Date.class, new org.skyway.spring.util.databinding.CustomDateEditor());
		binder.registerCustomEditor(String.class, new org.skyway.spring.util.databinding.StringEditor());
		binder.registerCustomEditor(Long.class, new org.skyway.spring.util.databinding.NaNHandlingNumberEditor(Long.class, true));
		binder.registerCustomEditor(Double.class, new org.skyway.spring.util.databinding.NaNHandlingNumberEditor(Double.class, true));
	}

	/**
	* Select an existing Team entity
	* 
	*/
	@RequestMapping(value = "/Team/{team_teamId}", method = RequestMethod.GET)
	@ResponseBody
	public Team loadTeam(@PathVariable Integer team_teamId) {
		return teamDAO.findTeamByPrimaryKey(team_teamId);
	}

	/**
	* Save an existing Match entity
	* 
	*/
	@RequestMapping(value = "/Team/{team_teamId}/matchsForTeamId2", method = RequestMethod.PUT)
	@ResponseBody
	public Match saveTeamMatchsForTeamId2(@PathVariable Integer team_teamId, @RequestBody Match matchsforteamid2) {
		teamService.saveTeamMatchsForTeamId2(team_teamId, matchsforteamid2);
		return matchDAO.findMatchByPrimaryKey(matchsforteamid2.getMatchId());
	}

	/**
	* Delete an existing Match entity
	* 
	*/
	@RequestMapping(value = "/Team/{team_teamId}/matchsForTeamId2/{match_matchId}", method = RequestMethod.DELETE)
	@ResponseBody
	public void deleteTeamMatchsForTeamId2(@PathVariable Integer team_teamId, @PathVariable Integer related_matchsforteamid2_matchId) {
		teamService.deleteTeamMatchsForTeamId2(team_teamId, related_matchsforteamid2_matchId);
	}

	/**
	* View an existing League entity
	* 
	*/
	@RequestMapping(value = "/Team/{team_teamId}/leagues/{league_leagueId}", method = RequestMethod.GET)
	@ResponseBody
	public League loadTeamLeagues(@PathVariable Integer team_teamId, @PathVariable Integer related_leagues_leagueId) {
		League league = leagueDAO.findLeagueByPrimaryKey(related_leagues_leagueId, -1, -1);

		return league;
	}

	/**
	* Save an existing Match entity
	* 
	*/
	@RequestMapping(value = "/Team/{team_teamId}/matchsForTeamIdWin", method = RequestMethod.PUT)
	@ResponseBody
	public Match saveTeamMatchsForTeamIdWin(@PathVariable Integer team_teamId, @RequestBody Match matchsforteamidwin) {
		teamService.saveTeamMatchsForTeamIdWin(team_teamId, matchsforteamidwin);
		return matchDAO.findMatchByPrimaryKey(matchsforteamidwin.getMatchId());
	}
}