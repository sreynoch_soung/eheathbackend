package ehealth.controller;

import ehealth.dao.CityDAO;
import ehealth.dao.CountryDAO;

import ehealth.domain.City;
import ehealth.domain.Country;

import ehealth.service.CountryService;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Controller;

import org.springframework.web.bind.WebDataBinder;

import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Spring Rest controller that handles CRUD requests for Country entities
 * 
 */

@Controller("CountryRestController")

public class CountryRestController {

	/**
	 * DAO injected by Spring that manages City entities
	 * 
	 */
	@Autowired
	private CityDAO cityDAO;

	/**
	 * DAO injected by Spring that manages Country entities
	 * 
	 */
	@Autowired
	private CountryDAO countryDAO;

	/**
	 * Service injected by Spring that provides CRUD operations for Country entities
	 * 
	 */
	@Autowired
	private CountryService countryService;

	/**
	 * Create a new Country entity
	 * 
	 */
	@RequestMapping(value = "/Country", method = RequestMethod.POST)
	@ResponseBody
	public Country newCountry(@RequestBody Country country) {
		countryService.saveCountry(country);
		return countryDAO.findCountryByPrimaryKey(country.getCountryId());
	}

	/**
	* Delete an existing City entity
	* 
	*/
	@RequestMapping(value = "/Country/{country_countryId}/cities/{city_cityId}", method = RequestMethod.DELETE)
	@ResponseBody
	public void deleteCountryCities(@PathVariable Integer country_countryId, @PathVariable Integer related_cities_cityId) {
		countryService.deleteCountryCities(country_countryId, related_cities_cityId);
	}

	/**
	* Show all City entities by Country
	* 
	*/
	@RequestMapping(value = "/Country/{country_countryId}/cities", method = RequestMethod.GET)
	@ResponseBody
	public List<City> getCountryCities(@PathVariable Integer country_countryId) {
		return new java.util.ArrayList<City>(countryDAO.findCountryByPrimaryKey(country_countryId).getCities());
	}

	/**
	* View an existing City entity
	* 
	*/
	@RequestMapping(value = "/Country/{country_countryId}/cities/{city_cityId}", method = RequestMethod.GET)
	@ResponseBody
	public City loadCountryCities(@PathVariable Integer country_countryId, @PathVariable Integer related_cities_cityId) {
		City city = cityDAO.findCityByPrimaryKey(related_cities_cityId, -1, -1);

		return city;
	}

	/**
	* Save an existing City entity
	* 
	*/
	@RequestMapping(value = "/Country/{country_countryId}/cities", method = RequestMethod.PUT)
	@ResponseBody
	public City saveCountryCities(@PathVariable Integer country_countryId, @RequestBody City cities) {
		countryService.saveCountryCities(country_countryId, cities);
		return cityDAO.findCityByPrimaryKey(cities.getCityId());
	}

	/**
	* Show all Country entities
	* 
	*/
	@RequestMapping(value = "/Country", method = RequestMethod.GET)
	@ResponseBody
	public List<Country> listCountrys() {
		return new java.util.ArrayList<Country>(countryService.loadCountrys());
	}

	/**
	* Create a new City entity
	* 
	*/
	@RequestMapping(value = "/Country/{country_countryId}/cities", method = RequestMethod.POST)
	@ResponseBody
	public City newCountryCities(@PathVariable Integer country_countryId, @RequestBody City city) {
		countryService.saveCountryCities(country_countryId, city);
		return cityDAO.findCityByPrimaryKey(city.getCityId());
	}

	/**
	* Register custom, context-specific property editors
	* 
	*/
	@InitBinder
	public void initBinder(WebDataBinder binder, HttpServletRequest request) { // Register static property editors.
		binder.registerCustomEditor(java.util.Calendar.class, new org.skyway.spring.util.databinding.CustomCalendarEditor());
		binder.registerCustomEditor(byte[].class, new org.springframework.web.multipart.support.ByteArrayMultipartFileEditor());
		binder.registerCustomEditor(boolean.class, new org.skyway.spring.util.databinding.EnhancedBooleanEditor(false));
		binder.registerCustomEditor(Boolean.class, new org.skyway.spring.util.databinding.EnhancedBooleanEditor(true));
		binder.registerCustomEditor(java.math.BigDecimal.class, new org.skyway.spring.util.databinding.NaNHandlingNumberEditor(java.math.BigDecimal.class, true));
		binder.registerCustomEditor(Integer.class, new org.skyway.spring.util.databinding.NaNHandlingNumberEditor(Integer.class, true));
		binder.registerCustomEditor(java.util.Date.class, new org.skyway.spring.util.databinding.CustomDateEditor());
		binder.registerCustomEditor(String.class, new org.skyway.spring.util.databinding.StringEditor());
		binder.registerCustomEditor(Long.class, new org.skyway.spring.util.databinding.NaNHandlingNumberEditor(Long.class, true));
		binder.registerCustomEditor(Double.class, new org.skyway.spring.util.databinding.NaNHandlingNumberEditor(Double.class, true));
	}

	/**
	* Select an existing Country entity
	* 
	*/
	@RequestMapping(value = "/Country/{country_countryId}", method = RequestMethod.GET)
	@ResponseBody
	public Country loadCountry(@PathVariable Integer country_countryId) {
		return countryDAO.findCountryByPrimaryKey(country_countryId);
	}

	/**
	* Delete an existing Country entity
	* 
	*/
	@RequestMapping(value = "/Country/{country_countryId}", method = RequestMethod.DELETE)
	@ResponseBody
	public void deleteCountry(@PathVariable Integer country_countryId) {
		Country country = countryDAO.findCountryByPrimaryKey(country_countryId);
		countryService.deleteCountry(country);
	}

	/**
	* Save an existing Country entity
	* 
	*/
	@RequestMapping(value = "/Country", method = RequestMethod.PUT)
	@ResponseBody
	public Country saveCountry(@RequestBody Country country) {
		countryService.saveCountry(country);
		return countryDAO.findCountryByPrimaryKey(country.getCountryId());
	}
}