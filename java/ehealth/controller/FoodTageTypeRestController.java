package ehealth.controller;

import ehealth.dao.FoodTageInputDAO;
import ehealth.dao.FoodTageTypeDAO;
import ehealth.dao.NutritionTargetDAO;

import ehealth.domain.FoodTageInput;
import ehealth.domain.FoodTageType;
import ehealth.domain.NutritionTarget;

import ehealth.service.FoodTageTypeService;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Controller;

import org.springframework.web.bind.WebDataBinder;

import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Spring Rest controller that handles CRUD requests for FoodTageType entities
 * 
 */

@Controller("FoodTageTypeRestController")

public class FoodTageTypeRestController {

	/**
	 * DAO injected by Spring that manages FoodTageInput entities
	 * 
	 */
	@Autowired
	private FoodTageInputDAO foodTageInputDAO;

	/**
	 * DAO injected by Spring that manages FoodTageType entities
	 * 
	 */
	@Autowired
	private FoodTageTypeDAO foodTageTypeDAO;

	/**
	 * DAO injected by Spring that manages NutritionTarget entities
	 * 
	 */
	@Autowired
	private NutritionTargetDAO nutritionTargetDAO;

	/**
	 * Service injected by Spring that provides CRUD operations for FoodTageType entities
	 * 
	 */
	@Autowired
	private FoodTageTypeService foodTageTypeService;

	/**
	 * Show all NutritionTarget entities by FoodTageType
	 * 
	 */
	@RequestMapping(value = "/FoodTageType/{foodtagetype_foodTageTypeId}/nutritionTargets", method = RequestMethod.GET)
	@ResponseBody
	public List<NutritionTarget> getFoodTageTypeNutritionTargets(@PathVariable Integer foodtagetype_foodTageTypeId) {
		return new java.util.ArrayList<NutritionTarget>(foodTageTypeDAO.findFoodTageTypeByPrimaryKey(foodtagetype_foodTageTypeId).getNutritionTargets());
	}

	/**
	* Show all FoodTageInput entities by FoodTageType
	* 
	*/
	@RequestMapping(value = "/FoodTageType/{foodtagetype_foodTageTypeId}/foodTageInputs", method = RequestMethod.GET)
	@ResponseBody
	public List<FoodTageInput> getFoodTageTypeFoodTageInputs(@PathVariable Integer foodtagetype_foodTageTypeId) {
		return new java.util.ArrayList<FoodTageInput>(foodTageTypeDAO.findFoodTageTypeByPrimaryKey(foodtagetype_foodTageTypeId).getFoodTageInputs());
	}

	/**
	* View an existing FoodTageInput entity
	* 
	*/
	@RequestMapping(value = "/FoodTageType/{foodtagetype_foodTageTypeId}/foodTageInputs/{foodtageinput_foodTageInputId}", method = RequestMethod.GET)
	@ResponseBody
	public FoodTageInput loadFoodTageTypeFoodTageInputs(@PathVariable Integer foodtagetype_foodTageTypeId, @PathVariable Integer related_foodtageinputs_foodTageInputId) {
		FoodTageInput foodtageinput = foodTageInputDAO.findFoodTageInputByPrimaryKey(related_foodtageinputs_foodTageInputId, -1, -1);

		return foodtageinput;
	}

	/**
	* Register custom, context-specific property editors
	* 
	*/
	@InitBinder
	public void initBinder(WebDataBinder binder, HttpServletRequest request) { // Register static property editors.
		binder.registerCustomEditor(java.util.Calendar.class, new org.skyway.spring.util.databinding.CustomCalendarEditor());
		binder.registerCustomEditor(byte[].class, new org.springframework.web.multipart.support.ByteArrayMultipartFileEditor());
		binder.registerCustomEditor(boolean.class, new org.skyway.spring.util.databinding.EnhancedBooleanEditor(false));
		binder.registerCustomEditor(Boolean.class, new org.skyway.spring.util.databinding.EnhancedBooleanEditor(true));
		binder.registerCustomEditor(java.math.BigDecimal.class, new org.skyway.spring.util.databinding.NaNHandlingNumberEditor(java.math.BigDecimal.class, true));
		binder.registerCustomEditor(Integer.class, new org.skyway.spring.util.databinding.NaNHandlingNumberEditor(Integer.class, true));
		binder.registerCustomEditor(java.util.Date.class, new org.skyway.spring.util.databinding.CustomDateEditor());
		binder.registerCustomEditor(String.class, new org.skyway.spring.util.databinding.StringEditor());
		binder.registerCustomEditor(Long.class, new org.skyway.spring.util.databinding.NaNHandlingNumberEditor(Long.class, true));
		binder.registerCustomEditor(Double.class, new org.skyway.spring.util.databinding.NaNHandlingNumberEditor(Double.class, true));
	}

	/**
	* Select an existing FoodTageType entity
	* 
	*/
	@RequestMapping(value = "/FoodTageType/{foodtagetype_foodTageTypeId}", method = RequestMethod.GET)
	@ResponseBody
	public FoodTageType loadFoodTageType(@PathVariable Integer foodtagetype_foodTageTypeId) {
		return foodTageTypeDAO.findFoodTageTypeByPrimaryKey(foodtagetype_foodTageTypeId);
	}

	/**
	* Create a new FoodTageInput entity
	* 
	*/
	@RequestMapping(value = "/FoodTageType/{foodtagetype_foodTageTypeId}/foodTageInputs", method = RequestMethod.POST)
	@ResponseBody
	public FoodTageInput newFoodTageTypeFoodTageInputs(@PathVariable Integer foodtagetype_foodTageTypeId, @RequestBody FoodTageInput foodtageinput) {
		foodTageTypeService.saveFoodTageTypeFoodTageInputs(foodtagetype_foodTageTypeId, foodtageinput);
		return foodTageInputDAO.findFoodTageInputByPrimaryKey(foodtageinput.getFoodTageInputId());
	}

	/**
	* Save an existing FoodTageInput entity
	* 
	*/
	@RequestMapping(value = "/FoodTageType/{foodtagetype_foodTageTypeId}/foodTageInputs", method = RequestMethod.PUT)
	@ResponseBody
	public FoodTageInput saveFoodTageTypeFoodTageInputs(@PathVariable Integer foodtagetype_foodTageTypeId, @RequestBody FoodTageInput foodtageinputs) {
		foodTageTypeService.saveFoodTageTypeFoodTageInputs(foodtagetype_foodTageTypeId, foodtageinputs);
		return foodTageInputDAO.findFoodTageInputByPrimaryKey(foodtageinputs.getFoodTageInputId());
	}

	/**
	* Delete an existing NutritionTarget entity
	* 
	*/
	@RequestMapping(value = "/FoodTageType/{foodtagetype_foodTageTypeId}/nutritionTargets/{nutritiontarget_nutritionTargetId}", method = RequestMethod.DELETE)
	@ResponseBody
	public void deleteFoodTageTypeNutritionTargets(@PathVariable Integer foodtagetype_foodTageTypeId, @PathVariable Integer related_nutritiontargets_nutritionTargetId) {
		foodTageTypeService.deleteFoodTageTypeNutritionTargets(foodtagetype_foodTageTypeId, related_nutritiontargets_nutritionTargetId);
	}

	/**
	* Delete an existing FoodTageType entity
	* 
	*/
	@RequestMapping(value = "/FoodTageType/{foodtagetype_foodTageTypeId}", method = RequestMethod.DELETE)
	@ResponseBody
	public void deleteFoodTageType(@PathVariable Integer foodtagetype_foodTageTypeId) {
		FoodTageType foodtagetype = foodTageTypeDAO.findFoodTageTypeByPrimaryKey(foodtagetype_foodTageTypeId);
		foodTageTypeService.deleteFoodTageType(foodtagetype);
	}

	/**
	* View an existing NutritionTarget entity
	* 
	*/
	@RequestMapping(value = "/FoodTageType/{foodtagetype_foodTageTypeId}/nutritionTargets/{nutritiontarget_nutritionTargetId}", method = RequestMethod.GET)
	@ResponseBody
	public NutritionTarget loadFoodTageTypeNutritionTargets(@PathVariable Integer foodtagetype_foodTageTypeId, @PathVariable Integer related_nutritiontargets_nutritionTargetId) {
		NutritionTarget nutritiontarget = nutritionTargetDAO.findNutritionTargetByPrimaryKey(related_nutritiontargets_nutritionTargetId, -1, -1);

		return nutritiontarget;
	}

	/**
	* Save an existing FoodTageType entity
	* 
	*/
	@RequestMapping(value = "/FoodTageType", method = RequestMethod.PUT)
	@ResponseBody
	public FoodTageType saveFoodTageType(@RequestBody FoodTageType foodtagetype) {
		foodTageTypeService.saveFoodTageType(foodtagetype);
		return foodTageTypeDAO.findFoodTageTypeByPrimaryKey(foodtagetype.getFoodTageTypeId());
	}

	/**
	* Create a new FoodTageType entity
	* 
	*/
	@RequestMapping(value = "/FoodTageType", method = RequestMethod.POST)
	@ResponseBody
	public FoodTageType newFoodTageType(@RequestBody FoodTageType foodtagetype) {
		foodTageTypeService.saveFoodTageType(foodtagetype);
		return foodTageTypeDAO.findFoodTageTypeByPrimaryKey(foodtagetype.getFoodTageTypeId());
	}

	/**
	* Save an existing NutritionTarget entity
	* 
	*/
	@RequestMapping(value = "/FoodTageType/{foodtagetype_foodTageTypeId}/nutritionTargets", method = RequestMethod.PUT)
	@ResponseBody
	public NutritionTarget saveFoodTageTypeNutritionTargets(@PathVariable Integer foodtagetype_foodTageTypeId, @RequestBody NutritionTarget nutritiontargets) {
		foodTageTypeService.saveFoodTageTypeNutritionTargets(foodtagetype_foodTageTypeId, nutritiontargets);
		return nutritionTargetDAO.findNutritionTargetByPrimaryKey(nutritiontargets.getNutritionTargetId());
	}

	/**
	* Show all FoodTageType entities
	* 
	*/
	@RequestMapping(value = "/FoodTageType", method = RequestMethod.GET)
	@ResponseBody
	public List<FoodTageType> listFoodTageTypes() {
		return new java.util.ArrayList<FoodTageType>(foodTageTypeService.loadFoodTageTypes());
	}

	/**
	* Delete an existing FoodTageInput entity
	* 
	*/
	@RequestMapping(value = "/FoodTageType/{foodtagetype_foodTageTypeId}/foodTageInputs/{foodtageinput_foodTageInputId}", method = RequestMethod.DELETE)
	@ResponseBody
	public void deleteFoodTageTypeFoodTageInputs(@PathVariable Integer foodtagetype_foodTageTypeId, @PathVariable Integer related_foodtageinputs_foodTageInputId) {
		foodTageTypeService.deleteFoodTageTypeFoodTageInputs(foodtagetype_foodTageTypeId, related_foodtageinputs_foodTageInputId);
	}

	/**
	* Create a new NutritionTarget entity
	* 
	*/
	@RequestMapping(value = "/FoodTageType/{foodtagetype_foodTageTypeId}/nutritionTargets", method = RequestMethod.POST)
	@ResponseBody
	public NutritionTarget newFoodTageTypeNutritionTargets(@PathVariable Integer foodtagetype_foodTageTypeId, @RequestBody NutritionTarget nutritiontarget) {
		foodTageTypeService.saveFoodTageTypeNutritionTargets(foodtagetype_foodTageTypeId, nutritiontarget);
		return nutritionTargetDAO.findNutritionTargetByPrimaryKey(nutritiontarget.getNutritionTargetId());
	}
}