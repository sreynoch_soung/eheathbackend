package ehealth.controller;

import ehealth.dao.ExerciseInputDAO;
import ehealth.dao.ExerciseTypeDAO;

import ehealth.domain.ExerciseInput;
import ehealth.domain.ExerciseType;

import ehealth.service.ExerciseTypeService;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Controller;

import org.springframework.web.bind.WebDataBinder;

import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Spring Rest controller that handles CRUD requests for ExerciseType entities
 * 
 */

@Controller("ExerciseTypeRestController")

public class ExerciseTypeRestController {

	/**
	 * DAO injected by Spring that manages ExerciseInput entities
	 * 
	 */
	@Autowired
	private ExerciseInputDAO exerciseInputDAO;

	/**
	 * DAO injected by Spring that manages ExerciseType entities
	 * 
	 */
	@Autowired
	private ExerciseTypeDAO exerciseTypeDAO;

	/**
	 * Service injected by Spring that provides CRUD operations for ExerciseType entities
	 * 
	 */
	@Autowired
	private ExerciseTypeService exerciseTypeService;

	/**
	 * Register custom, context-specific property editors
	 * 
	 */
	@InitBinder
	public void initBinder(WebDataBinder binder, HttpServletRequest request) { // Register static property editors.
		binder.registerCustomEditor(java.util.Calendar.class, new org.skyway.spring.util.databinding.CustomCalendarEditor());
		binder.registerCustomEditor(byte[].class, new org.springframework.web.multipart.support.ByteArrayMultipartFileEditor());
		binder.registerCustomEditor(boolean.class, new org.skyway.spring.util.databinding.EnhancedBooleanEditor(false));
		binder.registerCustomEditor(Boolean.class, new org.skyway.spring.util.databinding.EnhancedBooleanEditor(true));
		binder.registerCustomEditor(java.math.BigDecimal.class, new org.skyway.spring.util.databinding.NaNHandlingNumberEditor(java.math.BigDecimal.class, true));
		binder.registerCustomEditor(Integer.class, new org.skyway.spring.util.databinding.NaNHandlingNumberEditor(Integer.class, true));
		binder.registerCustomEditor(java.util.Date.class, new org.skyway.spring.util.databinding.CustomDateEditor());
		binder.registerCustomEditor(String.class, new org.skyway.spring.util.databinding.StringEditor());
		binder.registerCustomEditor(Long.class, new org.skyway.spring.util.databinding.NaNHandlingNumberEditor(Long.class, true));
		binder.registerCustomEditor(Double.class, new org.skyway.spring.util.databinding.NaNHandlingNumberEditor(Double.class, true));
	}

	/**
	* Delete an existing ExerciseInput entity
	* 
	*/
	@RequestMapping(value = "/ExerciseType/{exercisetype_exerciseTypeId}/exerciseInputs/{exerciseinput_exerciseInputId}", method = RequestMethod.DELETE)
	@ResponseBody
	public void deleteExerciseTypeExerciseInputs(@PathVariable Integer exercisetype_exerciseTypeId, @PathVariable Integer related_exerciseinputs_exerciseInputId) {
		exerciseTypeService.deleteExerciseTypeExerciseInputs(exercisetype_exerciseTypeId, related_exerciseinputs_exerciseInputId);
	}

	/**
	* Create a new ExerciseType entity
	* 
	*/
	@RequestMapping(value = "/ExerciseType", method = RequestMethod.POST)
	@ResponseBody
	public ExerciseType newExerciseType(@RequestBody ExerciseType exercisetype) {
		exerciseTypeService.saveExerciseType(exercisetype);
		return exerciseTypeDAO.findExerciseTypeByPrimaryKey(exercisetype.getExerciseTypeId());
	}

	/**
	* Show all ExerciseType entities
	* 
	*/
	@RequestMapping(value = "/ExerciseType", method = RequestMethod.GET)
	@ResponseBody
	public List<ExerciseType> listExerciseTypes() {
		return new java.util.ArrayList<ExerciseType>(exerciseTypeService.loadExerciseTypes());
	}

	/**
	* Save an existing ExerciseType entity
	* 
	*/
	@RequestMapping(value = "/ExerciseType", method = RequestMethod.PUT)
	@ResponseBody
	public ExerciseType saveExerciseType(@RequestBody ExerciseType exercisetype) {
		exerciseTypeService.saveExerciseType(exercisetype);
		return exerciseTypeDAO.findExerciseTypeByPrimaryKey(exercisetype.getExerciseTypeId());
	}

	/**
	* Save an existing ExerciseInput entity
	* 
	*/
	@RequestMapping(value = "/ExerciseType/{exercisetype_exerciseTypeId}/exerciseInputs", method = RequestMethod.PUT)
	@ResponseBody
	public ExerciseInput saveExerciseTypeExerciseInputs(@PathVariable Integer exercisetype_exerciseTypeId, @RequestBody ExerciseInput exerciseinputs) {
		exerciseTypeService.saveExerciseTypeExerciseInputs(exercisetype_exerciseTypeId, exerciseinputs);
		return exerciseInputDAO.findExerciseInputByPrimaryKey(exerciseinputs.getExerciseInputId());
	}

	/**
	* Delete an existing ExerciseType entity
	* 
	*/
	@RequestMapping(value = "/ExerciseType/{exercisetype_exerciseTypeId}", method = RequestMethod.DELETE)
	@ResponseBody
	public void deleteExerciseType(@PathVariable Integer exercisetype_exerciseTypeId) {
		ExerciseType exercisetype = exerciseTypeDAO.findExerciseTypeByPrimaryKey(exercisetype_exerciseTypeId);
		exerciseTypeService.deleteExerciseType(exercisetype);
	}

	/**
	* Select an existing ExerciseType entity
	* 
	*/
	@RequestMapping(value = "/ExerciseType/{exercisetype_exerciseTypeId}", method = RequestMethod.GET)
	@ResponseBody
	public ExerciseType loadExerciseType(@PathVariable Integer exercisetype_exerciseTypeId) {
		return exerciseTypeDAO.findExerciseTypeByPrimaryKey(exercisetype_exerciseTypeId);
	}

	/**
	* View an existing ExerciseInput entity
	* 
	*/
	@RequestMapping(value = "/ExerciseType/{exercisetype_exerciseTypeId}/exerciseInputs/{exerciseinput_exerciseInputId}", method = RequestMethod.GET)
	@ResponseBody
	public ExerciseInput loadExerciseTypeExerciseInputs(@PathVariable Integer exercisetype_exerciseTypeId, @PathVariable Integer related_exerciseinputs_exerciseInputId) {
		ExerciseInput exerciseinput = exerciseInputDAO.findExerciseInputByPrimaryKey(related_exerciseinputs_exerciseInputId, -1, -1);

		return exerciseinput;
	}

	/**
	* Create a new ExerciseInput entity
	* 
	*/
	@RequestMapping(value = "/ExerciseType/{exercisetype_exerciseTypeId}/exerciseInputs", method = RequestMethod.POST)
	@ResponseBody
	public ExerciseInput newExerciseTypeExerciseInputs(@PathVariable Integer exercisetype_exerciseTypeId, @RequestBody ExerciseInput exerciseinput) {
		exerciseTypeService.saveExerciseTypeExerciseInputs(exercisetype_exerciseTypeId, exerciseinput);
		return exerciseInputDAO.findExerciseInputByPrimaryKey(exerciseinput.getExerciseInputId());
	}

	/**
	* Show all ExerciseInput entities by ExerciseType
	* 
	*/
	@RequestMapping(value = "/ExerciseType/{exercisetype_exerciseTypeId}/exerciseInputs", method = RequestMethod.GET)
	@ResponseBody
	public List<ExerciseInput> getExerciseTypeExerciseInputs(@PathVariable Integer exercisetype_exerciseTypeId) {
		return new java.util.ArrayList<ExerciseInput>(exerciseTypeDAO.findExerciseTypeByPrimaryKey(exercisetype_exerciseTypeId).getExerciseInputs());
	}
}