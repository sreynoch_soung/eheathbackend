package ehealth.controller;

import ehealth.dao.EffortTypeDAO;
import ehealth.dao.ExerciseInputDAO;
import ehealth.dao.ExerciseTypeDAO;
import ehealth.dao.KidDAO;

import ehealth.domain.EffortType;
import ehealth.domain.ExerciseInput;
import ehealth.domain.ExerciseType;
import ehealth.domain.Kid;

import ehealth.service.ExerciseInputService;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Controller;

import org.springframework.web.bind.WebDataBinder;

import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Spring Rest controller that handles CRUD requests for ExerciseInput entities
 * 
 */

@Controller("ExerciseInputRestController")

public class ExerciseInputRestController {

	/**
	 * DAO injected by Spring that manages EffortType entities
	 * 
	 */
	@Autowired
	private EffortTypeDAO effortTypeDAO;

	/**
	 * DAO injected by Spring that manages ExerciseInput entities
	 * 
	 */
	@Autowired
	private ExerciseInputDAO exerciseInputDAO;

	/**
	 * DAO injected by Spring that manages ExerciseType entities
	 * 
	 */
	@Autowired
	private ExerciseTypeDAO exerciseTypeDAO;

	/**
	 * DAO injected by Spring that manages Kid entities
	 * 
	 */
	@Autowired
	private KidDAO kidDAO;

	/**
	 * Service injected by Spring that provides CRUD operations for ExerciseInput entities
	 * 
	 */
	@Autowired
	private ExerciseInputService exerciseInputService;

	/**
	 * Create a new EffortType entity
	 * 
	 */
	@RequestMapping(value = "/ExerciseInput/{exerciseinput_exerciseInputId}/effortType", method = RequestMethod.POST)
	@ResponseBody
	public EffortType newExerciseInputEffortType(@PathVariable Integer exerciseinput_exerciseInputId, @RequestBody EffortType efforttype) {
		exerciseInputService.saveExerciseInputEffortType(exerciseinput_exerciseInputId, efforttype);
		return effortTypeDAO.findEffortTypeByPrimaryKey(efforttype.getEffortTypeId());
	}

	/**
	* View an existing EffortType entity
	* 
	*/
	@RequestMapping(value = "/ExerciseInput/{exerciseinput_exerciseInputId}/effortType/{efforttype_effortTypeId}", method = RequestMethod.GET)
	@ResponseBody
	public EffortType loadExerciseInputEffortType(@PathVariable Integer exerciseinput_exerciseInputId, @PathVariable Integer related_efforttype_effortTypeId) {
		EffortType efforttype = effortTypeDAO.findEffortTypeByPrimaryKey(related_efforttype_effortTypeId, -1, -1);

		return efforttype;
	}

	/**
	* View an existing Kid entity
	* 
	*/
	@RequestMapping(value = "/ExerciseInput/{exerciseinput_exerciseInputId}/kid/{kid_kidId}", method = RequestMethod.GET)
	@ResponseBody
	public Kid loadExerciseInputKid(@PathVariable Integer exerciseinput_exerciseInputId, @PathVariable Integer related_kid_kidId) {
		Kid kid = kidDAO.findKidByPrimaryKey(related_kid_kidId, -1, -1);

		return kid;
	}

	/**
	* Register custom, context-specific property editors
	* 
	*/
	@InitBinder
	public void initBinder(WebDataBinder binder, HttpServletRequest request) { // Register static property editors.
		binder.registerCustomEditor(java.util.Calendar.class, new org.skyway.spring.util.databinding.CustomCalendarEditor());
		binder.registerCustomEditor(byte[].class, new org.springframework.web.multipart.support.ByteArrayMultipartFileEditor());
		binder.registerCustomEditor(boolean.class, new org.skyway.spring.util.databinding.EnhancedBooleanEditor(false));
		binder.registerCustomEditor(Boolean.class, new org.skyway.spring.util.databinding.EnhancedBooleanEditor(true));
		binder.registerCustomEditor(java.math.BigDecimal.class, new org.skyway.spring.util.databinding.NaNHandlingNumberEditor(java.math.BigDecimal.class, true));
		binder.registerCustomEditor(Integer.class, new org.skyway.spring.util.databinding.NaNHandlingNumberEditor(Integer.class, true));
		binder.registerCustomEditor(java.util.Date.class, new org.skyway.spring.util.databinding.CustomDateEditor());
		binder.registerCustomEditor(String.class, new org.skyway.spring.util.databinding.StringEditor());
		binder.registerCustomEditor(Long.class, new org.skyway.spring.util.databinding.NaNHandlingNumberEditor(Long.class, true));
		binder.registerCustomEditor(Double.class, new org.skyway.spring.util.databinding.NaNHandlingNumberEditor(Double.class, true));
	}

	/**
	* Get Kid entity by ExerciseInput
	* 
	*/
	@RequestMapping(value = "/ExerciseInput/{exerciseinput_exerciseInputId}/kid", method = RequestMethod.GET)
	@ResponseBody
	public Kid getExerciseInputKid(@PathVariable Integer exerciseinput_exerciseInputId) {
		return exerciseInputDAO.findExerciseInputByPrimaryKey(exerciseinput_exerciseInputId).getKid();
	}

	/**
	* Select an existing ExerciseInput entity
	* 
	*/
	@RequestMapping(value = "/ExerciseInput/{exerciseinput_exerciseInputId}", method = RequestMethod.GET)
	@ResponseBody
	public ExerciseInput loadExerciseInput(@PathVariable Integer exerciseinput_exerciseInputId) {
		return exerciseInputDAO.findExerciseInputByPrimaryKey(exerciseinput_exerciseInputId);
	}

	/**
	* Delete an existing ExerciseType entity
	* 
	*/
	@RequestMapping(value = "/ExerciseInput/{exerciseinput_exerciseInputId}/exerciseType/{exercisetype_exerciseTypeId}", method = RequestMethod.DELETE)
	@ResponseBody
	public void deleteExerciseInputExerciseType(@PathVariable Integer exerciseinput_exerciseInputId, @PathVariable Integer related_exercisetype_exerciseTypeId) {
		exerciseInputService.deleteExerciseInputExerciseType(exerciseinput_exerciseInputId, related_exercisetype_exerciseTypeId);
	}

	/**
	* Delete an existing Kid entity
	* 
	*/
	@RequestMapping(value = "/ExerciseInput/{exerciseinput_exerciseInputId}/kid/{kid_kidId}", method = RequestMethod.DELETE)
	@ResponseBody
	public void deleteExerciseInputKid(@PathVariable Integer exerciseinput_exerciseInputId, @PathVariable Integer related_kid_kidId) {
		exerciseInputService.deleteExerciseInputKid(exerciseinput_exerciseInputId, related_kid_kidId);
	}

	/**
	* Delete an existing EffortType entity
	* 
	*/
	@RequestMapping(value = "/ExerciseInput/{exerciseinput_exerciseInputId}/effortType/{efforttype_effortTypeId}", method = RequestMethod.DELETE)
	@ResponseBody
	public void deleteExerciseInputEffortType(@PathVariable Integer exerciseinput_exerciseInputId, @PathVariable Integer related_efforttype_effortTypeId) {
		exerciseInputService.deleteExerciseInputEffortType(exerciseinput_exerciseInputId, related_efforttype_effortTypeId);
	}

	/**
	* Create a new Kid entity
	* 
	*/
	@RequestMapping(value = "/ExerciseInput/{exerciseinput_exerciseInputId}/kid", method = RequestMethod.POST)
	@ResponseBody
	public Kid newExerciseInputKid(@PathVariable Integer exerciseinput_exerciseInputId, @RequestBody Kid kid) {
		exerciseInputService.saveExerciseInputKid(exerciseinput_exerciseInputId, kid);
		return kidDAO.findKidByPrimaryKey(kid.getKidId());
	}

	/**
	* Get EffortType entity by ExerciseInput
	* 
	*/
	@RequestMapping(value = "/ExerciseInput/{exerciseinput_exerciseInputId}/effortType", method = RequestMethod.GET)
	@ResponseBody
	public EffortType getExerciseInputEffortType(@PathVariable Integer exerciseinput_exerciseInputId) {
		return exerciseInputDAO.findExerciseInputByPrimaryKey(exerciseinput_exerciseInputId).getEffortType();
	}

	/**
	* Save an existing ExerciseInput entity
	* 
	*/
	@RequestMapping(value = "/ExerciseInput", method = RequestMethod.PUT)
	@ResponseBody
	public ExerciseInput saveExerciseInput(@RequestBody ExerciseInput exerciseinput) {
		exerciseInputService.saveExerciseInput(exerciseinput);
		return exerciseInputDAO.findExerciseInputByPrimaryKey(exerciseinput.getExerciseInputId());
	}

	/**
	* Create a new ExerciseInput entity
	* 
	*/
	@RequestMapping(value = "/ExerciseInput", method = RequestMethod.POST)
	@ResponseBody
	public ExerciseInput newExerciseInput(@RequestBody ExerciseInput exerciseinput) {
		exerciseInputService.saveExerciseInput(exerciseinput);
		return exerciseInputDAO.findExerciseInputByPrimaryKey(exerciseinput.getExerciseInputId());
	}

	/**
	* Show all ExerciseInput entities
	* 
	*/
	@RequestMapping(value = "/ExerciseInput", method = RequestMethod.GET)
	@ResponseBody
	public List<ExerciseInput> listExerciseInputs() {
		return new java.util.ArrayList<ExerciseInput>(exerciseInputService.loadExerciseInputs());
	}

	/**
	* Delete an existing ExerciseInput entity
	* 
	*/
	@RequestMapping(value = "/ExerciseInput/{exerciseinput_exerciseInputId}", method = RequestMethod.DELETE)
	@ResponseBody
	public void deleteExerciseInput(@PathVariable Integer exerciseinput_exerciseInputId) {
		ExerciseInput exerciseinput = exerciseInputDAO.findExerciseInputByPrimaryKey(exerciseinput_exerciseInputId);
		exerciseInputService.deleteExerciseInput(exerciseinput);
	}

	/**
	* Create a new ExerciseType entity
	* 
	*/
	@RequestMapping(value = "/ExerciseInput/{exerciseinput_exerciseInputId}/exerciseType", method = RequestMethod.POST)
	@ResponseBody
	public ExerciseType newExerciseInputExerciseType(@PathVariable Integer exerciseinput_exerciseInputId, @RequestBody ExerciseType exercisetype) {
		exerciseInputService.saveExerciseInputExerciseType(exerciseinput_exerciseInputId, exercisetype);
		return exerciseTypeDAO.findExerciseTypeByPrimaryKey(exercisetype.getExerciseTypeId());
	}

	/**
	* Get ExerciseType entity by ExerciseInput
	* 
	*/
	@RequestMapping(value = "/ExerciseInput/{exerciseinput_exerciseInputId}/exerciseType", method = RequestMethod.GET)
	@ResponseBody
	public ExerciseType getExerciseInputExerciseType(@PathVariable Integer exerciseinput_exerciseInputId) {
		return exerciseInputDAO.findExerciseInputByPrimaryKey(exerciseinput_exerciseInputId).getExerciseType();
	}

	/**
	* Save an existing Kid entity
	* 
	*/
	@RequestMapping(value = "/ExerciseInput/{exerciseinput_exerciseInputId}/kid", method = RequestMethod.PUT)
	@ResponseBody
	public Kid saveExerciseInputKid(@PathVariable Integer exerciseinput_exerciseInputId, @RequestBody Kid kid) {
		exerciseInputService.saveExerciseInputKid(exerciseinput_exerciseInputId, kid);
		return kidDAO.findKidByPrimaryKey(kid.getKidId());
	}

	/**
	* Save an existing ExerciseType entity
	* 
	*/
	@RequestMapping(value = "/ExerciseInput/{exerciseinput_exerciseInputId}/exerciseType", method = RequestMethod.PUT)
	@ResponseBody
	public ExerciseType saveExerciseInputExerciseType(@PathVariable Integer exerciseinput_exerciseInputId, @RequestBody ExerciseType exercisetype) {
		exerciseInputService.saveExerciseInputExerciseType(exerciseinput_exerciseInputId, exercisetype);
		return exerciseTypeDAO.findExerciseTypeByPrimaryKey(exercisetype.getExerciseTypeId());
	}

	/**
	* View an existing ExerciseType entity
	* 
	*/
	@RequestMapping(value = "/ExerciseInput/{exerciseinput_exerciseInputId}/exerciseType/{exercisetype_exerciseTypeId}", method = RequestMethod.GET)
	@ResponseBody
	public ExerciseType loadExerciseInputExerciseType(@PathVariable Integer exerciseinput_exerciseInputId, @PathVariable Integer related_exercisetype_exerciseTypeId) {
		ExerciseType exercisetype = exerciseTypeDAO.findExerciseTypeByPrimaryKey(related_exercisetype_exerciseTypeId, -1, -1);

		return exercisetype;
	}

	/**
	* Save an existing EffortType entity
	* 
	*/
	@RequestMapping(value = "/ExerciseInput/{exerciseinput_exerciseInputId}/effortType", method = RequestMethod.PUT)
	@ResponseBody
	public EffortType saveExerciseInputEffortType(@PathVariable Integer exerciseinput_exerciseInputId, @RequestBody EffortType efforttype) {
		exerciseInputService.saveExerciseInputEffortType(exerciseinput_exerciseInputId, efforttype);
		return effortTypeDAO.findEffortTypeByPrimaryKey(efforttype.getEffortTypeId());
	}
}