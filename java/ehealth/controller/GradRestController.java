package ehealth.controller;

import ehealth.dao.GradDAO;
import ehealth.dao.KidDAO;

import ehealth.domain.Grad;
import ehealth.domain.Kid;

import ehealth.service.GradService;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Controller;

import org.springframework.web.bind.WebDataBinder;

import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Spring Rest controller that handles CRUD requests for Grad entities
 * 
 */

@Controller("GradRestController")

public class GradRestController {

	/**
	 * DAO injected by Spring that manages Grad entities
	 * 
	 */
	@Autowired
	private GradDAO gradDAO;

	/**
	 * DAO injected by Spring that manages Kid entities
	 * 
	 */
	@Autowired
	private KidDAO kidDAO;

	/**
	 * Service injected by Spring that provides CRUD operations for Grad entities
	 * 
	 */
	@Autowired
	private GradService gradService;

	/**
	 * View an existing Kid entity
	 * 
	 */
	@RequestMapping(value = "/Grad/{grad_gradId}/kid/{kid_kidId}", method = RequestMethod.GET)
	@ResponseBody
	public Kid loadGradKid(@PathVariable Integer grad_gradId, @PathVariable Integer related_kid_kidId) {
		Kid kid = kidDAO.findKidByPrimaryKey(related_kid_kidId, -1, -1);

		return kid;
	}

	/**
	* Get Kid entity by Grad
	* 
	*/
	@RequestMapping(value = "/Grad/{grad_gradId}/kid", method = RequestMethod.GET)
	@ResponseBody
	public Kid getGradKid(@PathVariable Integer grad_gradId) {
		return gradDAO.findGradByPrimaryKey(grad_gradId).getKid();
	}

	/**
	* Delete an existing Kid entity
	* 
	*/
	@RequestMapping(value = "/Grad/{grad_gradId}/kid/{kid_kidId}", method = RequestMethod.DELETE)
	@ResponseBody
	public void deleteGradKid(@PathVariable Integer grad_gradId, @PathVariable Integer related_kid_kidId) {
		gradService.deleteGradKid(grad_gradId, related_kid_kidId);
	}

	/**
	* Select an existing Grad entity
	* 
	*/
	@RequestMapping(value = "/Grad/{grad_gradId}", method = RequestMethod.GET)
	@ResponseBody
	public Grad loadGrad(@PathVariable Integer grad_gradId) {
		return gradDAO.findGradByPrimaryKey(grad_gradId);
	}

	/**
	* Create a new Kid entity
	* 
	*/
	@RequestMapping(value = "/Grad/{grad_gradId}/kid", method = RequestMethod.POST)
	@ResponseBody
	public Kid newGradKid(@PathVariable Integer grad_gradId, @RequestBody Kid kid) {
		gradService.saveGradKid(grad_gradId, kid);
		return kidDAO.findKidByPrimaryKey(kid.getKidId());
	}

	/**
	* Save an existing Kid entity
	* 
	*/
	@RequestMapping(value = "/Grad/{grad_gradId}/kid", method = RequestMethod.PUT)
	@ResponseBody
	public Kid saveGradKid(@PathVariable Integer grad_gradId, @RequestBody Kid kid) {
		gradService.saveGradKid(grad_gradId, kid);
		return kidDAO.findKidByPrimaryKey(kid.getKidId());
	}

	/**
	* Save an existing Grad entity
	* 
	*/
	@RequestMapping(value = "/Grad", method = RequestMethod.PUT)
	@ResponseBody
	public Grad saveGrad(@RequestBody Grad grad) {
		gradService.saveGrad(grad);
		return gradDAO.findGradByPrimaryKey(grad.getGradId());
	}

	/**
	* Register custom, context-specific property editors
	* 
	*/
	@InitBinder
	public void initBinder(WebDataBinder binder, HttpServletRequest request) { // Register static property editors.
		binder.registerCustomEditor(java.util.Calendar.class, new org.skyway.spring.util.databinding.CustomCalendarEditor());
		binder.registerCustomEditor(byte[].class, new org.springframework.web.multipart.support.ByteArrayMultipartFileEditor());
		binder.registerCustomEditor(boolean.class, new org.skyway.spring.util.databinding.EnhancedBooleanEditor(false));
		binder.registerCustomEditor(Boolean.class, new org.skyway.spring.util.databinding.EnhancedBooleanEditor(true));
		binder.registerCustomEditor(java.math.BigDecimal.class, new org.skyway.spring.util.databinding.NaNHandlingNumberEditor(java.math.BigDecimal.class, true));
		binder.registerCustomEditor(Integer.class, new org.skyway.spring.util.databinding.NaNHandlingNumberEditor(Integer.class, true));
		binder.registerCustomEditor(java.util.Date.class, new org.skyway.spring.util.databinding.CustomDateEditor());
		binder.registerCustomEditor(String.class, new org.skyway.spring.util.databinding.StringEditor());
		binder.registerCustomEditor(Long.class, new org.skyway.spring.util.databinding.NaNHandlingNumberEditor(Long.class, true));
		binder.registerCustomEditor(Double.class, new org.skyway.spring.util.databinding.NaNHandlingNumberEditor(Double.class, true));
	}

	/**
	* Create a new Grad entity
	* 
	*/
	@RequestMapping(value = "/Grad", method = RequestMethod.POST)
	@ResponseBody
	public Grad newGrad(@RequestBody Grad grad) {
		gradService.saveGrad(grad);
		return gradDAO.findGradByPrimaryKey(grad.getGradId());
	}

	/**
	* Show all Grad entities
	* 
	*/
	@RequestMapping(value = "/Grad", method = RequestMethod.GET)
	@ResponseBody
	public List<Grad> listGrads() {
		return new java.util.ArrayList<Grad>(gradService.loadGrads());
	}

	/**
	* Delete an existing Grad entity
	* 
	*/
	@RequestMapping(value = "/Grad/{grad_gradId}", method = RequestMethod.DELETE)
	@ResponseBody
	public void deleteGrad(@PathVariable Integer grad_gradId) {
		Grad grad = gradDAO.findGradByPrimaryKey(grad_gradId);
		gradService.deleteGrad(grad);
	}
}