package ehealth.controller;

import ehealth.dao.KidDAO;
import ehealth.dao.KidMeasuresDAO;

import ehealth.domain.Kid;
import ehealth.domain.KidMeasures;

import ehealth.service.KidMeasuresService;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Controller;

import org.springframework.web.bind.WebDataBinder;

import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Spring Rest controller that handles CRUD requests for KidMeasures entities
 * 
 */

@Controller("KidMeasuresRestController")

public class KidMeasuresRestController {

	/**
	 * DAO injected by Spring that manages Kid entities
	 * 
	 */
	@Autowired
	private KidDAO kidDAO;

	/**
	 * DAO injected by Spring that manages KidMeasures entities
	 * 
	 */
	@Autowired
	private KidMeasuresDAO kidMeasuresDAO;

	/**
	 * Service injected by Spring that provides CRUD operations for KidMeasures entities
	 * 
	 */
	@Autowired
	private KidMeasuresService kidMeasuresService;

	/**
	 * Create a new Kid entity
	 * 
	 */
	@RequestMapping(value = "/KidMeasures/{kidmeasures_kidMeasuresId}/kids", method = RequestMethod.POST)
	@ResponseBody
	public Kid newKidMeasuresKids(@PathVariable Integer kidmeasures_kidMeasuresId, @RequestBody Kid kid) {
		kidMeasuresService.saveKidMeasuresKids(kidmeasures_kidMeasuresId, kid);
		return kidDAO.findKidByPrimaryKey(kid.getKidId());
	}

	/**
	* Delete an existing Kid entity
	* 
	*/
	@RequestMapping(value = "/KidMeasures/{kidmeasures_kidMeasuresId}/kids/{kid_kidId}", method = RequestMethod.DELETE)
	@ResponseBody
	public void deleteKidMeasuresKids(@PathVariable Integer kidmeasures_kidMeasuresId, @PathVariable Integer related_kids_kidId) {
		kidMeasuresService.deleteKidMeasuresKids(kidmeasures_kidMeasuresId, related_kids_kidId);
	}

	/**
	* Save an existing Kid entity
	* 
	*/
	@RequestMapping(value = "/KidMeasures/{kidmeasures_kidMeasuresId}/kids", method = RequestMethod.PUT)
	@ResponseBody
	public Kid saveKidMeasuresKids(@PathVariable Integer kidmeasures_kidMeasuresId, @RequestBody Kid kids) {
		kidMeasuresService.saveKidMeasuresKids(kidmeasures_kidMeasuresId, kids);
		return kidDAO.findKidByPrimaryKey(kids.getKidId());
	}

	/**
	* Save an existing KidMeasures entity
	* 
	*/
	@RequestMapping(value = "/KidMeasures", method = RequestMethod.PUT)
	@ResponseBody
	public KidMeasures saveKidMeasures(@RequestBody KidMeasures kidmeasures) {
		kidMeasuresService.saveKidMeasures(kidmeasures);
		return kidMeasuresDAO.findKidMeasuresByPrimaryKey(kidmeasures.getKidMeasuresId());
	}

	/**
	* Select an existing KidMeasures entity
	* 
	*/
	@RequestMapping(value = "/KidMeasures/{kidmeasures_kidMeasuresId}", method = RequestMethod.GET)
	@ResponseBody
	public KidMeasures loadKidMeasures(@PathVariable Integer kidmeasures_kidMeasuresId) {
		return kidMeasuresDAO.findKidMeasuresByPrimaryKey(kidmeasures_kidMeasuresId);
	}

	/**
	* Show all Kid entities by KidMeasures
	* 
	*/
	@RequestMapping(value = "/KidMeasures/{kidmeasures_kidMeasuresId}/kids", method = RequestMethod.GET)
	@ResponseBody
	public List<Kid> getKidMeasuresKids(@PathVariable Integer kidmeasures_kidMeasuresId) {
		return new java.util.ArrayList<Kid>(kidMeasuresDAO.findKidMeasuresByPrimaryKey(kidmeasures_kidMeasuresId).getKids());
	}

	/**
	* Show all KidMeasures entities
	* 
	*/
	@RequestMapping(value = "/KidMeasures", method = RequestMethod.GET)
	@ResponseBody
	public List<KidMeasures> listKidMeasuress() {
		return new java.util.ArrayList<KidMeasures>(kidMeasuresService.loadKidMeasuress());
	}

	/**
	* Delete an existing KidMeasures entity
	* 
	*/
	@RequestMapping(value = "/KidMeasures/{kidmeasures_kidMeasuresId}", method = RequestMethod.DELETE)
	@ResponseBody
	public void deleteKidMeasures(@PathVariable Integer kidmeasures_kidMeasuresId) {
		KidMeasures kidmeasures = kidMeasuresDAO.findKidMeasuresByPrimaryKey(kidmeasures_kidMeasuresId);
		kidMeasuresService.deleteKidMeasures(kidmeasures);
	}

	/**
	* Register custom, context-specific property editors
	* 
	*/
	@InitBinder
	public void initBinder(WebDataBinder binder, HttpServletRequest request) { // Register static property editors.
		binder.registerCustomEditor(java.util.Calendar.class, new org.skyway.spring.util.databinding.CustomCalendarEditor());
		binder.registerCustomEditor(byte[].class, new org.springframework.web.multipart.support.ByteArrayMultipartFileEditor());
		binder.registerCustomEditor(boolean.class, new org.skyway.spring.util.databinding.EnhancedBooleanEditor(false));
		binder.registerCustomEditor(Boolean.class, new org.skyway.spring.util.databinding.EnhancedBooleanEditor(true));
		binder.registerCustomEditor(java.math.BigDecimal.class, new org.skyway.spring.util.databinding.NaNHandlingNumberEditor(java.math.BigDecimal.class, true));
		binder.registerCustomEditor(Integer.class, new org.skyway.spring.util.databinding.NaNHandlingNumberEditor(Integer.class, true));
		binder.registerCustomEditor(java.util.Date.class, new org.skyway.spring.util.databinding.CustomDateEditor());
		binder.registerCustomEditor(String.class, new org.skyway.spring.util.databinding.StringEditor());
		binder.registerCustomEditor(Long.class, new org.skyway.spring.util.databinding.NaNHandlingNumberEditor(Long.class, true));
		binder.registerCustomEditor(Double.class, new org.skyway.spring.util.databinding.NaNHandlingNumberEditor(Double.class, true));
	}

	/**
	* Create a new KidMeasures entity
	* 
	*/
	@RequestMapping(value = "/KidMeasures", method = RequestMethod.POST)
	@ResponseBody
	public KidMeasures newKidMeasures(@RequestBody KidMeasures kidmeasures) {
		kidMeasuresService.saveKidMeasures(kidmeasures);
		return kidMeasuresDAO.findKidMeasuresByPrimaryKey(kidmeasures.getKidMeasuresId());
	}

	/**
	* View an existing Kid entity
	* 
	*/
	@RequestMapping(value = "/KidMeasures/{kidmeasures_kidMeasuresId}/kids/{kid_kidId}", method = RequestMethod.GET)
	@ResponseBody
	public Kid loadKidMeasuresKids(@PathVariable Integer kidmeasures_kidMeasuresId, @PathVariable Integer related_kids_kidId) {
		Kid kid = kidDAO.findKidByPrimaryKey(related_kids_kidId, -1, -1);

		return kid;
	}
}