package ehealth.controller;

import ehealth.dao.LeagueDAO;
import ehealth.dao.MatchDAO;
import ehealth.dao.TeamDAO;

import ehealth.domain.League;
import ehealth.domain.Match;
import ehealth.domain.Team;

import ehealth.service.LeagueService;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Controller;

import org.springframework.web.bind.WebDataBinder;

import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Spring Rest controller that handles CRUD requests for League entities
 * 
 */

@Controller("LeagueRestController")

public class LeagueRestController {

	/**
	 * DAO injected by Spring that manages League entities
	 * 
	 */
	@Autowired
	private LeagueDAO leagueDAO;

	/**
	 * DAO injected by Spring that manages Match entities
	 * 
	 */
	@Autowired
	private MatchDAO matchDAO;

	/**
	 * DAO injected by Spring that manages Team entities
	 * 
	 */
	@Autowired
	private TeamDAO teamDAO;

	/**
	 * Service injected by Spring that provides CRUD operations for League entities
	 * 
	 */
	@Autowired
	private LeagueService leagueService;

	/**
	 * Register custom, context-specific property editors
	 * 
	 */
	@InitBinder
	public void initBinder(WebDataBinder binder, HttpServletRequest request) { // Register static property editors.
		binder.registerCustomEditor(java.util.Calendar.class, new org.skyway.spring.util.databinding.CustomCalendarEditor());
		binder.registerCustomEditor(byte[].class, new org.springframework.web.multipart.support.ByteArrayMultipartFileEditor());
		binder.registerCustomEditor(boolean.class, new org.skyway.spring.util.databinding.EnhancedBooleanEditor(false));
		binder.registerCustomEditor(Boolean.class, new org.skyway.spring.util.databinding.EnhancedBooleanEditor(true));
		binder.registerCustomEditor(java.math.BigDecimal.class, new org.skyway.spring.util.databinding.NaNHandlingNumberEditor(java.math.BigDecimal.class, true));
		binder.registerCustomEditor(Integer.class, new org.skyway.spring.util.databinding.NaNHandlingNumberEditor(Integer.class, true));
		binder.registerCustomEditor(java.util.Date.class, new org.skyway.spring.util.databinding.CustomDateEditor());
		binder.registerCustomEditor(String.class, new org.skyway.spring.util.databinding.StringEditor());
		binder.registerCustomEditor(Long.class, new org.skyway.spring.util.databinding.NaNHandlingNumberEditor(Long.class, true));
		binder.registerCustomEditor(Double.class, new org.skyway.spring.util.databinding.NaNHandlingNumberEditor(Double.class, true));
	}

	/**
	* Select an existing League entity
	* 
	*/
	@RequestMapping(value = "/League/{league_leagueId}", method = RequestMethod.GET)
	@ResponseBody
	public League loadLeague(@PathVariable Integer league_leagueId) {
		return leagueDAO.findLeagueByPrimaryKey(league_leagueId);
	}

	/**
	* Delete an existing Match entity
	* 
	*/
	@RequestMapping(value = "/League/{league_leagueId}/match/{match_matchId}", method = RequestMethod.DELETE)
	@ResponseBody
	public void deleteLeagueMatch(@PathVariable Integer league_leagueId, @PathVariable Integer related_match_matchId) {
		leagueService.deleteLeagueMatch(league_leagueId, related_match_matchId);
	}

	/**
	* Save an existing Match entity
	* 
	*/
	@RequestMapping(value = "/League/{league_leagueId}/match", method = RequestMethod.PUT)
	@ResponseBody
	public Match saveLeagueMatch(@PathVariable Integer league_leagueId, @RequestBody Match match) {
		leagueService.saveLeagueMatch(league_leagueId, match);
		return matchDAO.findMatchByPrimaryKey(match.getMatchId());
	}

	/**
	* View an existing Team entity
	* 
	*/
	@RequestMapping(value = "/League/{league_leagueId}/team/{team_teamId}", method = RequestMethod.GET)
	@ResponseBody
	public Team loadLeagueTeam(@PathVariable Integer league_leagueId, @PathVariable Integer related_team_teamId) {
		Team team = teamDAO.findTeamByPrimaryKey(related_team_teamId, -1, -1);

		return team;
	}

	/**
	* Get Team entity by League
	* 
	*/
	@RequestMapping(value = "/League/{league_leagueId}/team", method = RequestMethod.GET)
	@ResponseBody
	public Team getLeagueTeam(@PathVariable Integer league_leagueId) {
		return leagueDAO.findLeagueByPrimaryKey(league_leagueId).getTeam();
	}

	/**
	* View an existing Match entity
	* 
	*/
	@RequestMapping(value = "/League/{league_leagueId}/match/{match_matchId}", method = RequestMethod.GET)
	@ResponseBody
	public Match loadLeagueMatch(@PathVariable Integer league_leagueId, @PathVariable Integer related_match_matchId) {
		Match match = matchDAO.findMatchByPrimaryKey(related_match_matchId, -1, -1);

		return match;
	}

	/**
	* Save an existing League entity
	* 
	*/
	@RequestMapping(value = "/League", method = RequestMethod.PUT)
	@ResponseBody
	public League saveLeague(@RequestBody League league) {
		leagueService.saveLeague(league);
		return leagueDAO.findLeagueByPrimaryKey(league.getLeagueId());
	}

	/**
	* Show all League entities
	* 
	*/
	@RequestMapping(value = "/League", method = RequestMethod.GET)
	@ResponseBody
	public List<League> listLeagues() {
		return new java.util.ArrayList<League>(leagueService.loadLeagues());
	}

	/**
	* Create a new Match entity
	* 
	*/
	@RequestMapping(value = "/League/{league_leagueId}/match", method = RequestMethod.POST)
	@ResponseBody
	public Match newLeagueMatch(@PathVariable Integer league_leagueId, @RequestBody Match match) {
		leagueService.saveLeagueMatch(league_leagueId, match);
		return matchDAO.findMatchByPrimaryKey(match.getMatchId());
	}

	/**
	* Save an existing Team entity
	* 
	*/
	@RequestMapping(value = "/League/{league_leagueId}/team", method = RequestMethod.PUT)
	@ResponseBody
	public Team saveLeagueTeam(@PathVariable Integer league_leagueId, @RequestBody Team team) {
		leagueService.saveLeagueTeam(league_leagueId, team);
		return teamDAO.findTeamByPrimaryKey(team.getTeamId());
	}

	/**
	* Get Match entity by League
	* 
	*/
	@RequestMapping(value = "/League/{league_leagueId}/match", method = RequestMethod.GET)
	@ResponseBody
	public Match getLeagueMatch(@PathVariable Integer league_leagueId) {
		return leagueDAO.findLeagueByPrimaryKey(league_leagueId).getMatch();
	}

	/**
	* Delete an existing Team entity
	* 
	*/
	@RequestMapping(value = "/League/{league_leagueId}/team/{team_teamId}", method = RequestMethod.DELETE)
	@ResponseBody
	public void deleteLeagueTeam(@PathVariable Integer league_leagueId, @PathVariable Integer related_team_teamId) {
		leagueService.deleteLeagueTeam(league_leagueId, related_team_teamId);
	}

	/**
	* Create a new Team entity
	* 
	*/
	@RequestMapping(value = "/League/{league_leagueId}/team", method = RequestMethod.POST)
	@ResponseBody
	public Team newLeagueTeam(@PathVariable Integer league_leagueId, @RequestBody Team team) {
		leagueService.saveLeagueTeam(league_leagueId, team);
		return teamDAO.findTeamByPrimaryKey(team.getTeamId());
	}

	/**
	* Delete an existing League entity
	* 
	*/
	@RequestMapping(value = "/League/{league_leagueId}", method = RequestMethod.DELETE)
	@ResponseBody
	public void deleteLeague(@PathVariable Integer league_leagueId) {
		League league = leagueDAO.findLeagueByPrimaryKey(league_leagueId);
		leagueService.deleteLeague(league);
	}

	/**
	* Create a new League entity
	* 
	*/
	@RequestMapping(value = "/League", method = RequestMethod.POST)
	@ResponseBody
	public League newLeague(@RequestBody League league) {
		leagueService.saveLeague(league);
		return leagueDAO.findLeagueByPrimaryKey(league.getLeagueId());
	}
}