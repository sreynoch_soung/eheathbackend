package ehealth.controller;

import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import ehealth.dao.AccountSettingDAO;
import ehealth.dao.BonusDAO;
import ehealth.dao.CityDAO;
import ehealth.dao.DoctorDAO;
import ehealth.dao.ExerciseInputDAO;
import ehealth.dao.ExerciseTargetDAO;
import ehealth.dao.GradDAO;
import ehealth.dao.KidChatDAO;
import ehealth.dao.KidDAO;
import ehealth.dao.KidMeasuresDAO;
import ehealth.dao.MealDAO;
import ehealth.dao.NutritionTargetDAO;
import ehealth.dao.ParentKidDAO;
import ehealth.dao.TeamDAO;
import ehealth.dao.UserDAO;
import ehealth.domain.AccountSetting;
import ehealth.domain.Bonus;
import ehealth.domain.City;
import ehealth.domain.Doctor;
import ehealth.domain.ExerciseInput;
import ehealth.domain.ExerciseTarget;
import ehealth.domain.Grad;
import ehealth.domain.Kid;
import ehealth.domain.KidChat;
import ehealth.domain.KidMeasures;
import ehealth.domain.Meal;
import ehealth.domain.NutritionTarget;
import ehealth.domain.ParentKid;
import ehealth.domain.Team;
import ehealth.domain.User;
import ehealth.service.KidService;

/**
 * Spring Rest controller that handles CRUD requests for Kid entities
 * 
 */

@Controller("KidRestController")

public class KidRestController {

	/**
	 * DAO injected by Spring that manages AccountSetting entities
	 * 
	 */
	@Autowired
	private AccountSettingDAO accountSettingDAO;

	/**
	 * DAO injected by Spring that manages Bonus entities
	 * 
	 */
	@Autowired
	private BonusDAO bonusDAO;

	/**
	 * DAO injected by Spring that manages City entities
	 * 
	 */
	@Autowired
	private CityDAO cityDAO;

	/**
	 * DAO injected by Spring that manages Doctor entities
	 * 
	 */
	@Autowired
	private DoctorDAO doctorDAO;

	/**
	 * DAO injected by Spring that manages ExerciseInput entities
	 * 
	 */
	@Autowired
	private ExerciseInputDAO exerciseInputDAO;

	/**
	 * DAO injected by Spring that manages ExerciseTarget entities
	 * 
	 */
	@Autowired
	private ExerciseTargetDAO exerciseTargetDAO;

	/**
	 * DAO injected by Spring that manages Grad entities
	 * 
	 */
	@Autowired
	private GradDAO gradDAO;

	/**
	 * DAO injected by Spring that manages KidChat entities
	 * 
	 */
	@Autowired
	private KidChatDAO kidChatDAO;

	/**
	 * DAO injected by Spring that manages Kid entities
	 * 
	 */
	@Autowired
	private KidDAO kidDAO;

	/**
	 * DAO injected by Spring that manages KidMeasures entities
	 * 
	 */
	@Autowired
	private KidMeasuresDAO kidMeasuresDAO;

	/**
	 * DAO injected by Spring that manages Meal entities
	 * 
	 */
	@Autowired
	private MealDAO mealDAO;

	/**
	 * DAO injected by Spring that manages NutritionTarget entities
	 * 
	 */
	@Autowired
	private NutritionTargetDAO nutritionTargetDAO;

	/**
	 * DAO injected by Spring that manages ParentKid entities
	 * 
	 */
	@Autowired
	private ParentKidDAO parentKidDAO;

	/**
	 * DAO injected by Spring that manages Team entities
	 * 
	 */
	@Autowired
	private TeamDAO teamDAO;

	/**
	 * DAO injected by Spring that manages User entities
	 * 
	 */
	@Autowired
	private UserDAO userDAO;

	/**
	 * Service injected by Spring that provides CRUD operations for Kid entities
	 * 
	 */
	@Autowired
	private KidService kidService;

	/**
	 * Show all Grad entities by Kid
	 * 
	 */
	@RequestMapping(value = "/Kid/{kid_kidId}/grads", method = RequestMethod.GET)
	@ResponseBody
	public List<Grad> getKidGrads(@PathVariable Integer kid_kidId) {
		return new java.util.ArrayList<Grad>(kidDAO.findKidByPrimaryKey(kid_kidId).getGrads());
	}

	/**
	* Save an existing AccountSetting entity
	* 
	*/
	@RequestMapping(value = "/Kid/{kid_kidId}/accountSetting", method = RequestMethod.PUT)
	@ResponseBody
	public AccountSetting saveKidAccountSetting(@PathVariable Integer kid_kidId, @RequestBody AccountSetting accountsetting) {
		kidService.saveKidAccountSetting(kid_kidId, accountsetting);
		return accountSettingDAO.findAccountSettingByPrimaryKey(accountsetting.getAccountSettingId());
	}

	/**
	* Save an existing Meal entity
	* 
	*/
	@RequestMapping(value = "/Kid/{kid_kidId}/meals", method = RequestMethod.PUT)
	@ResponseBody
	public Meal saveKidMeals(@PathVariable Integer kid_kidId, @RequestBody Meal meals) {
		kidService.saveKidMeals(kid_kidId, meals);
		return mealDAO.findMealByPrimaryKey(meals.getMealId());
	}

	/**
	* Create a new Doctor entity
	* 
	*/
	@RequestMapping(value = "/Kid/{kid_kidId}/doctor", method = RequestMethod.POST)
	@ResponseBody
	public Doctor newKidDoctor(@PathVariable Integer kid_kidId, @RequestBody Doctor doctor) {
		kidService.saveKidDoctor(kid_kidId, doctor);
		return doctorDAO.findDoctorByPrimaryKey(doctor.getDoctorId());
	}

	/**
	* Save an existing KidChat entity
	* 
	*/
	@RequestMapping(value = "/Kid/{kid_kidId}/kidChatsForKidId2", method = RequestMethod.PUT)
	@ResponseBody
	public KidChat saveKidKidChatsForKidId2(@PathVariable Integer kid_kidId, @RequestBody KidChat kidchatsforkidid2) {
		kidService.saveKidKidChatsForKidId2(kid_kidId, kidchatsforkidid2);
		return kidChatDAO.findKidChatByPrimaryKey(kidchatsforkidid2.getKidChatId());
	}

	/**
	* Create a new NutritionTarget entity
	* 
	*/
	@RequestMapping(value = "/Kid/{kid_kidId}/nutritionTargets", method = RequestMethod.POST)
	@ResponseBody
	public NutritionTarget newKidNutritionTargets(@PathVariable Integer kid_kidId, @RequestBody NutritionTarget nutritiontarget) {
		kidService.saveKidNutritionTargets(kid_kidId, nutritiontarget);
		return nutritionTargetDAO.findNutritionTargetByPrimaryKey(nutritiontarget.getNutritionTargetId());
	}

	/**
	* Get Doctor entity by Kid
	* 
	*/
	@RequestMapping(value = "/Kid/{kid_kidId}/doctor", method = RequestMethod.GET)
	@ResponseBody
	public Doctor getKidDoctor(@PathVariable Integer kid_kidId) {
		return kidDAO.findKidByPrimaryKey(kid_kidId).getDoctor();
	}

	/**
	* Create a new ExerciseTarget entity
	* 
	*/
	@RequestMapping(value = "/Kid/{kid_kidId}/exerciseTargets", method = RequestMethod.POST)
	@ResponseBody
	public ExerciseTarget newKidExerciseTargets(@PathVariable Integer kid_kidId, @RequestBody ExerciseTarget exercisetarget) {
		kidService.saveKidExerciseTargets(kid_kidId, exercisetarget);
		return exerciseTargetDAO.findExerciseTargetByPrimaryKey(exercisetarget.getExerciseTargetId());
	}

	/**
	* Get KidMeasures entity by Kid
	* 
	*/
	@RequestMapping(value = "/Kid/{kid_kidId}/kidMeasures", method = RequestMethod.GET)
	@ResponseBody
	public KidMeasures getKidKidMeasures(@PathVariable Integer kid_kidId) {
		return kidDAO.findKidByPrimaryKey(kid_kidId).getKidMeasures();
	}

	/**
	* Create a new KidChat entity
	* 
	*/
	@RequestMapping(value = "/Kid/{kid_kidId}/kidChatsForKidId1", method = RequestMethod.POST)
	@ResponseBody
	public KidChat newKidKidChatsForKidId1(@PathVariable Integer kid_kidId, @RequestBody KidChat kidchat) {
		kidService.saveKidKidChatsForKidId1(kid_kidId, kidchat);
		return kidChatDAO.findKidChatByPrimaryKey(kidchat.getKidChatId());
	}

	/**
	* Get Team entity by Kid
	* 
	*/
	@RequestMapping(value = "/Kid/{kid_kidId}/team", method = RequestMethod.GET)
	@ResponseBody
	public Team getKidTeam(@PathVariable Integer kid_kidId) {
		return kidDAO.findKidByPrimaryKey(kid_kidId).getTeam();
	}

	/**
	* Create a new ExerciseInput entity
	* 
	*/
	@RequestMapping(value = "/Kid/{kid_kidId}/exerciseInputs", method = RequestMethod.POST)
	@ResponseBody
	public ExerciseInput newKidExerciseInputs(@PathVariable Integer kid_kidId, @RequestBody ExerciseInput exerciseinput) {
		kidService.saveKidExerciseInputs(kid_kidId, exerciseinput);
		return exerciseInputDAO.findExerciseInputByPrimaryKey(exerciseinput.getExerciseInputId());
	}

	/**
	* Create a new Team entity
	* 
	*/
	@RequestMapping(value = "/Kid/{kid_kidId}/team", method = RequestMethod.POST)
	@ResponseBody
	public Team newKidTeam(@PathVariable Integer kid_kidId, @RequestBody Team team) {
		kidService.saveKidTeam(kid_kidId, team);
		return teamDAO.findTeamByPrimaryKey(team.getTeamId());
	}

	/**
	* Save an existing KidChat entity
	* 
	*/
	@RequestMapping(value = "/Kid/{kid_kidId}/kidChatsForKidId1", method = RequestMethod.PUT)
	@ResponseBody
	public KidChat saveKidKidChatsForKidId1(@PathVariable Integer kid_kidId, @RequestBody KidChat kidchatsforkidid1) {
		kidService.saveKidKidChatsForKidId1(kid_kidId, kidchatsforkidid1);
		return kidChatDAO.findKidChatByPrimaryKey(kidchatsforkidid1.getKidChatId());
	}

	/**
	* Save an existing User entity
	* 
	*/
	@RequestMapping(value = "/Kid/{kid_kidId}/user", method = RequestMethod.PUT)
	@ResponseBody
	public User saveKidUser(@PathVariable Integer kid_kidId, @RequestBody User user) {
		kidService.saveKidUser(kid_kidId, user);
		return userDAO.findUserByPrimaryKey(user.getUserId());
	}

	/**
	* Create a new Bonus entity
	* 
	*/
	@RequestMapping(value = "/Kid/{kid_kidId}/bonuses", method = RequestMethod.POST)
	@ResponseBody
	public Bonus newKidBonuses(@PathVariable Integer kid_kidId, @RequestBody Bonus bonus) {
		kidService.saveKidBonuses(kid_kidId, bonus);
		return bonusDAO.findBonusByPrimaryKey(bonus.getBonusId());
	}

	/**
	* Save an existing City entity
	* 
	*/
	@RequestMapping(value = "/Kid/{kid_kidId}/city", method = RequestMethod.PUT)
	@ResponseBody
	public City saveKidCity(@PathVariable Integer kid_kidId, @RequestBody City city) {
		kidService.saveKidCity(kid_kidId, city);
		return cityDAO.findCityByPrimaryKey(city.getCityId());
	}

	/**
	* Select an existing Kid entity
	* 
	*/
	@RequestMapping(value = "/Kid_DetailKid{kid_kidId}", method = RequestMethod.GET)
	@ResponseBody
	public ModelAndView loadKid(@PathVariable Integer kid_kidId) {
		Kid kid = kidDAO.findKidByKidId(kid_kidId);
		ModelAndView mav = new ModelAndView();

		mav.addObject("kid", kid);
		mav.setViewName("kid/detailKid");
		return mav;
	}

	/**
	* View an existing ParentKid entity
	* 
	*/
	@RequestMapping(value = "/Kid/{kid_kidId}/parentKids/{parentkid_parentKidId}", method = RequestMethod.GET)
	@ResponseBody
	public ParentKid loadKidParentKids(@PathVariable Integer kid_kidId, @PathVariable Integer related_parentkids_parentKidId) {
		ParentKid parentkid = parentKidDAO.findParentKidByPrimaryKey(related_parentkids_parentKidId, -1, -1);

		return parentkid;
	}

	/**
	* View an existing ExerciseInput entity
	* 
	*/
	@RequestMapping(value = "/Kid/{kid_kidId}/exerciseInputs/{exerciseinput_exerciseInputId}", method = RequestMethod.GET)
	@ResponseBody
	public ExerciseInput loadKidExerciseInputs(@PathVariable Integer kid_kidId, @PathVariable Integer related_exerciseinputs_exerciseInputId) {
		ExerciseInput exerciseinput = exerciseInputDAO.findExerciseInputByPrimaryKey(related_exerciseinputs_exerciseInputId, -1, -1);

		return exerciseinput;
	}

	/**
	* Delete an existing AccountSetting entity
	* 
	*/
	@RequestMapping(value = "/Kid/{kid_kidId}/accountSetting/{accountsetting_accountSettingId}", method = RequestMethod.DELETE)
	@ResponseBody
	public void deleteKidAccountSetting(@PathVariable Integer kid_kidId, @PathVariable Integer related_accountsetting_accountSettingId) {
		kidService.deleteKidAccountSetting(kid_kidId, related_accountsetting_accountSettingId);
	}

	/**
	* Save an existing NutritionTarget entity
	* 
	*/
	@RequestMapping(value = "/Kid/{kid_kidId}/nutritionTargets", method = RequestMethod.PUT)
	@ResponseBody
	public NutritionTarget saveKidNutritionTargets(@PathVariable Integer kid_kidId, @RequestBody NutritionTarget nutritiontargets) {
		kidService.saveKidNutritionTargets(kid_kidId, nutritiontargets);
		return nutritionTargetDAO.findNutritionTargetByPrimaryKey(nutritiontargets.getNutritionTargetId());
	}

	/**
	* Delete an existing Grad entity
	* 
	*/
	@RequestMapping(value = "/Kid/{kid_kidId}/grads/{grad_gradId}", method = RequestMethod.DELETE)
	@ResponseBody
	public void deleteKidGrads(@PathVariable Integer kid_kidId, @PathVariable Integer related_grads_gradId) {
		kidService.deleteKidGrads(kid_kidId, related_grads_gradId);
	}

	/**
	* Save an existing KidMeasures entity
	* 
	*/
	@RequestMapping(value = "/Kid/{kid_kidId}/kidMeasures", method = RequestMethod.PUT)
	@ResponseBody
	public KidMeasures saveKidKidMeasures(@PathVariable Integer kid_kidId, @RequestBody KidMeasures kidmeasures) {
		kidService.saveKidKidMeasures(kid_kidId, kidmeasures);
		return kidMeasuresDAO.findKidMeasuresByPrimaryKey(kidmeasures.getKidMeasuresId());
	}

	/**
	* Create a new AccountSetting entity
	* 
	*/
	@RequestMapping(value = "/Kid/{kid_kidId}/accountSetting", method = RequestMethod.POST)
	@ResponseBody
	public AccountSetting newKidAccountSetting(@PathVariable Integer kid_kidId, @RequestBody AccountSetting accountsetting) {
		kidService.saveKidAccountSetting(kid_kidId, accountsetting);
		return accountSettingDAO.findAccountSettingByPrimaryKey(accountsetting.getAccountSettingId());
	}

	/**
	* Get City entity by Kid
	* 
	*/
	@RequestMapping(value = "/Kid/{kid_kidId}/city", method = RequestMethod.GET)
	@ResponseBody
	public City getKidCity(@PathVariable Integer kid_kidId) {
		return kidDAO.findKidByPrimaryKey(kid_kidId).getCity();
	}

	/**
	* Save an existing Doctor entity
	* 
	*/
	@RequestMapping(value = "/Kid/{kid_kidId}/doctor", method = RequestMethod.PUT)
	@ResponseBody
	public Doctor saveKidDoctor(@PathVariable Integer kid_kidId, @RequestBody Doctor doctor) {
		kidService.saveKidDoctor(kid_kidId, doctor);
		return doctorDAO.findDoctorByPrimaryKey(doctor.getDoctorId());
	}

	/**
	* Create a new Kid entity
	* 
	*/
	@RequestMapping(value = "/Kid", method = RequestMethod.POST)
	@ResponseBody
	public Kid newKid(@RequestBody Kid kid) {
		kidService.saveKid(kid);
		return kidDAO.findKidByPrimaryKey(kid.getKidId());
	}

	/**
	* Show all Kid entities by doctor's name
	* 
	*/
	@RequestMapping(value = "/Kid_ListKids", method = RequestMethod.GET)
	@ResponseBody
	public ModelAndView listKids(@RequestParam String username) {
		Set<ehealth.domain.Doctor> setDoctor = userDAO.findUserByUsername(username).getDoctors();
		int doctorId = ((Doctor) setDoctor.toArray()[0]).getDoctorId();

		Set<Kid> setKids = (doctorDAO.findDoctorByPrimaryKey(doctorId).getKids());
		ModelAndView mav = new ModelAndView();

		mav.addObject("kids", setKids);
		mav.setViewName("kid/listKids");
		return mav;
	}

	/**
	* Register custom, context-specific property editors
	* 
	*/
	@InitBinder
	public void initBinder(WebDataBinder binder, HttpServletRequest request) { // Register static property editors.
		binder.registerCustomEditor(java.util.Calendar.class, new org.skyway.spring.util.databinding.CustomCalendarEditor());
		binder.registerCustomEditor(byte[].class, new org.springframework.web.multipart.support.ByteArrayMultipartFileEditor());
		binder.registerCustomEditor(boolean.class, new org.skyway.spring.util.databinding.EnhancedBooleanEditor(false));
		binder.registerCustomEditor(Boolean.class, new org.skyway.spring.util.databinding.EnhancedBooleanEditor(true));
		binder.registerCustomEditor(java.math.BigDecimal.class, new org.skyway.spring.util.databinding.NaNHandlingNumberEditor(java.math.BigDecimal.class, true));
		binder.registerCustomEditor(Integer.class, new org.skyway.spring.util.databinding.NaNHandlingNumberEditor(Integer.class, true));
		binder.registerCustomEditor(java.util.Date.class, new org.skyway.spring.util.databinding.CustomDateEditor());
		binder.registerCustomEditor(String.class, new org.skyway.spring.util.databinding.StringEditor());
		binder.registerCustomEditor(Long.class, new org.skyway.spring.util.databinding.NaNHandlingNumberEditor(Long.class, true));
		binder.registerCustomEditor(Double.class, new org.skyway.spring.util.databinding.NaNHandlingNumberEditor(Double.class, true));
	}

	/**
	* Show all KidChat entities by Kid
	* 
	*/
	@RequestMapping(value = "/Kid/{kid_kidId}/kidChatsForKidId1", method = RequestMethod.GET)
	@ResponseBody
	public List<KidChat> getKidKidChatsForKidId1(@PathVariable Integer kid_kidId) {
		return new java.util.ArrayList<KidChat>(kidDAO.findKidByPrimaryKey(kid_kidId).getKidChatsForKidId1());
	}

	/**
	* View an existing User entity
	* 
	*/
	@RequestMapping(value = "/Kid/{kid_kidId}/user/{user_userId}", method = RequestMethod.GET)
	@ResponseBody
	public User loadKidUser(@PathVariable Integer kid_kidId, @PathVariable Integer related_user_userId) {
		User user = userDAO.findUserByPrimaryKey(related_user_userId, -1, -1);

		return user;
	}

	/**
	* View an existing AccountSetting entity
	* 
	*/
	@RequestMapping(value = "/Kid/{kid_kidId}/accountSetting/{accountsetting_accountSettingId}", method = RequestMethod.GET)
	@ResponseBody
	public AccountSetting loadKidAccountSetting(@PathVariable Integer kid_kidId, @PathVariable Integer related_accountsetting_accountSettingId) {
		AccountSetting accountsetting = accountSettingDAO.findAccountSettingByPrimaryKey(related_accountsetting_accountSettingId, -1, -1);

		return accountsetting;
	}

	/**
	* Delete an existing Meal entity
	* 
	*/
	@RequestMapping(value = "/Kid/{kid_kidId}/meals/{meal_mealId}", method = RequestMethod.DELETE)
	@ResponseBody
	public void deleteKidMeals(@PathVariable Integer kid_kidId, @PathVariable Integer related_meals_mealId) {
		kidService.deleteKidMeals(kid_kidId, related_meals_mealId);
	}

	/**
	* Save an existing Bonus entity
	* 
	*/
	@RequestMapping(value = "/Kid/{kid_kidId}/bonuses", method = RequestMethod.PUT)
	@ResponseBody
	public Bonus saveKidBonuses(@PathVariable Integer kid_kidId, @RequestBody Bonus bonuses) {
		kidService.saveKidBonuses(kid_kidId, bonuses);
		return bonusDAO.findBonusByPrimaryKey(bonuses.getBonusId());
	}

	/**
	* Create a new User entity
	* 
	*/
	@RequestMapping(value = "/Kid/{kid_kidId}/user", method = RequestMethod.POST)
	@ResponseBody
	public User newKidUser(@PathVariable Integer kid_kidId, @RequestBody User user) {
		kidService.saveKidUser(kid_kidId, user);
		return userDAO.findUserByPrimaryKey(user.getUserId());
	}

	/**
	* Save an existing Grad entity
	* 
	*/
	@RequestMapping(value = "/Kid/{kid_kidId}/grads", method = RequestMethod.PUT)
	@ResponseBody
	public Grad saveKidGrads(@PathVariable Integer kid_kidId, @RequestBody Grad grads) {
		kidService.saveKidGrads(kid_kidId, grads);
		return gradDAO.findGradByPrimaryKey(grads.getGradId());
	}

	/**
	* View an existing ExerciseTarget entity
	* 
	*/
	@RequestMapping(value = "/Kid/{kid_kidId}/exerciseTargets/{exercisetarget_exerciseTargetId}", method = RequestMethod.GET)
	@ResponseBody
	public ExerciseTarget loadKidExerciseTargets(@PathVariable Integer kid_kidId, @PathVariable Integer related_exercisetargets_exerciseTargetId) {
		ExerciseTarget exercisetarget = exerciseTargetDAO.findExerciseTargetByPrimaryKey(related_exercisetargets_exerciseTargetId, -1, -1);

		return exercisetarget;
	}

	/**
	* Save an existing ParentKid entity
	* 
	*/
	@RequestMapping(value = "/Kid/{kid_kidId}/parentKids", method = RequestMethod.PUT)
	@ResponseBody
	public ParentKid saveKidParentKids(@PathVariable Integer kid_kidId, @RequestBody ParentKid parentkids) {
		kidService.saveKidParentKids(kid_kidId, parentkids);
		return parentKidDAO.findParentKidByPrimaryKey(parentkids.getParentKidId());
	}

	/**
	* Delete an existing KidChat entity
	* 
	*/
	@RequestMapping(value = "/Kid/{kid_kidId}/kidChatsForKidId2/{kidchat_kidChatId}", method = RequestMethod.DELETE)
	@ResponseBody
	public void deleteKidKidChatsForKidId2(@PathVariable Integer kid_kidId, @PathVariable Integer related_kidchatsforkidid2_kidChatId) {
		kidService.deleteKidKidChatsForKidId2(kid_kidId, related_kidchatsforkidid2_kidChatId);
	}

	/**
	* View an existing Meal entity
	* 
	*/
	@RequestMapping(value = "/Kid/{kid_kidId}/meals/{meal_mealId}", method = RequestMethod.GET)
	@ResponseBody
	public Meal loadKidMeals(@PathVariable Integer kid_kidId, @PathVariable Integer related_meals_mealId) {
		Meal meal = mealDAO.findMealByPrimaryKey(related_meals_mealId, -1, -1);

		return meal;
	}

	/**
	* Show all Bonus entities by Kid
	* 
	*/
	@RequestMapping(value = "/Kid/{kid_kidId}/bonuses", method = RequestMethod.GET)
	@ResponseBody
	public List<Bonus> getKidBonuses(@PathVariable Integer kid_kidId) {
		return new java.util.ArrayList<Bonus>(kidDAO.findKidByPrimaryKey(kid_kidId).getBonuses());
	}

	/**
	* Show all KidChat entities by Kid
	* 
	*/
	@RequestMapping(value = "/Kid/{kid_kidId}/kidChatsForKidId2", method = RequestMethod.GET)
	@ResponseBody
	public List<KidChat> getKidKidChatsForKidId2(@PathVariable Integer kid_kidId) {
		return new java.util.ArrayList<KidChat>(kidDAO.findKidByPrimaryKey(kid_kidId).getKidChatsForKidId2());
	}

	/**
	* Delete an existing Bonus entity
	* 
	*/
	@RequestMapping(value = "/Kid/{kid_kidId}/bonuses/{bonus_bonusId}", method = RequestMethod.DELETE)
	@ResponseBody
	public void deleteKidBonuses(@PathVariable Integer kid_kidId, @PathVariable Integer related_bonuses_bonusId) {
		kidService.deleteKidBonuses(kid_kidId, related_bonuses_bonusId);
	}

	/**
	* Save an existing Kid entity
	* 
	*/
	@RequestMapping(value = "/Kid", method = RequestMethod.PUT)
	@ResponseBody
	public Kid saveKid(@RequestBody Kid kid) {
		kidService.saveKid(kid);
		return kidDAO.findKidByPrimaryKey(kid.getKidId());
	}

	/**
	* View an existing KidChat entity
	* 
	*/
	@RequestMapping(value = "/Kid/{kid_kidId}/kidChatsForKidId2/{kidchat_kidChatId}", method = RequestMethod.GET)
	@ResponseBody
	public KidChat loadKidKidChatsForKidId2(@PathVariable Integer kid_kidId, @PathVariable Integer related_kidchatsforkidid2_kidChatId) {
		KidChat kidchat = kidChatDAO.findKidChatByPrimaryKey(related_kidchatsforkidid2_kidChatId, -1, -1);

		return kidchat;
	}

	/**
	* Create a new ParentKid entity
	* 
	*/
	@RequestMapping(value = "/Kid/{kid_kidId}/parentKids", method = RequestMethod.POST)
	@ResponseBody
	public ParentKid newKidParentKids(@PathVariable Integer kid_kidId, @RequestBody ParentKid parentkid) {
		kidService.saveKidParentKids(kid_kidId, parentkid);
		return parentKidDAO.findParentKidByPrimaryKey(parentkid.getParentKidId());
	}

	/**
	* Delete an existing ExerciseTarget entity
	* 
	*/
	@RequestMapping(value = "/Kid/{kid_kidId}/exerciseTargets/{exercisetarget_exerciseTargetId}", method = RequestMethod.DELETE)
	@ResponseBody
	public void deleteKidExerciseTargets(@PathVariable Integer kid_kidId, @PathVariable Integer related_exercisetargets_exerciseTargetId) {
		kidService.deleteKidExerciseTargets(kid_kidId, related_exercisetargets_exerciseTargetId);
	}

	/**
	* View an existing Team entity
	* 
	*/
	@RequestMapping(value = "/Kid/{kid_kidId}/team/{team_teamId}", method = RequestMethod.GET)
	@ResponseBody
	public Team loadKidTeam(@PathVariable Integer kid_kidId, @PathVariable Integer related_team_teamId) {
		Team team = teamDAO.findTeamByPrimaryKey(related_team_teamId, -1, -1);

		return team;
	}

	/**
	* Delete an existing Doctor entity
	* 
	*/
	@RequestMapping(value = "/Kid/{kid_kidId}/doctor/{doctor_doctorId}", method = RequestMethod.DELETE)
	@ResponseBody
	public void deleteKidDoctor(@PathVariable Integer kid_kidId, @PathVariable Integer related_doctor_doctorId) {
		kidService.deleteKidDoctor(kid_kidId, related_doctor_doctorId);
	}

	/**
	* Delete an existing User entity
	* 
	*/
	@RequestMapping(value = "/Kid/{kid_kidId}/user/{user_userId}", method = RequestMethod.DELETE)
	@ResponseBody
	public void deleteKidUser(@PathVariable Integer kid_kidId, @PathVariable Integer related_user_userId) {
		kidService.deleteKidUser(kid_kidId, related_user_userId);
	}

	/**
	* View an existing City entity
	* 
	*/
	@RequestMapping(value = "/Kid/{kid_kidId}/city/{city_cityId}", method = RequestMethod.GET)
	@ResponseBody
	public City loadKidCity(@PathVariable Integer kid_kidId, @PathVariable Integer related_city_cityId) {
		City city = cityDAO.findCityByPrimaryKey(related_city_cityId, -1, -1);

		return city;
	}

	/**
	* View an existing Bonus entity
	* 
	*/
	@RequestMapping(value = "/Kid/{kid_kidId}/bonuses/{bonus_bonusId}", method = RequestMethod.GET)
	@ResponseBody
	public Bonus loadKidBonuses(@PathVariable Integer kid_kidId, @PathVariable Integer related_bonuses_bonusId) {
		Bonus bonus = bonusDAO.findBonusByPrimaryKey(related_bonuses_bonusId, -1, -1);

		return bonus;
	}

	/**
	* Delete an existing ExerciseInput entity
	* 
	*/
	@RequestMapping(value = "/Kid/{kid_kidId}/exerciseInputs/{exerciseinput_exerciseInputId}", method = RequestMethod.DELETE)
	@ResponseBody
	public void deleteKidExerciseInputs(@PathVariable Integer kid_kidId, @PathVariable Integer related_exerciseinputs_exerciseInputId) {
		kidService.deleteKidExerciseInputs(kid_kidId, related_exerciseinputs_exerciseInputId);
	}

	/**
	* Create a new Grad entity
	* 
	*/
	@RequestMapping(value = "/Kid/{kid_kidId}/grads", method = RequestMethod.POST)
	@ResponseBody
	public Grad newKidGrads(@PathVariable Integer kid_kidId, @RequestBody Grad grad) {
		kidService.saveKidGrads(kid_kidId, grad);
		return gradDAO.findGradByPrimaryKey(grad.getGradId());
	}

	/**
	* View an existing Grad entity
	* 
	*/
	@RequestMapping(value = "/Kid/{kid_kidId}/grads/{grad_gradId}", method = RequestMethod.GET)
	@ResponseBody
	public Grad loadKidGrads(@PathVariable Integer kid_kidId, @PathVariable Integer related_grads_gradId) {
		Grad grad = gradDAO.findGradByPrimaryKey(related_grads_gradId, -1, -1);

		return grad;
	}

	/**
	* Delete an existing ParentKid entity
	* 
	*/
	@RequestMapping(value = "/Kid/{kid_kidId}/parentKids/{parentkid_parentKidId}", method = RequestMethod.DELETE)
	@ResponseBody
	public void deleteKidParentKids(@PathVariable Integer kid_kidId, @PathVariable Integer related_parentkids_parentKidId) {
		kidService.deleteKidParentKids(kid_kidId, related_parentkids_parentKidId);
	}

	/**
	* Show all ParentKid entities by Kid
	* 
	*/
	@RequestMapping(value = "/Kid/{kid_kidId}/parentKids", method = RequestMethod.GET)
	@ResponseBody
	public List<ParentKid> getKidParentKids(@PathVariable Integer kid_kidId) {
		return new java.util.ArrayList<ParentKid>(kidDAO.findKidByPrimaryKey(kid_kidId).getParentKids());
	}

	/**
	* Create a new KidMeasures entity
	* 
	*/
	@RequestMapping(value = "/Kid/{kid_kidId}/kidMeasures", method = RequestMethod.POST)
	@ResponseBody
	public KidMeasures newKidKidMeasures(@PathVariable Integer kid_kidId, @RequestBody KidMeasures kidmeasures) {
		kidService.saveKidKidMeasures(kid_kidId, kidmeasures);
		return kidMeasuresDAO.findKidMeasuresByPrimaryKey(kidmeasures.getKidMeasuresId());
	}

	/**
	* View an existing NutritionTarget entity
	* 
	*/
	@RequestMapping(value = "/Kid/{kid_kidId}/nutritionTargets/{nutritiontarget_nutritionTargetId}", method = RequestMethod.GET)
	@ResponseBody
	public NutritionTarget loadKidNutritionTargets(@PathVariable Integer kid_kidId, @PathVariable Integer related_nutritiontargets_nutritionTargetId) {
		NutritionTarget nutritiontarget = nutritionTargetDAO.findNutritionTargetByPrimaryKey(related_nutritiontargets_nutritionTargetId, -1, -1);

		return nutritiontarget;
	}

	/**
	* View an existing Doctor entity
	* 
	*/
	@RequestMapping(value = "/Kid/{kid_kidId}/doctor/{doctor_doctorId}", method = RequestMethod.GET)
	@ResponseBody
	public Doctor loadKidDoctor(@PathVariable Integer kid_kidId, @PathVariable Integer related_doctor_doctorId) {
		Doctor doctor = doctorDAO.findDoctorByPrimaryKey(related_doctor_doctorId, -1, -1);

		return doctor;
	}

	/**
	* Show all NutritionTarget entities by Kid
	* 
	*/
	@RequestMapping(value = "/Kid/{kid_kidId}/nutritionTargets", method = RequestMethod.GET)
	@ResponseBody
	public List<NutritionTarget> getKidNutritionTargets(@PathVariable Integer kid_kidId) {
		return new java.util.ArrayList<NutritionTarget>(kidDAO.findKidByPrimaryKey(kid_kidId).getNutritionTargets());
	}

	/**
	* Save an existing ExerciseTarget entity
	* 
	*/
	@RequestMapping(value = "/Kid/{kid_kidId}/exerciseTargets", method = RequestMethod.PUT)
	@ResponseBody
	public ExerciseTarget saveKidExerciseTargets(@PathVariable Integer kid_kidId, @RequestBody ExerciseTarget exercisetargets) {
		kidService.saveKidExerciseTargets(kid_kidId, exercisetargets);
		return exerciseTargetDAO.findExerciseTargetByPrimaryKey(exercisetargets.getExerciseTargetId());
	}

	/**
	* Get User entity by Kid
	* 
	*/
	@RequestMapping(value = "/Kid/{kid_kidId}/user", method = RequestMethod.GET)
	@ResponseBody
	public User getKidUser(@PathVariable Integer kid_kidId) {
		return kidDAO.findKidByPrimaryKey(kid_kidId).getUser();
	}

	/**
	* Delete an existing City entity
	* 
	*/
	@RequestMapping(value = "/Kid/{kid_kidId}/city/{city_cityId}", method = RequestMethod.DELETE)
	@ResponseBody
	public void deleteKidCity(@PathVariable Integer kid_kidId, @PathVariable Integer related_city_cityId) {
		kidService.deleteKidCity(kid_kidId, related_city_cityId);
	}

	/**
	* Show all Meal entities by Kid
	* 
	*/
	@RequestMapping(value = "/Kid/{kid_kidId}/meals", method = RequestMethod.GET)
	@ResponseBody
	public List<Meal> getKidMeals(@PathVariable Integer kid_kidId) {
		return new java.util.ArrayList<Meal>(kidDAO.findKidByPrimaryKey(kid_kidId).getMeals());
	}

	/**
	* Show all ExerciseInput entities by Kid
	* 
	*/
	@RequestMapping(value = "/Kid/{kid_kidId}/exerciseInputs", method = RequestMethod.GET)
	@ResponseBody
	public List<ExerciseInput> getKidExerciseInputs(@PathVariable Integer kid_kidId) {
		return new java.util.ArrayList<ExerciseInput>(kidDAO.findKidByPrimaryKey(kid_kidId).getExerciseInputs());
	}

	/**
	* Delete an existing NutritionTarget entity
	* 
	*/
	@RequestMapping(value = "/Kid/{kid_kidId}/nutritionTargets/{nutritiontarget_nutritionTargetId}", method = RequestMethod.DELETE)
	@ResponseBody
	public void deleteKidNutritionTargets(@PathVariable Integer kid_kidId, @PathVariable Integer related_nutritiontargets_nutritionTargetId) {
		kidService.deleteKidNutritionTargets(kid_kidId, related_nutritiontargets_nutritionTargetId);
	}

	/**
	* Save an existing ExerciseInput entity
	* 
	*/
	@RequestMapping(value = "/Kid/{kid_kidId}/exerciseInputs", method = RequestMethod.PUT)
	@ResponseBody
	public ExerciseInput saveKidExerciseInputs(@PathVariable Integer kid_kidId, @RequestBody ExerciseInput exerciseinputs) {
		kidService.saveKidExerciseInputs(kid_kidId, exerciseinputs);
		return exerciseInputDAO.findExerciseInputByPrimaryKey(exerciseinputs.getExerciseInputId());
	}

	/**
	* Delete an existing KidMeasures entity
	* 
	*/
	@RequestMapping(value = "/Kid/{kid_kidId}/kidMeasures/{kidmeasures_kidMeasuresId}", method = RequestMethod.DELETE)
	@ResponseBody
	public void deleteKidKidMeasures(@PathVariable Integer kid_kidId, @PathVariable Integer related_kidmeasures_kidMeasuresId) {
		kidService.deleteKidKidMeasures(kid_kidId, related_kidmeasures_kidMeasuresId);
	}

	/**
	* Delete an existing Kid entity
	* 
	*/
	@RequestMapping(value = "/Kid/{kid_kidId}", method = RequestMethod.DELETE)
	@ResponseBody
	public void deleteKid(@PathVariable Integer kid_kidId) {
		Kid kid = kidDAO.findKidByPrimaryKey(kid_kidId);
		kidService.deleteKid(kid);
	}

	/**
	* View an existing KidMeasures entity
	* 
	*/
	@RequestMapping(value = "/Kid/{kid_kidId}/kidMeasures/{kidmeasures_kidMeasuresId}", method = RequestMethod.GET)
	@ResponseBody
	public KidMeasures loadKidKidMeasures(@PathVariable Integer kid_kidId, @PathVariable Integer related_kidmeasures_kidMeasuresId) {
		KidMeasures kidmeasures = kidMeasuresDAO.findKidMeasuresByPrimaryKey(related_kidmeasures_kidMeasuresId, -1, -1);

		return kidmeasures;
	}

	/**
	* Save an existing Team entity
	* 
	*/
	@RequestMapping(value = "/Kid/{kid_kidId}/team", method = RequestMethod.PUT)
	@ResponseBody
	public Team saveKidTeam(@PathVariable Integer kid_kidId, @RequestBody Team team) {
		kidService.saveKidTeam(kid_kidId, team);
		return teamDAO.findTeamByPrimaryKey(team.getTeamId());
	}

	/**
	* Show all ExerciseTarget entities by Kid
	* 
	*/
	@RequestMapping(value = "/Kid/{kid_kidId}/exerciseTargets", method = RequestMethod.GET)
	@ResponseBody
	public List<ExerciseTarget> getKidExerciseTargets(@PathVariable Integer kid_kidId) {
		return new java.util.ArrayList<ExerciseTarget>(kidDAO.findKidByPrimaryKey(kid_kidId).getExerciseTargets());
	}

	/**
	* Delete an existing KidChat entity
	* 
	*/
	@RequestMapping(value = "/Kid/{kid_kidId}/kidChatsForKidId1/{kidchat_kidChatId}", method = RequestMethod.DELETE)
	@ResponseBody
	public void deleteKidKidChatsForKidId1(@PathVariable Integer kid_kidId, @PathVariable Integer related_kidchatsforkidid1_kidChatId) {
		kidService.deleteKidKidChatsForKidId1(kid_kidId, related_kidchatsforkidid1_kidChatId);
	}

	/**
	* Delete an existing Team entity
	* 
	*/
	@RequestMapping(value = "/Kid/{kid_kidId}/team/{team_teamId}", method = RequestMethod.DELETE)
	@ResponseBody
	public void deleteKidTeam(@PathVariable Integer kid_kidId, @PathVariable Integer related_team_teamId) {
		kidService.deleteKidTeam(kid_kidId, related_team_teamId);
	}

	/**
	* Create a new City entity
	* 
	*/
	@RequestMapping(value = "/Kid/{kid_kidId}/city", method = RequestMethod.POST)
	@ResponseBody
	public City newKidCity(@PathVariable Integer kid_kidId, @RequestBody City city) {
		kidService.saveKidCity(kid_kidId, city);
		return cityDAO.findCityByPrimaryKey(city.getCityId());
	}

	/**
	* Create a new KidChat entity
	* 
	*/
	@RequestMapping(value = "/Kid/{kid_kidId}/kidChatsForKidId2", method = RequestMethod.POST)
	@ResponseBody
	public KidChat newKidKidChatsForKidId2(@PathVariable Integer kid_kidId, @RequestBody KidChat kidchat) {
		kidService.saveKidKidChatsForKidId2(kid_kidId, kidchat);
		return kidChatDAO.findKidChatByPrimaryKey(kidchat.getKidChatId());
	}

	/**
	* Get AccountSetting entity by Kid
	* 
	*/
	@RequestMapping(value = "/Kid/{kid_kidId}/accountSetting", method = RequestMethod.GET)
	@ResponseBody
	public AccountSetting getKidAccountSetting(@PathVariable Integer kid_kidId) {
		return kidDAO.findKidByPrimaryKey(kid_kidId).getAccountSetting();
	}

	/**
	* View an existing KidChat entity
	* 
	*/
	@RequestMapping(value = "/Kid/{kid_kidId}/kidChatsForKidId1/{kidchat_kidChatId}", method = RequestMethod.GET)
	@ResponseBody
	public KidChat loadKidKidChatsForKidId1(@PathVariable Integer kid_kidId, @PathVariable Integer related_kidchatsforkidid1_kidChatId) {
		KidChat kidchat = kidChatDAO.findKidChatByPrimaryKey(related_kidchatsforkidid1_kidChatId, -1, -1);

		return kidchat;
	}

	/**
	* Create a new Meal entity
	* 
	*/
	@RequestMapping(value = "/Kid/{kid_kidId}/meals", method = RequestMethod.POST)
	@ResponseBody
	public Meal newKidMeals(@PathVariable Integer kid_kidId, @RequestBody Meal meal) {
		kidService.saveKidMeals(kid_kidId, meal);
		return mealDAO.findMealByPrimaryKey(meal.getMealId());
	}
}