package ehealth.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import ehealth.dao.DoctorDAO;
import ehealth.dao.FoodValidationDAO;
import ehealth.dao.KidDAO;
import ehealth.dao.ParentDAO;
import ehealth.dao.TimestampDAO;
import ehealth.dao.UserDAO;
import ehealth.dao.UserRoleDAO;
import ehealth.domain.Doctor;
import ehealth.domain.FoodValidation;
import ehealth.domain.Kid;
import ehealth.domain.Parent;
import ehealth.domain.Timestamp;
import ehealth.domain.User;
import ehealth.domain.UserRole;
import ehealth.service.UserService;

/**
 * Spring Rest controller that handles CRUD requests for User entities
 * 
 */

@Controller("UserRestController")

public class UserRestController {

	/**
	 * DAO injected by Spring that manages Doctor entities
	 * 
	 */
	@Autowired
	private DoctorDAO doctorDAO;

	/**
	 * DAO injected by Spring that manages FoodValidation entities
	 * 
	 */
	@Autowired
	private FoodValidationDAO foodValidationDAO;

	/**
	 * DAO injected by Spring that manages Kid entities
	 * 
	 */
	@Autowired
	private KidDAO kidDAO;

	/**
	 * DAO injected by Spring that manages Parent entities
	 * 
	 */
	@Autowired
	private ParentDAO parentDAO;

	/**
	 * DAO injected by Spring that manages Timestamp entities
	 * 
	 */
	@Autowired
	private TimestampDAO timestampDAO;

	/**
	 * DAO injected by Spring that manages User entities
	 * 
	 */
	@Autowired
	private UserDAO userDAO;

	/**
	 * DAO injected by Spring that manages UserRole entities
	 * 
	 */
	@Autowired
	private UserRoleDAO userRoleDAO;

	/**
	 * Service injected by Spring that provides CRUD operations for User entities
	 * 
	 */
	@Autowired
	private UserService userService;

	/**
	 * Create a new Doctor entity
	 * 
	 */
	@RequestMapping(value = "/User/{user_userId}/doctors", method = RequestMethod.POST)
	@ResponseBody
	public Doctor newUserDoctors(@PathVariable Integer user_userId, @RequestBody Doctor doctor) {
		userService.saveUserDoctors(user_userId, doctor);
		return doctorDAO.findDoctorByPrimaryKey(doctor.getDoctorId());
	}

	/**
	* Register custom, context-specific property editors
	* 
	*/
	@InitBinder
	public void initBinder(WebDataBinder binder, HttpServletRequest request) { // Register static property editors.
		binder.registerCustomEditor(java.util.Calendar.class, new org.skyway.spring.util.databinding.CustomCalendarEditor());
		binder.registerCustomEditor(byte[].class, new org.springframework.web.multipart.support.ByteArrayMultipartFileEditor());
		binder.registerCustomEditor(boolean.class, new org.skyway.spring.util.databinding.EnhancedBooleanEditor(false));
		binder.registerCustomEditor(Boolean.class, new org.skyway.spring.util.databinding.EnhancedBooleanEditor(true));
		binder.registerCustomEditor(java.math.BigDecimal.class, new org.skyway.spring.util.databinding.NaNHandlingNumberEditor(java.math.BigDecimal.class, true));
		binder.registerCustomEditor(Integer.class, new org.skyway.spring.util.databinding.NaNHandlingNumberEditor(Integer.class, true));
		binder.registerCustomEditor(java.util.Date.class, new org.skyway.spring.util.databinding.CustomDateEditor());
		binder.registerCustomEditor(String.class, new org.skyway.spring.util.databinding.StringEditor());
		binder.registerCustomEditor(Long.class, new org.skyway.spring.util.databinding.NaNHandlingNumberEditor(Long.class, true));
		binder.registerCustomEditor(Double.class, new org.skyway.spring.util.databinding.NaNHandlingNumberEditor(Double.class, true));
	}

	/**
	* Delete an existing User entity
	* 
	*/
	@RequestMapping(value = "/User/{user_userId}", method = RequestMethod.DELETE)
	@ResponseBody
	public void deleteUser(@PathVariable Integer user_userId) {
		User user = userDAO.findUserByPrimaryKey(user_userId);
		userService.deleteUser(user);
	}

	/**
	* Save an existing Parent entity
	* 
	*/
	@RequestMapping(value = "/User/{user_userId}/parents", method = RequestMethod.PUT)
	@ResponseBody
	public Parent saveUserParents(@PathVariable Integer user_userId, @RequestBody Parent parents) {
		userService.saveUserParents(user_userId, parents);
		return parentDAO.findParentByPrimaryKey(parents.getParentId());
	}

	/**
	* View an existing UserRole entity
	* 
	*/
	@RequestMapping(value = "/User/{user_userId}/userRole/{userrole_userRoleId}", method = RequestMethod.GET)
	@ResponseBody
	public UserRole loadUserUserRole(@PathVariable Integer user_userId, @PathVariable Integer related_userrole_userRoleId) {
		UserRole userrole = userRoleDAO.findUserRoleByPrimaryKey(related_userrole_userRoleId, -1, -1);

		return userrole;
	}

	/**
	* Delete an existing FoodValidation entity
	* 
	*/
	@RequestMapping(value = "/User/{user_userId}/foodValidations/{foodvalidation_foodValidationId}", method = RequestMethod.DELETE)
	@ResponseBody
	public void deleteUserFoodValidations(@PathVariable Integer user_userId, @PathVariable Integer related_foodvalidations_foodValidationId) {
		userService.deleteUserFoodValidations(user_userId, related_foodvalidations_foodValidationId);
	}

	/**
	* Get Timestamp entity by User
	* 
	*/
	@RequestMapping(value = "/User/{user_userId}/timestamp", method = RequestMethod.GET)
	@ResponseBody
	public Timestamp getUserTimestamp(@PathVariable Integer user_userId) {
		return userDAO.findUserByPrimaryKey(user_userId).getTimestamp();
	}

	/**
	* Save an existing UserRole entity
	* 
	*/
	@RequestMapping(value = "/User/{user_userId}/userRole", method = RequestMethod.PUT)
	@ResponseBody
	public UserRole saveUserUserRole(@PathVariable Integer user_userId, @RequestBody UserRole userrole) {
		userService.saveUserUserRole(user_userId, userrole);
		return userRoleDAO.findUserRoleByPrimaryKey(userrole.getUserRoleId());
	}

	/**
	* Get UserRole entity by User
	* 
	*/
	@RequestMapping(value = "/User/{user_userId}/userRole", method = RequestMethod.GET)
	@ResponseBody
	public UserRole getUserUserRole(@PathVariable Integer user_userId) {
		return userDAO.findUserByPrimaryKey(user_userId).getUserRole();
	}

	/**
	* View an existing Parent entity
	* 
	*/
	@RequestMapping(value = "/User/{user_userId}/parents/{parent_parentId}", method = RequestMethod.GET)
	@ResponseBody
	public Parent loadUserParents(@PathVariable Integer user_userId, @PathVariable Integer related_parents_parentId) {
		Parent parent = parentDAO.findParentByPrimaryKey(related_parents_parentId, -1, -1);

		return parent;
	}

	/**
	* Show all FoodValidation entities by User
	* 
	*/
	@RequestMapping(value = "/User/{user_userId}/foodValidations", method = RequestMethod.GET)
	@ResponseBody
	public List<FoodValidation> getUserFoodValidations(@PathVariable Integer user_userId) {
		return new java.util.ArrayList<FoodValidation>(userDAO.findUserByPrimaryKey(user_userId).getFoodValidations());
	}

	/**
	* View an existing FoodValidation entity
	* 
	*/
	@RequestMapping(value = "/User/{user_userId}/foodValidations/{foodvalidation_foodValidationId}", method = RequestMethod.GET)
	@ResponseBody
	public FoodValidation loadUserFoodValidations(@PathVariable Integer user_userId, @PathVariable Integer related_foodvalidations_foodValidationId) {
		FoodValidation foodvalidation = foodValidationDAO.findFoodValidationByPrimaryKey(related_foodvalidations_foodValidationId, -1, -1);

		return foodvalidation;
	}

	/**
	* Show all User entities
	* 
	*/
	@RequestMapping(value = "/User", method = RequestMethod.GET)
	@ResponseBody
	public List<User> listUsers() {
		return new java.util.ArrayList<User>(userService.loadUsers());
	}

	/**
	* Create a new UserRole entity
	* 
	*/
	@RequestMapping(value = "/User/{user_userId}/userRole", method = RequestMethod.POST)
	@ResponseBody
	public UserRole newUserUserRole(@PathVariable Integer user_userId, @RequestBody UserRole userrole) {
		userService.saveUserUserRole(user_userId, userrole);
		return userRoleDAO.findUserRoleByPrimaryKey(userrole.getUserRoleId());
	}

	/**
	* Save an existing Doctor entity
	* 
	*/
	@RequestMapping(value = "/User/{user_userId}/doctors", method = RequestMethod.PUT)
	@ResponseBody
	public Doctor saveUserDoctors(@PathVariable Integer user_userId, @RequestBody Doctor doctors) {
		userService.saveUserDoctors(user_userId, doctors);
		return doctorDAO.findDoctorByPrimaryKey(doctors.getDoctorId());
	}

	/**
	* Delete an existing Doctor entity
	* 
	*/
	@RequestMapping(value = "/User/{user_userId}/doctors/{doctor_doctorId}", method = RequestMethod.DELETE)
	@ResponseBody
	public void deleteUserDoctors(@PathVariable Integer user_userId, @PathVariable Integer related_doctors_doctorId) {
		userService.deleteUserDoctors(user_userId, related_doctors_doctorId);
	}

	/**
	* Show all Parent entities by User
	* 
	*/
	@RequestMapping(value = "/User/{user_userId}/parents", method = RequestMethod.GET)
	@ResponseBody
	public List<Parent> getUserParents(@PathVariable Integer user_userId) {
		return new java.util.ArrayList<Parent>(userDAO.findUserByPrimaryKey(user_userId).getParents());
	}

	/**
	* Delete an existing UserRole entity
	* 
	*/
	@RequestMapping(value = "/User/{user_userId}/userRole/{userrole_userRoleId}", method = RequestMethod.DELETE)
	@ResponseBody
	public void deleteUserUserRole(@PathVariable Integer user_userId, @PathVariable Integer related_userrole_userRoleId) {
		userService.deleteUserUserRole(user_userId, related_userrole_userRoleId);
	}

	/**
	* Save an existing Timestamp entity
	* 
	*/
	@RequestMapping(value = "/User/{user_userId}/timestamp", method = RequestMethod.PUT)
	@ResponseBody
	public Timestamp saveUserTimestamp(@PathVariable Integer user_userId, @RequestBody Timestamp timestamp) {
		userService.saveUserTimestamp(user_userId, timestamp);
		return timestampDAO.findTimestampByPrimaryKey(timestamp.getTimestampId());
	}

	/**
	* Show all Doctor entities by User
	* 
	*/
	@RequestMapping(value = "/User/{user_userId}/doctor", method = RequestMethod.GET)
	@ResponseBody
	public Doctor getUserDoctors(@PathVariable Integer user_userId) {
		
		return (Doctor) (userDAO.findUserByPrimaryKey(user_userId).getDoctors());
	}

	/**
	* Select an existing User entity
	* 
	*/
	@RequestMapping(value = "/User/{user_userId}", method = RequestMethod.GET)
	@ResponseBody
	public User loadUser(@PathVariable Integer user_userId) {
		return userDAO.findUserByPrimaryKey(user_userId);
	}

	/**
	* Show all Kid entities by User
	* 
	*/
	@RequestMapping(value = "/User/{user_userId}/kids", method = RequestMethod.GET)
	@ResponseBody
	public List<Kid> getUserKids(@PathVariable Integer user_userId) {
		return new java.util.ArrayList<Kid>(userDAO.findUserByPrimaryKey(user_userId).getKids());
	}

	/**
	* View an existing Timestamp entity
	* 
	*/
	@RequestMapping(value = "/User/{user_userId}/timestamp/{timestamp_timestampId}", method = RequestMethod.GET)
	@ResponseBody
	public Timestamp loadUserTimestamp(@PathVariable Integer user_userId, @PathVariable Integer related_timestamp_timestampId) {
		Timestamp timestamp = timestampDAO.findTimestampByPrimaryKey(related_timestamp_timestampId, -1, -1);

		return timestamp;
	}

	/**
	* View an existing Kid entity
	* 
	*/
	@RequestMapping(value = "/User/{user_userId}/kids/{kid_kidId}", method = RequestMethod.GET)
	@ResponseBody
	public Kid loadUserKids(@PathVariable Integer user_userId, @PathVariable Integer related_kids_kidId) {
		Kid kid = kidDAO.findKidByPrimaryKey(related_kids_kidId, -1, -1);

		return kid;
	}

	/**
	* Save an existing Kid entity
	* 
	*/
	@RequestMapping(value = "/User/{user_userId}/kids", method = RequestMethod.PUT)
	@ResponseBody
	public Kid saveUserKids(@PathVariable Integer user_userId, @RequestBody Kid kids) {
		userService.saveUserKids(user_userId, kids);
		return kidDAO.findKidByPrimaryKey(kids.getKidId());
	}

	/**
	* Delete an existing Timestamp entity
	* 
	*/
	@RequestMapping(value = "/User/{user_userId}/timestamp/{timestamp_timestampId}", method = RequestMethod.DELETE)
	@ResponseBody
	public void deleteUserTimestamp(@PathVariable Integer user_userId, @PathVariable Integer related_timestamp_timestampId) {
		userService.deleteUserTimestamp(user_userId, related_timestamp_timestampId);
	}

	/**
	* Create a new Kid entity
	* 
	*/
	@RequestMapping(value = "/User/{user_userId}/kids", method = RequestMethod.POST)
	@ResponseBody
	public Kid newUserKids(@PathVariable Integer user_userId, @RequestBody Kid kid) {
		userService.saveUserKids(user_userId, kid);
		return kidDAO.findKidByPrimaryKey(kid.getKidId());
	}

	/**
	* Delete an existing Kid entity
	* 
	*/
	@RequestMapping(value = "/User/{user_userId}/kids/{kid_kidId}", method = RequestMethod.DELETE)
	@ResponseBody
	public void deleteUserKids(@PathVariable Integer user_userId, @PathVariable Integer related_kids_kidId) {
		userService.deleteUserKids(user_userId, related_kids_kidId);
	}

	/**
	* Delete an existing Parent entity
	* 
	*/
	@RequestMapping(value = "/User/{user_userId}/parents/{parent_parentId}", method = RequestMethod.DELETE)
	@ResponseBody
	public void deleteUserParents(@PathVariable Integer user_userId, @PathVariable Integer related_parents_parentId) {
		userService.deleteUserParents(user_userId, related_parents_parentId);
	}

	/**
	* Create a new Parent entity
	* 
	*/
	@RequestMapping(value = "/User/{user_userId}/parents", method = RequestMethod.POST)
	@ResponseBody
	public Parent newUserParents(@PathVariable Integer user_userId, @RequestBody Parent parent) {
		userService.saveUserParents(user_userId, parent);
		return parentDAO.findParentByPrimaryKey(parent.getParentId());
	}

	/**
	* View an existing Doctor entity
	* 
	*/
	@RequestMapping(value = "/User/{user_userId}/doctors/{doctor_doctorId}", method = RequestMethod.GET)
	@ResponseBody
	public Doctor loadUserDoctors(@PathVariable Integer user_userId, @PathVariable Integer related_doctors_doctorId) {
		Doctor doctor = doctorDAO.findDoctorByPrimaryKey(related_doctors_doctorId, -1, -1);

		return doctor;
	}

	/**
	* Create a new Timestamp entity
	* 
	*/
	@RequestMapping(value = "/User/{user_userId}/timestamp", method = RequestMethod.POST)
	@ResponseBody
	public Timestamp newUserTimestamp(@PathVariable Integer user_userId, @RequestBody Timestamp timestamp) {
		userService.saveUserTimestamp(user_userId, timestamp);
		return timestampDAO.findTimestampByPrimaryKey(timestamp.getTimestampId());
	}

	/**
	* Save an existing FoodValidation entity
	* 
	*/
	@RequestMapping(value = "/User/{user_userId}/foodValidations", method = RequestMethod.PUT)
	@ResponseBody
	public FoodValidation saveUserFoodValidations(@PathVariable Integer user_userId, @RequestBody FoodValidation foodvalidations) {
		userService.saveUserFoodValidations(user_userId, foodvalidations);
		return foodValidationDAO.findFoodValidationByPrimaryKey(foodvalidations.getFoodValidationId());
	}

	/**
	* Save an existing User entity
	* 
	*/
	@RequestMapping(value = "/User", method = RequestMethod.PUT)
	@ResponseBody
	public User saveUser(@RequestBody User user) {
		userService.saveUser(user);
		return userDAO.findUserByPrimaryKey(user.getUserId());
	}

	/**
	* Create a new User entity
	* 
	*/
	@RequestMapping(value = "/User", method = RequestMethod.POST)
	@ResponseBody
	public User newUser(@RequestBody User user) {
		userService.saveUser(user);
		return userDAO.findUserByPrimaryKey(user.getUserId());
	}

	/**
	* Create a new FoodValidation entity
	* 
	*/
	@RequestMapping(value = "/User/{user_userId}/foodValidations", method = RequestMethod.POST)
	@ResponseBody
	public FoodValidation newUserFoodValidations(@PathVariable Integer user_userId, @RequestBody FoodValidation foodvalidation) {
		userService.saveUserFoodValidations(user_userId, foodvalidation);
		return foodValidationDAO.findFoodValidationByPrimaryKey(foodvalidation.getFoodValidationId());
	}
}