package ehealth.controller;

import ehealth.dao.ChatRecordDAO;
import ehealth.dao.KidChatDAO;
import ehealth.dao.KidDAO;

import ehealth.domain.ChatRecord;
import ehealth.domain.Kid;
import ehealth.domain.KidChat;

import ehealth.service.KidChatService;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Controller;

import org.springframework.web.bind.WebDataBinder;

import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Spring Rest controller that handles CRUD requests for KidChat entities
 * 
 */

@Controller("KidChatRestController")

public class KidChatRestController {

	/**
	 * DAO injected by Spring that manages ChatRecord entities
	 * 
	 */
	@Autowired
	private ChatRecordDAO chatRecordDAO;

	/**
	 * DAO injected by Spring that manages KidChat entities
	 * 
	 */
	@Autowired
	private KidChatDAO kidChatDAO;

	/**
	 * DAO injected by Spring that manages Kid entities
	 * 
	 */
	@Autowired
	private KidDAO kidDAO;

	/**
	 * Service injected by Spring that provides CRUD operations for KidChat entities
	 * 
	 */
	@Autowired
	private KidChatService kidChatService;

	/**
	 * Save an existing KidChat entity
	 * 
	 */
	@RequestMapping(value = "/KidChat", method = RequestMethod.PUT)
	@ResponseBody
	public KidChat saveKidChat(@RequestBody KidChat kidchat) {
		kidChatService.saveKidChat(kidchat);
		return kidChatDAO.findKidChatByPrimaryKey(kidchat.getKidChatId());
	}

	/**
	* Delete an existing Kid entity
	* 
	*/
	@RequestMapping(value = "/KidChat/{kidchat_kidChatId}/kidByKidId2/{kid_kidId}", method = RequestMethod.DELETE)
	@ResponseBody
	public void deleteKidChatKidByKidId2(@PathVariable Integer kidchat_kidChatId, @PathVariable Integer related_kidbykidid2_kidId) {
		kidChatService.deleteKidChatKidByKidId2(kidchat_kidChatId, related_kidbykidid2_kidId);
	}

	/**
	* Get Kid entity by KidChat
	* 
	*/
	@RequestMapping(value = "/KidChat/{kidchat_kidChatId}/kidByKidId1", method = RequestMethod.GET)
	@ResponseBody
	public Kid getKidChatKidByKidId1(@PathVariable Integer kidchat_kidChatId) {
		return kidChatDAO.findKidChatByPrimaryKey(kidchat_kidChatId).getKidByKidId1();
	}

	/**
	* Create a new KidChat entity
	* 
	*/
	@RequestMapping(value = "/KidChat", method = RequestMethod.POST)
	@ResponseBody
	public KidChat newKidChat(@RequestBody KidChat kidchat) {
		kidChatService.saveKidChat(kidchat);
		return kidChatDAO.findKidChatByPrimaryKey(kidchat.getKidChatId());
	}

	/**
	* Delete an existing KidChat entity
	* 
	*/
	@RequestMapping(value = "/KidChat/{kidchat_kidChatId}", method = RequestMethod.DELETE)
	@ResponseBody
	public void deleteKidChat(@PathVariable Integer kidchat_kidChatId) {
		KidChat kidchat = kidChatDAO.findKidChatByPrimaryKey(kidchat_kidChatId);
		kidChatService.deleteKidChat(kidchat);
	}

	/**
	* Register custom, context-specific property editors
	* 
	*/
	@InitBinder
	public void initBinder(WebDataBinder binder, HttpServletRequest request) { // Register static property editors.
		binder.registerCustomEditor(java.util.Calendar.class, new org.skyway.spring.util.databinding.CustomCalendarEditor());
		binder.registerCustomEditor(byte[].class, new org.springframework.web.multipart.support.ByteArrayMultipartFileEditor());
		binder.registerCustomEditor(boolean.class, new org.skyway.spring.util.databinding.EnhancedBooleanEditor(false));
		binder.registerCustomEditor(Boolean.class, new org.skyway.spring.util.databinding.EnhancedBooleanEditor(true));
		binder.registerCustomEditor(java.math.BigDecimal.class, new org.skyway.spring.util.databinding.NaNHandlingNumberEditor(java.math.BigDecimal.class, true));
		binder.registerCustomEditor(Integer.class, new org.skyway.spring.util.databinding.NaNHandlingNumberEditor(Integer.class, true));
		binder.registerCustomEditor(java.util.Date.class, new org.skyway.spring.util.databinding.CustomDateEditor());
		binder.registerCustomEditor(String.class, new org.skyway.spring.util.databinding.StringEditor());
		binder.registerCustomEditor(Long.class, new org.skyway.spring.util.databinding.NaNHandlingNumberEditor(Long.class, true));
		binder.registerCustomEditor(Double.class, new org.skyway.spring.util.databinding.NaNHandlingNumberEditor(Double.class, true));
	}

	/**
	* Get Kid entity by KidChat
	* 
	*/
	@RequestMapping(value = "/KidChat/{kidchat_kidChatId}/kidByKidId2", method = RequestMethod.GET)
	@ResponseBody
	public Kid getKidChatKidByKidId2(@PathVariable Integer kidchat_kidChatId) {
		return kidChatDAO.findKidChatByPrimaryKey(kidchat_kidChatId).getKidByKidId2();
	}

	/**
	* Get ChatRecord entity by KidChat
	* 
	*/
	@RequestMapping(value = "/KidChat/{kidchat_kidChatId}/chatRecord", method = RequestMethod.GET)
	@ResponseBody
	public ChatRecord getKidChatChatRecord(@PathVariable Integer kidchat_kidChatId) {
		return kidChatDAO.findKidChatByPrimaryKey(kidchat_kidChatId).getChatRecord();
	}

	/**
	* View an existing Kid entity
	* 
	*/
	@RequestMapping(value = "/KidChat/{kidchat_kidChatId}/kidByKidId1/{kid_kidId}", method = RequestMethod.GET)
	@ResponseBody
	public Kid loadKidChatKidByKidId1(@PathVariable Integer kidchat_kidChatId, @PathVariable Integer related_kidbykidid1_kidId) {
		Kid kid = kidDAO.findKidByPrimaryKey(related_kidbykidid1_kidId, -1, -1);

		return kid;
	}

	/**
	* Delete an existing Kid entity
	* 
	*/
	@RequestMapping(value = "/KidChat/{kidchat_kidChatId}/kidByKidId1/{kid_kidId}", method = RequestMethod.DELETE)
	@ResponseBody
	public void deleteKidChatKidByKidId1(@PathVariable Integer kidchat_kidChatId, @PathVariable Integer related_kidbykidid1_kidId) {
		kidChatService.deleteKidChatKidByKidId1(kidchat_kidChatId, related_kidbykidid1_kidId);
	}

	/**
	* View an existing ChatRecord entity
	* 
	*/
	@RequestMapping(value = "/KidChat/{kidchat_kidChatId}/chatRecord/{chatrecord_chatRecordId}", method = RequestMethod.GET)
	@ResponseBody
	public ChatRecord loadKidChatChatRecord(@PathVariable Integer kidchat_kidChatId, @PathVariable Integer related_chatrecord_chatRecordId) {
		ChatRecord chatrecord = chatRecordDAO.findChatRecordByPrimaryKey(related_chatrecord_chatRecordId, -1, -1);

		return chatrecord;
	}

	/**
	* Save an existing Kid entity
	* 
	*/
	@RequestMapping(value = "/KidChat/{kidchat_kidChatId}/kidByKidId2", method = RequestMethod.PUT)
	@ResponseBody
	public Kid saveKidChatKidByKidId2(@PathVariable Integer kidchat_kidChatId, @RequestBody Kid kidbykidid2) {
		kidChatService.saveKidChatKidByKidId2(kidchat_kidChatId, kidbykidid2);
		return kidDAO.findKidByPrimaryKey(kidbykidid2.getKidId());
	}

	/**
	* Create a new Kid entity
	* 
	*/
	@RequestMapping(value = "/KidChat/{kidchat_kidChatId}/kidByKidId2", method = RequestMethod.POST)
	@ResponseBody
	public Kid newKidChatKidByKidId2(@PathVariable Integer kidchat_kidChatId, @RequestBody Kid kid) {
		kidChatService.saveKidChatKidByKidId2(kidchat_kidChatId, kid);
		return kidDAO.findKidByPrimaryKey(kid.getKidId());
	}

	/**
	* Create a new Kid entity
	* 
	*/
	@RequestMapping(value = "/KidChat/{kidchat_kidChatId}/kidByKidId1", method = RequestMethod.POST)
	@ResponseBody
	public Kid newKidChatKidByKidId1(@PathVariable Integer kidchat_kidChatId, @RequestBody Kid kid) {
		kidChatService.saveKidChatKidByKidId1(kidchat_kidChatId, kid);
		return kidDAO.findKidByPrimaryKey(kid.getKidId());
	}

	/**
	* Select an existing KidChat entity
	* 
	*/
	@RequestMapping(value = "/KidChat/{kidchat_kidChatId}", method = RequestMethod.GET)
	@ResponseBody
	public KidChat loadKidChat(@PathVariable Integer kidchat_kidChatId) {
		return kidChatDAO.findKidChatByPrimaryKey(kidchat_kidChatId);
	}

	/**
	* Delete an existing ChatRecord entity
	* 
	*/
	@RequestMapping(value = "/KidChat/{kidchat_kidChatId}/chatRecord/{chatrecord_chatRecordId}", method = RequestMethod.DELETE)
	@ResponseBody
	public void deleteKidChatChatRecord(@PathVariable Integer kidchat_kidChatId, @PathVariable Integer related_chatrecord_chatRecordId) {
		kidChatService.deleteKidChatChatRecord(kidchat_kidChatId, related_chatrecord_chatRecordId);
	}

	/**
	* View an existing Kid entity
	* 
	*/
	@RequestMapping(value = "/KidChat/{kidchat_kidChatId}/kidByKidId2/{kid_kidId}", method = RequestMethod.GET)
	@ResponseBody
	public Kid loadKidChatKidByKidId2(@PathVariable Integer kidchat_kidChatId, @PathVariable Integer related_kidbykidid2_kidId) {
		Kid kid = kidDAO.findKidByPrimaryKey(related_kidbykidid2_kidId, -1, -1);

		return kid;
	}

	/**
	* Save an existing Kid entity
	* 
	*/
	@RequestMapping(value = "/KidChat/{kidchat_kidChatId}/kidByKidId1", method = RequestMethod.PUT)
	@ResponseBody
	public Kid saveKidChatKidByKidId1(@PathVariable Integer kidchat_kidChatId, @RequestBody Kid kidbykidid1) {
		kidChatService.saveKidChatKidByKidId1(kidchat_kidChatId, kidbykidid1);
		return kidDAO.findKidByPrimaryKey(kidbykidid1.getKidId());
	}

	/**
	* Show all KidChat entities
	* 
	*/
	@RequestMapping(value = "/KidChat", method = RequestMethod.GET)
	@ResponseBody
	public List<KidChat> listKidChats() {
		return new java.util.ArrayList<KidChat>(kidChatService.loadKidChats());
	}

	/**
	* Save an existing ChatRecord entity
	* 
	*/
	@RequestMapping(value = "/KidChat/{kidchat_kidChatId}/chatRecord", method = RequestMethod.PUT)
	@ResponseBody
	public ChatRecord saveKidChatChatRecord(@PathVariable Integer kidchat_kidChatId, @RequestBody ChatRecord chatrecord) {
		kidChatService.saveKidChatChatRecord(kidchat_kidChatId, chatrecord);
		return chatRecordDAO.findChatRecordByPrimaryKey(chatrecord.getChatRecordId());
	}

	/**
	* Create a new ChatRecord entity
	* 
	*/
	@RequestMapping(value = "/KidChat/{kidchat_kidChatId}/chatRecord", method = RequestMethod.POST)
	@ResponseBody
	public ChatRecord newKidChatChatRecord(@PathVariable Integer kidchat_kidChatId, @RequestBody ChatRecord chatrecord) {
		kidChatService.saveKidChatChatRecord(kidchat_kidChatId, chatrecord);
		return chatRecordDAO.findChatRecordByPrimaryKey(chatrecord.getChatRecordId());
	}
}