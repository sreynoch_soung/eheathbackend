package ehealth.controller;

import ehealth.dao.FoodPhotoDAO;
import ehealth.dao.FoodTageInputDAO;
import ehealth.dao.FoodTageTypeDAO;

import ehealth.domain.FoodPhoto;
import ehealth.domain.FoodTageInput;
import ehealth.domain.FoodTageType;

import ehealth.service.FoodTageInputService;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Controller;

import org.springframework.web.bind.WebDataBinder;

import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Spring Rest controller that handles CRUD requests for FoodTageInput entities
 * 
 */

@Controller("FoodTageInputRestController")

public class FoodTageInputRestController {

	/**
	 * DAO injected by Spring that manages FoodPhoto entities
	 * 
	 */
	@Autowired
	private FoodPhotoDAO foodPhotoDAO;

	/**
	 * DAO injected by Spring that manages FoodTageInput entities
	 * 
	 */
	@Autowired
	private FoodTageInputDAO foodTageInputDAO;

	/**
	 * DAO injected by Spring that manages FoodTageType entities
	 * 
	 */
	@Autowired
	private FoodTageTypeDAO foodTageTypeDAO;

	/**
	 * Service injected by Spring that provides CRUD operations for FoodTageInput entities
	 * 
	 */
	@Autowired
	private FoodTageInputService foodTageInputService;

	/**
	 * Register custom, context-specific property editors
	 * 
	 */
	@InitBinder
	public void initBinder(WebDataBinder binder, HttpServletRequest request) { // Register static property editors.
		binder.registerCustomEditor(java.util.Calendar.class, new org.skyway.spring.util.databinding.CustomCalendarEditor());
		binder.registerCustomEditor(byte[].class, new org.springframework.web.multipart.support.ByteArrayMultipartFileEditor());
		binder.registerCustomEditor(boolean.class, new org.skyway.spring.util.databinding.EnhancedBooleanEditor(false));
		binder.registerCustomEditor(Boolean.class, new org.skyway.spring.util.databinding.EnhancedBooleanEditor(true));
		binder.registerCustomEditor(java.math.BigDecimal.class, new org.skyway.spring.util.databinding.NaNHandlingNumberEditor(java.math.BigDecimal.class, true));
		binder.registerCustomEditor(Integer.class, new org.skyway.spring.util.databinding.NaNHandlingNumberEditor(Integer.class, true));
		binder.registerCustomEditor(java.util.Date.class, new org.skyway.spring.util.databinding.CustomDateEditor());
		binder.registerCustomEditor(String.class, new org.skyway.spring.util.databinding.StringEditor());
		binder.registerCustomEditor(Long.class, new org.skyway.spring.util.databinding.NaNHandlingNumberEditor(Long.class, true));
		binder.registerCustomEditor(Double.class, new org.skyway.spring.util.databinding.NaNHandlingNumberEditor(Double.class, true));
	}

	/**
	* Get FoodPhoto entity by FoodTageInput
	* 
	*/
	@RequestMapping(value = "/FoodTageInput/{foodtageinput_foodTageInputId}/foodPhoto", method = RequestMethod.GET)
	@ResponseBody
	public FoodPhoto getFoodTageInputFoodPhoto(@PathVariable Integer foodtageinput_foodTageInputId) {
		return foodTageInputDAO.findFoodTageInputByPrimaryKey(foodtageinput_foodTageInputId).getFoodPhoto();
	}

	/**
	* Delete an existing FoodTageInput entity
	* 
	*/
	@RequestMapping(value = "/FoodTageInput/{foodtageinput_foodTageInputId}", method = RequestMethod.DELETE)
	@ResponseBody
	public void deleteFoodTageInput(@PathVariable Integer foodtageinput_foodTageInputId) {
		FoodTageInput foodtageinput = foodTageInputDAO.findFoodTageInputByPrimaryKey(foodtageinput_foodTageInputId);
		foodTageInputService.deleteFoodTageInput(foodtageinput);
	}

	/**
	* Save an existing FoodTageInput entity
	* 
	*/
	@RequestMapping(value = "/FoodTageInput", method = RequestMethod.PUT)
	@ResponseBody
	public FoodTageInput saveFoodTageInput(@RequestBody FoodTageInput foodtageinput) {
		foodTageInputService.saveFoodTageInput(foodtageinput);
		return foodTageInputDAO.findFoodTageInputByPrimaryKey(foodtageinput.getFoodTageInputId());
	}

	/**
	* Create a new FoodTageInput entity
	* 
	*/
	@RequestMapping(value = "/FoodTageInput", method = RequestMethod.POST)
	@ResponseBody
	public FoodTageInput newFoodTageInput(@RequestBody FoodTageInput foodtageinput) {
		foodTageInputService.saveFoodTageInput(foodtageinput);
		return foodTageInputDAO.findFoodTageInputByPrimaryKey(foodtageinput.getFoodTageInputId());
	}

	/**
	* Create a new FoodPhoto entity
	* 
	*/
	@RequestMapping(value = "/FoodTageInput/{foodtageinput_foodTageInputId}/foodPhoto", method = RequestMethod.POST)
	@ResponseBody
	public FoodPhoto newFoodTageInputFoodPhoto(@PathVariable Integer foodtageinput_foodTageInputId, @RequestBody FoodPhoto foodphoto) {
		foodTageInputService.saveFoodTageInputFoodPhoto(foodtageinput_foodTageInputId, foodphoto);
		return foodPhotoDAO.findFoodPhotoByPrimaryKey(foodphoto.getFoodPhotoId());
	}

	/**
	* Get FoodTageType entity by FoodTageInput
	* 
	*/
	@RequestMapping(value = "/FoodTageInput/{foodtageinput_foodTageInputId}/foodTageType", method = RequestMethod.GET)
	@ResponseBody
	public FoodTageType getFoodTageInputFoodTageType(@PathVariable Integer foodtageinput_foodTageInputId) {
		return foodTageInputDAO.findFoodTageInputByPrimaryKey(foodtageinput_foodTageInputId).getFoodTageType();
	}

	/**
	* Save an existing FoodTageType entity
	* 
	*/
	@RequestMapping(value = "/FoodTageInput/{foodtageinput_foodTageInputId}/foodTageType", method = RequestMethod.PUT)
	@ResponseBody
	public FoodTageType saveFoodTageInputFoodTageType(@PathVariable Integer foodtageinput_foodTageInputId, @RequestBody FoodTageType foodtagetype) {
		foodTageInputService.saveFoodTageInputFoodTageType(foodtageinput_foodTageInputId, foodtagetype);
		return foodTageTypeDAO.findFoodTageTypeByPrimaryKey(foodtagetype.getFoodTageTypeId());
	}

	/**
	* Show all FoodTageInput entities
	* 
	*/
	@RequestMapping(value = "/FoodTageInput", method = RequestMethod.GET)
	@ResponseBody
	public List<FoodTageInput> listFoodTageInputs() {
		return new java.util.ArrayList<FoodTageInput>(foodTageInputService.loadFoodTageInputs());
	}

	/**
	* Delete an existing FoodPhoto entity
	* 
	*/
	@RequestMapping(value = "/FoodTageInput/{foodtageinput_foodTageInputId}/foodPhoto/{foodphoto_foodPhotoId}", method = RequestMethod.DELETE)
	@ResponseBody
	public void deleteFoodTageInputFoodPhoto(@PathVariable Integer foodtageinput_foodTageInputId, @PathVariable Integer related_foodphoto_foodPhotoId) {
		foodTageInputService.deleteFoodTageInputFoodPhoto(foodtageinput_foodTageInputId, related_foodphoto_foodPhotoId);
	}

	/**
	* View an existing FoodPhoto entity
	* 
	*/
	@RequestMapping(value = "/FoodTageInput/{foodtageinput_foodTageInputId}/foodPhoto/{foodphoto_foodPhotoId}", method = RequestMethod.GET)
	@ResponseBody
	public FoodPhoto loadFoodTageInputFoodPhoto(@PathVariable Integer foodtageinput_foodTageInputId, @PathVariable Integer related_foodphoto_foodPhotoId) {
		FoodPhoto foodphoto = foodPhotoDAO.findFoodPhotoByPrimaryKey(related_foodphoto_foodPhotoId, -1, -1);

		return foodphoto;
	}

	/**
	* Create a new FoodTageType entity
	* 
	*/
	@RequestMapping(value = "/FoodTageInput/{foodtageinput_foodTageInputId}/foodTageType", method = RequestMethod.POST)
	@ResponseBody
	public FoodTageType newFoodTageInputFoodTageType(@PathVariable Integer foodtageinput_foodTageInputId, @RequestBody FoodTageType foodtagetype) {
		foodTageInputService.saveFoodTageInputFoodTageType(foodtageinput_foodTageInputId, foodtagetype);
		return foodTageTypeDAO.findFoodTageTypeByPrimaryKey(foodtagetype.getFoodTageTypeId());
	}

	/**
	* Delete an existing FoodTageType entity
	* 
	*/
	@RequestMapping(value = "/FoodTageInput/{foodtageinput_foodTageInputId}/foodTageType/{foodtagetype_foodTageTypeId}", method = RequestMethod.DELETE)
	@ResponseBody
	public void deleteFoodTageInputFoodTageType(@PathVariable Integer foodtageinput_foodTageInputId, @PathVariable Integer related_foodtagetype_foodTageTypeId) {
		foodTageInputService.deleteFoodTageInputFoodTageType(foodtageinput_foodTageInputId, related_foodtagetype_foodTageTypeId);
	}

	/**
	* View an existing FoodTageType entity
	* 
	*/
	@RequestMapping(value = "/FoodTageInput/{foodtageinput_foodTageInputId}/foodTageType/{foodtagetype_foodTageTypeId}", method = RequestMethod.GET)
	@ResponseBody
	public FoodTageType loadFoodTageInputFoodTageType(@PathVariable Integer foodtageinput_foodTageInputId, @PathVariable Integer related_foodtagetype_foodTageTypeId) {
		FoodTageType foodtagetype = foodTageTypeDAO.findFoodTageTypeByPrimaryKey(related_foodtagetype_foodTageTypeId, -1, -1);

		return foodtagetype;
	}

	/**
	* Select an existing FoodTageInput entity
	* 
	*/
	@RequestMapping(value = "/FoodTageInput/{foodtageinput_foodTageInputId}", method = RequestMethod.GET)
	@ResponseBody
	public FoodTageInput loadFoodTageInput(@PathVariable Integer foodtageinput_foodTageInputId) {
		return foodTageInputDAO.findFoodTageInputByPrimaryKey(foodtageinput_foodTageInputId);
	}

	/**
	* Save an existing FoodPhoto entity
	* 
	*/
	@RequestMapping(value = "/FoodTageInput/{foodtageinput_foodTageInputId}/foodPhoto", method = RequestMethod.PUT)
	@ResponseBody
	public FoodPhoto saveFoodTageInputFoodPhoto(@PathVariable Integer foodtageinput_foodTageInputId, @RequestBody FoodPhoto foodphoto) {
		foodTageInputService.saveFoodTageInputFoodPhoto(foodtageinput_foodTageInputId, foodphoto);
		return foodPhotoDAO.findFoodPhotoByPrimaryKey(foodphoto.getFoodPhotoId());
	}
}