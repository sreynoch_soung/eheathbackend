package ehealth.controller;

import ehealth.dao.TimestampDAO;
import ehealth.dao.UserDAO;

import ehealth.domain.Timestamp;
import ehealth.domain.User;

import ehealth.service.TimestampService;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Controller;

import org.springframework.web.bind.WebDataBinder;

import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Spring Rest controller that handles CRUD requests for Timestamp entities
 * 
 */

@Controller("TimestampRestController")

public class TimestampRestController {

	/**
	 * DAO injected by Spring that manages Timestamp entities
	 * 
	 */
	@Autowired
	private TimestampDAO timestampDAO;

	/**
	 * DAO injected by Spring that manages User entities
	 * 
	 */
	@Autowired
	private UserDAO userDAO;

	/**
	 * Service injected by Spring that provides CRUD operations for Timestamp entities
	 * 
	 */
	@Autowired
	private TimestampService timestampService;

	/**
	 * Delete an existing User entity
	 * 
	 */
	@RequestMapping(value = "/Timestamp/{timestamp_timestampId}/users/{user_userId}", method = RequestMethod.DELETE)
	@ResponseBody
	public void deleteTimestampUsers(@PathVariable Integer timestamp_timestampId, @PathVariable Integer related_users_userId) {
		timestampService.deleteTimestampUsers(timestamp_timestampId, related_users_userId);
	}

	/**
	* Select an existing Timestamp entity
	* 
	*/
	@RequestMapping(value = "/Timestamp/{timestamp_timestampId}", method = RequestMethod.GET)
	@ResponseBody
	public Timestamp loadTimestamp(@PathVariable Integer timestamp_timestampId) {
		return timestampDAO.findTimestampByPrimaryKey(timestamp_timestampId);
	}

	/**
	* Create a new Timestamp entity
	* 
	*/
	@RequestMapping(value = "/Timestamp", method = RequestMethod.POST)
	@ResponseBody
	public Timestamp newTimestamp(@RequestBody Timestamp timestamp) {
		timestampService.saveTimestamp(timestamp);
		return timestampDAO.findTimestampByPrimaryKey(timestamp.getTimestampId());
	}

	/**
	* Show all Timestamp entities
	* 
	*/
	@RequestMapping(value = "/Timestamp", method = RequestMethod.GET)
	@ResponseBody
	public List<Timestamp> listTimestamps() {
		return new java.util.ArrayList<Timestamp>(timestampService.loadTimestamps());
	}

	/**
	* View an existing User entity
	* 
	*/
	@RequestMapping(value = "/Timestamp/{timestamp_timestampId}/users/{user_userId}", method = RequestMethod.GET)
	@ResponseBody
	public User loadTimestampUsers(@PathVariable Integer timestamp_timestampId, @PathVariable Integer related_users_userId) {
		User user = userDAO.findUserByPrimaryKey(related_users_userId, -1, -1);

		return user;
	}

	/**
	* Create a new User entity
	* 
	*/
	@RequestMapping(value = "/Timestamp/{timestamp_timestampId}/users", method = RequestMethod.POST)
	@ResponseBody
	public User newTimestampUsers(@PathVariable Integer timestamp_timestampId, @RequestBody User user) {
		timestampService.saveTimestampUsers(timestamp_timestampId, user);
		return userDAO.findUserByPrimaryKey(user.getUserId());
	}

	/**
	* Register custom, context-specific property editors
	* 
	*/
	@InitBinder
	public void initBinder(WebDataBinder binder, HttpServletRequest request) { // Register static property editors.
		binder.registerCustomEditor(java.util.Calendar.class, new org.skyway.spring.util.databinding.CustomCalendarEditor());
		binder.registerCustomEditor(byte[].class, new org.springframework.web.multipart.support.ByteArrayMultipartFileEditor());
		binder.registerCustomEditor(boolean.class, new org.skyway.spring.util.databinding.EnhancedBooleanEditor(false));
		binder.registerCustomEditor(Boolean.class, new org.skyway.spring.util.databinding.EnhancedBooleanEditor(true));
		binder.registerCustomEditor(java.math.BigDecimal.class, new org.skyway.spring.util.databinding.NaNHandlingNumberEditor(java.math.BigDecimal.class, true));
		binder.registerCustomEditor(Integer.class, new org.skyway.spring.util.databinding.NaNHandlingNumberEditor(Integer.class, true));
		binder.registerCustomEditor(java.util.Date.class, new org.skyway.spring.util.databinding.CustomDateEditor());
		binder.registerCustomEditor(String.class, new org.skyway.spring.util.databinding.StringEditor());
		binder.registerCustomEditor(Long.class, new org.skyway.spring.util.databinding.NaNHandlingNumberEditor(Long.class, true));
		binder.registerCustomEditor(Double.class, new org.skyway.spring.util.databinding.NaNHandlingNumberEditor(Double.class, true));
	}

	/**
	* Show all User entities by Timestamp
	* 
	*/
	@RequestMapping(value = "/Timestamp/{timestamp_timestampId}/users", method = RequestMethod.GET)
	@ResponseBody
	public List<User> getTimestampUsers(@PathVariable Integer timestamp_timestampId) {
		return new java.util.ArrayList<User>(timestampDAO.findTimestampByPrimaryKey(timestamp_timestampId).getUsers());
	}

	/**
	* Save an existing Timestamp entity
	* 
	*/
	@RequestMapping(value = "/Timestamp", method = RequestMethod.PUT)
	@ResponseBody
	public Timestamp saveTimestamp(@RequestBody Timestamp timestamp) {
		timestampService.saveTimestamp(timestamp);
		return timestampDAO.findTimestampByPrimaryKey(timestamp.getTimestampId());
	}

	/**
	* Save an existing User entity
	* 
	*/
	@RequestMapping(value = "/Timestamp/{timestamp_timestampId}/users", method = RequestMethod.PUT)
	@ResponseBody
	public User saveTimestampUsers(@PathVariable Integer timestamp_timestampId, @RequestBody User users) {
		timestampService.saveTimestampUsers(timestamp_timestampId, users);
		return userDAO.findUserByPrimaryKey(users.getUserId());
	}

	/**
	* Delete an existing Timestamp entity
	* 
	*/
	@RequestMapping(value = "/Timestamp/{timestamp_timestampId}", method = RequestMethod.DELETE)
	@ResponseBody
	public void deleteTimestamp(@PathVariable Integer timestamp_timestampId) {
		Timestamp timestamp = timestampDAO.findTimestampByPrimaryKey(timestamp_timestampId);
		timestampService.deleteTimestamp(timestamp);
	}
}