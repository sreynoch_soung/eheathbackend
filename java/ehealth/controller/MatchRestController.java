package ehealth.controller;

import ehealth.dao.LeagueDAO;
import ehealth.dao.MatchDAO;
import ehealth.dao.TeamDAO;

import ehealth.domain.League;
import ehealth.domain.Match;
import ehealth.domain.Team;

import ehealth.service.MatchService;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Controller;

import org.springframework.web.bind.WebDataBinder;

import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Spring Rest controller that handles CRUD requests for Match entities
 * 
 */

@Controller("MatchRestController")

public class MatchRestController {

	/**
	 * DAO injected by Spring that manages League entities
	 * 
	 */
	@Autowired
	private LeagueDAO leagueDAO;

	/**
	 * DAO injected by Spring that manages Match entities
	 * 
	 */
	@Autowired
	private MatchDAO matchDAO;

	/**
	 * DAO injected by Spring that manages Team entities
	 * 
	 */
	@Autowired
	private TeamDAO teamDAO;

	/**
	 * Service injected by Spring that provides CRUD operations for Match entities
	 * 
	 */
	@Autowired
	private MatchService matchService;

	/**
	 * Delete an existing League entity
	 * 
	 */
	@RequestMapping(value = "/Match/{match_matchId}/leagues/{league_leagueId}", method = RequestMethod.DELETE)
	@ResponseBody
	public void deleteMatchLeagues(@PathVariable Integer match_matchId, @PathVariable Integer related_leagues_leagueId) {
		matchService.deleteMatchLeagues(match_matchId, related_leagues_leagueId);
	}

	/**
	* Create a new Team entity
	* 
	*/
	@RequestMapping(value = "/Match/{match_matchId}/teamByTeamId2", method = RequestMethod.POST)
	@ResponseBody
	public Team newMatchTeamByTeamId2(@PathVariable Integer match_matchId, @RequestBody Team team) {
		matchService.saveMatchTeamByTeamId2(match_matchId, team);
		return teamDAO.findTeamByPrimaryKey(team.getTeamId());
	}

	/**
	* Save an existing League entity
	* 
	*/
	@RequestMapping(value = "/Match/{match_matchId}/leagues", method = RequestMethod.PUT)
	@ResponseBody
	public League saveMatchLeagues(@PathVariable Integer match_matchId, @RequestBody League leagues) {
		matchService.saveMatchLeagues(match_matchId, leagues);
		return leagueDAO.findLeagueByPrimaryKey(leagues.getLeagueId());
	}

	/**
	* Create a new Team entity
	* 
	*/
	@RequestMapping(value = "/Match/{match_matchId}/teamByTeamIdWin", method = RequestMethod.POST)
	@ResponseBody
	public Team newMatchTeamByTeamIdWin(@PathVariable Integer match_matchId, @RequestBody Team team) {
		matchService.saveMatchTeamByTeamIdWin(match_matchId, team);
		return teamDAO.findTeamByPrimaryKey(team.getTeamId());
	}

	/**
	* Delete an existing Team entity
	* 
	*/
	@RequestMapping(value = "/Match/{match_matchId}/teamByTeamIdWin/{team_teamId}", method = RequestMethod.DELETE)
	@ResponseBody
	public void deleteMatchTeamByTeamIdWin(@PathVariable Integer match_matchId, @PathVariable Integer related_teambyteamidwin_teamId) {
		matchService.deleteMatchTeamByTeamIdWin(match_matchId, related_teambyteamidwin_teamId);
	}

	/**
	* Create a new Team entity
	* 
	*/
	@RequestMapping(value = "/Match/{match_matchId}/teamByTeamId1", method = RequestMethod.POST)
	@ResponseBody
	public Team newMatchTeamByTeamId1(@PathVariable Integer match_matchId, @RequestBody Team team) {
		matchService.saveMatchTeamByTeamId1(match_matchId, team);
		return teamDAO.findTeamByPrimaryKey(team.getTeamId());
	}

	/**
	* Save an existing Team entity
	* 
	*/
	@RequestMapping(value = "/Match/{match_matchId}/teamByTeamIdWin", method = RequestMethod.PUT)
	@ResponseBody
	public Team saveMatchTeamByTeamIdWin(@PathVariable Integer match_matchId, @RequestBody Team teambyteamidwin) {
		matchService.saveMatchTeamByTeamIdWin(match_matchId, teambyteamidwin);
		return teamDAO.findTeamByPrimaryKey(teambyteamidwin.getTeamId());
	}

	/**
	* Save an existing Team entity
	* 
	*/
	@RequestMapping(value = "/Match/{match_matchId}/teamByTeamId1", method = RequestMethod.PUT)
	@ResponseBody
	public Team saveMatchTeamByTeamId1(@PathVariable Integer match_matchId, @RequestBody Team teambyteamid1) {
		matchService.saveMatchTeamByTeamId1(match_matchId, teambyteamid1);
		return teamDAO.findTeamByPrimaryKey(teambyteamid1.getTeamId());
	}

	/**
	* View an existing League entity
	* 
	*/
	@RequestMapping(value = "/Match/{match_matchId}/leagues/{league_leagueId}", method = RequestMethod.GET)
	@ResponseBody
	public League loadMatchLeagues(@PathVariable Integer match_matchId, @PathVariable Integer related_leagues_leagueId) {
		League league = leagueDAO.findLeagueByPrimaryKey(related_leagues_leagueId, -1, -1);

		return league;
	}

	/**
	* Show all League entities by Match
	* 
	*/
	@RequestMapping(value = "/Match/{match_matchId}/leagues", method = RequestMethod.GET)
	@ResponseBody
	public List<League> getMatchLeagues(@PathVariable Integer match_matchId) {
		return new java.util.ArrayList<League>(matchDAO.findMatchByPrimaryKey(match_matchId).getLeagues());
	}

	/**
	* Save an existing Team entity
	* 
	*/
	@RequestMapping(value = "/Match/{match_matchId}/teamByTeamId2", method = RequestMethod.PUT)
	@ResponseBody
	public Team saveMatchTeamByTeamId2(@PathVariable Integer match_matchId, @RequestBody Team teambyteamid2) {
		matchService.saveMatchTeamByTeamId2(match_matchId, teambyteamid2);
		return teamDAO.findTeamByPrimaryKey(teambyteamid2.getTeamId());
	}

	/**
	* Get Team entity by Match
	* 
	*/
	@RequestMapping(value = "/Match/{match_matchId}/teamByTeamId2", method = RequestMethod.GET)
	@ResponseBody
	public Team getMatchTeamByTeamId2(@PathVariable Integer match_matchId) {
		return matchDAO.findMatchByPrimaryKey(match_matchId).getTeamByTeamId2();
	}

	/**
	* Get Team entity by Match
	* 
	*/
	@RequestMapping(value = "/Match/{match_matchId}/teamByTeamId1", method = RequestMethod.GET)
	@ResponseBody
	public Team getMatchTeamByTeamId1(@PathVariable Integer match_matchId) {
		return matchDAO.findMatchByPrimaryKey(match_matchId).getTeamByTeamId1();
	}

	/**
	* View an existing Team entity
	* 
	*/
	@RequestMapping(value = "/Match/{match_matchId}/teamByTeamId1/{team_teamId}", method = RequestMethod.GET)
	@ResponseBody
	public Team loadMatchTeamByTeamId1(@PathVariable Integer match_matchId, @PathVariable Integer related_teambyteamid1_teamId) {
		Team team = teamDAO.findTeamByPrimaryKey(related_teambyteamid1_teamId, -1, -1);

		return team;
	}

	/**
	* Create a new League entity
	* 
	*/
	@RequestMapping(value = "/Match/{match_matchId}/leagues", method = RequestMethod.POST)
	@ResponseBody
	public League newMatchLeagues(@PathVariable Integer match_matchId, @RequestBody League league) {
		matchService.saveMatchLeagues(match_matchId, league);
		return leagueDAO.findLeagueByPrimaryKey(league.getLeagueId());
	}

	/**
	* Select an existing Match entity
	* 
	*/
	@RequestMapping(value = "/Match/{match_matchId}", method = RequestMethod.GET)
	@ResponseBody
	public Match loadMatch(@PathVariable Integer match_matchId) {
		return matchDAO.findMatchByPrimaryKey(match_matchId);
	}

	/**
	* Delete an existing Match entity
	* 
	*/
	@RequestMapping(value = "/Match/{match_matchId}", method = RequestMethod.DELETE)
	@ResponseBody
	public void deleteMatch(@PathVariable Integer match_matchId) {
		Match match = matchDAO.findMatchByPrimaryKey(match_matchId);
		matchService.deleteMatch(match);
	}

	/**
	* View an existing Team entity
	* 
	*/
	@RequestMapping(value = "/Match/{match_matchId}/teamByTeamId2/{team_teamId}", method = RequestMethod.GET)
	@ResponseBody
	public Team loadMatchTeamByTeamId2(@PathVariable Integer match_matchId, @PathVariable Integer related_teambyteamid2_teamId) {
		Team team = teamDAO.findTeamByPrimaryKey(related_teambyteamid2_teamId, -1, -1);

		return team;
	}

	/**
	* View an existing Team entity
	* 
	*/
	@RequestMapping(value = "/Match/{match_matchId}/teamByTeamIdWin/{team_teamId}", method = RequestMethod.GET)
	@ResponseBody
	public Team loadMatchTeamByTeamIdWin(@PathVariable Integer match_matchId, @PathVariable Integer related_teambyteamidwin_teamId) {
		Team team = teamDAO.findTeamByPrimaryKey(related_teambyteamidwin_teamId, -1, -1);

		return team;
	}

	/**
	* Get Team entity by Match
	* 
	*/
	@RequestMapping(value = "/Match/{match_matchId}/teamByTeamIdWin", method = RequestMethod.GET)
	@ResponseBody
	public Team getMatchTeamByTeamIdWin(@PathVariable Integer match_matchId) {
		return matchDAO.findMatchByPrimaryKey(match_matchId).getTeamByTeamIdWin();
	}

	/**
	* Save an existing Match entity
	* 
	*/
	@RequestMapping(value = "/Match", method = RequestMethod.PUT)
	@ResponseBody
	public Match saveMatch(@RequestBody Match match) {
		matchService.saveMatch(match);
		return matchDAO.findMatchByPrimaryKey(match.getMatchId());
	}

	/**
	* Create a new Match entity
	* 
	*/
	@RequestMapping(value = "/Match", method = RequestMethod.POST)
	@ResponseBody
	public Match newMatch(@RequestBody Match match) {
		matchService.saveMatch(match);
		return matchDAO.findMatchByPrimaryKey(match.getMatchId());
	}

	/**
	* Register custom, context-specific property editors
	* 
	*/
	@InitBinder
	public void initBinder(WebDataBinder binder, HttpServletRequest request) { // Register static property editors.
		binder.registerCustomEditor(java.util.Calendar.class, new org.skyway.spring.util.databinding.CustomCalendarEditor());
		binder.registerCustomEditor(byte[].class, new org.springframework.web.multipart.support.ByteArrayMultipartFileEditor());
		binder.registerCustomEditor(boolean.class, new org.skyway.spring.util.databinding.EnhancedBooleanEditor(false));
		binder.registerCustomEditor(Boolean.class, new org.skyway.spring.util.databinding.EnhancedBooleanEditor(true));
		binder.registerCustomEditor(java.math.BigDecimal.class, new org.skyway.spring.util.databinding.NaNHandlingNumberEditor(java.math.BigDecimal.class, true));
		binder.registerCustomEditor(Integer.class, new org.skyway.spring.util.databinding.NaNHandlingNumberEditor(Integer.class, true));
		binder.registerCustomEditor(java.util.Date.class, new org.skyway.spring.util.databinding.CustomDateEditor());
		binder.registerCustomEditor(String.class, new org.skyway.spring.util.databinding.StringEditor());
		binder.registerCustomEditor(Long.class, new org.skyway.spring.util.databinding.NaNHandlingNumberEditor(Long.class, true));
		binder.registerCustomEditor(Double.class, new org.skyway.spring.util.databinding.NaNHandlingNumberEditor(Double.class, true));
	}

	/**
	* Show all Match entities
	* 
	*/
	@RequestMapping(value = "/Match", method = RequestMethod.GET)
	@ResponseBody
	public List<Match> listMatchs() {
		return new java.util.ArrayList<Match>(matchService.loadMatchs());
	}

	/**
	* Delete an existing Team entity
	* 
	*/
	@RequestMapping(value = "/Match/{match_matchId}/teamByTeamId2/{team_teamId}", method = RequestMethod.DELETE)
	@ResponseBody
	public void deleteMatchTeamByTeamId2(@PathVariable Integer match_matchId, @PathVariable Integer related_teambyteamid2_teamId) {
		matchService.deleteMatchTeamByTeamId2(match_matchId, related_teambyteamid2_teamId);
	}

	/**
	* Delete an existing Team entity
	* 
	*/
	@RequestMapping(value = "/Match/{match_matchId}/teamByTeamId1/{team_teamId}", method = RequestMethod.DELETE)
	@ResponseBody
	public void deleteMatchTeamByTeamId1(@PathVariable Integer match_matchId, @PathVariable Integer related_teambyteamid1_teamId) {
		matchService.deleteMatchTeamByTeamId1(match_matchId, related_teambyteamid1_teamId);
	}
}