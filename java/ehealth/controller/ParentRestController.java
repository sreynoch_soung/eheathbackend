package ehealth.controller;

import ehealth.dao.AccountSettingDAO;
import ehealth.dao.CityDAO;
import ehealth.dao.ParentDAO;
import ehealth.dao.ParentKidDAO;
import ehealth.dao.UserDAO;

import ehealth.domain.AccountSetting;
import ehealth.domain.City;
import ehealth.domain.Parent;
import ehealth.domain.ParentKid;
import ehealth.domain.User;

import ehealth.service.ParentService;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Controller;

import org.springframework.web.bind.WebDataBinder;

import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Spring Rest controller that handles CRUD requests for Parent entities
 * 
 */

@Controller("ParentRestController")

public class ParentRestController {

	/**
	 * DAO injected by Spring that manages AccountSetting entities
	 * 
	 */
	@Autowired
	private AccountSettingDAO accountSettingDAO;

	/**
	 * DAO injected by Spring that manages City entities
	 * 
	 */
	@Autowired
	private CityDAO cityDAO;

	/**
	 * DAO injected by Spring that manages Parent entities
	 * 
	 */
	@Autowired
	private ParentDAO parentDAO;

	/**
	 * DAO injected by Spring that manages ParentKid entities
	 * 
	 */
	@Autowired
	private ParentKidDAO parentKidDAO;

	/**
	 * DAO injected by Spring that manages User entities
	 * 
	 */
	@Autowired
	private UserDAO userDAO;

	/**
	 * Service injected by Spring that provides CRUD operations for Parent entities
	 * 
	 */
	@Autowired
	private ParentService parentService;

	/**
	 * Delete an existing ParentKid entity
	 * 
	 */
	@RequestMapping(value = "/Parent/{parent_parentId}/parentKids/{parentkid_parentKidId}", method = RequestMethod.DELETE)
	@ResponseBody
	public void deleteParentParentKids(@PathVariable Integer parent_parentId, @PathVariable Integer related_parentkids_parentKidId) {
		parentService.deleteParentParentKids(parent_parentId, related_parentkids_parentKidId);
	}

	/**
	* Delete an existing User entity
	* 
	*/
	@RequestMapping(value = "/Parent/{parent_parentId}/user/{user_userId}", method = RequestMethod.DELETE)
	@ResponseBody
	public void deleteParentUser(@PathVariable Integer parent_parentId, @PathVariable Integer related_user_userId) {
		parentService.deleteParentUser(parent_parentId, related_user_userId);
	}

	/**
	* Register custom, context-specific property editors
	* 
	*/
	@InitBinder
	public void initBinder(WebDataBinder binder, HttpServletRequest request) { // Register static property editors.
		binder.registerCustomEditor(java.util.Calendar.class, new org.skyway.spring.util.databinding.CustomCalendarEditor());
		binder.registerCustomEditor(byte[].class, new org.springframework.web.multipart.support.ByteArrayMultipartFileEditor());
		binder.registerCustomEditor(boolean.class, new org.skyway.spring.util.databinding.EnhancedBooleanEditor(false));
		binder.registerCustomEditor(Boolean.class, new org.skyway.spring.util.databinding.EnhancedBooleanEditor(true));
		binder.registerCustomEditor(java.math.BigDecimal.class, new org.skyway.spring.util.databinding.NaNHandlingNumberEditor(java.math.BigDecimal.class, true));
		binder.registerCustomEditor(Integer.class, new org.skyway.spring.util.databinding.NaNHandlingNumberEditor(Integer.class, true));
		binder.registerCustomEditor(java.util.Date.class, new org.skyway.spring.util.databinding.CustomDateEditor());
		binder.registerCustomEditor(String.class, new org.skyway.spring.util.databinding.StringEditor());
		binder.registerCustomEditor(Long.class, new org.skyway.spring.util.databinding.NaNHandlingNumberEditor(Long.class, true));
		binder.registerCustomEditor(Double.class, new org.skyway.spring.util.databinding.NaNHandlingNumberEditor(Double.class, true));
	}

	/**
	* Get User entity by Parent
	* 
	*/
	@RequestMapping(value = "/Parent/{parent_parentId}/user", method = RequestMethod.GET)
	@ResponseBody
	public User getParentUser(@PathVariable Integer parent_parentId) {
		return parentDAO.findParentByPrimaryKey(parent_parentId).getUser();
	}

	/**
	* Save an existing Parent entity
	* 
	*/
	@RequestMapping(value = "/Parent", method = RequestMethod.PUT)
	@ResponseBody
	public Parent saveParent(@RequestBody Parent parent) {
		parentService.saveParent(parent);
		return parentDAO.findParentByPrimaryKey(parent.getParentId());
	}

	/**
	* Save an existing City entity
	* 
	*/
	@RequestMapping(value = "/Parent/{parent_parentId}/city", method = RequestMethod.PUT)
	@ResponseBody
	public City saveParentCity(@PathVariable Integer parent_parentId, @RequestBody City city) {
		parentService.saveParentCity(parent_parentId, city);
		return cityDAO.findCityByPrimaryKey(city.getCityId());
	}

	/**
	* Get AccountSetting entity by Parent
	* 
	*/
	@RequestMapping(value = "/Parent/{parent_parentId}/accountSetting", method = RequestMethod.GET)
	@ResponseBody
	public AccountSetting getParentAccountSetting(@PathVariable Integer parent_parentId) {
		return parentDAO.findParentByPrimaryKey(parent_parentId).getAccountSetting();
	}

	/**
	* Create a new User entity
	* 
	*/
	@RequestMapping(value = "/Parent/{parent_parentId}/user", method = RequestMethod.POST)
	@ResponseBody
	public User newParentUser(@PathVariable Integer parent_parentId, @RequestBody User user) {
		parentService.saveParentUser(parent_parentId, user);
		return userDAO.findUserByPrimaryKey(user.getUserId());
	}

	/**
	* Create a new City entity
	* 
	*/
	@RequestMapping(value = "/Parent/{parent_parentId}/city", method = RequestMethod.POST)
	@ResponseBody
	public City newParentCity(@PathVariable Integer parent_parentId, @RequestBody City city) {
		parentService.saveParentCity(parent_parentId, city);
		return cityDAO.findCityByPrimaryKey(city.getCityId());
	}

	/**
	* Save an existing AccountSetting entity
	* 
	*/
	@RequestMapping(value = "/Parent/{parent_parentId}/accountSetting", method = RequestMethod.PUT)
	@ResponseBody
	public AccountSetting saveParentAccountSetting(@PathVariable Integer parent_parentId, @RequestBody AccountSetting accountsetting) {
		parentService.saveParentAccountSetting(parent_parentId, accountsetting);
		return accountSettingDAO.findAccountSettingByPrimaryKey(accountsetting.getAccountSettingId());
	}

	/**
	* Show all ParentKid entities by Parent
	* 
	*/
	@RequestMapping(value = "/Parent/{parent_parentId}/parentKids", method = RequestMethod.GET)
	@ResponseBody
	public List<ParentKid> getParentParentKids(@PathVariable Integer parent_parentId) {
		return new java.util.ArrayList<ParentKid>(parentDAO.findParentByPrimaryKey(parent_parentId).getParentKids());
	}

	/**
	* Delete an existing Parent entity
	* 
	*/
	@RequestMapping(value = "/Parent/{parent_parentId}", method = RequestMethod.DELETE)
	@ResponseBody
	public void deleteParent(@PathVariable Integer parent_parentId) {
		Parent parent = parentDAO.findParentByPrimaryKey(parent_parentId);
		parentService.deleteParent(parent);
	}

	/**
	* Create a new ParentKid entity
	* 
	*/
	@RequestMapping(value = "/Parent/{parent_parentId}/parentKids", method = RequestMethod.POST)
	@ResponseBody
	public ParentKid newParentParentKids(@PathVariable Integer parent_parentId, @RequestBody ParentKid parentkid) {
		parentService.saveParentParentKids(parent_parentId, parentkid);
		return parentKidDAO.findParentKidByPrimaryKey(parentkid.getParentKidId());
	}

	/**
	* View an existing City entity
	* 
	*/
	@RequestMapping(value = "/Parent/{parent_parentId}/city/{city_cityId}", method = RequestMethod.GET)
	@ResponseBody
	public City loadParentCity(@PathVariable Integer parent_parentId, @PathVariable Integer related_city_cityId) {
		City city = cityDAO.findCityByPrimaryKey(related_city_cityId, -1, -1);

		return city;
	}

	/**
	* Select an existing Parent entity
	* 
	*/
	@RequestMapping(value = "/Parent/{parent_parentId}", method = RequestMethod.GET)
	@ResponseBody
	public Parent loadParent(@PathVariable Integer parent_parentId) {
		return parentDAO.findParentByPrimaryKey(parent_parentId);
	}

	/**
	* Create a new AccountSetting entity
	* 
	*/
	@RequestMapping(value = "/Parent/{parent_parentId}/accountSetting", method = RequestMethod.POST)
	@ResponseBody
	public AccountSetting newParentAccountSetting(@PathVariable Integer parent_parentId, @RequestBody AccountSetting accountsetting) {
		parentService.saveParentAccountSetting(parent_parentId, accountsetting);
		return accountSettingDAO.findAccountSettingByPrimaryKey(accountsetting.getAccountSettingId());
	}

	/**
	* Save an existing User entity
	* 
	*/
	@RequestMapping(value = "/Parent/{parent_parentId}/user", method = RequestMethod.PUT)
	@ResponseBody
	public User saveParentUser(@PathVariable Integer parent_parentId, @RequestBody User user) {
		parentService.saveParentUser(parent_parentId, user);
		return userDAO.findUserByPrimaryKey(user.getUserId());
	}

	/**
	* Show all Parent entities
	* 
	*/
	@RequestMapping(value = "/Parent", method = RequestMethod.GET)
	@ResponseBody
	public List<Parent> listParents() {
		return new java.util.ArrayList<Parent>(parentService.loadParents());
	}

	/**
	* View an existing ParentKid entity
	* 
	*/
	@RequestMapping(value = "/Parent/{parent_parentId}/parentKids/{parentkid_parentKidId}", method = RequestMethod.GET)
	@ResponseBody
	public ParentKid loadParentParentKids(@PathVariable Integer parent_parentId, @PathVariable Integer related_parentkids_parentKidId) {
		ParentKid parentkid = parentKidDAO.findParentKidByPrimaryKey(related_parentkids_parentKidId, -1, -1);

		return parentkid;
	}

	/**
	* Delete an existing AccountSetting entity
	* 
	*/
	@RequestMapping(value = "/Parent/{parent_parentId}/accountSetting/{accountsetting_accountSettingId}", method = RequestMethod.DELETE)
	@ResponseBody
	public void deleteParentAccountSetting(@PathVariable Integer parent_parentId, @PathVariable Integer related_accountsetting_accountSettingId) {
		parentService.deleteParentAccountSetting(parent_parentId, related_accountsetting_accountSettingId);
	}

	/**
	* Create a new Parent entity
	* 
	*/
	@RequestMapping(value = "/Parent", method = RequestMethod.POST)
	@ResponseBody
	public Parent newParent(@RequestBody Parent parent) {
		parentService.saveParent(parent);
		return parentDAO.findParentByPrimaryKey(parent.getParentId());
	}

	/**
	* View an existing AccountSetting entity
	* 
	*/
	@RequestMapping(value = "/Parent/{parent_parentId}/accountSetting/{accountsetting_accountSettingId}", method = RequestMethod.GET)
	@ResponseBody
	public AccountSetting loadParentAccountSetting(@PathVariable Integer parent_parentId, @PathVariable Integer related_accountsetting_accountSettingId) {
		AccountSetting accountsetting = accountSettingDAO.findAccountSettingByPrimaryKey(related_accountsetting_accountSettingId, -1, -1);

		return accountsetting;
	}

	/**
	* Save an existing ParentKid entity
	* 
	*/
	@RequestMapping(value = "/Parent/{parent_parentId}/parentKids", method = RequestMethod.PUT)
	@ResponseBody
	public ParentKid saveParentParentKids(@PathVariable Integer parent_parentId, @RequestBody ParentKid parentkids) {
		parentService.saveParentParentKids(parent_parentId, parentkids);
		return parentKidDAO.findParentKidByPrimaryKey(parentkids.getParentKidId());
	}

	/**
	* View an existing User entity
	* 
	*/
	@RequestMapping(value = "/Parent/{parent_parentId}/user/{user_userId}", method = RequestMethod.GET)
	@ResponseBody
	public User loadParentUser(@PathVariable Integer parent_parentId, @PathVariable Integer related_user_userId) {
		User user = userDAO.findUserByPrimaryKey(related_user_userId, -1, -1);

		return user;
	}

	/**
	* Delete an existing City entity
	* 
	*/
	@RequestMapping(value = "/Parent/{parent_parentId}/city/{city_cityId}", method = RequestMethod.DELETE)
	@ResponseBody
	public void deleteParentCity(@PathVariable Integer parent_parentId, @PathVariable Integer related_city_cityId) {
		parentService.deleteParentCity(parent_parentId, related_city_cityId);
	}

	/**
	* Get City entity by Parent
	* 
	*/
	@RequestMapping(value = "/Parent/{parent_parentId}/city", method = RequestMethod.GET)
	@ResponseBody
	public City getParentCity(@PathVariable Integer parent_parentId) {
		return parentDAO.findParentByPrimaryKey(parent_parentId).getCity();
	}
}