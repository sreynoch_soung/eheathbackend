package ehealth.controller;

import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import common.RandomCodeKey;
import ehealth.dao.DoctorDAO;
import ehealth.dao.ExerciseTargetDAO;
import ehealth.dao.KidDAO;
import ehealth.dao.NutritionTargetDAO;
import ehealth.dao.UserDAO;
import ehealth.domain.Doctor;
import ehealth.domain.ExerciseTarget;
import ehealth.domain.Kid;
import ehealth.domain.NutritionTarget;
import ehealth.domain.User;
import ehealth.service.DoctorService;
import ehealth.service.UserService;

/**
 * Spring Rest controller that handles CRUD requests for Doctor entities
 * 
 */

@Controller("DoctorRestController")

public class DoctorRestController {

	/**
	 * DAO injected by Spring that manages Doctor entities
	 * 
	 */
	@Autowired
	private DoctorDAO doctorDAO;

	/**
	 * DAO injected by Spring that manages ExerciseTarget entities
	 * 
	 */
	@Autowired
	private ExerciseTargetDAO exerciseTargetDAO;

	/**
	 * DAO injected by Spring that manages Kid entities
	 * 
	 */
	@Autowired
	private KidDAO kidDAO;

	/**
	 * DAO injected by Spring that manages NutritionTarget entities
	 * 
	 */
	@Autowired
	private NutritionTargetDAO nutritionTargetDAO;

	/**
	 * DAO injected by Spring that manages User entities
	 * 
	 */
	@Autowired
	private UserDAO userDAO;

	/**
	 * Service injected by Spring that provides CRUD operations for Doctor entities
	 * 
	 */
	@Autowired
	private DoctorService doctorService;
	
	/**
	 * Service injected by Spring that provides CRUD operations for User entities
	 * 
	 */
	@Autowired
	private UserService userService;

	/**
	 * Delete an existing User entity
	 * 
	 */
	@RequestMapping(value = "/Doctor/{doctor_doctorId}/user/{user_userId}", method = RequestMethod.DELETE)
	@ResponseBody
	public void deleteDoctorUser(@PathVariable Integer doctor_doctorId, @PathVariable Integer related_user_userId) {
		doctorService.deleteDoctorUser(doctor_doctorId, related_user_userId);
	}

	/**
	* Save an existing Kid entity
	* 
	*/
	@RequestMapping(value = "/Doctor/{doctor_doctorId}/kids", method = RequestMethod.PUT)
	@ResponseBody
	public Kid saveDoctorKids(@PathVariable Integer doctor_doctorId, @RequestBody Kid kids) {
		doctorService.saveDoctorKids(doctor_doctorId, kids);
		return kidDAO.findKidByPrimaryKey(kids.getKidId());
	}

	/**
	* Select an existing Doctor entity
	* 
	*/
	@RequestMapping(value = "/Doctor_ProfileDoctor", method = RequestMethod.GET)
	@ResponseBody
	public ModelAndView loadDoctor(@RequestParam String username) {
		User user = userDAO.findUserByUsername(username);
		Set<ehealth.domain.Doctor> setDoctor = user.getDoctors();
		Doctor doctor = ((Doctor) setDoctor.toArray()[0]);
	
		ModelAndView mav = new ModelAndView();

		mav.addObject("user", user);
		mav.addObject("doctor", doctor);
		mav.setViewName("doctor/profileDoctor");
		return mav;
	}
	
	/**
	* Select an existing Doctor entity
	* 
	*/
	@RequestMapping(value = "/Doctor_NewCodeKey", method = RequestMethod.GET)
	@ResponseBody
	public ModelAndView doctor_NewCodeKey(@RequestParam String username) {
		User user = userDAO.findUserByUsername(username);
		Set<ehealth.domain.Doctor> setDoctor = user.getDoctors();
		Doctor doctor = ((Doctor) setDoctor.toArray()[0]);
		doctor.setCodeKey(uniqueCodeKey());
		doctorService.saveDoctor(doctor);
		ModelAndView mav = new ModelAndView();
		mav=new ModelAndView("redirect:/Doctor_ProfileDoctor?username="+username);
		return mav;
	}

	/**
	* Show all Kid entities by Doctor
	* 
	*/
	@RequestMapping(value = "/Doctor/{doctor_doctorId}/kids", method = RequestMethod.GET)
	@ResponseBody
	public List<Kid> getDoctorKids(@PathVariable Integer doctor_doctorId) {
		return new java.util.ArrayList<Kid>(doctorDAO.findDoctorByPrimaryKey(doctor_doctorId).getKids());
	}

	/**
	* Get User entity by Doctor
	* 
	*/
	@RequestMapping(value = "/Doctor/{doctor_doctorId}/user", method = RequestMethod.GET)
	@ResponseBody
	public User getDoctorUser(@PathVariable Integer doctor_doctorId) {
		return doctorDAO.findDoctorByPrimaryKey(doctor_doctorId).getUser();
	}
	/**
	* Get doctor entity by CodeKey
	* 
	*/
	@RequestMapping(value = "/Doctor_GetByCodeKey", method = RequestMethod.GET)
	@ResponseBody
	public String getDoctorByCodeKey(@RequestParam String codeKey) {
		ObjectMapper mapper = new ObjectMapper();
		String doctorJson = null;
		try {
			doctorJson = mapper.writeValueAsString(doctorService.findDoctorByCodeKey(codeKey));
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return doctorJson;
	}

	/**
	* Save an existing NutritionTarget entity
	* 
	*/
	@RequestMapping(value = "/Doctor/{doctor_doctorId}/nutritionTargets", method = RequestMethod.PUT)
	@ResponseBody
	public NutritionTarget saveDoctorNutritionTargets(@PathVariable Integer doctor_doctorId, @RequestBody NutritionTarget nutritiontargets) {
		doctorService.saveDoctorNutritionTargets(doctor_doctorId, nutritiontargets);
		return nutritionTargetDAO.findNutritionTargetByPrimaryKey(nutritiontargets.getNutritionTargetId());
	}

	/**
	* Delete an existing NutritionTarget entity
	* 
	*/
	@RequestMapping(value = "/Doctor/{doctor_doctorId}/nutritionTargets/{nutritiontarget_nutritionTargetId}", method = RequestMethod.DELETE)
	@ResponseBody
	public void deleteDoctorNutritionTargets(@PathVariable Integer doctor_doctorId, @PathVariable Integer related_nutritiontargets_nutritionTargetId) {
		doctorService.deleteDoctorNutritionTargets(doctor_doctorId, related_nutritiontargets_nutritionTargetId);
	}

	/**
	* Create a new User entity
	* 
	*/
	@RequestMapping(value = "/Doctor/{doctor_doctorId}/user", method = RequestMethod.POST)
	@ResponseBody
	public User newDoctorUser(@PathVariable Integer doctor_doctorId, @RequestBody User user) {
		doctorService.saveDoctorUser(doctor_doctorId, user);
		return userDAO.findUserByPrimaryKey(user.getUserId());
	}

	/**
	* Save an existing ExerciseTarget entity
	* 
	*/
	@RequestMapping(value = "/Doctor/{doctor_doctorId}/exerciseTargets", method = RequestMethod.PUT)
	@ResponseBody
	public ExerciseTarget saveDoctorExerciseTargets(@PathVariable Integer doctor_doctorId, @RequestBody ExerciseTarget exercisetargets) {
		doctorService.saveDoctorExerciseTargets(doctor_doctorId, exercisetargets);
		return exerciseTargetDAO.findExerciseTargetByPrimaryKey(exercisetargets.getExerciseTargetId());
	}

	/**
	* Delete an existing Doctor entity
	* 
	*/
	@RequestMapping(value = "/Doctor/{doctor_doctorId}", method = RequestMethod.DELETE)
	@ResponseBody
	public void deleteDoctor(@PathVariable Integer doctor_doctorId) {
		Doctor doctor = doctorDAO.findDoctorByPrimaryKey(doctor_doctorId);
		doctorService.deleteDoctor(doctor);
	}

	/**
	* View an existing User entity
	* 
	*/
	@RequestMapping(value = "/Doctor/{doctor_doctorId}/user/{user_userId}", method = RequestMethod.GET)
	@ResponseBody
	public User loadDoctorUser(@PathVariable Integer doctor_doctorId, @PathVariable Integer related_user_userId) {
		User user = userDAO.findUserByPrimaryKey(related_user_userId, -1, -1);

		return user;
	}

	/**
	* Register custom, context-specific property editors
	* 
	*/
	@InitBinder
	public void initBinder(WebDataBinder binder, HttpServletRequest request) { // Register static property editors.
		binder.registerCustomEditor(java.util.Calendar.class, new org.skyway.spring.util.databinding.CustomCalendarEditor());
		binder.registerCustomEditor(byte[].class, new org.springframework.web.multipart.support.ByteArrayMultipartFileEditor());
		binder.registerCustomEditor(boolean.class, new org.skyway.spring.util.databinding.EnhancedBooleanEditor(false));
		binder.registerCustomEditor(Boolean.class, new org.skyway.spring.util.databinding.EnhancedBooleanEditor(true));
		binder.registerCustomEditor(java.math.BigDecimal.class, new org.skyway.spring.util.databinding.NaNHandlingNumberEditor(java.math.BigDecimal.class, true));
		binder.registerCustomEditor(Integer.class, new org.skyway.spring.util.databinding.NaNHandlingNumberEditor(Integer.class, true));
		binder.registerCustomEditor(java.util.Date.class, new org.skyway.spring.util.databinding.CustomDateEditor());
		binder.registerCustomEditor(String.class, new org.skyway.spring.util.databinding.StringEditor());
		binder.registerCustomEditor(Long.class, new org.skyway.spring.util.databinding.NaNHandlingNumberEditor(Long.class, true));
		binder.registerCustomEditor(Double.class, new org.skyway.spring.util.databinding.NaNHandlingNumberEditor(Double.class, true));
	}

	/**
	* View an existing Kid entity
	* 
	*/
	@RequestMapping(value = "/Doctor/{doctor_doctorId}/kids/{kid_kidId}", method = RequestMethod.GET)
	@ResponseBody
	public Kid loadDoctorKids(@PathVariable Integer doctor_doctorId, @PathVariable Integer related_kids_kidId) {
		Kid kid = kidDAO.findKidByPrimaryKey(related_kids_kidId, -1, -1);

		return kid;
	}

	/**
	* Create a new Kid entity
	* 
	*/
	@RequestMapping(value = "/Doctor/{doctor_doctorId}/kids", method = RequestMethod.POST)
	@ResponseBody
	public Kid newDoctorKids(@PathVariable Integer doctor_doctorId, @RequestBody Kid kid) {
		doctorService.saveDoctorKids(doctor_doctorId, kid);
		return kidDAO.findKidByPrimaryKey(kid.getKidId());
	}

	/**
	* Save an existing Doctor entity
	* 
	*/
	@RequestMapping(value = "/Doctor", method = RequestMethod.PUT)
	@ResponseBody
	public Doctor saveDoctor(@RequestBody Doctor doctor) {
		doctorService.saveDoctor(doctor);
		return doctorDAO.findDoctorByPrimaryKey(doctor.getDoctorId());
	}
	
	/**
	* Update an existing Doctor entity
	* 
	*/
	@RequestMapping(value = "/Doctor/UpdateDoctor", method = RequestMethod.POST)
	public ModelAndView updateDoctor(@Valid User user, BindingResult result) {
		userService.saveUser(user);
		Set<ehealth.domain.Doctor> setDoctor = user.getDoctors();
		Doctor doctor = ((Doctor) setDoctor.toArray()[0]);
		ModelAndView mav = new ModelAndView();

		mav.addObject("user", user);
		mav.addObject("doctor", doctor);
		mav.setViewName("doctor/profileDoctor");
		return mav;
	}

	/**
	* Delete an existing Kid entity
	* 
	*/
	@RequestMapping(value = "/Doctor/{doctor_doctorId}/kids/{kid_kidId}", method = RequestMethod.DELETE)
	@ResponseBody
	public void deleteDoctorKids(@PathVariable Integer doctor_doctorId, @PathVariable Integer related_kids_kidId) {
		doctorService.deleteDoctorKids(doctor_doctorId, related_kids_kidId);
	}

	/**
	* Save an existing User entity
	* 
	*/
	@RequestMapping(value = "/Doctor/{doctor_doctorId}/user", method = RequestMethod.PUT)
	@ResponseBody
	public User saveDoctorUser(@PathVariable Integer doctor_doctorId, @RequestBody User user) {
		doctorService.saveDoctorUser(doctor_doctorId, user);
		return userDAO.findUserByPrimaryKey(user.getUserId());
	}

	/**
	* Show all NutritionTarget entities by Doctor
	* 
	*/
	@RequestMapping(value = "/Doctor/{doctor_doctorId}/nutritionTargets", method = RequestMethod.GET)
	@ResponseBody
	public List<NutritionTarget> getDoctorNutritionTargets(@PathVariable Integer doctor_doctorId) {
		return new java.util.ArrayList<NutritionTarget>(doctorDAO.findDoctorByPrimaryKey(doctor_doctorId).getNutritionTargets());
	}

	/**
	* Create a new NutritionTarget entity
	* 
	*/
	@RequestMapping(value = "/Doctor/{doctor_doctorId}/nutritionTargets", method = RequestMethod.POST)
	@ResponseBody
	public NutritionTarget newDoctorNutritionTargets(@PathVariable Integer doctor_doctorId, @RequestBody NutritionTarget nutritiontarget) {
		doctorService.saveDoctorNutritionTargets(doctor_doctorId, nutritiontarget);
		return nutritionTargetDAO.findNutritionTargetByPrimaryKey(nutritiontarget.getNutritionTargetId());
	}

	/**
	* Show all ExerciseTarget entities by Doctor
	* 
	*/
	@RequestMapping(value = "/Doctor/{doctor_doctorId}/exerciseTargets", method = RequestMethod.GET)
	@ResponseBody
	public List<ExerciseTarget> getDoctorExerciseTargets(@PathVariable Integer doctor_doctorId) {
		return new java.util.ArrayList<ExerciseTarget>(doctorDAO.findDoctorByPrimaryKey(doctor_doctorId).getExerciseTargets());
	}

	/**
	* Create a new Doctor entity
	* 
	*/
	@RequestMapping(value = "/Doctor", method = RequestMethod.POST)
	@ResponseBody
	public Doctor newDoctor(@RequestBody Doctor doctor) {
		doctorService.saveDoctor(doctor);
		return doctorDAO.findDoctorByPrimaryKey(doctor.getDoctorId());
	}

	/**
	* Delete an existing ExerciseTarget entity
	* 
	*/
	@RequestMapping(value = "/Doctor/{doctor_doctorId}/exerciseTargets/{exercisetarget_exerciseTargetId}", method = RequestMethod.DELETE)
	@ResponseBody
	public void deleteDoctorExerciseTargets(@PathVariable Integer doctor_doctorId, @PathVariable Integer related_exercisetargets_exerciseTargetId) {
		doctorService.deleteDoctorExerciseTargets(doctor_doctorId, related_exercisetargets_exerciseTargetId);
	}

	/**
	* View an existing NutritionTarget entity
	* 
	*/
	@RequestMapping(value = "/Doctor/{doctor_doctorId}/nutritionTargets/{nutritiontarget_nutritionTargetId}", method = RequestMethod.GET)
	@ResponseBody
	public NutritionTarget loadDoctorNutritionTargets(@PathVariable Integer doctor_doctorId, @PathVariable Integer related_nutritiontargets_nutritionTargetId) {
		NutritionTarget nutritiontarget = nutritionTargetDAO.findNutritionTargetByPrimaryKey(related_nutritiontargets_nutritionTargetId, -1, -1);

		return nutritiontarget;
	}

	/**
	* View an existing ExerciseTarget entity
	* 
	*/
	@RequestMapping(value = "/Doctor/{doctor_doctorId}/exerciseTargets/{exercisetarget_exerciseTargetId}", method = RequestMethod.GET)
	@ResponseBody
	public ExerciseTarget loadDoctorExerciseTargets(@PathVariable Integer doctor_doctorId, @PathVariable Integer related_exercisetargets_exerciseTargetId) {
		ExerciseTarget exercisetarget = exerciseTargetDAO.findExerciseTargetByPrimaryKey(related_exercisetargets_exerciseTargetId, -1, -1);

		return exercisetarget;
	}

	/**
	* Show all Doctor entities
	* 
	*/
	@RequestMapping(value = "/Doctor", method = RequestMethod.GET)
	@ResponseBody
	public List<Doctor> listDoctors() {
		return new java.util.ArrayList<Doctor>(doctorService.loadDoctors());
	}

	/**
	* Create a new ExerciseTarget entity
	* 
	*/
	@RequestMapping(value = "/Doctor/{doctor_doctorId}/exerciseTargets", method = RequestMethod.POST)
	@ResponseBody
	public ExerciseTarget newDoctorExerciseTargets(@PathVariable Integer doctor_doctorId, @RequestBody ExerciseTarget exercisetarget) {
		doctorService.saveDoctorExerciseTargets(doctor_doctorId, exercisetarget);
		return exerciseTargetDAO.findExerciseTargetByPrimaryKey(exercisetarget.getExerciseTargetId());
	}
	
	public String uniqueCodeKey() {
		Boolean isDubplicateKey = true;
		String codeKey = null;
		RandomCodeKey rc = new RandomCodeKey();
		
			while (isDubplicateKey) {
				codeKey = rc.randomCodeKey();
				if (doctorService.findDoctorByCodeKey(codeKey).isEmpty()) {
					isDubplicateKey = false;
				} else {
					isDubplicateKey = true;
				}
			}
			
		return codeKey;
	}
}