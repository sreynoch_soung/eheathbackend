package ehealth.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import ehealth.dao.KidDAO;
import ehealth.domain.Kid;

@Controller("TargetRestController")

public class TargetRestController {
	/**
	 * DAO injected by Spring that manages Kid entities
	 * 
	 */
	@Autowired
	private KidDAO kidDAO;

	@RequestMapping(value = "/Target_NewTarget", method = RequestMethod.GET)
	@ResponseBody
	public ModelAndView newTarget(@RequestParam int kid_id) {
		Kid kid = kidDAO.findKidByPrimaryKey(kid_id);
		
		ModelAndView mav = new ModelAndView();
		mav.addObject("kid", kid);
		mav.setViewName("target/editTarget");
		return mav;
	}
	
	@RequestMapping(value = "/Target_ListTargets", method = RequestMethod.GET)
	@ResponseBody
	public ModelAndView listTarget(@RequestParam int kid_id) {
		Kid kid = kidDAO.findKidByPrimaryKey(kid_id);
		
		ModelAndView mav = new ModelAndView();
		mav.addObject("kid", kid);
		mav.setViewName("target/listTargets");
		return mav;
	}

}
