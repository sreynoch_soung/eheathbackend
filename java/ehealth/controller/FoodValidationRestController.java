package ehealth.controller;

import ehealth.dao.FoodPhotoDAO;
import ehealth.dao.FoodValidationDAO;
import ehealth.dao.UserDAO;

import ehealth.domain.FoodPhoto;
import ehealth.domain.FoodValidation;
import ehealth.domain.User;

import ehealth.service.FoodValidationService;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Controller;

import org.springframework.web.bind.WebDataBinder;

import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Spring Rest controller that handles CRUD requests for FoodValidation entities
 * 
 */

@Controller("FoodValidationRestController")

public class FoodValidationRestController {

	/**
	 * DAO injected by Spring that manages FoodPhoto entities
	 * 
	 */
	@Autowired
	private FoodPhotoDAO foodPhotoDAO;

	/**
	 * DAO injected by Spring that manages FoodValidation entities
	 * 
	 */
	@Autowired
	private FoodValidationDAO foodValidationDAO;

	/**
	 * DAO injected by Spring that manages User entities
	 * 
	 */
	@Autowired
	private UserDAO userDAO;

	/**
	 * Service injected by Spring that provides CRUD operations for FoodValidation entities
	 * 
	 */
	@Autowired
	private FoodValidationService foodValidationService;

	/**
	 * Create a new FoodValidation entity
	 * 
	 */
	@RequestMapping(value = "/FoodValidation", method = RequestMethod.POST)
	@ResponseBody
	public FoodValidation newFoodValidation(@RequestBody FoodValidation foodvalidation) {
		foodValidationService.saveFoodValidation(foodvalidation);
		return foodValidationDAO.findFoodValidationByPrimaryKey(foodvalidation.getFoodValidationId());
	}

	/**
	* Delete an existing FoodValidation entity
	* 
	*/
	@RequestMapping(value = "/FoodValidation/{foodvalidation_foodValidationId}", method = RequestMethod.DELETE)
	@ResponseBody
	public void deleteFoodValidation(@PathVariable Integer foodvalidation_foodValidationId) {
		FoodValidation foodvalidation = foodValidationDAO.findFoodValidationByPrimaryKey(foodvalidation_foodValidationId);
		foodValidationService.deleteFoodValidation(foodvalidation);
	}

	/**
	* Register custom, context-specific property editors
	* 
	*/
	@InitBinder
	public void initBinder(WebDataBinder binder, HttpServletRequest request) { // Register static property editors.
		binder.registerCustomEditor(java.util.Calendar.class, new org.skyway.spring.util.databinding.CustomCalendarEditor());
		binder.registerCustomEditor(byte[].class, new org.springframework.web.multipart.support.ByteArrayMultipartFileEditor());
		binder.registerCustomEditor(boolean.class, new org.skyway.spring.util.databinding.EnhancedBooleanEditor(false));
		binder.registerCustomEditor(Boolean.class, new org.skyway.spring.util.databinding.EnhancedBooleanEditor(true));
		binder.registerCustomEditor(java.math.BigDecimal.class, new org.skyway.spring.util.databinding.NaNHandlingNumberEditor(java.math.BigDecimal.class, true));
		binder.registerCustomEditor(Integer.class, new org.skyway.spring.util.databinding.NaNHandlingNumberEditor(Integer.class, true));
		binder.registerCustomEditor(java.util.Date.class, new org.skyway.spring.util.databinding.CustomDateEditor());
		binder.registerCustomEditor(String.class, new org.skyway.spring.util.databinding.StringEditor());
		binder.registerCustomEditor(Long.class, new org.skyway.spring.util.databinding.NaNHandlingNumberEditor(Long.class, true));
		binder.registerCustomEditor(Double.class, new org.skyway.spring.util.databinding.NaNHandlingNumberEditor(Double.class, true));
	}

	/**
	* Create a new User entity
	* 
	*/
	@RequestMapping(value = "/FoodValidation/{foodvalidation_foodValidationId}/user", method = RequestMethod.POST)
	@ResponseBody
	public User newFoodValidationUser(@PathVariable Integer foodvalidation_foodValidationId, @RequestBody User user) {
		foodValidationService.saveFoodValidationUser(foodvalidation_foodValidationId, user);
		return userDAO.findUserByPrimaryKey(user.getUserId());
	}

	/**
	* Get FoodPhoto entity by FoodValidation
	* 
	*/
	@RequestMapping(value = "/FoodValidation/{foodvalidation_foodValidationId}/foodPhoto", method = RequestMethod.GET)
	@ResponseBody
	public FoodPhoto getFoodValidationFoodPhoto(@PathVariable Integer foodvalidation_foodValidationId) {
		return foodValidationDAO.findFoodValidationByPrimaryKey(foodvalidation_foodValidationId).getFoodPhoto();
	}

	/**
	* Create a new FoodPhoto entity
	* 
	*/
	@RequestMapping(value = "/FoodValidation/{foodvalidation_foodValidationId}/foodPhoto", method = RequestMethod.POST)
	@ResponseBody
	public FoodPhoto newFoodValidationFoodPhoto(@PathVariable Integer foodvalidation_foodValidationId, @RequestBody FoodPhoto foodphoto) {
		foodValidationService.saveFoodValidationFoodPhoto(foodvalidation_foodValidationId, foodphoto);
		return foodPhotoDAO.findFoodPhotoByPrimaryKey(foodphoto.getFoodPhotoId());
	}

	/**
	* Save an existing FoodPhoto entity
	* 
	*/
	@RequestMapping(value = "/FoodValidation/{foodvalidation_foodValidationId}/foodPhoto", method = RequestMethod.PUT)
	@ResponseBody
	public FoodPhoto saveFoodValidationFoodPhoto(@PathVariable Integer foodvalidation_foodValidationId, @RequestBody FoodPhoto foodphoto) {
		foodValidationService.saveFoodValidationFoodPhoto(foodvalidation_foodValidationId, foodphoto);
		return foodPhotoDAO.findFoodPhotoByPrimaryKey(foodphoto.getFoodPhotoId());
	}

	/**
	* Show all FoodValidation entities
	* 
	*/
	@RequestMapping(value = "/FoodValidation", method = RequestMethod.GET)
	@ResponseBody
	public List<FoodValidation> listFoodValidations() {
		return new java.util.ArrayList<FoodValidation>(foodValidationService.loadFoodValidations());
	}

	/**
	* Delete an existing User entity
	* 
	*/
	@RequestMapping(value = "/FoodValidation/{foodvalidation_foodValidationId}/user/{user_userId}", method = RequestMethod.DELETE)
	@ResponseBody
	public void deleteFoodValidationUser(@PathVariable Integer foodvalidation_foodValidationId, @PathVariable Integer related_user_userId) {
		foodValidationService.deleteFoodValidationUser(foodvalidation_foodValidationId, related_user_userId);
	}

	/**
	* Delete an existing FoodPhoto entity
	* 
	*/
	@RequestMapping(value = "/FoodValidation/{foodvalidation_foodValidationId}/foodPhoto/{foodphoto_foodPhotoId}", method = RequestMethod.DELETE)
	@ResponseBody
	public void deleteFoodValidationFoodPhoto(@PathVariable Integer foodvalidation_foodValidationId, @PathVariable Integer related_foodphoto_foodPhotoId) {
		foodValidationService.deleteFoodValidationFoodPhoto(foodvalidation_foodValidationId, related_foodphoto_foodPhotoId);
	}

	/**
	* View an existing FoodPhoto entity
	* 
	*/
	@RequestMapping(value = "/FoodValidation/{foodvalidation_foodValidationId}/foodPhoto/{foodphoto_foodPhotoId}", method = RequestMethod.GET)
	@ResponseBody
	public FoodPhoto loadFoodValidationFoodPhoto(@PathVariable Integer foodvalidation_foodValidationId, @PathVariable Integer related_foodphoto_foodPhotoId) {
		FoodPhoto foodphoto = foodPhotoDAO.findFoodPhotoByPrimaryKey(related_foodphoto_foodPhotoId, -1, -1);

		return foodphoto;
	}

	/**
	* View an existing User entity
	* 
	*/
	@RequestMapping(value = "/FoodValidation/{foodvalidation_foodValidationId}/user/{user_userId}", method = RequestMethod.GET)
	@ResponseBody
	public User loadFoodValidationUser(@PathVariable Integer foodvalidation_foodValidationId, @PathVariable Integer related_user_userId) {
		User user = userDAO.findUserByPrimaryKey(related_user_userId, -1, -1);

		return user;
	}

	/**
	* Select an existing FoodValidation entity
	* 
	*/
	@RequestMapping(value = "/FoodValidation/{foodvalidation_foodValidationId}", method = RequestMethod.GET)
	@ResponseBody
	public FoodValidation loadFoodValidation(@PathVariable Integer foodvalidation_foodValidationId) {
		return foodValidationDAO.findFoodValidationByPrimaryKey(foodvalidation_foodValidationId);
	}

	/**
	* Save an existing FoodValidation entity
	* 
	*/
	@RequestMapping(value = "/FoodValidation", method = RequestMethod.PUT)
	@ResponseBody
	public FoodValidation saveFoodValidation(@RequestBody FoodValidation foodvalidation) {
		foodValidationService.saveFoodValidation(foodvalidation);
		return foodValidationDAO.findFoodValidationByPrimaryKey(foodvalidation.getFoodValidationId());
	}

	/**
	* Save an existing User entity
	* 
	*/
	@RequestMapping(value = "/FoodValidation/{foodvalidation_foodValidationId}/user", method = RequestMethod.PUT)
	@ResponseBody
	public User saveFoodValidationUser(@PathVariable Integer foodvalidation_foodValidationId, @RequestBody User user) {
		foodValidationService.saveFoodValidationUser(foodvalidation_foodValidationId, user);
		return userDAO.findUserByPrimaryKey(user.getUserId());
	}

	/**
	* Get User entity by FoodValidation
	* 
	*/
	@RequestMapping(value = "/FoodValidation/{foodvalidation_foodValidationId}/user", method = RequestMethod.GET)
	@ResponseBody
	public User getFoodValidationUser(@PathVariable Integer foodvalidation_foodValidationId) {
		return foodValidationDAO.findFoodValidationByPrimaryKey(foodvalidation_foodValidationId).getUser();
	}
}