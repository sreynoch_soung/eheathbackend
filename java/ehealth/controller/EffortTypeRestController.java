package ehealth.controller;

import ehealth.dao.EffortTypeDAO;
import ehealth.dao.ExerciseInputDAO;

import ehealth.domain.EffortType;
import ehealth.domain.ExerciseInput;

import ehealth.service.EffortTypeService;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Controller;

import org.springframework.web.bind.WebDataBinder;

import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Spring Rest controller that handles CRUD requests for EffortType entities
 * 
 */

@Controller("EffortTypeRestController")

public class EffortTypeRestController {

	/**
	 * DAO injected by Spring that manages EffortType entities
	 * 
	 */
	@Autowired
	private EffortTypeDAO effortTypeDAO;

	/**
	 * DAO injected by Spring that manages ExerciseInput entities
	 * 
	 */
	@Autowired
	private ExerciseInputDAO exerciseInputDAO;

	/**
	 * Service injected by Spring that provides CRUD operations for EffortType entities
	 * 
	 */
	@Autowired
	private EffortTypeService effortTypeService;

	/**
	 * Delete an existing ExerciseInput entity
	 * 
	 */
	@RequestMapping(value = "/EffortType/{efforttype_effortTypeId}/exerciseInputs/{exerciseinput_exerciseInputId}", method = RequestMethod.DELETE)
	@ResponseBody
	public void deleteEffortTypeExerciseInputs(@PathVariable Integer efforttype_effortTypeId, @PathVariable Integer related_exerciseinputs_exerciseInputId) {
		effortTypeService.deleteEffortTypeExerciseInputs(efforttype_effortTypeId, related_exerciseinputs_exerciseInputId);
	}

	/**
	* Save an existing ExerciseInput entity
	* 
	*/
	@RequestMapping(value = "/EffortType/{efforttype_effortTypeId}/exerciseInputs", method = RequestMethod.PUT)
	@ResponseBody
	public ExerciseInput saveEffortTypeExerciseInputs(@PathVariable Integer efforttype_effortTypeId, @RequestBody ExerciseInput exerciseinputs) {
		effortTypeService.saveEffortTypeExerciseInputs(efforttype_effortTypeId, exerciseinputs);
		return exerciseInputDAO.findExerciseInputByPrimaryKey(exerciseinputs.getExerciseInputId());
	}

	/**
	* View an existing ExerciseInput entity
	* 
	*/
	@RequestMapping(value = "/EffortType/{efforttype_effortTypeId}/exerciseInputs/{exerciseinput_exerciseInputId}", method = RequestMethod.GET)
	@ResponseBody
	public ExerciseInput loadEffortTypeExerciseInputs(@PathVariable Integer efforttype_effortTypeId, @PathVariable Integer related_exerciseinputs_exerciseInputId) {
		ExerciseInput exerciseinput = exerciseInputDAO.findExerciseInputByPrimaryKey(related_exerciseinputs_exerciseInputId, -1, -1);

		return exerciseinput;
	}

	/**
	* Delete an existing EffortType entity
	* 
	*/
	@RequestMapping(value = "/EffortType/{efforttype_effortTypeId}", method = RequestMethod.DELETE)
	@ResponseBody
	public void deleteEffortType(@PathVariable Integer efforttype_effortTypeId) {
		EffortType efforttype = effortTypeDAO.findEffortTypeByPrimaryKey(efforttype_effortTypeId);
		effortTypeService.deleteEffortType(efforttype);
	}

	/**
	* Save an existing EffortType entity
	* 
	*/
	@RequestMapping(value = "/EffortType", method = RequestMethod.PUT)
	@ResponseBody
	public EffortType saveEffortType(@RequestBody EffortType efforttype) {
		effortTypeService.saveEffortType(efforttype);
		return effortTypeDAO.findEffortTypeByPrimaryKey(efforttype.getEffortTypeId());
	}

	/**
	* Select an existing EffortType entity
	* 
	*/
	@RequestMapping(value = "/EffortType/{efforttype_effortTypeId}", method = RequestMethod.GET)
	@ResponseBody
	public EffortType loadEffortType(@PathVariable Integer efforttype_effortTypeId) {
		return effortTypeDAO.findEffortTypeByPrimaryKey(efforttype_effortTypeId);
	}

	/**
	* Show all ExerciseInput entities by EffortType
	* 
	*/
	@RequestMapping(value = "/EffortType/{efforttype_effortTypeId}/exerciseInputs", method = RequestMethod.GET)
	@ResponseBody
	public List<ExerciseInput> getEffortTypeExerciseInputs(@PathVariable Integer efforttype_effortTypeId) {
		return new java.util.ArrayList<ExerciseInput>(effortTypeDAO.findEffortTypeByPrimaryKey(efforttype_effortTypeId).getExerciseInputs());
	}

	/**
	* Show all EffortType entities
	* 
	*/
	@RequestMapping(value = "/EffortType", method = RequestMethod.GET)
	@ResponseBody
	public List<EffortType> listEffortTypes() {
		return new java.util.ArrayList<EffortType>(effortTypeService.loadEffortTypes());
	}

	/**
	* Create a new ExerciseInput entity
	* 
	*/
	@RequestMapping(value = "/EffortType/{efforttype_effortTypeId}/exerciseInputs", method = RequestMethod.POST)
	@ResponseBody
	public ExerciseInput newEffortTypeExerciseInputs(@PathVariable Integer efforttype_effortTypeId, @RequestBody ExerciseInput exerciseinput) {
		effortTypeService.saveEffortTypeExerciseInputs(efforttype_effortTypeId, exerciseinput);
		return exerciseInputDAO.findExerciseInputByPrimaryKey(exerciseinput.getExerciseInputId());
	}

	/**
	* Register custom, context-specific property editors
	* 
	*/
	@InitBinder
	public void initBinder(WebDataBinder binder, HttpServletRequest request) { // Register static property editors.
		binder.registerCustomEditor(java.util.Calendar.class, new org.skyway.spring.util.databinding.CustomCalendarEditor());
		binder.registerCustomEditor(byte[].class, new org.springframework.web.multipart.support.ByteArrayMultipartFileEditor());
		binder.registerCustomEditor(boolean.class, new org.skyway.spring.util.databinding.EnhancedBooleanEditor(false));
		binder.registerCustomEditor(Boolean.class, new org.skyway.spring.util.databinding.EnhancedBooleanEditor(true));
		binder.registerCustomEditor(java.math.BigDecimal.class, new org.skyway.spring.util.databinding.NaNHandlingNumberEditor(java.math.BigDecimal.class, true));
		binder.registerCustomEditor(Integer.class, new org.skyway.spring.util.databinding.NaNHandlingNumberEditor(Integer.class, true));
		binder.registerCustomEditor(java.util.Date.class, new org.skyway.spring.util.databinding.CustomDateEditor());
		binder.registerCustomEditor(String.class, new org.skyway.spring.util.databinding.StringEditor());
		binder.registerCustomEditor(Long.class, new org.skyway.spring.util.databinding.NaNHandlingNumberEditor(Long.class, true));
		binder.registerCustomEditor(Double.class, new org.skyway.spring.util.databinding.NaNHandlingNumberEditor(Double.class, true));
	}

	/**
	* Create a new EffortType entity
	* 
	*/
	@RequestMapping(value = "/EffortType", method = RequestMethod.POST)
	@ResponseBody
	public EffortType newEffortType(@RequestBody EffortType efforttype) {
		effortTypeService.saveEffortType(efforttype);
		return effortTypeDAO.findEffortTypeByPrimaryKey(efforttype.getEffortTypeId());
	}
}