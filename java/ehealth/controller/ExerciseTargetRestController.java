package ehealth.controller;

import ehealth.dao.DoctorDAO;
import ehealth.dao.ExerciseTargetDAO;
import ehealth.dao.KidDAO;

import ehealth.domain.Doctor;
import ehealth.domain.ExerciseTarget;
import ehealth.domain.Kid;

import ehealth.service.ExerciseTargetService;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Controller;

import org.springframework.web.bind.WebDataBinder;

import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Spring Rest controller that handles CRUD requests for ExerciseTarget entities
 * 
 */

@Controller("ExerciseTargetRestController")

public class ExerciseTargetRestController {

	/**
	 * DAO injected by Spring that manages Doctor entities
	 * 
	 */
	@Autowired
	private DoctorDAO doctorDAO;

	/**
	 * DAO injected by Spring that manages ExerciseTarget entities
	 * 
	 */
	@Autowired
	private ExerciseTargetDAO exerciseTargetDAO;

	/**
	 * DAO injected by Spring that manages Kid entities
	 * 
	 */
	@Autowired
	private KidDAO kidDAO;

	/**
	 * Service injected by Spring that provides CRUD operations for ExerciseTarget entities
	 * 
	 */
	@Autowired
	private ExerciseTargetService exerciseTargetService;

	/**
	 * Save an existing Kid entity
	 * 
	 */
	@RequestMapping(value = "/ExerciseTarget/{exercisetarget_exerciseTargetId}/kid", method = RequestMethod.PUT)
	@ResponseBody
	public Kid saveExerciseTargetKid(@PathVariable Integer exercisetarget_exerciseTargetId, @RequestBody Kid kid) {
		exerciseTargetService.saveExerciseTargetKid(exercisetarget_exerciseTargetId, kid);
		return kidDAO.findKidByPrimaryKey(kid.getKidId());
	}

	/**
	* Delete an existing Doctor entity
	* 
	*/
	@RequestMapping(value = "/ExerciseTarget/{exercisetarget_exerciseTargetId}/doctor/{doctor_doctorId}", method = RequestMethod.DELETE)
	@ResponseBody
	public void deleteExerciseTargetDoctor(@PathVariable Integer exercisetarget_exerciseTargetId, @PathVariable Integer related_doctor_doctorId) {
		exerciseTargetService.deleteExerciseTargetDoctor(exercisetarget_exerciseTargetId, related_doctor_doctorId);
	}

	/**
	* Create a new Doctor entity
	* 
	*/
	@RequestMapping(value = "/ExerciseTarget/{exercisetarget_exerciseTargetId}/doctor", method = RequestMethod.POST)
	@ResponseBody
	public Doctor newExerciseTargetDoctor(@PathVariable Integer exercisetarget_exerciseTargetId, @RequestBody Doctor doctor) {
		exerciseTargetService.saveExerciseTargetDoctor(exercisetarget_exerciseTargetId, doctor);
		return doctorDAO.findDoctorByPrimaryKey(doctor.getDoctorId());
	}

	/**
	* View an existing Kid entity
	* 
	*/
	@RequestMapping(value = "/ExerciseTarget/{exercisetarget_exerciseTargetId}/kid/{kid_kidId}", method = RequestMethod.GET)
	@ResponseBody
	public Kid loadExerciseTargetKid(@PathVariable Integer exercisetarget_exerciseTargetId, @PathVariable Integer related_kid_kidId) {
		Kid kid = kidDAO.findKidByPrimaryKey(related_kid_kidId, -1, -1);

		return kid;
	}

	/**
	* Get Doctor entity by ExerciseTarget
	* 
	*/
	@RequestMapping(value = "/ExerciseTarget/{exercisetarget_exerciseTargetId}/doctor", method = RequestMethod.GET)
	@ResponseBody
	public Doctor getExerciseTargetDoctor(@PathVariable Integer exercisetarget_exerciseTargetId) {
		return exerciseTargetDAO.findExerciseTargetByPrimaryKey(exercisetarget_exerciseTargetId).getDoctor();
	}

	/**
	* Create a new Kid entity
	* 
	*/
	@RequestMapping(value = "/ExerciseTarget/{exercisetarget_exerciseTargetId}/kid", method = RequestMethod.POST)
	@ResponseBody
	public Kid newExerciseTargetKid(@PathVariable Integer exercisetarget_exerciseTargetId, @RequestBody Kid kid) {
		exerciseTargetService.saveExerciseTargetKid(exercisetarget_exerciseTargetId, kid);
		return kidDAO.findKidByPrimaryKey(kid.getKidId());
	}

	/**
	* Get Kid entity by ExerciseTarget
	* 
	*/
	@RequestMapping(value = "/ExerciseTarget/{exercisetarget_exerciseTargetId}/kid", method = RequestMethod.GET)
	@ResponseBody
	public Kid getExerciseTargetKid(@PathVariable Integer exercisetarget_exerciseTargetId) {
		return exerciseTargetDAO.findExerciseTargetByPrimaryKey(exercisetarget_exerciseTargetId).getKid();
	}

	/**
	* Show all ExerciseTarget entities
	* 
	*/
	@RequestMapping(value = "/ExerciseTarget", method = RequestMethod.GET)
	@ResponseBody
	public List<ExerciseTarget> listExerciseTargets() {
		return new java.util.ArrayList<ExerciseTarget>(exerciseTargetService.loadExerciseTargets());
	}

	/**
	* Create a new ExerciseTarget entity
	* 
	*/
	@RequestMapping(value = "/ExerciseTarget", method = RequestMethod.POST)
	@ResponseBody
	public ExerciseTarget newExerciseTarget(@RequestBody ExerciseTarget exercisetarget) {
		exerciseTargetService.saveExerciseTarget(exercisetarget);
		return exerciseTargetDAO.findExerciseTargetByPrimaryKey(exercisetarget.getExerciseTargetId());
	}

	/**
	* Delete an existing ExerciseTarget entity
	* 
	*/
	@RequestMapping(value = "/ExerciseTarget/{exercisetarget_exerciseTargetId}", method = RequestMethod.DELETE)
	@ResponseBody
	public void deleteExerciseTarget(@PathVariable Integer exercisetarget_exerciseTargetId) {
		ExerciseTarget exercisetarget = exerciseTargetDAO.findExerciseTargetByPrimaryKey(exercisetarget_exerciseTargetId);
		exerciseTargetService.deleteExerciseTarget(exercisetarget);
	}

	/**
	* Save an existing Doctor entity
	* 
	*/
	@RequestMapping(value = "/ExerciseTarget/{exercisetarget_exerciseTargetId}/doctor", method = RequestMethod.PUT)
	@ResponseBody
	public Doctor saveExerciseTargetDoctor(@PathVariable Integer exercisetarget_exerciseTargetId, @RequestBody Doctor doctor) {
		exerciseTargetService.saveExerciseTargetDoctor(exercisetarget_exerciseTargetId, doctor);
		return doctorDAO.findDoctorByPrimaryKey(doctor.getDoctorId());
	}

	/**
	* View an existing Doctor entity
	* 
	*/
	@RequestMapping(value = "/ExerciseTarget/{exercisetarget_exerciseTargetId}/doctor/{doctor_doctorId}", method = RequestMethod.GET)
	@ResponseBody
	public Doctor loadExerciseTargetDoctor(@PathVariable Integer exercisetarget_exerciseTargetId, @PathVariable Integer related_doctor_doctorId) {
		Doctor doctor = doctorDAO.findDoctorByPrimaryKey(related_doctor_doctorId, -1, -1);

		return doctor;
	}

	/**
	* Delete an existing Kid entity
	* 
	*/
	@RequestMapping(value = "/ExerciseTarget/{exercisetarget_exerciseTargetId}/kid/{kid_kidId}", method = RequestMethod.DELETE)
	@ResponseBody
	public void deleteExerciseTargetKid(@PathVariable Integer exercisetarget_exerciseTargetId, @PathVariable Integer related_kid_kidId) {
		exerciseTargetService.deleteExerciseTargetKid(exercisetarget_exerciseTargetId, related_kid_kidId);
	}

	/**
	* Select an existing ExerciseTarget entity
	* 
	*/
	@RequestMapping(value = "/ExerciseTarget/{exercisetarget_exerciseTargetId}", method = RequestMethod.GET)
	@ResponseBody
	public ExerciseTarget loadExerciseTarget(@PathVariable Integer exercisetarget_exerciseTargetId) {
		return exerciseTargetDAO.findExerciseTargetByPrimaryKey(exercisetarget_exerciseTargetId);
	}

	/**
	* Save an existing ExerciseTarget entity
	* 
	*/
	@RequestMapping(value = "/ExerciseTarget", method = RequestMethod.PUT)
	@ResponseBody
	public ExerciseTarget saveExerciseTarget(@RequestBody ExerciseTarget exercisetarget) {
		exerciseTargetService.saveExerciseTarget(exercisetarget);
		return exerciseTargetDAO.findExerciseTargetByPrimaryKey(exercisetarget.getExerciseTargetId());
	}

	/**
	* Register custom, context-specific property editors
	* 
	*/
	@InitBinder
	public void initBinder(WebDataBinder binder, HttpServletRequest request) { // Register static property editors.
		binder.registerCustomEditor(java.util.Calendar.class, new org.skyway.spring.util.databinding.CustomCalendarEditor());
		binder.registerCustomEditor(byte[].class, new org.springframework.web.multipart.support.ByteArrayMultipartFileEditor());
		binder.registerCustomEditor(boolean.class, new org.skyway.spring.util.databinding.EnhancedBooleanEditor(false));
		binder.registerCustomEditor(Boolean.class, new org.skyway.spring.util.databinding.EnhancedBooleanEditor(true));
		binder.registerCustomEditor(java.math.BigDecimal.class, new org.skyway.spring.util.databinding.NaNHandlingNumberEditor(java.math.BigDecimal.class, true));
		binder.registerCustomEditor(Integer.class, new org.skyway.spring.util.databinding.NaNHandlingNumberEditor(Integer.class, true));
		binder.registerCustomEditor(java.util.Date.class, new org.skyway.spring.util.databinding.CustomDateEditor());
		binder.registerCustomEditor(String.class, new org.skyway.spring.util.databinding.StringEditor());
		binder.registerCustomEditor(Long.class, new org.skyway.spring.util.databinding.NaNHandlingNumberEditor(Long.class, true));
		binder.registerCustomEditor(Double.class, new org.skyway.spring.util.databinding.NaNHandlingNumberEditor(Double.class, true));
	}
}