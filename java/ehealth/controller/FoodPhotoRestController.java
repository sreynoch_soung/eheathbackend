package ehealth.controller;

import ehealth.dao.FoodPhotoDAO;
import ehealth.dao.FoodTageInputDAO;
import ehealth.dao.FoodValidationDAO;
import ehealth.dao.MealDAO;

import ehealth.domain.FoodPhoto;
import ehealth.domain.FoodTageInput;
import ehealth.domain.FoodValidation;
import ehealth.domain.Meal;

import ehealth.service.FoodPhotoService;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Controller;

import org.springframework.web.bind.WebDataBinder;

import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Spring Rest controller that handles CRUD requests for FoodPhoto entities
 * 
 */

@Controller("FoodPhotoRestController")

public class FoodPhotoRestController {

	/**
	 * DAO injected by Spring that manages FoodPhoto entities
	 * 
	 */
	@Autowired
	private FoodPhotoDAO foodPhotoDAO;

	/**
	 * DAO injected by Spring that manages FoodTageInput entities
	 * 
	 */
	@Autowired
	private FoodTageInputDAO foodTageInputDAO;

	/**
	 * DAO injected by Spring that manages FoodValidation entities
	 * 
	 */
	@Autowired
	private FoodValidationDAO foodValidationDAO;

	/**
	 * DAO injected by Spring that manages Meal entities
	 * 
	 */
	@Autowired
	private MealDAO mealDAO;

	/**
	 * Service injected by Spring that provides CRUD operations for FoodPhoto entities
	 * 
	 */
	@Autowired
	private FoodPhotoService foodPhotoService;

	/**
	 * Create a new FoodPhoto entity
	 * 
	 */
	@RequestMapping(value = "/FoodPhoto", method = RequestMethod.POST)
	@ResponseBody
	public FoodPhoto newFoodPhoto(@RequestBody FoodPhoto foodphoto) {
		foodPhotoService.saveFoodPhoto(foodphoto);
		return foodPhotoDAO.findFoodPhotoByPrimaryKey(foodphoto.getFoodPhotoId());
	}

	/**
	* Save an existing Meal entity
	* 
	*/
	@RequestMapping(value = "/FoodPhoto/{foodphoto_foodPhotoId}/meal", method = RequestMethod.PUT)
	@ResponseBody
	public Meal saveFoodPhotoMeal(@PathVariable Integer foodphoto_foodPhotoId, @RequestBody Meal meal) {
		foodPhotoService.saveFoodPhotoMeal(foodphoto_foodPhotoId, meal);
		return mealDAO.findMealByPrimaryKey(meal.getMealId());
	}

	/**
	* Register custom, context-specific property editors
	* 
	*/
	@InitBinder
	public void initBinder(WebDataBinder binder, HttpServletRequest request) { // Register static property editors.
		binder.registerCustomEditor(java.util.Calendar.class, new org.skyway.spring.util.databinding.CustomCalendarEditor());
		binder.registerCustomEditor(byte[].class, new org.springframework.web.multipart.support.ByteArrayMultipartFileEditor());
		binder.registerCustomEditor(boolean.class, new org.skyway.spring.util.databinding.EnhancedBooleanEditor(false));
		binder.registerCustomEditor(Boolean.class, new org.skyway.spring.util.databinding.EnhancedBooleanEditor(true));
		binder.registerCustomEditor(java.math.BigDecimal.class, new org.skyway.spring.util.databinding.NaNHandlingNumberEditor(java.math.BigDecimal.class, true));
		binder.registerCustomEditor(Integer.class, new org.skyway.spring.util.databinding.NaNHandlingNumberEditor(Integer.class, true));
		binder.registerCustomEditor(java.util.Date.class, new org.skyway.spring.util.databinding.CustomDateEditor());
		binder.registerCustomEditor(String.class, new org.skyway.spring.util.databinding.StringEditor());
		binder.registerCustomEditor(Long.class, new org.skyway.spring.util.databinding.NaNHandlingNumberEditor(Long.class, true));
		binder.registerCustomEditor(Double.class, new org.skyway.spring.util.databinding.NaNHandlingNumberEditor(Double.class, true));
	}

	/**
	* Create a new FoodTageInput entity
	* 
	*/
	@RequestMapping(value = "/FoodPhoto/{foodphoto_foodPhotoId}/foodTageInputs", method = RequestMethod.POST)
	@ResponseBody
	public FoodTageInput newFoodPhotoFoodTageInputs(@PathVariable Integer foodphoto_foodPhotoId, @RequestBody FoodTageInput foodtageinput) {
		foodPhotoService.saveFoodPhotoFoodTageInputs(foodphoto_foodPhotoId, foodtageinput);
		return foodTageInputDAO.findFoodTageInputByPrimaryKey(foodtageinput.getFoodTageInputId());
	}

	/**
	* Create a new Meal entity
	* 
	*/
	@RequestMapping(value = "/FoodPhoto/{foodphoto_foodPhotoId}/meal", method = RequestMethod.POST)
	@ResponseBody
	public Meal newFoodPhotoMeal(@PathVariable Integer foodphoto_foodPhotoId, @RequestBody Meal meal) {
		foodPhotoService.saveFoodPhotoMeal(foodphoto_foodPhotoId, meal);
		return mealDAO.findMealByPrimaryKey(meal.getMealId());
	}

	/**
	* Show all FoodTageInput entities by FoodPhoto
	* 
	*/
	@RequestMapping(value = "/FoodPhoto/{foodphoto_foodPhotoId}/foodTageInputs", method = RequestMethod.GET)
	@ResponseBody
	public List<FoodTageInput> getFoodPhotoFoodTageInputs(@PathVariable Integer foodphoto_foodPhotoId) {
		return new java.util.ArrayList<FoodTageInput>(foodPhotoDAO.findFoodPhotoByPrimaryKey(foodphoto_foodPhotoId).getFoodTageInputs());
	}

	/**
	* Show all FoodPhoto entities
	* 
	*/
	@RequestMapping(value = "/FoodPhoto", method = RequestMethod.GET)
	@ResponseBody
	public List<FoodPhoto> listFoodPhotos() {
		return new java.util.ArrayList<FoodPhoto>(foodPhotoService.loadFoodPhotos());
	}

	/**
	* Delete an existing FoodTageInput entity
	* 
	*/
	@RequestMapping(value = "/FoodPhoto/{foodphoto_foodPhotoId}/foodTageInputs/{foodtageinput_foodTageInputId}", method = RequestMethod.DELETE)
	@ResponseBody
	public void deleteFoodPhotoFoodTageInputs(@PathVariable Integer foodphoto_foodPhotoId, @PathVariable Integer related_foodtageinputs_foodTageInputId) {
		foodPhotoService.deleteFoodPhotoFoodTageInputs(foodphoto_foodPhotoId, related_foodtageinputs_foodTageInputId);
	}

	/**
	* View an existing FoodTageInput entity
	* 
	*/
	@RequestMapping(value = "/FoodPhoto/{foodphoto_foodPhotoId}/foodTageInputs/{foodtageinput_foodTageInputId}", method = RequestMethod.GET)
	@ResponseBody
	public FoodTageInput loadFoodPhotoFoodTageInputs(@PathVariable Integer foodphoto_foodPhotoId, @PathVariable Integer related_foodtageinputs_foodTageInputId) {
		FoodTageInput foodtageinput = foodTageInputDAO.findFoodTageInputByPrimaryKey(related_foodtageinputs_foodTageInputId, -1, -1);

		return foodtageinput;
	}

	/**
	* Select an existing FoodPhoto entity
	* 
	*/
	@RequestMapping(value = "/FoodPhoto/{foodphoto_foodPhotoId}", method = RequestMethod.GET)
	@ResponseBody
	public FoodPhoto loadFoodPhoto(@PathVariable Integer foodphoto_foodPhotoId) {
		return foodPhotoDAO.findFoodPhotoByPrimaryKey(foodphoto_foodPhotoId);
	}

	/**
	* Save an existing FoodValidation entity
	* 
	*/
	@RequestMapping(value = "/FoodPhoto/{foodphoto_foodPhotoId}/foodValidations", method = RequestMethod.PUT)
	@ResponseBody
	public FoodValidation saveFoodPhotoFoodValidations(@PathVariable Integer foodphoto_foodPhotoId, @RequestBody FoodValidation foodvalidations) {
		foodPhotoService.saveFoodPhotoFoodValidations(foodphoto_foodPhotoId, foodvalidations);
		return foodValidationDAO.findFoodValidationByPrimaryKey(foodvalidations.getFoodValidationId());
	}

	/**
	* Create a new FoodValidation entity
	* 
	*/
	@RequestMapping(value = "/FoodPhoto/{foodphoto_foodPhotoId}/foodValidations", method = RequestMethod.POST)
	@ResponseBody
	public FoodValidation newFoodPhotoFoodValidations(@PathVariable Integer foodphoto_foodPhotoId, @RequestBody FoodValidation foodvalidation) {
		foodPhotoService.saveFoodPhotoFoodValidations(foodphoto_foodPhotoId, foodvalidation);
		return foodValidationDAO.findFoodValidationByPrimaryKey(foodvalidation.getFoodValidationId());
	}

	/**
	* Delete an existing FoodPhoto entity
	* 
	*/
	@RequestMapping(value = "/FoodPhoto/{foodphoto_foodPhotoId}", method = RequestMethod.DELETE)
	@ResponseBody
	public void deleteFoodPhoto(@PathVariable Integer foodphoto_foodPhotoId) {
		FoodPhoto foodphoto = foodPhotoDAO.findFoodPhotoByPrimaryKey(foodphoto_foodPhotoId);
		foodPhotoService.deleteFoodPhoto(foodphoto);
	}

	/**
	* Save an existing FoodTageInput entity
	* 
	*/
	@RequestMapping(value = "/FoodPhoto/{foodphoto_foodPhotoId}/foodTageInputs", method = RequestMethod.PUT)
	@ResponseBody
	public FoodTageInput saveFoodPhotoFoodTageInputs(@PathVariable Integer foodphoto_foodPhotoId, @RequestBody FoodTageInput foodtageinputs) {
		foodPhotoService.saveFoodPhotoFoodTageInputs(foodphoto_foodPhotoId, foodtageinputs);
		return foodTageInputDAO.findFoodTageInputByPrimaryKey(foodtageinputs.getFoodTageInputId());
	}

	/**
	* Get Meal entity by FoodPhoto
	* 
	*/
	@RequestMapping(value = "/FoodPhoto/{foodphoto_foodPhotoId}/meal", method = RequestMethod.GET)
	@ResponseBody
	public Meal getFoodPhotoMeal(@PathVariable Integer foodphoto_foodPhotoId) {
		return foodPhotoDAO.findFoodPhotoByPrimaryKey(foodphoto_foodPhotoId).getMeal();
	}

	/**
	* Delete an existing FoodValidation entity
	* 
	*/
	@RequestMapping(value = "/FoodPhoto/{foodphoto_foodPhotoId}/foodValidations/{foodvalidation_foodValidationId}", method = RequestMethod.DELETE)
	@ResponseBody
	public void deleteFoodPhotoFoodValidations(@PathVariable Integer foodphoto_foodPhotoId, @PathVariable Integer related_foodvalidations_foodValidationId) {
		foodPhotoService.deleteFoodPhotoFoodValidations(foodphoto_foodPhotoId, related_foodvalidations_foodValidationId);
	}

	/**
	* Save an existing FoodPhoto entity
	* 
	*/
	@RequestMapping(value = "/FoodPhoto", method = RequestMethod.PUT)
	@ResponseBody
	public FoodPhoto saveFoodPhoto(@RequestBody FoodPhoto foodphoto) {
		foodPhotoService.saveFoodPhoto(foodphoto);
		return foodPhotoDAO.findFoodPhotoByPrimaryKey(foodphoto.getFoodPhotoId());
	}

	/**
	* View an existing FoodValidation entity
	* 
	*/
	@RequestMapping(value = "/FoodPhoto/{foodphoto_foodPhotoId}/foodValidations/{foodvalidation_foodValidationId}", method = RequestMethod.GET)
	@ResponseBody
	public FoodValidation loadFoodPhotoFoodValidations(@PathVariable Integer foodphoto_foodPhotoId, @PathVariable Integer related_foodvalidations_foodValidationId) {
		FoodValidation foodvalidation = foodValidationDAO.findFoodValidationByPrimaryKey(related_foodvalidations_foodValidationId, -1, -1);

		return foodvalidation;
	}

	/**
	* Delete an existing Meal entity
	* 
	*/
	@RequestMapping(value = "/FoodPhoto/{foodphoto_foodPhotoId}/meal/{meal_mealId}", method = RequestMethod.DELETE)
	@ResponseBody
	public void deleteFoodPhotoMeal(@PathVariable Integer foodphoto_foodPhotoId, @PathVariable Integer related_meal_mealId) {
		foodPhotoService.deleteFoodPhotoMeal(foodphoto_foodPhotoId, related_meal_mealId);
	}

	/**
	* Show all FoodValidation entities by FoodPhoto
	* 
	*/
	@RequestMapping(value = "/FoodPhoto/{foodphoto_foodPhotoId}/foodValidations", method = RequestMethod.GET)
	@ResponseBody
	public List<FoodValidation> getFoodPhotoFoodValidations(@PathVariable Integer foodphoto_foodPhotoId) {
		return new java.util.ArrayList<FoodValidation>(foodPhotoDAO.findFoodPhotoByPrimaryKey(foodphoto_foodPhotoId).getFoodValidations());
	}

	/**
	* View an existing Meal entity
	* 
	*/
	@RequestMapping(value = "/FoodPhoto/{foodphoto_foodPhotoId}/meal/{meal_mealId}", method = RequestMethod.GET)
	@ResponseBody
	public Meal loadFoodPhotoMeal(@PathVariable Integer foodphoto_foodPhotoId, @PathVariable Integer related_meal_mealId) {
		Meal meal = mealDAO.findMealByPrimaryKey(related_meal_mealId, -1, -1);

		return meal;
	}
}