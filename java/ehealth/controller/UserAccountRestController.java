package ehealth.controller;

import java.util.Calendar;
import java.util.Iterator;
import java.util.TimeZone;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import common.RandomCodeKey;
import ehealth.domain.City;
import ehealth.domain.Doctor;
import ehealth.domain.Kid;
import ehealth.domain.Parent;
import ehealth.domain.ParentKid;
import ehealth.domain.Timestamp;
import ehealth.domain.User;
import ehealth.domain.UserAccountDoctor;
import ehealth.domain.UserAccountKid;
import ehealth.domain.UserAccountParent;
import ehealth.domain.UserRole;
import ehealth.service.CityService;
import ehealth.service.DoctorService;
import ehealth.service.KidService;
import ehealth.service.ParentKidService;
import ehealth.service.ParentService;
import ehealth.service.TimestampService;
import ehealth.service.UserRoleService;
import ehealth.service.UserService;

@Controller("UserAccountRestController")
public class UserAccountRestController {
	/**
	 * Service injected by Spring that provides CRUD operations for Timestamp
	 * entities
	 * 
	 */
	@Autowired
	private TimestampService timestampService;

	/**
	 * Service injected by Spring that provides CRUD operations for User
	 * entities
	 * 
	 */
	@Autowired
	private UserService userService;

	/**
	 * Service injected by Spring that provides CRUD operations for Doctor
	 * entities
	 * 
	 */
	@Autowired
	private DoctorService doctorService;

	/**
	 * Service injected by Spring that provides CRUD operations for Kid entities
	 * 
	 */
	@Autowired
	private KidService kidService;

	/**
	 * Service injected by Spring that provides CRUD operations for City
	 * entities
	 * 
	 */
	@Autowired
	private CityService cityService;

	/**
	 * Service injected by Spring that provides CRUD operations for Parent
	 * entities
	 * 
	 */
	@Autowired
	private ParentService parentService;

	/**
	 * Service injected by Spring that provides CRUD operations for ParentKid
	 * entities
	 * 
	 */
	@Autowired
	private ParentKidService parentkidService;

	/**
	 * Service injected by Spring that provides CRUD operations for UserRole
	 * entities
	 * 
	 */
	@Autowired
	private UserRoleService userroleService;

	
	
	@RequestMapping(value = "/RegisterDoctor", method = RequestMethod.POST)
	@ResponseBody
	public int saveUserAccountDoctor(@RequestBody UserAccountDoctor useraccountdoctor) {
		Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
		Timestamp timestamp = new Timestamp();
		timestamp.setCreateDate(calendar);
		timestamp = timestampService.saveTimestamp(timestamp);
		useraccountdoctor.getUser().setTimestamp(timestamp);
		useraccountdoctor.getUser().setEnabled(true);

		useraccountdoctor.getUser().setUserRole(userroleService.findUserRoleByPrimaryKey(1));
		User user = userService.saveUser(useraccountdoctor.getUser());
		useraccountdoctor.setUser(user);

		timestampService.saveTimestampUsers(timestamp.getTimestampId(), user);

		Doctor doctor = new Doctor();
		doctor.setUser(user);
		doctor.setCodeKey(uniqueCodeKey(false));
		doctor.setHospital(useraccountdoctor.getHospitalName());
		doctor = doctorService.saveDoctor(doctor);

		userService.saveUserDoctors(user.getUserId(), doctor);

		return doctor.getDoctorId();
	}

	@RequestMapping(value = "/RegisterKid", method = RequestMethod.POST)
	@ResponseBody
	public String saveUserAccountKid(@RequestBody UserAccountKid useraccountkid) {
		Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
		Timestamp timestamp = new Timestamp();
		timestamp.setCreateDate(calendar);
		timestamp = timestampService.saveTimestamp(timestamp);
		useraccountkid.getUser().setTimestamp(timestamp);
		useraccountkid.getUser().setEnabled(true);
		UserRole userrole = userroleService.findUserRoleByPrimaryKey(3);
		useraccountkid.getUser().setUserRole(userrole);

		User user = userService.saveUser(useraccountkid.getUser());
		useraccountkid.setUser(user);
		timestampService.saveTimestampUsers(timestamp.getTimestampId(), user);

		Kid kid = new Kid();
		kid.setUser(user);
		City city = cityService.findCityByPrimaryKey(useraccountkid.getKidCityId());
		kid.setCity(city);
		kid.setConnected(false);
		kid.setCodeKey(uniqueCodeKey(true));

		kid = kidService.saveKid(kid);
		if (useraccountkid.getDoctorId() != 0) {
			Doctor doctor = doctorService.findDoctorByPrimaryKey(useraccountkid.getDoctorId());
			kid = kidService.saveKidDoctor(kid.getKidId(), doctor);
		}
		userService.saveUserKids(user.getUserId(), kid);
		ObjectMapper mapper = new ObjectMapper();
		String jsonInString = null;
		try {
			jsonInString = mapper.writeValueAsString(kid.getKidId());
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return jsonInString;
	}

	@RequestMapping(value = "/RegisterParent", method = RequestMethod.POST)
	@ResponseBody
	public UserAccountParent saveUserAccountParent(@RequestBody UserAccountParent useraccountparent) {
		if (useraccountparent.getKid() != null) {
			Timestamp timestamp = new Timestamp();
			Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
			timestamp.setCreateDate(calendar);
			timestamp = timestampService.saveTimestamp(timestamp);
			useraccountparent.getUser().setTimestamp(timestamp);

			User user = userService.saveUser(useraccountparent.getUser());
			useraccountparent.setUser(user);
			timestampService.saveTimestampUsers(timestamp.getTimestampId(), user);

			useraccountparent.getParent().setUser(user);
			City city = cityService.findCityByPrimaryKey(useraccountparent.getParentCityId());
			useraccountparent.getParent().setCity(city);
			Parent parent = parentService.saveParent(useraccountparent.getParent());
			useraccountparent.setParent(parent);
			userService.saveUserParents(user.getUserId(), parent);

			Iterator<Kid> kidIterator = useraccountparent.getKid().iterator();
			ParentKid parentkidObj = new ParentKid();
			while (kidIterator.hasNext()) {

				parentkidObj.setKid(kidIterator.next());
				parentkidObj.setParent(parent);
				parentkidService.saveParentKid(parentkidObj);

			}
			return useraccountparent;
		}
		return null;
	}
	
	public String uniqueCodeKey(boolean isKid) {
		Boolean isDubplicateKey = true;
		String codeKey = null;
		RandomCodeKey rc = new RandomCodeKey();
		if (isKid) {
			while (isDubplicateKey) {
				codeKey = rc.randomCodeKey();
				if (kidService.findKidByCodeKey(codeKey).isEmpty()) {
					isDubplicateKey = false;
				} else {
					isDubplicateKey = true;
				}
			}
		} else {
			while (isDubplicateKey) {
				codeKey = rc.randomCodeKey();
				if (doctorService.findDoctorByCodeKey(codeKey).isEmpty()) {
					isDubplicateKey = false;
				} else {
					isDubplicateKey = true;
				}
			}
		}
		return codeKey;
	}
}
