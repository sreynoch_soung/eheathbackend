package ehealth.controller;

import ehealth.dao.KidDAO;
import ehealth.dao.ParentDAO;
import ehealth.dao.ParentKidDAO;

import ehealth.domain.Kid;
import ehealth.domain.Parent;
import ehealth.domain.ParentKid;

import ehealth.service.ParentKidService;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Controller;

import org.springframework.web.bind.WebDataBinder;

import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Spring Rest controller that handles CRUD requests for ParentKid entities
 * 
 */

@Controller("ParentKidRestController")

public class ParentKidRestController {

	/**
	 * DAO injected by Spring that manages Kid entities
	 * 
	 */
	@Autowired
	private KidDAO kidDAO;

	/**
	 * DAO injected by Spring that manages Parent entities
	 * 
	 */
	@Autowired
	private ParentDAO parentDAO;

	/**
	 * DAO injected by Spring that manages ParentKid entities
	 * 
	 */
	@Autowired
	private ParentKidDAO parentKidDAO;

	/**
	 * Service injected by Spring that provides CRUD operations for ParentKid entities
	 * 
	 */
	@Autowired
	private ParentKidService parentKidService;

	/**
	 * Save an existing Kid entity
	 * 
	 */
	@RequestMapping(value = "/ParentKid/{parentkid_parentKidId}/kid", method = RequestMethod.PUT)
	@ResponseBody
	public Kid saveParentKidKid(@PathVariable Integer parentkid_parentKidId, @RequestBody Kid kid) {
		parentKidService.saveParentKidKid(parentkid_parentKidId, kid);
		return kidDAO.findKidByPrimaryKey(kid.getKidId());
	}

	/**
	* Register custom, context-specific property editors
	* 
	*/
	@InitBinder
	public void initBinder(WebDataBinder binder, HttpServletRequest request) { // Register static property editors.
		binder.registerCustomEditor(java.util.Calendar.class, new org.skyway.spring.util.databinding.CustomCalendarEditor());
		binder.registerCustomEditor(byte[].class, new org.springframework.web.multipart.support.ByteArrayMultipartFileEditor());
		binder.registerCustomEditor(boolean.class, new org.skyway.spring.util.databinding.EnhancedBooleanEditor(false));
		binder.registerCustomEditor(Boolean.class, new org.skyway.spring.util.databinding.EnhancedBooleanEditor(true));
		binder.registerCustomEditor(java.math.BigDecimal.class, new org.skyway.spring.util.databinding.NaNHandlingNumberEditor(java.math.BigDecimal.class, true));
		binder.registerCustomEditor(Integer.class, new org.skyway.spring.util.databinding.NaNHandlingNumberEditor(Integer.class, true));
		binder.registerCustomEditor(java.util.Date.class, new org.skyway.spring.util.databinding.CustomDateEditor());
		binder.registerCustomEditor(String.class, new org.skyway.spring.util.databinding.StringEditor());
		binder.registerCustomEditor(Long.class, new org.skyway.spring.util.databinding.NaNHandlingNumberEditor(Long.class, true));
		binder.registerCustomEditor(Double.class, new org.skyway.spring.util.databinding.NaNHandlingNumberEditor(Double.class, true));
	}

	/**
	* View an existing Kid entity
	* 
	*/
	@RequestMapping(value = "/ParentKid/{parentkid_parentKidId}/kid/{kid_kidId}", method = RequestMethod.GET)
	@ResponseBody
	public Kid loadParentKidKid(@PathVariable Integer parentkid_parentKidId, @PathVariable Integer related_kid_kidId) {
		Kid kid = kidDAO.findKidByPrimaryKey(related_kid_kidId, -1, -1);

		return kid;
	}

	/**
	* Show all ParentKid entities
	* 
	*/
	@RequestMapping(value = "/ParentKid", method = RequestMethod.GET)
	@ResponseBody
	public List<ParentKid> listParentKids() {
		return new java.util.ArrayList<ParentKid>(parentKidService.loadParentKids());
	}

	/**
	* Delete an existing Kid entity
	* 
	*/
	@RequestMapping(value = "/ParentKid/{parentkid_parentKidId}/kid/{kid_kidId}", method = RequestMethod.DELETE)
	@ResponseBody
	public void deleteParentKidKid(@PathVariable Integer parentkid_parentKidId, @PathVariable Integer related_kid_kidId) {
		parentKidService.deleteParentKidKid(parentkid_parentKidId, related_kid_kidId);
	}

	/**
	* Get Parent entity by ParentKid
	* 
	*/
	@RequestMapping(value = "/ParentKid/{parentkid_parentKidId}/parent", method = RequestMethod.GET)
	@ResponseBody
	public Parent getParentKidParent(@PathVariable Integer parentkid_parentKidId) {
		return parentKidDAO.findParentKidByPrimaryKey(parentkid_parentKidId).getParent();
	}

	/**
	* Save an existing Parent entity
	* 
	*/
	@RequestMapping(value = "/ParentKid/{parentkid_parentKidId}/parent", method = RequestMethod.PUT)
	@ResponseBody
	public Parent saveParentKidParent(@PathVariable Integer parentkid_parentKidId, @RequestBody Parent parent) {
		parentKidService.saveParentKidParent(parentkid_parentKidId, parent);
		return parentDAO.findParentByPrimaryKey(parent.getParentId());
	}

	/**
	* Create a new ParentKid entity
	* 
	*/
	@RequestMapping(value = "/ParentKid", method = RequestMethod.POST)
	@ResponseBody
	public ParentKid newParentKid(@RequestBody ParentKid parentkid) {
		parentKidService.saveParentKid(parentkid);
		return parentKidDAO.findParentKidByPrimaryKey(parentkid.getParentKidId());
	}

	/**
	* Delete an existing ParentKid entity
	* 
	*/
	@RequestMapping(value = "/ParentKid/{parentkid_parentKidId}", method = RequestMethod.DELETE)
	@ResponseBody
	public void deleteParentKid(@PathVariable Integer parentkid_parentKidId) {
		ParentKid parentkid = parentKidDAO.findParentKidByPrimaryKey(parentkid_parentKidId);
		parentKidService.deleteParentKid(parentkid);
	}

	/**
	* Create a new Kid entity
	* 
	*/
	@RequestMapping(value = "/ParentKid/{parentkid_parentKidId}/kid", method = RequestMethod.POST)
	@ResponseBody
	public Kid newParentKidKid(@PathVariable Integer parentkid_parentKidId, @RequestBody Kid kid) {
		parentKidService.saveParentKidKid(parentkid_parentKidId, kid);
		return kidDAO.findKidByPrimaryKey(kid.getKidId());
	}

	/**
	* View an existing Parent entity
	* 
	*/
	@RequestMapping(value = "/ParentKid/{parentkid_parentKidId}/parent/{parent_parentId}", method = RequestMethod.GET)
	@ResponseBody
	public Parent loadParentKidParent(@PathVariable Integer parentkid_parentKidId, @PathVariable Integer related_parent_parentId) {
		Parent parent = parentDAO.findParentByPrimaryKey(related_parent_parentId, -1, -1);

		return parent;
	}

	/**
	* Create a new Parent entity
	* 
	*/
	@RequestMapping(value = "/ParentKid/{parentkid_parentKidId}/parent", method = RequestMethod.POST)
	@ResponseBody
	public Parent newParentKidParent(@PathVariable Integer parentkid_parentKidId, @RequestBody Parent parent) {
		parentKidService.saveParentKidParent(parentkid_parentKidId, parent);
		return parentDAO.findParentByPrimaryKey(parent.getParentId());
	}

	/**
	* Select an existing ParentKid entity
	* 
	*/
	@RequestMapping(value = "/ParentKid/{parentkid_parentKidId}", method = RequestMethod.GET)
	@ResponseBody
	public ParentKid loadParentKid(@PathVariable Integer parentkid_parentKidId) {
		return parentKidDAO.findParentKidByPrimaryKey(parentkid_parentKidId);
	}

	/**
	* Save an existing ParentKid entity
	* 
	*/
	@RequestMapping(value = "/ParentKid", method = RequestMethod.PUT)
	@ResponseBody
	public ParentKid saveParentKid(@RequestBody ParentKid parentkid) {
		parentKidService.saveParentKid(parentkid);
		return parentKidDAO.findParentKidByPrimaryKey(parentkid.getParentKidId());
	}

	/**
	* Delete an existing Parent entity
	* 
	*/
	@RequestMapping(value = "/ParentKid/{parentkid_parentKidId}/parent/{parent_parentId}", method = RequestMethod.DELETE)
	@ResponseBody
	public void deleteParentKidParent(@PathVariable Integer parentkid_parentKidId, @PathVariable Integer related_parent_parentId) {
		parentKidService.deleteParentKidParent(parentkid_parentKidId, related_parent_parentId);
	}

	/**
	* Get Kid entity by ParentKid
	* 
	*/
	@RequestMapping(value = "/ParentKid/{parentkid_parentKidId}/kid", method = RequestMethod.GET)
	@ResponseBody
	public Kid getParentKidKid(@PathVariable Integer parentkid_parentKidId) {
		return parentKidDAO.findParentKidByPrimaryKey(parentkid_parentKidId).getKid();
	}
}