package ehealth.controller;

import java.util.Iterator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import ehealth.domain.EffortType;
import ehealth.domain.ExerciseTarget;
import ehealth.domain.ExerciseTargetInput;
import ehealth.service.ExerciseTargetService;

public class EerciseTargetInputRestController {
	/**
	 * Service injected by Spring that provides CRUD operations for Exercise
	 * Target Input entities
	 * 
	 */
	@Autowired
	private ExerciseTargetService exerciseTargetService;

	/**
	 * Save an NutritionTargetInput entity
	 * 
	 */
	@RequestMapping(value = "/ExerciseTargetInput", method = RequestMethod.POST)
	@ResponseBody
	public ExerciseTargetInput saveExerciseTargetInput(@RequestBody ExerciseTargetInput exercisetargetinput) {
		
		Iterator<EffortType> effortTypeIterator = exercisetargetinput.getEfforttypes().iterator();
		ExerciseTarget exercisetarget = exercisetargetinput.getExercisetarget();
		
		while (effortTypeIterator.hasNext()) {
			exercisetarget.setEffortType(effortTypeIterator.next());
			exerciseTargetService.saveExerciseTarget(exercisetarget);
		}
		return exercisetargetinput;
	}
}
