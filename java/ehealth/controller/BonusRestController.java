package ehealth.controller;

import ehealth.dao.BonusDAO;
import ehealth.dao.KidDAO;

import ehealth.domain.Bonus;
import ehealth.domain.Kid;

import ehealth.service.BonusService;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Controller;

import org.springframework.web.bind.WebDataBinder;

import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Spring Rest controller that handles CRUD requests for Bonus entities
 * 
 */

@Controller("BonusRestController")

public class BonusRestController {

	/**
	 * DAO injected by Spring that manages Bonus entities
	 * 
	 */
	@Autowired
	private BonusDAO bonusDAO;

	/**
	 * DAO injected by Spring that manages Kid entities
	 * 
	 */
	@Autowired
	private KidDAO kidDAO;

	/**
	 * Service injected by Spring that provides CRUD operations for Bonus entities
	 * 
	 */
	@Autowired
	private BonusService bonusService;

	/**
	 * Save an existing Kid entity
	 * 
	 */
	@RequestMapping(value = "/Bonus/{bonus_bonusId}/kid", method = RequestMethod.PUT)
	@ResponseBody
	public Kid saveBonusKid(@PathVariable Integer bonus_bonusId, @RequestBody Kid kid) {
		bonusService.saveBonusKid(bonus_bonusId, kid);
		return kidDAO.findKidByPrimaryKey(kid.getKidId());
	}

	/**
	* Create a new Bonus entity
	* 
	*/
	@RequestMapping(value = "/Bonus", method = RequestMethod.POST)
	@ResponseBody
	public Bonus newBonus(@RequestBody Bonus bonus) {
		bonusService.saveBonus(bonus);
		return bonusDAO.findBonusByPrimaryKey(bonus.getBonusId());
	}

	/**
	* Get Kid entity by Bonus
	* 
	*/
	@RequestMapping(value = "/Bonus/{bonus_bonusId}/kid", method = RequestMethod.GET)
	@ResponseBody
	public Kid getBonusKid(@PathVariable Integer bonus_bonusId) {
		return bonusDAO.findBonusByPrimaryKey(bonus_bonusId).getKid();
	}

	/**
	* Save an existing Bonus entity
	* 
	*/
	@RequestMapping(value = "/Bonus", method = RequestMethod.PUT)
	@ResponseBody
	public Bonus saveBonus(@RequestBody Bonus bonus) {
		bonusService.saveBonus(bonus);
		return bonusDAO.findBonusByPrimaryKey(bonus.getBonusId());
	}

	/**
	* Delete an existing Kid entity
	* 
	*/
	@RequestMapping(value = "/Bonus/{bonus_bonusId}/kid/{kid_kidId}", method = RequestMethod.DELETE)
	@ResponseBody
	public void deleteBonusKid(@PathVariable Integer bonus_bonusId, @PathVariable Integer related_kid_kidId) {
		bonusService.deleteBonusKid(bonus_bonusId, related_kid_kidId);
	}

	/**
	* Select an existing Bonus entity
	* 
	*/
	@RequestMapping(value = "/Bonus/{bonus_bonusId}", method = RequestMethod.GET)
	@ResponseBody
	public Bonus loadBonus(@PathVariable Integer bonus_bonusId) {
		return bonusDAO.findBonusByPrimaryKey(bonus_bonusId);
	}

	/**
	* Show all Bonus entities
	* 
	*/
	@RequestMapping(value = "/Bonus", method = RequestMethod.GET)
	@ResponseBody
	public List<Bonus> listBonuss() {
		return new java.util.ArrayList<Bonus>(bonusService.loadBonuss());
	}

	/**
	* View an existing Kid entity
	* 
	*/
	@RequestMapping(value = "/Bonus/{bonus_bonusId}/kid/{kid_kidId}", method = RequestMethod.GET)
	@ResponseBody
	public Kid loadBonusKid(@PathVariable Integer bonus_bonusId, @PathVariable Integer related_kid_kidId) {
		Kid kid = kidDAO.findKidByPrimaryKey(related_kid_kidId, -1, -1);

		return kid;
	}

	/**
	* Register custom, context-specific property editors
	* 
	*/
	@InitBinder
	public void initBinder(WebDataBinder binder, HttpServletRequest request) { // Register static property editors.
		binder.registerCustomEditor(java.util.Calendar.class, new org.skyway.spring.util.databinding.CustomCalendarEditor());
		binder.registerCustomEditor(byte[].class, new org.springframework.web.multipart.support.ByteArrayMultipartFileEditor());
		binder.registerCustomEditor(boolean.class, new org.skyway.spring.util.databinding.EnhancedBooleanEditor(false));
		binder.registerCustomEditor(Boolean.class, new org.skyway.spring.util.databinding.EnhancedBooleanEditor(true));
		binder.registerCustomEditor(java.math.BigDecimal.class, new org.skyway.spring.util.databinding.NaNHandlingNumberEditor(java.math.BigDecimal.class, true));
		binder.registerCustomEditor(Integer.class, new org.skyway.spring.util.databinding.NaNHandlingNumberEditor(Integer.class, true));
		binder.registerCustomEditor(java.util.Date.class, new org.skyway.spring.util.databinding.CustomDateEditor());
		binder.registerCustomEditor(String.class, new org.skyway.spring.util.databinding.StringEditor());
		binder.registerCustomEditor(Long.class, new org.skyway.spring.util.databinding.NaNHandlingNumberEditor(Long.class, true));
		binder.registerCustomEditor(Double.class, new org.skyway.spring.util.databinding.NaNHandlingNumberEditor(Double.class, true));
	}

	/**
	* Create a new Kid entity
	* 
	*/
	@RequestMapping(value = "/Bonus/{bonus_bonusId}/kid", method = RequestMethod.POST)
	@ResponseBody
	public Kid newBonusKid(@PathVariable Integer bonus_bonusId, @RequestBody Kid kid) {
		bonusService.saveBonusKid(bonus_bonusId, kid);
		return kidDAO.findKidByPrimaryKey(kid.getKidId());
	}

	/**
	* Delete an existing Bonus entity
	* 
	*/
	@RequestMapping(value = "/Bonus/{bonus_bonusId}", method = RequestMethod.DELETE)
	@ResponseBody
	public void deleteBonus(@PathVariable Integer bonus_bonusId) {
		Bonus bonus = bonusDAO.findBonusByPrimaryKey(bonus_bonusId);
		bonusService.deleteBonus(bonus);
	}
}