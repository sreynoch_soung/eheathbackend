package ehealth.controller;

import ehealth.dao.UserDAO;
import ehealth.dao.UserRoleDAO;

import ehealth.domain.User;
import ehealth.domain.UserRole;

import ehealth.service.UserRoleService;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Controller;

import org.springframework.web.bind.WebDataBinder;

import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Spring Rest controller that handles CRUD requests for UserRole entities
 * 
 */

@Controller("UserRoleRestController")

public class UserRoleRestController {

	/**
	 * DAO injected by Spring that manages User entities
	 * 
	 */
	@Autowired
	private UserDAO userDAO;

	/**
	 * DAO injected by Spring that manages UserRole entities
	 * 
	 */
	@Autowired
	private UserRoleDAO userRoleDAO;

	/**
	 * Service injected by Spring that provides CRUD operations for UserRole entities
	 * 
	 */
	@Autowired
	private UserRoleService userRoleService;

	/**
	 * Create a new UserRole entity
	 * 
	 */
	@RequestMapping(value = "/UserRole", method = RequestMethod.POST)
	@ResponseBody
	public UserRole newUserRole(@RequestBody UserRole userrole) {
		userRoleService.saveUserRole(userrole);
		return userRoleDAO.findUserRoleByPrimaryKey(userrole.getUserRoleId());
	}

	/**
	* Select an existing UserRole entity
	* 
	*/
	@RequestMapping(value = "/UserRole/{userrole_userRoleId}", method = RequestMethod.GET)
	@ResponseBody
	public UserRole loadUserRole(@PathVariable Integer userrole_userRoleId) {
		return userRoleDAO.findUserRoleByPrimaryKey(userrole_userRoleId);
	}

	/**
	* View an existing User entity
	* 
	*/
	@RequestMapping(value = "/UserRole/{userrole_userRoleId}/users/{user_userId}", method = RequestMethod.GET)
	@ResponseBody
	public User loadUserRoleUsers(@PathVariable Integer userrole_userRoleId, @PathVariable Integer related_users_userId) {
		User user = userDAO.findUserByPrimaryKey(related_users_userId, -1, -1);

		return user;
	}

	/**
	* Show all User entities by UserRole
	* 
	*/
	@RequestMapping(value = "/UserRole/{userrole_userRoleId}/users", method = RequestMethod.GET)
	@ResponseBody
	public List<User> getUserRoleUsers(@PathVariable Integer userrole_userRoleId) {
		return new java.util.ArrayList<User>(userRoleDAO.findUserRoleByPrimaryKey(userrole_userRoleId).getUsers());
	}

	/**
	* Create a new User entity
	* 
	*/
	@RequestMapping(value = "/UserRole/{userrole_userRoleId}/users", method = RequestMethod.POST)
	@ResponseBody
	public User newUserRoleUsers(@PathVariable Integer userrole_userRoleId, @RequestBody User user) {
		userRoleService.saveUserRoleUsers(userrole_userRoleId, user);
		return userDAO.findUserByPrimaryKey(user.getUserId());
	}

	/**
	* Show all UserRole entities
	* 
	*/
	@RequestMapping(value = "/UserRole", method = RequestMethod.GET)
	@ResponseBody
	public List<UserRole> listUserRoles() {
		return new java.util.ArrayList<UserRole>(userRoleService.loadUserRoles());
	}

	/**
	* Save an existing UserRole entity
	* 
	*/
	@RequestMapping(value = "/UserRole", method = RequestMethod.PUT)
	@ResponseBody
	public UserRole saveUserRole(@RequestBody UserRole userrole) {
		userRoleService.saveUserRole(userrole);
		return userRoleDAO.findUserRoleByPrimaryKey(userrole.getUserRoleId());
	}

	/**
	* Delete an existing UserRole entity
	* 
	*/
	@RequestMapping(value = "/UserRole/{userrole_userRoleId}", method = RequestMethod.DELETE)
	@ResponseBody
	public void deleteUserRole(@PathVariable Integer userrole_userRoleId) {
		UserRole userrole = userRoleDAO.findUserRoleByPrimaryKey(userrole_userRoleId);
		userRoleService.deleteUserRole(userrole);
	}

	/**
	* Register custom, context-specific property editors
	* 
	*/
	@InitBinder
	public void initBinder(WebDataBinder binder, HttpServletRequest request) { // Register static property editors.
		binder.registerCustomEditor(java.util.Calendar.class, new org.skyway.spring.util.databinding.CustomCalendarEditor());
		binder.registerCustomEditor(byte[].class, new org.springframework.web.multipart.support.ByteArrayMultipartFileEditor());
		binder.registerCustomEditor(boolean.class, new org.skyway.spring.util.databinding.EnhancedBooleanEditor(false));
		binder.registerCustomEditor(Boolean.class, new org.skyway.spring.util.databinding.EnhancedBooleanEditor(true));
		binder.registerCustomEditor(java.math.BigDecimal.class, new org.skyway.spring.util.databinding.NaNHandlingNumberEditor(java.math.BigDecimal.class, true));
		binder.registerCustomEditor(Integer.class, new org.skyway.spring.util.databinding.NaNHandlingNumberEditor(Integer.class, true));
		binder.registerCustomEditor(java.util.Date.class, new org.skyway.spring.util.databinding.CustomDateEditor());
		binder.registerCustomEditor(String.class, new org.skyway.spring.util.databinding.StringEditor());
		binder.registerCustomEditor(Long.class, new org.skyway.spring.util.databinding.NaNHandlingNumberEditor(Long.class, true));
		binder.registerCustomEditor(Double.class, new org.skyway.spring.util.databinding.NaNHandlingNumberEditor(Double.class, true));
	}

	/**
	* Save an existing User entity
	* 
	*/
	@RequestMapping(value = "/UserRole/{userrole_userRoleId}/users", method = RequestMethod.PUT)
	@ResponseBody
	public User saveUserRoleUsers(@PathVariable Integer userrole_userRoleId, @RequestBody User users) {
		userRoleService.saveUserRoleUsers(userrole_userRoleId, users);
		return userDAO.findUserByPrimaryKey(users.getUserId());
	}

	/**
	* Delete an existing User entity
	* 
	*/
	@RequestMapping(value = "/UserRole/{userrole_userRoleId}/users/{user_userId}", method = RequestMethod.DELETE)
	@ResponseBody
	public void deleteUserRoleUsers(@PathVariable Integer userrole_userRoleId, @PathVariable Integer related_users_userId) {
		userRoleService.deleteUserRoleUsers(userrole_userRoleId, related_users_userId);
	}
}