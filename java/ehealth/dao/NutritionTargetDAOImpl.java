
package ehealth.dao;

import ehealth.domain.NutritionTarget;

import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.skyway.spring.util.dao.AbstractJpaDao;

import org.springframework.dao.DataAccessException;

import org.springframework.stereotype.Repository;

import org.springframework.transaction.annotation.Transactional;

/**
 * DAO to manage NutritionTarget entities.
 * 
 */
@Repository("NutritionTargetDAO")

@Transactional
public class NutritionTargetDAOImpl extends AbstractJpaDao<NutritionTarget> implements NutritionTargetDAO {

	/**
	 * Set of entity classes managed by this DAO.  Typically a DAO manages a single entity.
	 *
	 */
	private final static Set<Class<?>> dataTypes = new HashSet<Class<?>>(Arrays.asList(new Class<?>[] {
			NutritionTarget.class }));

	/**
	 * EntityManager injected by Spring for persistence unit dbdriver
	 *
	 */
	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Instantiates a new NutritionTargetDAOImpl
	 *
	 */
	public NutritionTargetDAOImpl() {
		super();
	}

	/**
	 * Get the entity manager that manages persistence unit 
	 *
	 */
	public EntityManager getEntityManager() {
		return entityManager;
	}

	/**
	 * Returns the set of entity classes managed by this DAO.
	 *
	 */
	public Set<Class<?>> getTypes() {
		return dataTypes;
	}

	/**
	 * JPQL Query - findNutritionTargetByPrimaryKey
	 *
	 */
	@Transactional
	public NutritionTarget findNutritionTargetByPrimaryKey(Integer nutritionTargetId) throws DataAccessException {

		return findNutritionTargetByPrimaryKey(nutritionTargetId, -1, -1);
	}

	/**
	 * JPQL Query - findNutritionTargetByPrimaryKey
	 *
	 */

	@Transactional
	public NutritionTarget findNutritionTargetByPrimaryKey(Integer nutritionTargetId, int startResult, int maxRows) throws DataAccessException {
		try {
			Query query = createNamedQuery("findNutritionTargetByPrimaryKey", startResult, maxRows, nutritionTargetId);
			return (ehealth.domain.NutritionTarget) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * JPQL Query - findNutritionTargetByNutritionTargetId
	 *
	 */
	@Transactional
	public NutritionTarget findNutritionTargetByNutritionTargetId(Integer nutritionTargetId) throws DataAccessException {

		return findNutritionTargetByNutritionTargetId(nutritionTargetId, -1, -1);
	}

	/**
	 * JPQL Query - findNutritionTargetByNutritionTargetId
	 *
	 */

	@Transactional
	public NutritionTarget findNutritionTargetByNutritionTargetId(Integer nutritionTargetId, int startResult, int maxRows) throws DataAccessException {
		try {
			Query query = createNamedQuery("findNutritionTargetByNutritionTargetId", startResult, maxRows, nutritionTargetId);
			return (ehealth.domain.NutritionTarget) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * JPQL Query - findNutritionTargetByDate
	 *
	 */
	@Transactional
	public Set<NutritionTarget> findNutritionTargetByDate(java.util.Calendar date) throws DataAccessException {

		return findNutritionTargetByDate(date, -1, -1);
	}

	/**
	 * JPQL Query - findNutritionTargetByDate
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<NutritionTarget> findNutritionTargetByDate(java.util.Calendar date, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findNutritionTargetByDate", startResult, maxRows, date);
		return new LinkedHashSet<NutritionTarget>(query.getResultList());
	}

	/**
	 * JPQL Query - findAllNutritionTargets
	 *
	 */
	@Transactional
	public Set<NutritionTarget> findAllNutritionTargets() throws DataAccessException {

		return findAllNutritionTargets(-1, -1);
	}

	/**
	 * JPQL Query - findAllNutritionTargets
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<NutritionTarget> findAllNutritionTargets(int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findAllNutritionTargets", startResult, maxRows);
		return new LinkedHashSet<NutritionTarget>(query.getResultList());
	}

	/**
	 * Used to determine whether or not to merge the entity or persist the entity when calling Store
	 * @see store
	 * 
	 *
	 */
	public boolean canBeMerged(NutritionTarget entity) {
		return true;
	}
}
