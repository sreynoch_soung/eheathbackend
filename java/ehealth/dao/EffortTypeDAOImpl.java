
package ehealth.dao;

import ehealth.domain.EffortType;

import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.skyway.spring.util.dao.AbstractJpaDao;

import org.springframework.dao.DataAccessException;

import org.springframework.stereotype.Repository;

import org.springframework.transaction.annotation.Transactional;

/**
 * DAO to manage EffortType entities.
 * 
 */
@Repository("EffortTypeDAO")

@Transactional
public class EffortTypeDAOImpl extends AbstractJpaDao<EffortType> implements EffortTypeDAO {

	/**
	 * Set of entity classes managed by this DAO.  Typically a DAO manages a single entity.
	 *
	 */
	private final static Set<Class<?>> dataTypes = new HashSet<Class<?>>(Arrays.asList(new Class<?>[] {
			EffortType.class }));

	/**
	 * EntityManager injected by Spring for persistence unit dbdriver
	 *
	 */
	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Instantiates a new EffortTypeDAOImpl
	 *
	 */
	public EffortTypeDAOImpl() {
		super();
	}

	/**
	 * Get the entity manager that manages persistence unit 
	 *
	 */
	public EntityManager getEntityManager() {
		return entityManager;
	}

	/**
	 * Returns the set of entity classes managed by this DAO.
	 *
	 */
	public Set<Class<?>> getTypes() {
		return dataTypes;
	}

	/**
	 * JPQL Query - findEffortTypeByTitleContaining
	 *
	 */
	@Transactional
	public Set<EffortType> findEffortTypeByTitleContaining(String title) throws DataAccessException {

		return findEffortTypeByTitleContaining(title, -1, -1);
	}

	/**
	 * JPQL Query - findEffortTypeByTitleContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<EffortType> findEffortTypeByTitleContaining(String title, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findEffortTypeByTitleContaining", startResult, maxRows, title);
		return new LinkedHashSet<EffortType>(query.getResultList());
	}

	/**
	 * JPQL Query - findEffortTypeByTitle
	 *
	 */
	@Transactional
	public Set<EffortType> findEffortTypeByTitle(String title) throws DataAccessException {

		return findEffortTypeByTitle(title, -1, -1);
	}

	/**
	 * JPQL Query - findEffortTypeByTitle
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<EffortType> findEffortTypeByTitle(String title, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findEffortTypeByTitle", startResult, maxRows, title);
		return new LinkedHashSet<EffortType>(query.getResultList());
	}

	/**
	 * JPQL Query - findAllEffortTypes
	 *
	 */
	@Transactional
	public Set<EffortType> findAllEffortTypes() throws DataAccessException {

		return findAllEffortTypes(-1, -1);
	}

	/**
	 * JPQL Query - findAllEffortTypes
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<EffortType> findAllEffortTypes(int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findAllEffortTypes", startResult, maxRows);
		return new LinkedHashSet<EffortType>(query.getResultList());
	}

	/**
	 * JPQL Query - findEffortTypeByEffortTypeId
	 *
	 */
	@Transactional
	public EffortType findEffortTypeByEffortTypeId(Integer effortTypeId) throws DataAccessException {

		return findEffortTypeByEffortTypeId(effortTypeId, -1, -1);
	}

	/**
	 * JPQL Query - findEffortTypeByEffortTypeId
	 *
	 */

	@Transactional
	public EffortType findEffortTypeByEffortTypeId(Integer effortTypeId, int startResult, int maxRows) throws DataAccessException {
		try {
			Query query = createNamedQuery("findEffortTypeByEffortTypeId", startResult, maxRows, effortTypeId);
			return (ehealth.domain.EffortType) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * JPQL Query - findEffortTypeByPrimaryKey
	 *
	 */
	@Transactional
	public EffortType findEffortTypeByPrimaryKey(Integer effortTypeId) throws DataAccessException {

		return findEffortTypeByPrimaryKey(effortTypeId, -1, -1);
	}

	/**
	 * JPQL Query - findEffortTypeByPrimaryKey
	 *
	 */

	@Transactional
	public EffortType findEffortTypeByPrimaryKey(Integer effortTypeId, int startResult, int maxRows) throws DataAccessException {
		try {
			Query query = createNamedQuery("findEffortTypeByPrimaryKey", startResult, maxRows, effortTypeId);
			return (ehealth.domain.EffortType) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * Used to determine whether or not to merge the entity or persist the entity when calling Store
	 * @see store
	 * 
	 *
	 */
	public boolean canBeMerged(EffortType entity) {
		return true;
	}
}
