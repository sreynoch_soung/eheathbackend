
package ehealth.dao;

import ehealth.domain.FoodTageInput;

import java.util.Set;

import org.skyway.spring.util.dao.JpaDao;

import org.springframework.dao.DataAccessException;

/**
 * DAO to manage FoodTageInput entities.
 * 
 */
public interface FoodTageInputDAO extends JpaDao<FoodTageInput> {

	/**
	 * JPQL Query - findFoodTageInputByValue
	 *
	 */
	public Set<FoodTageInput> findFoodTageInputByValue(Integer value) throws DataAccessException;

	/**
	 * JPQL Query - findFoodTageInputByValue
	 *
	 */
	public Set<FoodTageInput> findFoodTageInputByValue(Integer value, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findFoodTageInputByPrimaryKey
	 *
	 */
	public FoodTageInput findFoodTageInputByPrimaryKey(Integer foodTageInputId) throws DataAccessException;

	/**
	 * JPQL Query - findFoodTageInputByPrimaryKey
	 *
	 */
	public FoodTageInput findFoodTageInputByPrimaryKey(Integer foodTageInputId, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findFoodTageInputByFoodTageInputId
	 *
	 */
	public FoodTageInput findFoodTageInputByFoodTageInputId(Integer foodTageInputId_1) throws DataAccessException;

	/**
	 * JPQL Query - findFoodTageInputByFoodTageInputId
	 *
	 */
	public FoodTageInput findFoodTageInputByFoodTageInputId(Integer foodTageInputId_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findAllFoodTageInputs
	 *
	 */
	public Set<FoodTageInput> findAllFoodTageInputs() throws DataAccessException;

	/**
	 * JPQL Query - findAllFoodTageInputs
	 *
	 */
	public Set<FoodTageInput> findAllFoodTageInputs(int startResult, int maxRows) throws DataAccessException;

}