
package ehealth.dao;

import ehealth.domain.Meal;

import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.skyway.spring.util.dao.AbstractJpaDao;

import org.springframework.dao.DataAccessException;

import org.springframework.stereotype.Repository;

import org.springframework.transaction.annotation.Transactional;

/**
 * DAO to manage Meal entities.
 * 
 */
@Repository("MealDAO")

@Transactional
public class MealDAOImpl extends AbstractJpaDao<Meal> implements MealDAO {

	/**
	 * Set of entity classes managed by this DAO.  Typically a DAO manages a single entity.
	 *
	 */
	private final static Set<Class<?>> dataTypes = new HashSet<Class<?>>(Arrays.asList(new Class<?>[] { Meal.class }));

	/**
	 * EntityManager injected by Spring for persistence unit dbdriver
	 *
	 */
	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Instantiates a new MealDAOImpl
	 *
	 */
	public MealDAOImpl() {
		super();
	}

	/**
	 * Get the entity manager that manages persistence unit 
	 *
	 */
	public EntityManager getEntityManager() {
		return entityManager;
	}

	/**
	 * Returns the set of entity classes managed by this DAO.
	 *
	 */
	public Set<Class<?>> getTypes() {
		return dataTypes;
	}

	/**
	 * JPQL Query - findMealByMealId
	 *
	 */
	@Transactional
	public Meal findMealByMealId(Integer mealId) throws DataAccessException {

		return findMealByMealId(mealId, -1, -1);
	}

	/**
	 * JPQL Query - findMealByMealId
	 *
	 */

	@Transactional
	public Meal findMealByMealId(Integer mealId, int startResult, int maxRows) throws DataAccessException {
		try {
			Query query = createNamedQuery("findMealByMealId", startResult, maxRows, mealId);
			return (ehealth.domain.Meal) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * JPQL Query - findMealByDate
	 *
	 */
	@Transactional
	public Set<Meal> findMealByDate(java.util.Calendar date) throws DataAccessException {

		return findMealByDate(date, -1, -1);
	}

	/**
	 * JPQL Query - findMealByDate
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<Meal> findMealByDate(java.util.Calendar date, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findMealByDate", startResult, maxRows, date);
		return new LinkedHashSet<Meal>(query.getResultList());
	}

	/**
	 * JPQL Query - findAllMeals
	 *
	 */
	@Transactional
	public Set<Meal> findAllMeals() throws DataAccessException {

		return findAllMeals(-1, -1);
	}

	/**
	 * JPQL Query - findAllMeals
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<Meal> findAllMeals(int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findAllMeals", startResult, maxRows);
		return new LinkedHashSet<Meal>(query.getResultList());
	}

	/**
	 * JPQL Query - findMealByPrimaryKey
	 *
	 */
	@Transactional
	public Meal findMealByPrimaryKey(Integer mealId) throws DataAccessException {

		return findMealByPrimaryKey(mealId, -1, -1);
	}

	/**
	 * JPQL Query - findMealByPrimaryKey
	 *
	 */

	@Transactional
	public Meal findMealByPrimaryKey(Integer mealId, int startResult, int maxRows) throws DataAccessException {
		try {
			Query query = createNamedQuery("findMealByPrimaryKey", startResult, maxRows, mealId);
			return (ehealth.domain.Meal) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * Used to determine whether or not to merge the entity or persist the entity when calling Store
	 * @see store
	 * 
	 *
	 */
	public boolean canBeMerged(Meal entity) {
		return true;
	}
}
