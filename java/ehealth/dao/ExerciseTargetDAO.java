
package ehealth.dao;

import ehealth.domain.ExerciseTarget;

import java.math.BigDecimal;

import java.util.Calendar;
import java.util.Set;

import org.skyway.spring.util.dao.JpaDao;

import org.springframework.dao.DataAccessException;

/**
 * DAO to manage ExerciseTarget entities.
 * 
 */
public interface ExerciseTargetDAO extends JpaDao<ExerciseTarget> {

	/**
	 * JPQL Query - findExerciseTargetByDate
	 *
	 */
	public Set<ExerciseTarget> findExerciseTargetByDate(java.util.Calendar date) throws DataAccessException;

	/**
	 * JPQL Query - findExerciseTargetByDate
	 *
	 */
	public Set<ExerciseTarget> findExerciseTargetByDate(Calendar date, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findExerciseTargetByPrimaryKey
	 *
	 */
	public ExerciseTarget findExerciseTargetByPrimaryKey(Integer exerciseTargetId) throws DataAccessException;

	/**
	 * JPQL Query - findExerciseTargetByPrimaryKey
	 *
	 */
	public ExerciseTarget findExerciseTargetByPrimaryKey(Integer exerciseTargetId, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findExerciseTargetByValue
	 *
	 */
	public Set<ExerciseTarget> findExerciseTargetByValue(java.math.BigDecimal value) throws DataAccessException;

	/**
	 * JPQL Query - findExerciseTargetByValue
	 *
	 */
	public Set<ExerciseTarget> findExerciseTargetByValue(BigDecimal value, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findAllExerciseTargets
	 *
	 */
	public Set<ExerciseTarget> findAllExerciseTargets() throws DataAccessException;

	/**
	 * JPQL Query - findAllExerciseTargets
	 *
	 */
	public Set<ExerciseTarget> findAllExerciseTargets(int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findExerciseTargetByExerciseTargetId
	 *
	 */
	public ExerciseTarget findExerciseTargetByExerciseTargetId(Integer exerciseTargetId_1) throws DataAccessException;

	/**
	 * JPQL Query - findExerciseTargetByExerciseTargetId
	 *
	 */
	public ExerciseTarget findExerciseTargetByExerciseTargetId(Integer exerciseTargetId_1, int startResult, int maxRows) throws DataAccessException;

}