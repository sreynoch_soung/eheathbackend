
package ehealth.dao;

import ehealth.domain.Country;

import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.skyway.spring.util.dao.AbstractJpaDao;

import org.springframework.dao.DataAccessException;

import org.springframework.stereotype.Repository;

import org.springframework.transaction.annotation.Transactional;

/**
 * DAO to manage Country entities.
 * 
 */
@Repository("CountryDAO")

@Transactional
public class CountryDAOImpl extends AbstractJpaDao<Country> implements CountryDAO {

	/**
	 * Set of entity classes managed by this DAO.  Typically a DAO manages a single entity.
	 *
	 */
	private final static Set<Class<?>> dataTypes = new HashSet<Class<?>>(Arrays.asList(new Class<?>[] {
			Country.class }));

	/**
	 * EntityManager injected by Spring for persistence unit dbdriver
	 *
	 */
	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Instantiates a new CountryDAOImpl
	 *
	 */
	public CountryDAOImpl() {
		super();
	}

	/**
	 * Get the entity manager that manages persistence unit 
	 *
	 */
	public EntityManager getEntityManager() {
		return entityManager;
	}

	/**
	 * Returns the set of entity classes managed by this DAO.
	 *
	 */
	public Set<Class<?>> getTypes() {
		return dataTypes;
	}

	/**
	 * JPQL Query - findCountryByCountryId
	 *
	 */
	@Transactional
	public Country findCountryByCountryId(Integer countryId) throws DataAccessException {

		return findCountryByCountryId(countryId, -1, -1);
	}

	/**
	 * JPQL Query - findCountryByCountryId
	 *
	 */

	@Transactional
	public Country findCountryByCountryId(Integer countryId, int startResult, int maxRows) throws DataAccessException {
		try {
			Query query = createNamedQuery("findCountryByCountryId", startResult, maxRows, countryId);
			return (ehealth.domain.Country) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * JPQL Query - findCountryByNameContaining
	 *
	 */
	@Transactional
	public Set<Country> findCountryByNameContaining(String name) throws DataAccessException {

		return findCountryByNameContaining(name, -1, -1);
	}

	/**
	 * JPQL Query - findCountryByNameContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<Country> findCountryByNameContaining(String name, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findCountryByNameContaining", startResult, maxRows, name);
		return new LinkedHashSet<Country>(query.getResultList());
	}

	/**
	 * JPQL Query - findAllCountrys
	 *
	 */
	@Transactional
	public Set<Country> findAllCountrys() throws DataAccessException {

		return findAllCountrys(-1, -1);
	}

	/**
	 * JPQL Query - findAllCountrys
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<Country> findAllCountrys(int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findAllCountrys", startResult, maxRows);
		return new LinkedHashSet<Country>(query.getResultList());
	}

	/**
	 * JPQL Query - findCountryByName
	 *
	 */
	@Transactional
	public Set<Country> findCountryByName(String name) throws DataAccessException {

		return findCountryByName(name, -1, -1);
	}

	/**
	 * JPQL Query - findCountryByName
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<Country> findCountryByName(String name, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findCountryByName", startResult, maxRows, name);
		return new LinkedHashSet<Country>(query.getResultList());
	}

	/**
	 * JPQL Query - findCountryByPrimaryKey
	 *
	 */
	@Transactional
	public Country findCountryByPrimaryKey(Integer countryId) throws DataAccessException {

		return findCountryByPrimaryKey(countryId, -1, -1);
	}

	/**
	 * JPQL Query - findCountryByPrimaryKey
	 *
	 */

	@Transactional
	public Country findCountryByPrimaryKey(Integer countryId, int startResult, int maxRows) throws DataAccessException {
		try {
			Query query = createNamedQuery("findCountryByPrimaryKey", startResult, maxRows, countryId);
			return (ehealth.domain.Country) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * Used to determine whether or not to merge the entity or persist the entity when calling Store
	 * @see store
	 * 
	 *
	 */
	public boolean canBeMerged(Country entity) {
		return true;
	}
}
