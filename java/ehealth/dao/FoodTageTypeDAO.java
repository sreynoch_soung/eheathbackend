
package ehealth.dao;

import ehealth.domain.FoodTageType;

import java.util.Set;

import org.skyway.spring.util.dao.JpaDao;

import org.springframework.dao.DataAccessException;

/**
 * DAO to manage FoodTageType entities.
 * 
 */
public interface FoodTageTypeDAO extends JpaDao<FoodTageType> {

	/**
	 * JPQL Query - findFoodTageTypeByFoodTageTypeId
	 *
	 */
	public FoodTageType findFoodTageTypeByFoodTageTypeId(Integer foodTageTypeId) throws DataAccessException;

	/**
	 * JPQL Query - findFoodTageTypeByFoodTageTypeId
	 *
	 */
	public FoodTageType findFoodTageTypeByFoodTageTypeId(Integer foodTageTypeId, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findFoodTageTypeByTypeNameContaining
	 *
	 */
	public Set<FoodTageType> findFoodTageTypeByTypeNameContaining(String typeName) throws DataAccessException;

	/**
	 * JPQL Query - findFoodTageTypeByTypeNameContaining
	 *
	 */
	public Set<FoodTageType> findFoodTageTypeByTypeNameContaining(String typeName, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findFoodTageTypeByPrimaryKey
	 *
	 */
	public FoodTageType findFoodTageTypeByPrimaryKey(Integer foodTageTypeId_1) throws DataAccessException;

	/**
	 * JPQL Query - findFoodTageTypeByPrimaryKey
	 *
	 */
	public FoodTageType findFoodTageTypeByPrimaryKey(Integer foodTageTypeId_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findFoodTageTypeByTypeName
	 *
	 */
	public Set<FoodTageType> findFoodTageTypeByTypeName(String typeName_1) throws DataAccessException;

	/**
	 * JPQL Query - findFoodTageTypeByTypeName
	 *
	 */
	public Set<FoodTageType> findFoodTageTypeByTypeName(String typeName_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findAllFoodTageTypes
	 *
	 */
	public Set<FoodTageType> findAllFoodTageTypes() throws DataAccessException;

	/**
	 * JPQL Query - findAllFoodTageTypes
	 *
	 */
	public Set<FoodTageType> findAllFoodTageTypes(int startResult, int maxRows) throws DataAccessException;

}