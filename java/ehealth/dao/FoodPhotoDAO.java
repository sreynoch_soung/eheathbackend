
package ehealth.dao;

import ehealth.domain.FoodPhoto;

import java.util.Set;

import org.skyway.spring.util.dao.JpaDao;

import org.springframework.dao.DataAccessException;

/**
 * DAO to manage FoodPhoto entities.
 * 
 */
public interface FoodPhotoDAO extends JpaDao<FoodPhoto> {

	/**
	 * JPQL Query - findFoodPhotoByUrlContaining
	 *
	 */
	public Set<FoodPhoto> findFoodPhotoByUrlContaining(String url) throws DataAccessException;

	/**
	 * JPQL Query - findFoodPhotoByUrlContaining
	 *
	 */
	public Set<FoodPhoto> findFoodPhotoByUrlContaining(String url, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findFoodPhotoByFinalValidationValue
	 *
	 */
	public Set<FoodPhoto> findFoodPhotoByFinalValidationValue(Integer finalValidationValue) throws DataAccessException;

	/**
	 * JPQL Query - findFoodPhotoByFinalValidationValue
	 *
	 */
	public Set<FoodPhoto> findFoodPhotoByFinalValidationValue(Integer finalValidationValue, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findFoodPhotoByUrl
	 *
	 */
	public Set<FoodPhoto> findFoodPhotoByUrl(String url_1) throws DataAccessException;

	/**
	 * JPQL Query - findFoodPhotoByUrl
	 *
	 */
	public Set<FoodPhoto> findFoodPhotoByUrl(String url_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findFoodPhotoByPrimaryKey
	 *
	 */
	public FoodPhoto findFoodPhotoByPrimaryKey(Integer foodPhotoId) throws DataAccessException;

	/**
	 * JPQL Query - findFoodPhotoByPrimaryKey
	 *
	 */
	public FoodPhoto findFoodPhotoByPrimaryKey(Integer foodPhotoId, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findFoodPhotoByFoodPhotoId
	 *
	 */
	public FoodPhoto findFoodPhotoByFoodPhotoId(Integer foodPhotoId_1) throws DataAccessException;

	/**
	 * JPQL Query - findFoodPhotoByFoodPhotoId
	 *
	 */
	public FoodPhoto findFoodPhotoByFoodPhotoId(Integer foodPhotoId_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findAllFoodPhotos
	 *
	 */
	public Set<FoodPhoto> findAllFoodPhotos() throws DataAccessException;

	/**
	 * JPQL Query - findAllFoodPhotos
	 *
	 */
	public Set<FoodPhoto> findAllFoodPhotos(int startResult, int maxRows) throws DataAccessException;

}