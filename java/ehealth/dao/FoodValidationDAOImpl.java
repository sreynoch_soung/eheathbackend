
package ehealth.dao;

import ehealth.domain.FoodValidation;

import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.skyway.spring.util.dao.AbstractJpaDao;

import org.springframework.dao.DataAccessException;

import org.springframework.stereotype.Repository;

import org.springframework.transaction.annotation.Transactional;

/**
 * DAO to manage FoodValidation entities.
 * 
 */
@Repository("FoodValidationDAO")

@Transactional
public class FoodValidationDAOImpl extends AbstractJpaDao<FoodValidation> implements FoodValidationDAO {

	/**
	 * Set of entity classes managed by this DAO.  Typically a DAO manages a single entity.
	 *
	 */
	private final static Set<Class<?>> dataTypes = new HashSet<Class<?>>(Arrays.asList(new Class<?>[] {
			FoodValidation.class }));

	/**
	 * EntityManager injected by Spring for persistence unit dbdriver
	 *
	 */
	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Instantiates a new FoodValidationDAOImpl
	 *
	 */
	public FoodValidationDAOImpl() {
		super();
	}

	/**
	 * Get the entity manager that manages persistence unit 
	 *
	 */
	public EntityManager getEntityManager() {
		return entityManager;
	}

	/**
	 * Returns the set of entity classes managed by this DAO.
	 *
	 */
	public Set<Class<?>> getTypes() {
		return dataTypes;
	}

	/**
	 * JPQL Query - findFoodValidationByPrimaryKey
	 *
	 */
	@Transactional
	public FoodValidation findFoodValidationByPrimaryKey(Integer foodValidationId) throws DataAccessException {

		return findFoodValidationByPrimaryKey(foodValidationId, -1, -1);
	}

	/**
	 * JPQL Query - findFoodValidationByPrimaryKey
	 *
	 */

	@Transactional
	public FoodValidation findFoodValidationByPrimaryKey(Integer foodValidationId, int startResult, int maxRows) throws DataAccessException {
		try {
			Query query = createNamedQuery("findFoodValidationByPrimaryKey", startResult, maxRows, foodValidationId);
			return (ehealth.domain.FoodValidation) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * JPQL Query - findFoodValidationByValue
	 *
	 */
	@Transactional
	public Set<FoodValidation> findFoodValidationByValue(Boolean value) throws DataAccessException {

		return findFoodValidationByValue(value, -1, -1);
	}

	/**
	 * JPQL Query - findFoodValidationByValue
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<FoodValidation> findFoodValidationByValue(Boolean value, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findFoodValidationByValue", startResult, maxRows, value);
		return new LinkedHashSet<FoodValidation>(query.getResultList());
	}

	/**
	 * JPQL Query - findFoodValidationByTime
	 *
	 */
	@Transactional
	public Set<FoodValidation> findFoodValidationByTime(java.util.Calendar time) throws DataAccessException {

		return findFoodValidationByTime(time, -1, -1);
	}

	/**
	 * JPQL Query - findFoodValidationByTime
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<FoodValidation> findFoodValidationByTime(java.util.Calendar time, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findFoodValidationByTime", startResult, maxRows, time);
		return new LinkedHashSet<FoodValidation>(query.getResultList());
	}

	/**
	 * JPQL Query - findFoodValidationByFoodValidationId
	 *
	 */
	@Transactional
	public FoodValidation findFoodValidationByFoodValidationId(Integer foodValidationId) throws DataAccessException {

		return findFoodValidationByFoodValidationId(foodValidationId, -1, -1);
	}

	/**
	 * JPQL Query - findFoodValidationByFoodValidationId
	 *
	 */

	@Transactional
	public FoodValidation findFoodValidationByFoodValidationId(Integer foodValidationId, int startResult, int maxRows) throws DataAccessException {
		try {
			Query query = createNamedQuery("findFoodValidationByFoodValidationId", startResult, maxRows, foodValidationId);
			return (ehealth.domain.FoodValidation) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * JPQL Query - findAllFoodValidations
	 *
	 */
	@Transactional
	public Set<FoodValidation> findAllFoodValidations() throws DataAccessException {

		return findAllFoodValidations(-1, -1);
	}

	/**
	 * JPQL Query - findAllFoodValidations
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<FoodValidation> findAllFoodValidations(int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findAllFoodValidations", startResult, maxRows);
		return new LinkedHashSet<FoodValidation>(query.getResultList());
	}

	/**
	 * Used to determine whether or not to merge the entity or persist the entity when calling Store
	 * @see store
	 * 
	 *
	 */
	public boolean canBeMerged(FoodValidation entity) {
		return true;
	}
}
