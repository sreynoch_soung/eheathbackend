
package ehealth.dao;

import ehealth.domain.Team;

import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.skyway.spring.util.dao.AbstractJpaDao;

import org.springframework.dao.DataAccessException;

import org.springframework.stereotype.Repository;

import org.springframework.transaction.annotation.Transactional;

/**
 * DAO to manage Team entities.
 * 
 */
@Repository("TeamDAO")

@Transactional
public class TeamDAOImpl extends AbstractJpaDao<Team> implements TeamDAO {

	/**
	 * Set of entity classes managed by this DAO.  Typically a DAO manages a single entity.
	 *
	 */
	private final static Set<Class<?>> dataTypes = new HashSet<Class<?>>(Arrays.asList(new Class<?>[] { Team.class }));

	/**
	 * EntityManager injected by Spring for persistence unit dbdriver
	 *
	 */
	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Instantiates a new TeamDAOImpl
	 *
	 */
	public TeamDAOImpl() {
		super();
	}

	/**
	 * Get the entity manager that manages persistence unit 
	 *
	 */
	public EntityManager getEntityManager() {
		return entityManager;
	}

	/**
	 * Returns the set of entity classes managed by this DAO.
	 *
	 */
	public Set<Class<?>> getTypes() {
		return dataTypes;
	}

	/**
	 * JPQL Query - findTeamByPrimaryKey
	 *
	 */
	@Transactional
	public Team findTeamByPrimaryKey(Integer teamId) throws DataAccessException {

		return findTeamByPrimaryKey(teamId, -1, -1);
	}

	/**
	 * JPQL Query - findTeamByPrimaryKey
	 *
	 */

	@Transactional
	public Team findTeamByPrimaryKey(Integer teamId, int startResult, int maxRows) throws DataAccessException {
		try {
			Query query = createNamedQuery("findTeamByPrimaryKey", startResult, maxRows, teamId);
			return (ehealth.domain.Team) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * JPQL Query - findTeamByTeamId
	 *
	 */
	@Transactional
	public Team findTeamByTeamId(Integer teamId) throws DataAccessException {

		return findTeamByTeamId(teamId, -1, -1);
	}

	/**
	 * JPQL Query - findTeamByTeamId
	 *
	 */

	@Transactional
	public Team findTeamByTeamId(Integer teamId, int startResult, int maxRows) throws DataAccessException {
		try {
			Query query = createNamedQuery("findTeamByTeamId", startResult, maxRows, teamId);
			return (ehealth.domain.Team) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * JPQL Query - findTeamByNumMember
	 *
	 */
	@Transactional
	public Set<Team> findTeamByNumMember(Integer numMember) throws DataAccessException {

		return findTeamByNumMember(numMember, -1, -1);
	}

	/**
	 * JPQL Query - findTeamByNumMember
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<Team> findTeamByNumMember(Integer numMember, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findTeamByNumMember", startResult, maxRows, numMember);
		return new LinkedHashSet<Team>(query.getResultList());
	}

	/**
	 * JPQL Query - findTeamByNameContaining
	 *
	 */
	@Transactional
	public Set<Team> findTeamByNameContaining(String name) throws DataAccessException {

		return findTeamByNameContaining(name, -1, -1);
	}

	/**
	 * JPQL Query - findTeamByNameContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<Team> findTeamByNameContaining(String name, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findTeamByNameContaining", startResult, maxRows, name);
		return new LinkedHashSet<Team>(query.getResultList());
	}

	/**
	 * JPQL Query - findTeamByName
	 *
	 */
	@Transactional
	public Set<Team> findTeamByName(String name) throws DataAccessException {

		return findTeamByName(name, -1, -1);
	}

	/**
	 * JPQL Query - findTeamByName
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<Team> findTeamByName(String name, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findTeamByName", startResult, maxRows, name);
		return new LinkedHashSet<Team>(query.getResultList());
	}

	/**
	 * JPQL Query - findAllTeams
	 *
	 */
	@Transactional
	public Set<Team> findAllTeams() throws DataAccessException {

		return findAllTeams(-1, -1);
	}

	/**
	 * JPQL Query - findAllTeams
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<Team> findAllTeams(int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findAllTeams", startResult, maxRows);
		return new LinkedHashSet<Team>(query.getResultList());
	}

	/**
	 * Used to determine whether or not to merge the entity or persist the entity when calling Store
	 * @see store
	 * 
	 *
	 */
	public boolean canBeMerged(Team entity) {
		return true;
	}
}
