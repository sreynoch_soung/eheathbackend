
package ehealth.dao;

import ehealth.domain.ChatRecord;

import java.util.Calendar;
import java.util.Set;

import org.skyway.spring.util.dao.JpaDao;

import org.springframework.dao.DataAccessException;

/**
 * DAO to manage ChatRecord entities.
 * 
 */
public interface ChatRecordDAO extends JpaDao<ChatRecord> {

	/**
	 * JPQL Query - findChatRecordByDate
	 *
	 */
	public Set<ChatRecord> findChatRecordByDate(java.util.Calendar date) throws DataAccessException;

	/**
	 * JPQL Query - findChatRecordByDate
	 *
	 */
	public Set<ChatRecord> findChatRecordByDate(Calendar date, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findChatRecordByMessageContaining
	 *
	 */
	public Set<ChatRecord> findChatRecordByMessageContaining(String message) throws DataAccessException;

	/**
	 * JPQL Query - findChatRecordByMessageContaining
	 *
	 */
	public Set<ChatRecord> findChatRecordByMessageContaining(String message, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findChatRecordByPrimaryKey
	 *
	 */
	public ChatRecord findChatRecordByPrimaryKey(Integer chatRecordId) throws DataAccessException;

	/**
	 * JPQL Query - findChatRecordByPrimaryKey
	 *
	 */
	public ChatRecord findChatRecordByPrimaryKey(Integer chatRecordId, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findAllChatRecords
	 *
	 */
	public Set<ChatRecord> findAllChatRecords() throws DataAccessException;

	/**
	 * JPQL Query - findAllChatRecords
	 *
	 */
	public Set<ChatRecord> findAllChatRecords(int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findChatRecordByChatRecordId
	 *
	 */
	public ChatRecord findChatRecordByChatRecordId(Integer chatRecordId_1) throws DataAccessException;

	/**
	 * JPQL Query - findChatRecordByChatRecordId
	 *
	 */
	public ChatRecord findChatRecordByChatRecordId(Integer chatRecordId_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findChatRecordByMessage
	 *
	 */
	public Set<ChatRecord> findChatRecordByMessage(String message_1) throws DataAccessException;

	/**
	 * JPQL Query - findChatRecordByMessage
	 *
	 */
	public Set<ChatRecord> findChatRecordByMessage(String message_1, int startResult, int maxRows) throws DataAccessException;

}