
package ehealth.dao;

import ehealth.domain.Grad;

import java.util.Calendar;
import java.util.Set;

import org.skyway.spring.util.dao.JpaDao;

import org.springframework.dao.DataAccessException;

/**
 * DAO to manage Grad entities.
 * 
 */
public interface GradDAO extends JpaDao<Grad> {

	/**
	 * JPQL Query - findGradByDate
	 *
	 */
	public Set<Grad> findGradByDate(java.util.Calendar date) throws DataAccessException;

	/**
	 * JPQL Query - findGradByDate
	 *
	 */
	public Set<Grad> findGradByDate(Calendar date, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findGradByValueContaining
	 *
	 */
	public Set<Grad> findGradByValueContaining(String value) throws DataAccessException;

	/**
	 * JPQL Query - findGradByValueContaining
	 *
	 */
	public Set<Grad> findGradByValueContaining(String value, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findGradByPrimaryKey
	 *
	 */
	public Grad findGradByPrimaryKey(Integer gradId) throws DataAccessException;

	/**
	 * JPQL Query - findGradByPrimaryKey
	 *
	 */
	public Grad findGradByPrimaryKey(Integer gradId, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findGradByValue
	 *
	 */
	public Set<Grad> findGradByValue(String value_1) throws DataAccessException;

	/**
	 * JPQL Query - findGradByValue
	 *
	 */
	public Set<Grad> findGradByValue(String value_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findAllGrads
	 *
	 */
	public Set<Grad> findAllGrads() throws DataAccessException;

	/**
	 * JPQL Query - findAllGrads
	 *
	 */
	public Set<Grad> findAllGrads(int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findGradByGradId
	 *
	 */
	public Grad findGradByGradId(Integer gradId_1) throws DataAccessException;

	/**
	 * JPQL Query - findGradByGradId
	 *
	 */
	public Grad findGradByGradId(Integer gradId_1, int startResult, int maxRows) throws DataAccessException;

}