
package ehealth.dao;

import ehealth.domain.League;

import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.skyway.spring.util.dao.AbstractJpaDao;

import org.springframework.dao.DataAccessException;

import org.springframework.stereotype.Repository;

import org.springframework.transaction.annotation.Transactional;

/**
 * DAO to manage League entities.
 * 
 */
@Repository("LeagueDAO")

@Transactional
public class LeagueDAOImpl extends AbstractJpaDao<League> implements LeagueDAO {

	/**
	 * Set of entity classes managed by this DAO.  Typically a DAO manages a single entity.
	 *
	 */
	private final static Set<Class<?>> dataTypes = new HashSet<Class<?>>(Arrays.asList(new Class<?>[] {
			League.class }));

	/**
	 * EntityManager injected by Spring for persistence unit dbdriver
	 *
	 */
	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Instantiates a new LeagueDAOImpl
	 *
	 */
	public LeagueDAOImpl() {
		super();
	}

	/**
	 * Get the entity manager that manages persistence unit 
	 *
	 */
	public EntityManager getEntityManager() {
		return entityManager;
	}

	/**
	 * Returns the set of entity classes managed by this DAO.
	 *
	 */
	public Set<Class<?>> getTypes() {
		return dataTypes;
	}

	/**
	 * JPQL Query - findLeagueByLeagueName
	 *
	 */
	@Transactional
	public Set<League> findLeagueByLeagueName(String leagueName) throws DataAccessException {

		return findLeagueByLeagueName(leagueName, -1, -1);
	}

	/**
	 * JPQL Query - findLeagueByLeagueName
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<League> findLeagueByLeagueName(String leagueName, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findLeagueByLeagueName", startResult, maxRows, leagueName);
		return new LinkedHashSet<League>(query.getResultList());
	}

	/**
	 * JPQL Query - findLeagueByLeagueId
	 *
	 */
	@Transactional
	public League findLeagueByLeagueId(Integer leagueId) throws DataAccessException {

		return findLeagueByLeagueId(leagueId, -1, -1);
	}

	/**
	 * JPQL Query - findLeagueByLeagueId
	 *
	 */

	@Transactional
	public League findLeagueByLeagueId(Integer leagueId, int startResult, int maxRows) throws DataAccessException {
		try {
			Query query = createNamedQuery("findLeagueByLeagueId", startResult, maxRows, leagueId);
			return (ehealth.domain.League) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * JPQL Query - findLeagueByLeagueNameContaining
	 *
	 */
	@Transactional
	public Set<League> findLeagueByLeagueNameContaining(String leagueName) throws DataAccessException {

		return findLeagueByLeagueNameContaining(leagueName, -1, -1);
	}

	/**
	 * JPQL Query - findLeagueByLeagueNameContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<League> findLeagueByLeagueNameContaining(String leagueName, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findLeagueByLeagueNameContaining", startResult, maxRows, leagueName);
		return new LinkedHashSet<League>(query.getResultList());
	}

	/**
	 * JPQL Query - findLeagueByMatchDateContaining
	 *
	 */
	@Transactional
	public Set<League> findLeagueByMatchDateContaining(String matchDate) throws DataAccessException {

		return findLeagueByMatchDateContaining(matchDate, -1, -1);
	}

	/**
	 * JPQL Query - findLeagueByMatchDateContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<League> findLeagueByMatchDateContaining(String matchDate, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findLeagueByMatchDateContaining", startResult, maxRows, matchDate);
		return new LinkedHashSet<League>(query.getResultList());
	}

	/**
	 * JPQL Query - findLeagueByPoint
	 *
	 */
	@Transactional
	public Set<League> findLeagueByPoint(String point) throws DataAccessException {

		return findLeagueByPoint(point, -1, -1);
	}

	/**
	 * JPQL Query - findLeagueByPoint
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<League> findLeagueByPoint(String point, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findLeagueByPoint", startResult, maxRows, point);
		return new LinkedHashSet<League>(query.getResultList());
	}

	/**
	 * JPQL Query - findAllLeagues
	 *
	 */
	@Transactional
	public Set<League> findAllLeagues() throws DataAccessException {

		return findAllLeagues(-1, -1);
	}

	/**
	 * JPQL Query - findAllLeagues
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<League> findAllLeagues(int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findAllLeagues", startResult, maxRows);
		return new LinkedHashSet<League>(query.getResultList());
	}

	/**
	 * JPQL Query - findLeagueByMatchDate
	 *
	 */
	@Transactional
	public Set<League> findLeagueByMatchDate(String matchDate) throws DataAccessException {

		return findLeagueByMatchDate(matchDate, -1, -1);
	}

	/**
	 * JPQL Query - findLeagueByMatchDate
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<League> findLeagueByMatchDate(String matchDate, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findLeagueByMatchDate", startResult, maxRows, matchDate);
		return new LinkedHashSet<League>(query.getResultList());
	}

	/**
	 * JPQL Query - findLeagueByPrimaryKey
	 *
	 */
	@Transactional
	public League findLeagueByPrimaryKey(Integer leagueId) throws DataAccessException {

		return findLeagueByPrimaryKey(leagueId, -1, -1);
	}

	/**
	 * JPQL Query - findLeagueByPrimaryKey
	 *
	 */

	@Transactional
	public League findLeagueByPrimaryKey(Integer leagueId, int startResult, int maxRows) throws DataAccessException {
		try {
			Query query = createNamedQuery("findLeagueByPrimaryKey", startResult, maxRows, leagueId);
			return (ehealth.domain.League) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * JPQL Query - findLeagueByPointContaining
	 *
	 */
	@Transactional
	public Set<League> findLeagueByPointContaining(String point) throws DataAccessException {

		return findLeagueByPointContaining(point, -1, -1);
	}

	/**
	 * JPQL Query - findLeagueByPointContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<League> findLeagueByPointContaining(String point, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findLeagueByPointContaining", startResult, maxRows, point);
		return new LinkedHashSet<League>(query.getResultList());
	}

	/**
	 * Used to determine whether or not to merge the entity or persist the entity when calling Store
	 * @see store
	 * 
	 *
	 */
	public boolean canBeMerged(League entity) {
		return true;
	}
}
