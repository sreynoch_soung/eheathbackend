
package ehealth.dao;

import ehealth.domain.ParentKid;

import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.skyway.spring.util.dao.AbstractJpaDao;

import org.springframework.dao.DataAccessException;

import org.springframework.stereotype.Repository;

import org.springframework.transaction.annotation.Transactional;

/**
 * DAO to manage ParentKid entities.
 * 
 */
@Repository("ParentKidDAO")

@Transactional
public class ParentKidDAOImpl extends AbstractJpaDao<ParentKid> implements ParentKidDAO {

	/**
	 * Set of entity classes managed by this DAO.  Typically a DAO manages a single entity.
	 *
	 */
	private final static Set<Class<?>> dataTypes = new HashSet<Class<?>>(Arrays.asList(new Class<?>[] {
			ParentKid.class }));

	/**
	 * EntityManager injected by Spring for persistence unit dbdriver
	 *
	 */
	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Instantiates a new ParentKidDAOImpl
	 *
	 */
	public ParentKidDAOImpl() {
		super();
	}

	/**
	 * Get the entity manager that manages persistence unit 
	 *
	 */
	public EntityManager getEntityManager() {
		return entityManager;
	}

	/**
	 * Returns the set of entity classes managed by this DAO.
	 *
	 */
	public Set<Class<?>> getTypes() {
		return dataTypes;
	}

	/**
	 * JPQL Query - findAllParentKids
	 *
	 */
	@Transactional
	public Set<ParentKid> findAllParentKids() throws DataAccessException {

		return findAllParentKids(-1, -1);
	}

	/**
	 * JPQL Query - findAllParentKids
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ParentKid> findAllParentKids(int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findAllParentKids", startResult, maxRows);
		return new LinkedHashSet<ParentKid>(query.getResultList());
	}

	/**
	 * JPQL Query - findParentKidByParentKidId
	 *
	 */
	@Transactional
	public ParentKid findParentKidByParentKidId(Integer parentKidId) throws DataAccessException {

		return findParentKidByParentKidId(parentKidId, -1, -1);
	}

	/**
	 * JPQL Query - findParentKidByParentKidId
	 *
	 */

	@Transactional
	public ParentKid findParentKidByParentKidId(Integer parentKidId, int startResult, int maxRows) throws DataAccessException {
		try {
			Query query = createNamedQuery("findParentKidByParentKidId", startResult, maxRows, parentKidId);
			return (ehealth.domain.ParentKid) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * JPQL Query - findParentKidByPrimaryKey
	 *
	 */
	@Transactional
	public ParentKid findParentKidByPrimaryKey(Integer parentKidId) throws DataAccessException {

		return findParentKidByPrimaryKey(parentKidId, -1, -1);
	}

	/**
	 * JPQL Query - findParentKidByPrimaryKey
	 *
	 */

	@Transactional
	public ParentKid findParentKidByPrimaryKey(Integer parentKidId, int startResult, int maxRows) throws DataAccessException {
		try {
			Query query = createNamedQuery("findParentKidByPrimaryKey", startResult, maxRows, parentKidId);
			return (ehealth.domain.ParentKid) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * Used to determine whether or not to merge the entity or persist the entity when calling Store
	 * @see store
	 * 
	 *
	 */
	public boolean canBeMerged(ParentKid entity) {
		return true;
	}
}
