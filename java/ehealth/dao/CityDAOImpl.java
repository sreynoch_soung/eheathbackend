
package ehealth.dao;

import ehealth.domain.City;

import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.skyway.spring.util.dao.AbstractJpaDao;

import org.springframework.dao.DataAccessException;

import org.springframework.stereotype.Repository;

import org.springframework.transaction.annotation.Transactional;

/**
 * DAO to manage City entities.
 * 
 */
@Repository("CityDAO")

@Transactional
public class CityDAOImpl extends AbstractJpaDao<City> implements CityDAO {

	/**
	 * Set of entity classes managed by this DAO.  Typically a DAO manages a single entity.
	 *
	 */
	private final static Set<Class<?>> dataTypes = new HashSet<Class<?>>(Arrays.asList(new Class<?>[] { City.class }));

	/**
	 * EntityManager injected by Spring for persistence unit dbdriver
	 *
	 */
	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Instantiates a new CityDAOImpl
	 *
	 */
	public CityDAOImpl() {
		super();
	}

	/**
	 * Get the entity manager that manages persistence unit 
	 *
	 */
	public EntityManager getEntityManager() {
		return entityManager;
	}

	/**
	 * Returns the set of entity classes managed by this DAO.
	 *
	 */
	public Set<Class<?>> getTypes() {
		return dataTypes;
	}

	/**
	 * JPQL Query - findCityByNameContaining
	 *
	 */
	@Transactional
	public Set<City> findCityByNameContaining(String name) throws DataAccessException {

		return findCityByNameContaining(name, -1, -1);
	}

	/**
	 * JPQL Query - findCityByNameContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<City> findCityByNameContaining(String name, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findCityByNameContaining", startResult, maxRows, name);
		return new LinkedHashSet<City>(query.getResultList());
	}

	/**
	 * JPQL Query - findCityByCityId
	 *
	 */
	@Transactional
	public City findCityByCityId(Integer cityId) throws DataAccessException {

		return findCityByCityId(cityId, -1, -1);
	}

	/**
	 * JPQL Query - findCityByCityId
	 *
	 */

	@Transactional
	public City findCityByCityId(Integer cityId, int startResult, int maxRows) throws DataAccessException {
		try {
			Query query = createNamedQuery("findCityByCityId", startResult, maxRows, cityId);
			return (ehealth.domain.City) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * JPQL Query - findCityByName
	 *
	 */
	@Transactional
	public Set<City> findCityByName(String name) throws DataAccessException {

		return findCityByName(name, -1, -1);
	}

	/**
	 * JPQL Query - findCityByName
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<City> findCityByName(String name, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findCityByName", startResult, maxRows, name);
		return new LinkedHashSet<City>(query.getResultList());
	}

	/**
	 * JPQL Query - findAllCitys
	 *
	 */
	@Transactional
	public Set<City> findAllCitys() throws DataAccessException {

		return findAllCitys(-1, -1);
	}

	/**
	 * JPQL Query - findAllCitys
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<City> findAllCitys(int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findAllCitys", startResult, maxRows);
		return new LinkedHashSet<City>(query.getResultList());
	}

	/**
	 * JPQL Query - findCityByPrimaryKey
	 *
	 */
	@Transactional
	public City findCityByPrimaryKey(Integer cityId) throws DataAccessException {

		return findCityByPrimaryKey(cityId, -1, -1);
	}

	/**
	 * JPQL Query - findCityByPrimaryKey
	 *
	 */

	@Transactional
	public City findCityByPrimaryKey(Integer cityId, int startResult, int maxRows) throws DataAccessException {
		try {
			Query query = createNamedQuery("findCityByPrimaryKey", startResult, maxRows, cityId);
			return (ehealth.domain.City) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * Used to determine whether or not to merge the entity or persist the entity when calling Store
	 * @see store
	 * 
	 *
	 */
	public boolean canBeMerged(City entity) {
		return true;
	}
}
