
package ehealth.dao;

import ehealth.domain.KidChat;

import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.skyway.spring.util.dao.AbstractJpaDao;

import org.springframework.dao.DataAccessException;

import org.springframework.stereotype.Repository;

import org.springframework.transaction.annotation.Transactional;

/**
 * DAO to manage KidChat entities.
 * 
 */
@Repository("KidChatDAO")

@Transactional
public class KidChatDAOImpl extends AbstractJpaDao<KidChat> implements KidChatDAO {

	/**
	 * Set of entity classes managed by this DAO.  Typically a DAO manages a single entity.
	 *
	 */
	private final static Set<Class<?>> dataTypes = new HashSet<Class<?>>(Arrays.asList(new Class<?>[] {
			KidChat.class }));

	/**
	 * EntityManager injected by Spring for persistence unit dbdriver
	 *
	 */
	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Instantiates a new KidChatDAOImpl
	 *
	 */
	public KidChatDAOImpl() {
		super();
	}

	/**
	 * Get the entity manager that manages persistence unit 
	 *
	 */
	public EntityManager getEntityManager() {
		return entityManager;
	}

	/**
	 * Returns the set of entity classes managed by this DAO.
	 *
	 */
	public Set<Class<?>> getTypes() {
		return dataTypes;
	}

	/**
	 * JPQL Query - findKidChatByPrimaryKey
	 *
	 */
	@Transactional
	public KidChat findKidChatByPrimaryKey(Integer kidChatId) throws DataAccessException {

		return findKidChatByPrimaryKey(kidChatId, -1, -1);
	}

	/**
	 * JPQL Query - findKidChatByPrimaryKey
	 *
	 */

	@Transactional
	public KidChat findKidChatByPrimaryKey(Integer kidChatId, int startResult, int maxRows) throws DataAccessException {
		try {
			Query query = createNamedQuery("findKidChatByPrimaryKey", startResult, maxRows, kidChatId);
			return (ehealth.domain.KidChat) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * JPQL Query - findKidChatByKidChatId
	 *
	 */
	@Transactional
	public KidChat findKidChatByKidChatId(Integer kidChatId) throws DataAccessException {

		return findKidChatByKidChatId(kidChatId, -1, -1);
	}

	/**
	 * JPQL Query - findKidChatByKidChatId
	 *
	 */

	@Transactional
	public KidChat findKidChatByKidChatId(Integer kidChatId, int startResult, int maxRows) throws DataAccessException {
		try {
			Query query = createNamedQuery("findKidChatByKidChatId", startResult, maxRows, kidChatId);
			return (ehealth.domain.KidChat) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * JPQL Query - findAllKidChats
	 *
	 */
	@Transactional
	public Set<KidChat> findAllKidChats() throws DataAccessException {

		return findAllKidChats(-1, -1);
	}

	/**
	 * JPQL Query - findAllKidChats
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<KidChat> findAllKidChats(int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findAllKidChats", startResult, maxRows);
		return new LinkedHashSet<KidChat>(query.getResultList());
	}

	/**
	 * Used to determine whether or not to merge the entity or persist the entity when calling Store
	 * @see store
	 * 
	 *
	 */
	public boolean canBeMerged(KidChat entity) {
		return true;
	}
}
