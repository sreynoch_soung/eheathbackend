
package ehealth.dao;

import ehealth.domain.User;

import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.skyway.spring.util.dao.AbstractJpaDao;

import org.springframework.dao.DataAccessException;

import org.springframework.stereotype.Repository;

import org.springframework.transaction.annotation.Transactional;

/**
 * DAO to manage User entities.
 * 
 */
@Repository("UserDAO")

@Transactional
public class UserDAOImpl extends AbstractJpaDao<User> implements UserDAO {

	/**
	 * Set of entity classes managed by this DAO.  Typically a DAO manages a single entity.
	 *
	 */
	private final static Set<Class<?>> dataTypes = new HashSet<Class<?>>(Arrays.asList(new Class<?>[] { User.class }));

	/**
	 * EntityManager injected by Spring for persistence unit dbdriver
	 *
	 */
	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Instantiates a new UserDAOImpl
	 *
	 */
	public UserDAOImpl() {
		super();
	}

	/**
	 * Get the entity manager that manages persistence unit 
	 *
	 */
	public EntityManager getEntityManager() {
		return entityManager;
	}

	/**
	 * Returns the set of entity classes managed by this DAO.
	 *
	 */
	public Set<Class<?>> getTypes() {
		return dataTypes;
	}

	/**
	 * JPQL Query - findUserByPassword
	 *
	 */
	@Transactional
	public Set<User> findUserByPassword(String password) throws DataAccessException {

		return findUserByPassword(password, -1, -1);
	}

	/**
	 * JPQL Query - findUserByPassword
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<User> findUserByPassword(String password, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findUserByPassword", startResult, maxRows, password);
		return new LinkedHashSet<User>(query.getResultList());
	}

	/**
	 * JPQL Query - findUserByUsernameContaining
	 *
	 */
	@Transactional
	public Set<User> findUserByUsernameContaining(String username) throws DataAccessException {

		return findUserByUsernameContaining(username, -1, -1);
	}

	/**
	 * JPQL Query - findUserByUsernameContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<User> findUserByUsernameContaining(String username, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findUserByUsernameContaining", startResult, maxRows, username);
		return new LinkedHashSet<User>(query.getResultList());
	}

	/**
	 * JPQL Query - findUserByAge
	 *
	 */
	@Transactional
	public Set<User> findUserByAge(Integer age) throws DataAccessException {

		return findUserByAge(age, -1, -1);
	}

	/**
	 * JPQL Query - findUserByAge
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<User> findUserByAge(Integer age, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findUserByAge", startResult, maxRows, age);
		return new LinkedHashSet<User>(query.getResultList());
	}

	/**
	 * JPQL Query - findAllUsers
	 *
	 */
	@Transactional
	public Set<User> findAllUsers() throws DataAccessException {

		return findAllUsers(-1, -1);
	}

	/**
	 * JPQL Query - findAllUsers
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<User> findAllUsers(int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findAllUsers", startResult, maxRows);
		return new LinkedHashSet<User>(query.getResultList());
	}

	/**
	 * JPQL Query - findUserBySexContaining
	 *
	 */
	@Transactional
	public Set<User> findUserBySexContaining(String sex) throws DataAccessException {

		return findUserBySexContaining(sex, -1, -1);
	}

	/**
	 * JPQL Query - findUserBySexContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<User> findUserBySexContaining(String sex, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findUserBySexContaining", startResult, maxRows, sex);
		return new LinkedHashSet<User>(query.getResultList());
	}

	/**
	 * JPQL Query - findUserByName
	 *
	 */
	@Transactional
	public Set<User> findUserByName(String name) throws DataAccessException {

		return findUserByName(name, -1, -1);
	}

	/**
	 * JPQL Query - findUserByName
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<User> findUserByName(String name, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findUserByName", startResult, maxRows, name);
		return new LinkedHashSet<User>(query.getResultList());
	}

	/**
	 * JPQL Query - findUserByTelephoneContaining
	 *
	 */
	@Transactional
	public Set<User> findUserByTelephoneContaining(String telephone) throws DataAccessException {

		return findUserByTelephoneContaining(telephone, -1, -1);
	}

	/**
	 * JPQL Query - findUserByTelephoneContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<User> findUserByTelephoneContaining(String telephone, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findUserByTelephoneContaining", startResult, maxRows, telephone);
		return new LinkedHashSet<User>(query.getResultList());
	}

	/**
	 * JPQL Query - findUserBySurenameContaining
	 *
	 */
	@Transactional
	public Set<User> findUserBySurenameContaining(String surename) throws DataAccessException {

		return findUserBySurenameContaining(surename, -1, -1);
	}

	/**
	 * JPQL Query - findUserBySurenameContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<User> findUserBySurenameContaining(String surename, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findUserBySurenameContaining", startResult, maxRows, surename);
		return new LinkedHashSet<User>(query.getResultList());
	}

	/**
	 * JPQL Query - findUserByNameContaining
	 *
	 */
	@Transactional
	public Set<User> findUserByNameContaining(String name) throws DataAccessException {

		return findUserByNameContaining(name, -1, -1);
	}

	/**
	 * JPQL Query - findUserByNameContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<User> findUserByNameContaining(String name, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findUserByNameContaining", startResult, maxRows, name);
		return new LinkedHashSet<User>(query.getResultList());
	}

	/**
	 * JPQL Query - findUserByPasswordContaining
	 *
	 */
	@Transactional
	public Set<User> findUserByPasswordContaining(String password) throws DataAccessException {

		return findUserByPasswordContaining(password, -1, -1);
	}

	/**
	 * JPQL Query - findUserByPasswordContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<User> findUserByPasswordContaining(String password, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findUserByPasswordContaining", startResult, maxRows, password);
		return new LinkedHashSet<User>(query.getResultList());
	}

	/**
	 * JPQL Query - findUserByEnabled
	 *
	 */
	@Transactional
	public Set<User> findUserByEnabled(Boolean enabled) throws DataAccessException {

		return findUserByEnabled(enabled, -1, -1);
	}

	/**
	 * JPQL Query - findUserByEnabled
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<User> findUserByEnabled(Boolean enabled, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findUserByEnabled", startResult, maxRows, enabled);
		return new LinkedHashSet<User>(query.getResultList());
	}

	/**
	 * JPQL Query - findUserBySex
	 *
	 */
	@Transactional
	public Set<User> findUserBySex(String sex) throws DataAccessException {

		return findUserBySex(sex, -1, -1);
	}

	/**
	 * JPQL Query - findUserBySex
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<User> findUserBySex(String sex, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findUserBySex", startResult, maxRows, sex);
		return new LinkedHashSet<User>(query.getResultList());
	}

	/**
	 * JPQL Query - findUserByUsername
	 *
	 */
	@Transactional
	public User findUserByUsername(String username) throws DataAccessException {

		return findUserByUsername(username, -1, -1);
	}

	/**
	 * JPQL Query - findUserByUsername
	 *
	 */

	@Transactional
	public User findUserByUsername(String username, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findUserByUsername", startResult, maxRows, username);
		return (ehealth.domain.User) query.getSingleResult();
	}

	/**
	 * JPQL Query - findUserByTelephone
	 *
	 */
	@Transactional
	public Set<User> findUserByTelephone(String telephone) throws DataAccessException {

		return findUserByTelephone(telephone, -1, -1);
	}

	/**
	 * JPQL Query - findUserByTelephone
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<User> findUserByTelephone(String telephone, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findUserByTelephone", startResult, maxRows, telephone);
		return new LinkedHashSet<User>(query.getResultList());
	}

	/**
	 * JPQL Query - findUserByPrimaryKey
	 *
	 */
	@Transactional
	public User findUserByPrimaryKey(Integer userId) throws DataAccessException {

		return findUserByPrimaryKey(userId, -1, -1);
	}

	/**
	 * JPQL Query - findUserByPrimaryKey
	 *
	 */

	@Transactional
	public User findUserByPrimaryKey(Integer userId, int startResult, int maxRows) throws DataAccessException {
		try {
			Query query = createNamedQuery("findUserByPrimaryKey", startResult, maxRows, userId);
			return (ehealth.domain.User) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * JPQL Query - findUserByUserId
	 *
	 */
	@Transactional
	public User findUserByUserId(Integer userId) throws DataAccessException {

		return findUserByUserId(userId, -1, -1);
	}

	/**
	 * JPQL Query - findUserByUserId
	 *
	 */

	@Transactional
	public User findUserByUserId(Integer userId, int startResult, int maxRows) throws DataAccessException {
		try {
			Query query = createNamedQuery("findUserByUserId", startResult, maxRows, userId);
			return (ehealth.domain.User) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * JPQL Query - findUserByEmail
	 *
	 */
	@Transactional
	public Set<User> findUserByEmail(String email) throws DataAccessException {

		return findUserByEmail(email, -1, -1);
	}

	/**
	 * JPQL Query - findUserByEmail
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<User> findUserByEmail(String email, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findUserByEmail", startResult, maxRows, email);
		return new LinkedHashSet<User>(query.getResultList());
	}

	/**
	 * JPQL Query - findUserByEmailContaining
	 *
	 */
	@Transactional
	public Set<User> findUserByEmailContaining(String email) throws DataAccessException {

		return findUserByEmailContaining(email, -1, -1);
	}

	/**
	 * JPQL Query - findUserByEmailContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<User> findUserByEmailContaining(String email, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findUserByEmailContaining", startResult, maxRows, email);
		return new LinkedHashSet<User>(query.getResultList());
	}

	/**
	 * JPQL Query - findUserBySurename
	 *
	 */
	@Transactional
	public Set<User> findUserBySurename(String surename) throws DataAccessException {

		return findUserBySurename(surename, -1, -1);
	}

	/**
	 * JPQL Query - findUserBySurename
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<User> findUserBySurename(String surename, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findUserBySurename", startResult, maxRows, surename);
		return new LinkedHashSet<User>(query.getResultList());
	}

	/**
	 * Used to determine whether or not to merge the entity or persist the entity when calling Store
	 * @see store
	 * 
	 *
	 */
	public boolean canBeMerged(User entity) {
		return true;
	}
}
