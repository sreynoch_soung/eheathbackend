
package ehealth.dao;

import ehealth.domain.AccountSetting;

import java.util.Set;

import org.skyway.spring.util.dao.JpaDao;

import org.springframework.dao.DataAccessException;

/**
 * DAO to manage AccountSetting entities.
 * 
 */
public interface AccountSettingDAO extends JpaDao<AccountSetting> {

	/**
	 * JPQL Query - findAccountSettingByAccountSettingId
	 *
	 */
	public AccountSetting findAccountSettingByAccountSettingId(Integer accountSettingId) throws DataAccessException;

	/**
	 * JPQL Query - findAccountSettingByAccountSettingId
	 *
	 */
	public AccountSetting findAccountSettingByAccountSettingId(Integer accountSettingId, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findAccountSettingByNumPhotoValidateGroup
	 *
	 */
	public Set<AccountSetting> findAccountSettingByNumPhotoValidateGroup(Integer numPhotoValidateGroup) throws DataAccessException;

	/**
	 * JPQL Query - findAccountSettingByNumPhotoValidateGroup
	 *
	 */
	public Set<AccountSetting> findAccountSettingByNumPhotoValidateGroup(Integer numPhotoValidateGroup, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findAllAccountSettings
	 *
	 */
	public Set<AccountSetting> findAllAccountSettings() throws DataAccessException;

	/**
	 * JPQL Query - findAllAccountSettings
	 *
	 */
	public Set<AccountSetting> findAllAccountSettings(int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findAccountSettingByPrimaryKey
	 *
	 */
	public AccountSetting findAccountSettingByPrimaryKey(Integer accountSettingId_1) throws DataAccessException;

	/**
	 * JPQL Query - findAccountSettingByPrimaryKey
	 *
	 */
	public AccountSetting findAccountSettingByPrimaryKey(Integer accountSettingId_1, int startResult, int maxRows) throws DataAccessException;

}