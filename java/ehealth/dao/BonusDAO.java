
package ehealth.dao;

import ehealth.domain.Bonus;

import java.util.Set;

import org.skyway.spring.util.dao.JpaDao;

import org.springframework.dao.DataAccessException;

/**
 * DAO to manage Bonus entities.
 * 
 */
public interface BonusDAO extends JpaDao<Bonus> {

	/**
	 * JPQL Query - findBonusByActivityTypeContaining
	 *
	 */
	public Set<Bonus> findBonusByActivityTypeContaining(String activityType) throws DataAccessException;

	/**
	 * JPQL Query - findBonusByActivityTypeContaining
	 *
	 */
	public Set<Bonus> findBonusByActivityTypeContaining(String activityType, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findAllBonuss
	 *
	 */
	public Set<Bonus> findAllBonuss() throws DataAccessException;

	/**
	 * JPQL Query - findAllBonuss
	 *
	 */
	public Set<Bonus> findAllBonuss(int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findBonusByActivityType
	 *
	 */
	public Set<Bonus> findBonusByActivityType(String activityType_1) throws DataAccessException;

	/**
	 * JPQL Query - findBonusByActivityType
	 *
	 */
	public Set<Bonus> findBonusByActivityType(String activityType_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findBonusByPrimaryKey
	 *
	 */
	public Bonus findBonusByPrimaryKey(Integer bonusId) throws DataAccessException;

	/**
	 * JPQL Query - findBonusByPrimaryKey
	 *
	 */
	public Bonus findBonusByPrimaryKey(Integer bonusId, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findBonusByPoint
	 *
	 */
	public Set<Bonus> findBonusByPoint(Integer point) throws DataAccessException;

	/**
	 * JPQL Query - findBonusByPoint
	 *
	 */
	public Set<Bonus> findBonusByPoint(Integer point, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findBonusByBonusId
	 *
	 */
	public Bonus findBonusByBonusId(Integer bonusId_1) throws DataAccessException;

	/**
	 * JPQL Query - findBonusByBonusId
	 *
	 */
	public Bonus findBonusByBonusId(Integer bonusId_1, int startResult, int maxRows) throws DataAccessException;

}