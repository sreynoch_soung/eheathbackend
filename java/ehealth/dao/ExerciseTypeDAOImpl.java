
package ehealth.dao;

import ehealth.domain.ExerciseType;

import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.skyway.spring.util.dao.AbstractJpaDao;

import org.springframework.dao.DataAccessException;

import org.springframework.stereotype.Repository;

import org.springframework.transaction.annotation.Transactional;

/**
 * DAO to manage ExerciseType entities.
 * 
 */
@Repository("ExerciseTypeDAO")

@Transactional
public class ExerciseTypeDAOImpl extends AbstractJpaDao<ExerciseType> implements ExerciseTypeDAO {

	/**
	 * Set of entity classes managed by this DAO.  Typically a DAO manages a single entity.
	 *
	 */
	private final static Set<Class<?>> dataTypes = new HashSet<Class<?>>(Arrays.asList(new Class<?>[] {
			ExerciseType.class }));

	/**
	 * EntityManager injected by Spring for persistence unit dbdriver
	 *
	 */
	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Instantiates a new ExerciseTypeDAOImpl
	 *
	 */
	public ExerciseTypeDAOImpl() {
		super();
	}

	/**
	 * Get the entity manager that manages persistence unit 
	 *
	 */
	public EntityManager getEntityManager() {
		return entityManager;
	}

	/**
	 * Returns the set of entity classes managed by this DAO.
	 *
	 */
	public Set<Class<?>> getTypes() {
		return dataTypes;
	}

	/**
	 * JPQL Query - findExerciseTypeByTypeName
	 *
	 */
	@Transactional
	public Set<ExerciseType> findExerciseTypeByTypeName(String typeName) throws DataAccessException {

		return findExerciseTypeByTypeName(typeName, -1, -1);
	}

	/**
	 * JPQL Query - findExerciseTypeByTypeName
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ExerciseType> findExerciseTypeByTypeName(String typeName, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findExerciseTypeByTypeName", startResult, maxRows, typeName);
		return new LinkedHashSet<ExerciseType>(query.getResultList());
	}

	/**
	 * JPQL Query - findAllExerciseTypes
	 *
	 */
	@Transactional
	public Set<ExerciseType> findAllExerciseTypes() throws DataAccessException {

		return findAllExerciseTypes(-1, -1);
	}

	/**
	 * JPQL Query - findAllExerciseTypes
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ExerciseType> findAllExerciseTypes(int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findAllExerciseTypes", startResult, maxRows);
		return new LinkedHashSet<ExerciseType>(query.getResultList());
	}

	/**
	 * JPQL Query - findExerciseTypeByPrimaryKey
	 *
	 */
	@Transactional
	public ExerciseType findExerciseTypeByPrimaryKey(Integer exerciseTypeId) throws DataAccessException {

		return findExerciseTypeByPrimaryKey(exerciseTypeId, -1, -1);
	}

	/**
	 * JPQL Query - findExerciseTypeByPrimaryKey
	 *
	 */

	@Transactional
	public ExerciseType findExerciseTypeByPrimaryKey(Integer exerciseTypeId, int startResult, int maxRows) throws DataAccessException {
		try {
			Query query = createNamedQuery("findExerciseTypeByPrimaryKey", startResult, maxRows, exerciseTypeId);
			return (ehealth.domain.ExerciseType) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * JPQL Query - findExerciseTypeByExerciseTypeId
	 *
	 */
	@Transactional
	public ExerciseType findExerciseTypeByExerciseTypeId(Integer exerciseTypeId) throws DataAccessException {

		return findExerciseTypeByExerciseTypeId(exerciseTypeId, -1, -1);
	}

	/**
	 * JPQL Query - findExerciseTypeByExerciseTypeId
	 *
	 */

	@Transactional
	public ExerciseType findExerciseTypeByExerciseTypeId(Integer exerciseTypeId, int startResult, int maxRows) throws DataAccessException {
		try {
			Query query = createNamedQuery("findExerciseTypeByExerciseTypeId", startResult, maxRows, exerciseTypeId);
			return (ehealth.domain.ExerciseType) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * JPQL Query - findExerciseTypeByTypeNameContaining
	 *
	 */
	@Transactional
	public Set<ExerciseType> findExerciseTypeByTypeNameContaining(String typeName) throws DataAccessException {

		return findExerciseTypeByTypeNameContaining(typeName, -1, -1);
	}

	/**
	 * JPQL Query - findExerciseTypeByTypeNameContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ExerciseType> findExerciseTypeByTypeNameContaining(String typeName, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findExerciseTypeByTypeNameContaining", startResult, maxRows, typeName);
		return new LinkedHashSet<ExerciseType>(query.getResultList());
	}

	/**
	 * Used to determine whether or not to merge the entity or persist the entity when calling Store
	 * @see store
	 * 
	 *
	 */
	public boolean canBeMerged(ExerciseType entity) {
		return true;
	}
}
