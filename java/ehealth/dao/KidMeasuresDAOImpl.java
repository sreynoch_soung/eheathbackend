
package ehealth.dao;

import ehealth.domain.KidMeasures;

import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.skyway.spring.util.dao.AbstractJpaDao;

import org.springframework.dao.DataAccessException;

import org.springframework.stereotype.Repository;

import org.springframework.transaction.annotation.Transactional;

/**
 * DAO to manage KidMeasures entities.
 * 
 */
@Repository("KidMeasuresDAO")

@Transactional
public class KidMeasuresDAOImpl extends AbstractJpaDao<KidMeasures> implements KidMeasuresDAO {

	/**
	 * Set of entity classes managed by this DAO.  Typically a DAO manages a single entity.
	 *
	 */
	private final static Set<Class<?>> dataTypes = new HashSet<Class<?>>(Arrays.asList(new Class<?>[] {
			KidMeasures.class }));

	/**
	 * EntityManager injected by Spring for persistence unit dbdriver
	 *
	 */
	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Instantiates a new KidMeasuresDAOImpl
	 *
	 */
	public KidMeasuresDAOImpl() {
		super();
	}

	/**
	 * Get the entity manager that manages persistence unit 
	 *
	 */
	public EntityManager getEntityManager() {
		return entityManager;
	}

	/**
	 * Returns the set of entity classes managed by this DAO.
	 *
	 */
	public Set<Class<?>> getTypes() {
		return dataTypes;
	}

	/**
	 * JPQL Query - findAllKidMeasuress
	 *
	 */
	@Transactional
	public Set<KidMeasures> findAllKidMeasuress() throws DataAccessException {

		return findAllKidMeasuress(-1, -1);
	}

	/**
	 * JPQL Query - findAllKidMeasuress
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<KidMeasures> findAllKidMeasuress(int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findAllKidMeasuress", startResult, maxRows);
		return new LinkedHashSet<KidMeasures>(query.getResultList());
	}

	/**
	 * JPQL Query - findKidMeasuresByMets
	 *
	 */
	@Transactional
	public Set<KidMeasures> findKidMeasuresByMets(Integer mets) throws DataAccessException {

		return findKidMeasuresByMets(mets, -1, -1);
	}

	/**
	 * JPQL Query - findKidMeasuresByMets
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<KidMeasures> findKidMeasuresByMets(Integer mets, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findKidMeasuresByMets", startResult, maxRows, mets);
		return new LinkedHashSet<KidMeasures>(query.getResultList());
	}

	/**
	 * JPQL Query - findKidMeasuresByHabitContaining
	 *
	 */
	@Transactional
	public Set<KidMeasures> findKidMeasuresByHabitContaining(String habit) throws DataAccessException {

		return findKidMeasuresByHabitContaining(habit, -1, -1);
	}

	/**
	 * JPQL Query - findKidMeasuresByHabitContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<KidMeasures> findKidMeasuresByHabitContaining(String habit, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findKidMeasuresByHabitContaining", startResult, maxRows, habit);
		return new LinkedHashSet<KidMeasures>(query.getResultList());
	}

	/**
	 * JPQL Query - findKidMeasuresByBloodSugarLevel
	 *
	 */
	@Transactional
	public Set<KidMeasures> findKidMeasuresByBloodSugarLevel(Integer bloodSugarLevel) throws DataAccessException {

		return findKidMeasuresByBloodSugarLevel(bloodSugarLevel, -1, -1);
	}

	/**
	 * JPQL Query - findKidMeasuresByBloodSugarLevel
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<KidMeasures> findKidMeasuresByBloodSugarLevel(Integer bloodSugarLevel, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findKidMeasuresByBloodSugarLevel", startResult, maxRows, bloodSugarLevel);
		return new LinkedHashSet<KidMeasures>(query.getResultList());
	}

	/**
	 * JPQL Query - findKidMeasuresByKidMeasuresId
	 *
	 */
	@Transactional
	public KidMeasures findKidMeasuresByKidMeasuresId(Integer kidMeasuresId) throws DataAccessException {

		return findKidMeasuresByKidMeasuresId(kidMeasuresId, -1, -1);
	}

	/**
	 * JPQL Query - findKidMeasuresByKidMeasuresId
	 *
	 */

	@Transactional
	public KidMeasures findKidMeasuresByKidMeasuresId(Integer kidMeasuresId, int startResult, int maxRows) throws DataAccessException {
		try {
			Query query = createNamedQuery("findKidMeasuresByKidMeasuresId", startResult, maxRows, kidMeasuresId);
			return (ehealth.domain.KidMeasures) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * JPQL Query - findKidMeasuresByHabit
	 *
	 */
	@Transactional
	public Set<KidMeasures> findKidMeasuresByHabit(String habit) throws DataAccessException {

		return findKidMeasuresByHabit(habit, -1, -1);
	}

	/**
	 * JPQL Query - findKidMeasuresByHabit
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<KidMeasures> findKidMeasuresByHabit(String habit, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findKidMeasuresByHabit", startResult, maxRows, habit);
		return new LinkedHashSet<KidMeasures>(query.getResultList());
	}

	/**
	 * JPQL Query - findKidMeasuresByGlucoseLevel
	 *
	 */
	@Transactional
	public Set<KidMeasures> findKidMeasuresByGlucoseLevel(Integer glucoseLevel) throws DataAccessException {

		return findKidMeasuresByGlucoseLevel(glucoseLevel, -1, -1);
	}

	/**
	 * JPQL Query - findKidMeasuresByGlucoseLevel
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<KidMeasures> findKidMeasuresByGlucoseLevel(Integer glucoseLevel, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findKidMeasuresByGlucoseLevel", startResult, maxRows, glucoseLevel);
		return new LinkedHashSet<KidMeasures>(query.getResultList());
	}

	/**
	 * JPQL Query - findKidMeasuresByPrimaryKey
	 *
	 */
	@Transactional
	public KidMeasures findKidMeasuresByPrimaryKey(Integer kidMeasuresId) throws DataAccessException {

		return findKidMeasuresByPrimaryKey(kidMeasuresId, -1, -1);
	}

	/**
	 * JPQL Query - findKidMeasuresByPrimaryKey
	 *
	 */

	@Transactional
	public KidMeasures findKidMeasuresByPrimaryKey(Integer kidMeasuresId, int startResult, int maxRows) throws DataAccessException {
		try {
			Query query = createNamedQuery("findKidMeasuresByPrimaryKey", startResult, maxRows, kidMeasuresId);
			return (ehealth.domain.KidMeasures) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * Used to determine whether or not to merge the entity or persist the entity when calling Store
	 * @see store
	 * 
	 *
	 */
	public boolean canBeMerged(KidMeasures entity) {
		return true;
	}
}
