
package ehealth.dao;

import ehealth.domain.Parent;

import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.skyway.spring.util.dao.AbstractJpaDao;

import org.springframework.dao.DataAccessException;

import org.springframework.stereotype.Repository;

import org.springframework.transaction.annotation.Transactional;

/**
 * DAO to manage Parent entities.
 * 
 */
@Repository("ParentDAO")

@Transactional
public class ParentDAOImpl extends AbstractJpaDao<Parent> implements ParentDAO {

	/**
	 * Set of entity classes managed by this DAO.  Typically a DAO manages a single entity.
	 *
	 */
	private final static Set<Class<?>> dataTypes = new HashSet<Class<?>>(Arrays.asList(new Class<?>[] {
			Parent.class }));

	/**
	 * EntityManager injected by Spring for persistence unit dbdriver
	 *
	 */
	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Instantiates a new ParentDAOImpl
	 *
	 */
	public ParentDAOImpl() {
		super();
	}

	/**
	 * Get the entity manager that manages persistence unit 
	 *
	 */
	public EntityManager getEntityManager() {
		return entityManager;
	}

	/**
	 * Returns the set of entity classes managed by this DAO.
	 *
	 */
	public Set<Class<?>> getTypes() {
		return dataTypes;
	}

	/**
	 * JPQL Query - findParentByPrimaryKey
	 *
	 */
	@Transactional
	public Parent findParentByPrimaryKey(Integer parentId) throws DataAccessException {

		return findParentByPrimaryKey(parentId, -1, -1);
	}

	/**
	 * JPQL Query - findParentByPrimaryKey
	 *
	 */

	@Transactional
	public Parent findParentByPrimaryKey(Integer parentId, int startResult, int maxRows) throws DataAccessException {
		try {
			Query query = createNamedQuery("findParentByPrimaryKey", startResult, maxRows, parentId);
			return (ehealth.domain.Parent) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * JPQL Query - findParentByAddressContaining
	 *
	 */
	@Transactional
	public Set<Parent> findParentByAddressContaining(String address) throws DataAccessException {

		return findParentByAddressContaining(address, -1, -1);
	}

	/**
	 * JPQL Query - findParentByAddressContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<Parent> findParentByAddressContaining(String address, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findParentByAddressContaining", startResult, maxRows, address);
		return new LinkedHashSet<Parent>(query.getResultList());
	}

	/**
	 * JPQL Query - findParentByAddress
	 *
	 */
	@Transactional
	public Set<Parent> findParentByAddress(String address) throws DataAccessException {

		return findParentByAddress(address, -1, -1);
	}

	/**
	 * JPQL Query - findParentByAddress
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<Parent> findParentByAddress(String address, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findParentByAddress", startResult, maxRows, address);
		return new LinkedHashSet<Parent>(query.getResultList());
	}

	/**
	 * JPQL Query - findParentByOccupation
	 *
	 */
	@Transactional
	public Set<Parent> findParentByOccupation(String occupation) throws DataAccessException {

		return findParentByOccupation(occupation, -1, -1);
	}

	/**
	 * JPQL Query - findParentByOccupation
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<Parent> findParentByOccupation(String occupation, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findParentByOccupation", startResult, maxRows, occupation);
		return new LinkedHashSet<Parent>(query.getResultList());
	}

	/**
	 * JPQL Query - findAllParents
	 *
	 */
	@Transactional
	public Set<Parent> findAllParents() throws DataAccessException {

		return findAllParents(-1, -1);
	}

	/**
	 * JPQL Query - findAllParents
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<Parent> findAllParents(int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findAllParents", startResult, maxRows);
		return new LinkedHashSet<Parent>(query.getResultList());
	}

	/**
	 * JPQL Query - findParentByOccupationContaining
	 *
	 */
	@Transactional
	public Set<Parent> findParentByOccupationContaining(String occupation) throws DataAccessException {

		return findParentByOccupationContaining(occupation, -1, -1);
	}

	/**
	 * JPQL Query - findParentByOccupationContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<Parent> findParentByOccupationContaining(String occupation, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findParentByOccupationContaining", startResult, maxRows, occupation);
		return new LinkedHashSet<Parent>(query.getResultList());
	}

	/**
	 * JPQL Query - findParentByParentId
	 *
	 */
	@Transactional
	public Parent findParentByParentId(Integer parentId) throws DataAccessException {

		return findParentByParentId(parentId, -1, -1);
	}

	/**
	 * JPQL Query - findParentByParentId
	 *
	 */

	@Transactional
	public Parent findParentByParentId(Integer parentId, int startResult, int maxRows) throws DataAccessException {
		try {
			Query query = createNamedQuery("findParentByParentId", startResult, maxRows, parentId);
			return (ehealth.domain.Parent) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * Used to determine whether or not to merge the entity or persist the entity when calling Store
	 * @see store
	 * 
	 *
	 */
	public boolean canBeMerged(Parent entity) {
		return true;
	}
}
