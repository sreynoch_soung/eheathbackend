
package ehealth.dao;

import ehealth.domain.ExerciseTarget;

import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.skyway.spring.util.dao.AbstractJpaDao;

import org.springframework.dao.DataAccessException;

import org.springframework.stereotype.Repository;

import org.springframework.transaction.annotation.Transactional;

/**
 * DAO to manage ExerciseTarget entities.
 * 
 */
@Repository("ExerciseTargetDAO")

@Transactional
public class ExerciseTargetDAOImpl extends AbstractJpaDao<ExerciseTarget> implements ExerciseTargetDAO {

	/**
	 * Set of entity classes managed by this DAO.  Typically a DAO manages a single entity.
	 *
	 */
	private final static Set<Class<?>> dataTypes = new HashSet<Class<?>>(Arrays.asList(new Class<?>[] {
			ExerciseTarget.class }));

	/**
	 * EntityManager injected by Spring for persistence unit dbdriver
	 *
	 */
	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Instantiates a new ExerciseTargetDAOImpl
	 *
	 */
	public ExerciseTargetDAOImpl() {
		super();
	}

	/**
	 * Get the entity manager that manages persistence unit 
	 *
	 */
	public EntityManager getEntityManager() {
		return entityManager;
	}

	/**
	 * Returns the set of entity classes managed by this DAO.
	 *
	 */
	public Set<Class<?>> getTypes() {
		return dataTypes;
	}

	/**
	 * JPQL Query - findExerciseTargetByDate
	 *
	 */
	@Transactional
	public Set<ExerciseTarget> findExerciseTargetByDate(java.util.Calendar date) throws DataAccessException {

		return findExerciseTargetByDate(date, -1, -1);
	}

	/**
	 * JPQL Query - findExerciseTargetByDate
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ExerciseTarget> findExerciseTargetByDate(java.util.Calendar date, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findExerciseTargetByDate", startResult, maxRows, date);
		return new LinkedHashSet<ExerciseTarget>(query.getResultList());
	}

	/**
	 * JPQL Query - findExerciseTargetByPrimaryKey
	 *
	 */
	@Transactional
	public ExerciseTarget findExerciseTargetByPrimaryKey(Integer exerciseTargetId) throws DataAccessException {

		return findExerciseTargetByPrimaryKey(exerciseTargetId, -1, -1);
	}

	/**
	 * JPQL Query - findExerciseTargetByPrimaryKey
	 *
	 */

	@Transactional
	public ExerciseTarget findExerciseTargetByPrimaryKey(Integer exerciseTargetId, int startResult, int maxRows) throws DataAccessException {
		try {
			Query query = createNamedQuery("findExerciseTargetByPrimaryKey", startResult, maxRows, exerciseTargetId);
			return (ehealth.domain.ExerciseTarget) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * JPQL Query - findExerciseTargetByValue
	 *
	 */
	@Transactional
	public Set<ExerciseTarget> findExerciseTargetByValue(java.math.BigDecimal value) throws DataAccessException {

		return findExerciseTargetByValue(value, -1, -1);
	}

	/**
	 * JPQL Query - findExerciseTargetByValue
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ExerciseTarget> findExerciseTargetByValue(java.math.BigDecimal value, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findExerciseTargetByValue", startResult, maxRows, value);
		return new LinkedHashSet<ExerciseTarget>(query.getResultList());
	}

	/**
	 * JPQL Query - findAllExerciseTargets
	 *
	 */
	@Transactional
	public Set<ExerciseTarget> findAllExerciseTargets() throws DataAccessException {

		return findAllExerciseTargets(-1, -1);
	}

	/**
	 * JPQL Query - findAllExerciseTargets
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ExerciseTarget> findAllExerciseTargets(int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findAllExerciseTargets", startResult, maxRows);
		return new LinkedHashSet<ExerciseTarget>(query.getResultList());
	}

	/**
	 * JPQL Query - findExerciseTargetByExerciseTargetId
	 *
	 */
	@Transactional
	public ExerciseTarget findExerciseTargetByExerciseTargetId(Integer exerciseTargetId) throws DataAccessException {

		return findExerciseTargetByExerciseTargetId(exerciseTargetId, -1, -1);
	}

	/**
	 * JPQL Query - findExerciseTargetByExerciseTargetId
	 *
	 */

	@Transactional
	public ExerciseTarget findExerciseTargetByExerciseTargetId(Integer exerciseTargetId, int startResult, int maxRows) throws DataAccessException {
		try {
			Query query = createNamedQuery("findExerciseTargetByExerciseTargetId", startResult, maxRows, exerciseTargetId);
			return (ehealth.domain.ExerciseTarget) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * Used to determine whether or not to merge the entity or persist the entity when calling Store
	 * @see store
	 * 
	 *
	 */
	public boolean canBeMerged(ExerciseTarget entity) {
		return true;
	}
}
