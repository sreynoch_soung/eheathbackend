
package ehealth.dao;

import ehealth.domain.Country;

import java.util.Set;

import org.skyway.spring.util.dao.JpaDao;

import org.springframework.dao.DataAccessException;

/**
 * DAO to manage Country entities.
 * 
 */
public interface CountryDAO extends JpaDao<Country> {

	/**
	 * JPQL Query - findCountryByCountryId
	 *
	 */
	public Country findCountryByCountryId(Integer countryId) throws DataAccessException;

	/**
	 * JPQL Query - findCountryByCountryId
	 *
	 */
	public Country findCountryByCountryId(Integer countryId, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findCountryByNameContaining
	 *
	 */
	public Set<Country> findCountryByNameContaining(String name) throws DataAccessException;

	/**
	 * JPQL Query - findCountryByNameContaining
	 *
	 */
	public Set<Country> findCountryByNameContaining(String name, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findAllCountrys
	 *
	 */
	public Set<Country> findAllCountrys() throws DataAccessException;

	/**
	 * JPQL Query - findAllCountrys
	 *
	 */
	public Set<Country> findAllCountrys(int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findCountryByName
	 *
	 */
	public Set<Country> findCountryByName(String name_1) throws DataAccessException;

	/**
	 * JPQL Query - findCountryByName
	 *
	 */
	public Set<Country> findCountryByName(String name_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findCountryByPrimaryKey
	 *
	 */
	public Country findCountryByPrimaryKey(Integer countryId_1) throws DataAccessException;

	/**
	 * JPQL Query - findCountryByPrimaryKey
	 *
	 */
	public Country findCountryByPrimaryKey(Integer countryId_1, int startResult, int maxRows) throws DataAccessException;

}