
package ehealth.dao;

import ehealth.domain.ParentKid;

import java.util.Set;

import org.skyway.spring.util.dao.JpaDao;

import org.springframework.dao.DataAccessException;

/**
 * DAO to manage ParentKid entities.
 * 
 */
public interface ParentKidDAO extends JpaDao<ParentKid> {

	/**
	 * JPQL Query - findAllParentKids
	 *
	 */
	public Set<ParentKid> findAllParentKids() throws DataAccessException;

	/**
	 * JPQL Query - findAllParentKids
	 *
	 */
	public Set<ParentKid> findAllParentKids(int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findParentKidByParentKidId
	 *
	 */
	public ParentKid findParentKidByParentKidId(Integer parentKidId) throws DataAccessException;

	/**
	 * JPQL Query - findParentKidByParentKidId
	 *
	 */
	public ParentKid findParentKidByParentKidId(Integer parentKidId, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findParentKidByPrimaryKey
	 *
	 */
	public ParentKid findParentKidByPrimaryKey(Integer parentKidId_1) throws DataAccessException;

	/**
	 * JPQL Query - findParentKidByPrimaryKey
	 *
	 */
	public ParentKid findParentKidByPrimaryKey(Integer parentKidId_1, int startResult, int maxRows) throws DataAccessException;

}