
package ehealth.dao;

import ehealth.domain.FoodPhoto;

import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.skyway.spring.util.dao.AbstractJpaDao;

import org.springframework.dao.DataAccessException;

import org.springframework.stereotype.Repository;

import org.springframework.transaction.annotation.Transactional;

/**
 * DAO to manage FoodPhoto entities.
 * 
 */
@Repository("FoodPhotoDAO")

@Transactional
public class FoodPhotoDAOImpl extends AbstractJpaDao<FoodPhoto> implements FoodPhotoDAO {

	/**
	 * Set of entity classes managed by this DAO.  Typically a DAO manages a single entity.
	 *
	 */
	private final static Set<Class<?>> dataTypes = new HashSet<Class<?>>(Arrays.asList(new Class<?>[] {
			FoodPhoto.class }));

	/**
	 * EntityManager injected by Spring for persistence unit dbdriver
	 *
	 */
	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Instantiates a new FoodPhotoDAOImpl
	 *
	 */
	public FoodPhotoDAOImpl() {
		super();
	}

	/**
	 * Get the entity manager that manages persistence unit 
	 *
	 */
	public EntityManager getEntityManager() {
		return entityManager;
	}

	/**
	 * Returns the set of entity classes managed by this DAO.
	 *
	 */
	public Set<Class<?>> getTypes() {
		return dataTypes;
	}

	/**
	 * JPQL Query - findFoodPhotoByUrlContaining
	 *
	 */
	@Transactional
	public Set<FoodPhoto> findFoodPhotoByUrlContaining(String url) throws DataAccessException {

		return findFoodPhotoByUrlContaining(url, -1, -1);
	}

	/**
	 * JPQL Query - findFoodPhotoByUrlContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<FoodPhoto> findFoodPhotoByUrlContaining(String url, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findFoodPhotoByUrlContaining", startResult, maxRows, url);
		return new LinkedHashSet<FoodPhoto>(query.getResultList());
	}

	/**
	 * JPQL Query - findFoodPhotoByFinalValidationValue
	 *
	 */
	@Transactional
	public Set<FoodPhoto> findFoodPhotoByFinalValidationValue(Integer finalValidationValue) throws DataAccessException {

		return findFoodPhotoByFinalValidationValue(finalValidationValue, -1, -1);
	}

	/**
	 * JPQL Query - findFoodPhotoByFinalValidationValue
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<FoodPhoto> findFoodPhotoByFinalValidationValue(Integer finalValidationValue, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findFoodPhotoByFinalValidationValue", startResult, maxRows, finalValidationValue);
		return new LinkedHashSet<FoodPhoto>(query.getResultList());
	}

	/**
	 * JPQL Query - findFoodPhotoByUrl
	 *
	 */
	@Transactional
	public Set<FoodPhoto> findFoodPhotoByUrl(String url) throws DataAccessException {

		return findFoodPhotoByUrl(url, -1, -1);
	}

	/**
	 * JPQL Query - findFoodPhotoByUrl
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<FoodPhoto> findFoodPhotoByUrl(String url, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findFoodPhotoByUrl", startResult, maxRows, url);
		return new LinkedHashSet<FoodPhoto>(query.getResultList());
	}

	/**
	 * JPQL Query - findFoodPhotoByPrimaryKey
	 *
	 */
	@Transactional
	public FoodPhoto findFoodPhotoByPrimaryKey(Integer foodPhotoId) throws DataAccessException {

		return findFoodPhotoByPrimaryKey(foodPhotoId, -1, -1);
	}

	/**
	 * JPQL Query - findFoodPhotoByPrimaryKey
	 *
	 */

	@Transactional
	public FoodPhoto findFoodPhotoByPrimaryKey(Integer foodPhotoId, int startResult, int maxRows) throws DataAccessException {
		try {
			Query query = createNamedQuery("findFoodPhotoByPrimaryKey", startResult, maxRows, foodPhotoId);
			return (ehealth.domain.FoodPhoto) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * JPQL Query - findFoodPhotoByFoodPhotoId
	 *
	 */
	@Transactional
	public FoodPhoto findFoodPhotoByFoodPhotoId(Integer foodPhotoId) throws DataAccessException {

		return findFoodPhotoByFoodPhotoId(foodPhotoId, -1, -1);
	}

	/**
	 * JPQL Query - findFoodPhotoByFoodPhotoId
	 *
	 */

	@Transactional
	public FoodPhoto findFoodPhotoByFoodPhotoId(Integer foodPhotoId, int startResult, int maxRows) throws DataAccessException {
		try {
			Query query = createNamedQuery("findFoodPhotoByFoodPhotoId", startResult, maxRows, foodPhotoId);
			return (ehealth.domain.FoodPhoto) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * JPQL Query - findAllFoodPhotos
	 *
	 */
	@Transactional
	public Set<FoodPhoto> findAllFoodPhotos() throws DataAccessException {

		return findAllFoodPhotos(-1, -1);
	}

	/**
	 * JPQL Query - findAllFoodPhotos
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<FoodPhoto> findAllFoodPhotos(int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findAllFoodPhotos", startResult, maxRows);
		return new LinkedHashSet<FoodPhoto>(query.getResultList());
	}

	/**
	 * Used to determine whether or not to merge the entity or persist the entity when calling Store
	 * @see store
	 * 
	 *
	 */
	public boolean canBeMerged(FoodPhoto entity) {
		return true;
	}
}
