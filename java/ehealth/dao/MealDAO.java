
package ehealth.dao;

import ehealth.domain.Meal;

import java.util.Calendar;
import java.util.Set;

import org.skyway.spring.util.dao.JpaDao;

import org.springframework.dao.DataAccessException;

/**
 * DAO to manage Meal entities.
 * 
 */
public interface MealDAO extends JpaDao<Meal> {

	/**
	 * JPQL Query - findMealByMealId
	 *
	 */
	public Meal findMealByMealId(Integer mealId) throws DataAccessException;

	/**
	 * JPQL Query - findMealByMealId
	 *
	 */
	public Meal findMealByMealId(Integer mealId, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findMealByDate
	 *
	 */
	public Set<Meal> findMealByDate(java.util.Calendar date) throws DataAccessException;

	/**
	 * JPQL Query - findMealByDate
	 *
	 */
	public Set<Meal> findMealByDate(Calendar date, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findAllMeals
	 *
	 */
	public Set<Meal> findAllMeals() throws DataAccessException;

	/**
	 * JPQL Query - findAllMeals
	 *
	 */
	public Set<Meal> findAllMeals(int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findMealByPrimaryKey
	 *
	 */
	public Meal findMealByPrimaryKey(Integer mealId_1) throws DataAccessException;

	/**
	 * JPQL Query - findMealByPrimaryKey
	 *
	 */
	public Meal findMealByPrimaryKey(Integer mealId_1, int startResult, int maxRows) throws DataAccessException;

}