
package ehealth.dao;

import ehealth.domain.UserRole;

import java.util.Set;

import org.skyway.spring.util.dao.JpaDao;

import org.springframework.dao.DataAccessException;

/**
 * DAO to manage UserRole entities.
 * 
 */
public interface UserRoleDAO extends JpaDao<UserRole> {

	/**
	 * JPQL Query - findUserRoleByRoleTitle
	 *
	 */
	public Set<UserRole> findUserRoleByRoleTitle(String roleTitle) throws DataAccessException;

	/**
	 * JPQL Query - findUserRoleByRoleTitle
	 *
	 */
	public Set<UserRole> findUserRoleByRoleTitle(String roleTitle, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findUserRoleByPrimaryKey
	 *
	 */
	public UserRole findUserRoleByPrimaryKey(Integer userRoleId) throws DataAccessException;

	/**
	 * JPQL Query - findUserRoleByPrimaryKey
	 *
	 */
	public UserRole findUserRoleByPrimaryKey(Integer userRoleId, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findUserRoleByRoleTitleContaining
	 *
	 */
	public Set<UserRole> findUserRoleByRoleTitleContaining(String roleTitle_1) throws DataAccessException;

	/**
	 * JPQL Query - findUserRoleByRoleTitleContaining
	 *
	 */
	public Set<UserRole> findUserRoleByRoleTitleContaining(String roleTitle_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findAllUserRoles
	 *
	 */
	public Set<UserRole> findAllUserRoles() throws DataAccessException;

	/**
	 * JPQL Query - findAllUserRoles
	 *
	 */
	public Set<UserRole> findAllUserRoles(int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findUserRoleByUserRoleId
	 *
	 */
	public UserRole findUserRoleByUserRoleId(Integer userRoleId_1) throws DataAccessException;

	/**
	 * JPQL Query - findUserRoleByUserRoleId
	 *
	 */
	public UserRole findUserRoleByUserRoleId(Integer userRoleId_1, int startResult, int maxRows) throws DataAccessException;

}