
package ehealth.dao;

import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.skyway.spring.util.dao.AbstractJpaDao;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import ehealth.domain.Kid;

/**
 * DAO to manage Kid entities.
 * 
 */
@Repository("KidDAO")

@Transactional
public class KidDAOImpl extends AbstractJpaDao<Kid> implements KidDAO {

	/**
	 * Set of entity classes managed by this DAO.  Typically a DAO manages a single entity.
	 *
	 */
	private final static Set<Class<?>> dataTypes = new HashSet<Class<?>>(Arrays.asList(new Class<?>[] { Kid.class }));

	/**
	 * EntityManager injected by Spring for persistence unit dbdriver
	 *
	 */
	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Instantiates a new KidDAOImpl
	 *
	 */
	public KidDAOImpl() {
		super();
	}

	/**
	 * Get the entity manager that manages persistence unit 
	 *
	 */
	public EntityManager getEntityManager() {
		return entityManager;
	}

	/**
	 * Returns the set of entity classes managed by this DAO.
	 *
	 */
	public Set<Class<?>> getTypes() {
		return dataTypes;
	}

	/**
	 * JPQL Query - findKidByAddressContaining
	 *
	 */
	@Transactional
	public Set<Kid> findKidByAddressContaining(String address) throws DataAccessException {

		return findKidByAddressContaining(address, -1, -1);
	}

	/**
	 * JPQL Query - findKidByAddressContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<Kid> findKidByAddressContaining(String address, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findKidByAddressContaining", startResult, maxRows, address);
		return new LinkedHashSet<Kid>(query.getResultList());
	}

	/**
	 * JPQL Query - findAllKids
	 *
	 */
	@Transactional
	public Set<Kid> findAllKids() throws DataAccessException {

		return findAllKids(-1, -1);
	}

	/**
	 * JPQL Query - findAllKids
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<Kid> findAllKids(int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findAllKids", startResult, maxRows);
		System.out.println(query.getResultList());
		return new LinkedHashSet<Kid>(query.getResultList());
	}

	/**
	 * JPQL Query - findKidByKidId
	 *
	 */
	@Transactional
	public Kid findKidByKidId(Integer kidId) throws DataAccessException {

		return findKidByKidId(kidId, -1, -1);
	}

	/**
	 * JPQL Query - findKidByKidId
	 *
	 */

	@Transactional
	public Kid findKidByKidId(Integer kidId, int startResult, int maxRows) throws DataAccessException {
		try {
			Query query = createNamedQuery("findKidByKidId", startResult, maxRows, kidId);
			return (ehealth.domain.Kid) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * JPQL Query - findKidByCodeKeyContaining
	 *
	 */
	@Transactional
	public Set<Kid> findKidByCodeKeyContaining(String codeKey) throws DataAccessException {

		return findKidByCodeKeyContaining(codeKey, -1, -1);
	}

	/**
	 * JPQL Query - findKidByCodeKeyContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<Kid> findKidByCodeKeyContaining(String codeKey, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findKidByCodeKeyContaining", startResult, maxRows, codeKey);
		return new LinkedHashSet<Kid>(query.getResultList());
	}

	/**
	 * JPQL Query - findKidByAddress
	 *
	 */
	@Transactional
	public Set<Kid> findKidByAddress(String address) throws DataAccessException {

		return findKidByAddress(address, -1, -1);
	}

	/**
	 * JPQL Query - findKidByAddress
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<Kid> findKidByAddress(String address, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findKidByAddress", startResult, maxRows, address);
		return new LinkedHashSet<Kid>(query.getResultList());
	}

	/**
	 * JPQL Query - findKidByCodeKey
	 *
	 */
	@Transactional
	public Set<Kid> findKidByCodeKey(String codeKey) throws DataAccessException {

		return findKidByCodeKey(codeKey, -1, -1);
	}

	/**
	 * JPQL Query - findKidByCodeKey
	 *
	 */
	@SuppressWarnings("unchecked")
	@Transactional
	public Set<Kid> findKidByCodeKey(String codeKey, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findKidByCodeKey", startResult, maxRows, codeKey);
		return  new LinkedHashSet<Kid>(query.getResultList());
	}

	/**
	 * JPQL Query - findKidByHigh
	 *
	 */
	@Transactional
	public Set<Kid> findKidByHigh(java.math.BigDecimal high) throws DataAccessException {

		return findKidByHigh(high, -1, -1);
	}

	/**
	 * JPQL Query - findKidByHigh
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<Kid> findKidByHigh(java.math.BigDecimal high, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findKidByHigh", startResult, maxRows, high);
		return new LinkedHashSet<Kid>(query.getResultList());
	}

	/**
	 * JPQL Query - findKidByWeight
	 *
	 */
	@Transactional
	public Set<Kid> findKidByWeight(java.math.BigDecimal weight) throws DataAccessException {

		return findKidByWeight(weight, -1, -1);
	}

	/**
	 * JPQL Query - findKidByWeight
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<Kid> findKidByWeight(java.math.BigDecimal weight, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findKidByWeight", startResult, maxRows, weight);
		return new LinkedHashSet<Kid>(query.getResultList());
	}

	/**
	 * JPQL Query - findKidByPrimaryKey
	 *
	 */
	@Transactional
	public Kid findKidByPrimaryKey(Integer kidId) throws DataAccessException {

		return findKidByPrimaryKey(kidId, -1, -1);
	}

	/**
	 * JPQL Query - findKidByPrimaryKey
	 *
	 */

	@Transactional
	public Kid findKidByPrimaryKey(Integer kidId, int startResult, int maxRows) throws DataAccessException {
		try {
			Query query = createNamedQuery("findKidByPrimaryKey", startResult, maxRows, kidId);
			return (ehealth.domain.Kid) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}
	

	/**
	 * Used to determine whether or not to merge the entity or persist the entity when calling Store
	 * @see store
	 * 
	 *
	 */
	public boolean canBeMerged(Kid entity) {
		return true;
	}
}
