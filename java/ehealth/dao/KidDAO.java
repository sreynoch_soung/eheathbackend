
package ehealth.dao;

import ehealth.domain.Kid;

import java.math.BigDecimal;

import java.util.Set;

import org.skyway.spring.util.dao.JpaDao;

import org.springframework.dao.DataAccessException;

/**
 * DAO to manage Kid entities.
 * 
 */
public interface KidDAO extends JpaDao<Kid> {

	/**
	 * JPQL Query - findKidByAddressContaining
	 *
	 */
	public Set<Kid> findKidByAddressContaining(String address) throws DataAccessException;

	/**
	 * JPQL Query - findKidByAddressContaining
	 *
	 */
	public Set<Kid> findKidByAddressContaining(String address, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findAllKids
	 *
	 */
	public Set<Kid> findAllKids() throws DataAccessException;

	/**
	 * JPQL Query - findAllKids
	 *
	 */
	public Set<Kid> findAllKids(int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findKidByKidId
	 *
	 */
	public Kid findKidByKidId(Integer kidId) throws DataAccessException;

	/**
	 * JPQL Query - findKidByKidId
	 *
	 */
	public Kid findKidByKidId(Integer kidId, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findKidByCodeKeyContaining
	 *
	 */
	public Set<Kid> findKidByCodeKeyContaining(String codeKey) throws DataAccessException;

	/**
	 * JPQL Query - findKidByCodeKeyContaining
	 *
	 */
	public Set<Kid> findKidByCodeKeyContaining(String codeKey, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findKidByAddress
	 *
	 */
	public Set<Kid> findKidByAddress(String address_1) throws DataAccessException;

	/**
	 * JPQL Query - findKidByAddress
	 *
	 */
	public Set<Kid> findKidByAddress(String address_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findKidByCodeKey
	 *
	 */
	public Set<Kid> findKidByCodeKey(String codeKey_1) throws DataAccessException;

	/**
	 * JPQL Query - findKidByCodeKey
	 *
	 */
	public Set<Kid> findKidByCodeKey(String codeKey_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findKidByHigh
	 *
	 */
	public Set<Kid> findKidByHigh(java.math.BigDecimal high) throws DataAccessException;

	/**
	 * JPQL Query - findKidByHigh
	 *
	 */
	public Set<Kid> findKidByHigh(BigDecimal high, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findKidByWeight
	 *
	 */
	public Set<Kid> findKidByWeight(java.math.BigDecimal weight) throws DataAccessException;

	/**
	 * JPQL Query - findKidByWeight
	 *
	 */
	public Set<Kid> findKidByWeight(BigDecimal weight, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findKidByPrimaryKey
	 *
	 */
	public Kid findKidByPrimaryKey(Integer kidId_1) throws DataAccessException;

	/**
	 * JPQL Query - findKidByPrimaryKey
	 *
	 */
	public Kid findKidByPrimaryKey(Integer kidId_1, int startResult, int maxRows) throws DataAccessException;

}