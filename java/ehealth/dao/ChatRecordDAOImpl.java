
package ehealth.dao;

import ehealth.domain.ChatRecord;

import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.skyway.spring.util.dao.AbstractJpaDao;

import org.springframework.dao.DataAccessException;

import org.springframework.stereotype.Repository;

import org.springframework.transaction.annotation.Transactional;

/**
 * DAO to manage ChatRecord entities.
 * 
 */
@Repository("ChatRecordDAO")

@Transactional
public class ChatRecordDAOImpl extends AbstractJpaDao<ChatRecord> implements ChatRecordDAO {

	/**
	 * Set of entity classes managed by this DAO.  Typically a DAO manages a single entity.
	 *
	 */
	private final static Set<Class<?>> dataTypes = new HashSet<Class<?>>(Arrays.asList(new Class<?>[] {
			ChatRecord.class }));

	/**
	 * EntityManager injected by Spring for persistence unit dbdriver
	 *
	 */
	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Instantiates a new ChatRecordDAOImpl
	 *
	 */
	public ChatRecordDAOImpl() {
		super();
	}

	/**
	 * Get the entity manager that manages persistence unit 
	 *
	 */
	public EntityManager getEntityManager() {
		return entityManager;
	}

	/**
	 * Returns the set of entity classes managed by this DAO.
	 *
	 */
	public Set<Class<?>> getTypes() {
		return dataTypes;
	}

	/**
	 * JPQL Query - findChatRecordByDate
	 *
	 */
	@Transactional
	public Set<ChatRecord> findChatRecordByDate(java.util.Calendar date) throws DataAccessException {

		return findChatRecordByDate(date, -1, -1);
	}

	/**
	 * JPQL Query - findChatRecordByDate
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ChatRecord> findChatRecordByDate(java.util.Calendar date, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findChatRecordByDate", startResult, maxRows, date);
		return new LinkedHashSet<ChatRecord>(query.getResultList());
	}

	/**
	 * JPQL Query - findChatRecordByMessageContaining
	 *
	 */
	@Transactional
	public Set<ChatRecord> findChatRecordByMessageContaining(String message) throws DataAccessException {

		return findChatRecordByMessageContaining(message, -1, -1);
	}

	/**
	 * JPQL Query - findChatRecordByMessageContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ChatRecord> findChatRecordByMessageContaining(String message, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findChatRecordByMessageContaining", startResult, maxRows, message);
		return new LinkedHashSet<ChatRecord>(query.getResultList());
	}

	/**
	 * JPQL Query - findChatRecordByPrimaryKey
	 *
	 */
	@Transactional
	public ChatRecord findChatRecordByPrimaryKey(Integer chatRecordId) throws DataAccessException {

		return findChatRecordByPrimaryKey(chatRecordId, -1, -1);
	}

	/**
	 * JPQL Query - findChatRecordByPrimaryKey
	 *
	 */

	@Transactional
	public ChatRecord findChatRecordByPrimaryKey(Integer chatRecordId, int startResult, int maxRows) throws DataAccessException {
		try {
			Query query = createNamedQuery("findChatRecordByPrimaryKey", startResult, maxRows, chatRecordId);
			return (ehealth.domain.ChatRecord) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * JPQL Query - findAllChatRecords
	 *
	 */
	@Transactional
	public Set<ChatRecord> findAllChatRecords() throws DataAccessException {

		return findAllChatRecords(-1, -1);
	}

	/**
	 * JPQL Query - findAllChatRecords
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ChatRecord> findAllChatRecords(int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findAllChatRecords", startResult, maxRows);
		return new LinkedHashSet<ChatRecord>(query.getResultList());
	}

	/**
	 * JPQL Query - findChatRecordByChatRecordId
	 *
	 */
	@Transactional
	public ChatRecord findChatRecordByChatRecordId(Integer chatRecordId) throws DataAccessException {

		return findChatRecordByChatRecordId(chatRecordId, -1, -1);
	}

	/**
	 * JPQL Query - findChatRecordByChatRecordId
	 *
	 */

	@Transactional
	public ChatRecord findChatRecordByChatRecordId(Integer chatRecordId, int startResult, int maxRows) throws DataAccessException {
		try {
			Query query = createNamedQuery("findChatRecordByChatRecordId", startResult, maxRows, chatRecordId);
			return (ehealth.domain.ChatRecord) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * JPQL Query - findChatRecordByMessage
	 *
	 */
	@Transactional
	public Set<ChatRecord> findChatRecordByMessage(String message) throws DataAccessException {

		return findChatRecordByMessage(message, -1, -1);
	}

	/**
	 * JPQL Query - findChatRecordByMessage
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ChatRecord> findChatRecordByMessage(String message, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findChatRecordByMessage", startResult, maxRows, message);
		return new LinkedHashSet<ChatRecord>(query.getResultList());
	}

	/**
	 * Used to determine whether or not to merge the entity or persist the entity when calling Store
	 * @see store
	 * 
	 *
	 */
	public boolean canBeMerged(ChatRecord entity) {
		return true;
	}
}
