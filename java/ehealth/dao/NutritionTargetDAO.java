
package ehealth.dao;

import ehealth.domain.NutritionTarget;

import java.util.Calendar;
import java.util.Set;

import org.skyway.spring.util.dao.JpaDao;

import org.springframework.dao.DataAccessException;

/**
 * DAO to manage NutritionTarget entities.
 * 
 */
public interface NutritionTargetDAO extends JpaDao<NutritionTarget> {

	/**
	 * JPQL Query - findNutritionTargetByPrimaryKey
	 *
	 */
	public NutritionTarget findNutritionTargetByPrimaryKey(Integer nutritionTargetId) throws DataAccessException;

	/**
	 * JPQL Query - findNutritionTargetByPrimaryKey
	 *
	 */
	public NutritionTarget findNutritionTargetByPrimaryKey(Integer nutritionTargetId, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findNutritionTargetByNutritionTargetId
	 *
	 */
	public NutritionTarget findNutritionTargetByNutritionTargetId(Integer nutritionTargetId_1) throws DataAccessException;

	/**
	 * JPQL Query - findNutritionTargetByNutritionTargetId
	 *
	 */
	public NutritionTarget findNutritionTargetByNutritionTargetId(Integer nutritionTargetId_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findNutritionTargetByDate
	 *
	 */
	public Set<NutritionTarget> findNutritionTargetByDate(java.util.Calendar date) throws DataAccessException;

	/**
	 * JPQL Query - findNutritionTargetByDate
	 *
	 */
	public Set<NutritionTarget> findNutritionTargetByDate(Calendar date, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findAllNutritionTargets
	 *
	 */
	public Set<NutritionTarget> findAllNutritionTargets() throws DataAccessException;

	/**
	 * JPQL Query - findAllNutritionTargets
	 *
	 */
	public Set<NutritionTarget> findAllNutritionTargets(int startResult, int maxRows) throws DataAccessException;

}