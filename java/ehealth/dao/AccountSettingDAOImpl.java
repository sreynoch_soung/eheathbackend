
package ehealth.dao;

import ehealth.domain.AccountSetting;

import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.skyway.spring.util.dao.AbstractJpaDao;

import org.springframework.dao.DataAccessException;

import org.springframework.stereotype.Repository;

import org.springframework.transaction.annotation.Transactional;

/**
 * DAO to manage AccountSetting entities.
 * 
 */
@Repository("AccountSettingDAO")

@Transactional
public class AccountSettingDAOImpl extends AbstractJpaDao<AccountSetting> implements AccountSettingDAO {

	/**
	 * Set of entity classes managed by this DAO.  Typically a DAO manages a single entity.
	 *
	 */
	private final static Set<Class<?>> dataTypes = new HashSet<Class<?>>(Arrays.asList(new Class<?>[] {
			AccountSetting.class }));

	/**
	 * EntityManager injected by Spring for persistence unit dbdriver
	 *
	 */
	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Instantiates a new AccountSettingDAOImpl
	 *
	 */
	public AccountSettingDAOImpl() {
		super();
	}

	/**
	 * Get the entity manager that manages persistence unit 
	 *
	 */
	public EntityManager getEntityManager() {
		return entityManager;
	}

	/**
	 * Returns the set of entity classes managed by this DAO.
	 *
	 */
	public Set<Class<?>> getTypes() {
		return dataTypes;
	}

	/**
	 * JPQL Query - findAccountSettingByAccountSettingId
	 *
	 */
	@Transactional
	public AccountSetting findAccountSettingByAccountSettingId(Integer accountSettingId) throws DataAccessException {

		return findAccountSettingByAccountSettingId(accountSettingId, -1, -1);
	}

	/**
	 * JPQL Query - findAccountSettingByAccountSettingId
	 *
	 */

	@Transactional
	public AccountSetting findAccountSettingByAccountSettingId(Integer accountSettingId, int startResult, int maxRows) throws DataAccessException {
		try {
			Query query = createNamedQuery("findAccountSettingByAccountSettingId", startResult, maxRows, accountSettingId);
			return (ehealth.domain.AccountSetting) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * JPQL Query - findAccountSettingByNumPhotoValidateGroup
	 *
	 */
	@Transactional
	public Set<AccountSetting> findAccountSettingByNumPhotoValidateGroup(Integer numPhotoValidateGroup) throws DataAccessException {

		return findAccountSettingByNumPhotoValidateGroup(numPhotoValidateGroup, -1, -1);
	}

	/**
	 * JPQL Query - findAccountSettingByNumPhotoValidateGroup
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<AccountSetting> findAccountSettingByNumPhotoValidateGroup(Integer numPhotoValidateGroup, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findAccountSettingByNumPhotoValidateGroup", startResult, maxRows, numPhotoValidateGroup);
		return new LinkedHashSet<AccountSetting>(query.getResultList());
	}

	/**
	 * JPQL Query - findAllAccountSettings
	 *
	 */
	@Transactional
	public Set<AccountSetting> findAllAccountSettings() throws DataAccessException {

		return findAllAccountSettings(-1, -1);
	}

	/**
	 * JPQL Query - findAllAccountSettings
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<AccountSetting> findAllAccountSettings(int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findAllAccountSettings", startResult, maxRows);
		return new LinkedHashSet<AccountSetting>(query.getResultList());
	}

	/**
	 * JPQL Query - findAccountSettingByPrimaryKey
	 *
	 */
	@Transactional
	public AccountSetting findAccountSettingByPrimaryKey(Integer accountSettingId) throws DataAccessException {

		return findAccountSettingByPrimaryKey(accountSettingId, -1, -1);
	}

	/**
	 * JPQL Query - findAccountSettingByPrimaryKey
	 *
	 */

	@Transactional
	public AccountSetting findAccountSettingByPrimaryKey(Integer accountSettingId, int startResult, int maxRows) throws DataAccessException {
		try {
			Query query = createNamedQuery("findAccountSettingByPrimaryKey", startResult, maxRows, accountSettingId);
			return (ehealth.domain.AccountSetting) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * Used to determine whether or not to merge the entity or persist the entity when calling Store
	 * @see store
	 * 
	 *
	 */
	public boolean canBeMerged(AccountSetting entity) {
		return true;
	}
}
