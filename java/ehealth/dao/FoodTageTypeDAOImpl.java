
package ehealth.dao;

import ehealth.domain.FoodTageType;

import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.skyway.spring.util.dao.AbstractJpaDao;

import org.springframework.dao.DataAccessException;

import org.springframework.stereotype.Repository;

import org.springframework.transaction.annotation.Transactional;

/**
 * DAO to manage FoodTageType entities.
 * 
 */
@Repository("FoodTageTypeDAO")

@Transactional
public class FoodTageTypeDAOImpl extends AbstractJpaDao<FoodTageType> implements FoodTageTypeDAO {

	/**
	 * Set of entity classes managed by this DAO.  Typically a DAO manages a single entity.
	 *
	 */
	private final static Set<Class<?>> dataTypes = new HashSet<Class<?>>(Arrays.asList(new Class<?>[] {
			FoodTageType.class }));

	/**
	 * EntityManager injected by Spring for persistence unit dbdriver
	 *
	 */
	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Instantiates a new FoodTageTypeDAOImpl
	 *
	 */
	public FoodTageTypeDAOImpl() {
		super();
	}

	/**
	 * Get the entity manager that manages persistence unit 
	 *
	 */
	public EntityManager getEntityManager() {
		return entityManager;
	}

	/**
	 * Returns the set of entity classes managed by this DAO.
	 *
	 */
	public Set<Class<?>> getTypes() {
		return dataTypes;
	}

	/**
	 * JPQL Query - findFoodTageTypeByFoodTageTypeId
	 *
	 */
	@Transactional
	public FoodTageType findFoodTageTypeByFoodTageTypeId(Integer foodTageTypeId) throws DataAccessException {

		return findFoodTageTypeByFoodTageTypeId(foodTageTypeId, -1, -1);
	}

	/**
	 * JPQL Query - findFoodTageTypeByFoodTageTypeId
	 *
	 */

	@Transactional
	public FoodTageType findFoodTageTypeByFoodTageTypeId(Integer foodTageTypeId, int startResult, int maxRows) throws DataAccessException {
		try {
			Query query = createNamedQuery("findFoodTageTypeByFoodTageTypeId", startResult, maxRows, foodTageTypeId);
			return (ehealth.domain.FoodTageType) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * JPQL Query - findFoodTageTypeByTypeNameContaining
	 *
	 */
	@Transactional
	public Set<FoodTageType> findFoodTageTypeByTypeNameContaining(String typeName) throws DataAccessException {

		return findFoodTageTypeByTypeNameContaining(typeName, -1, -1);
	}

	/**
	 * JPQL Query - findFoodTageTypeByTypeNameContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<FoodTageType> findFoodTageTypeByTypeNameContaining(String typeName, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findFoodTageTypeByTypeNameContaining", startResult, maxRows, typeName);
		return new LinkedHashSet<FoodTageType>(query.getResultList());
	}

	/**
	 * JPQL Query - findFoodTageTypeByPrimaryKey
	 *
	 */
	@Transactional
	public FoodTageType findFoodTageTypeByPrimaryKey(Integer foodTageTypeId) throws DataAccessException {

		return findFoodTageTypeByPrimaryKey(foodTageTypeId, -1, -1);
	}

	/**
	 * JPQL Query - findFoodTageTypeByPrimaryKey
	 *
	 */

	@Transactional
	public FoodTageType findFoodTageTypeByPrimaryKey(Integer foodTageTypeId, int startResult, int maxRows) throws DataAccessException {
		try {
			Query query = createNamedQuery("findFoodTageTypeByPrimaryKey", startResult, maxRows, foodTageTypeId);
			return (ehealth.domain.FoodTageType) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * JPQL Query - findFoodTageTypeByTypeName
	 *
	 */
	@Transactional
	public Set<FoodTageType> findFoodTageTypeByTypeName(String typeName) throws DataAccessException {

		return findFoodTageTypeByTypeName(typeName, -1, -1);
	}

	/**
	 * JPQL Query - findFoodTageTypeByTypeName
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<FoodTageType> findFoodTageTypeByTypeName(String typeName, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findFoodTageTypeByTypeName", startResult, maxRows, typeName);
		return new LinkedHashSet<FoodTageType>(query.getResultList());
	}

	/**
	 * JPQL Query - findAllFoodTageTypes
	 *
	 */
	@Transactional
	public Set<FoodTageType> findAllFoodTageTypes() throws DataAccessException {

		return findAllFoodTageTypes(-1, -1);
	}

	/**
	 * JPQL Query - findAllFoodTageTypes
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<FoodTageType> findAllFoodTageTypes(int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findAllFoodTageTypes", startResult, maxRows);
		return new LinkedHashSet<FoodTageType>(query.getResultList());
	}

	/**
	 * Used to determine whether or not to merge the entity or persist the entity when calling Store
	 * @see store
	 * 
	 *
	 */
	public boolean canBeMerged(FoodTageType entity) {
		return true;
	}
}
