
package ehealth.dao;

import ehealth.domain.Doctor;

import java.util.Set;

import org.skyway.spring.util.dao.JpaDao;

import org.springframework.dao.DataAccessException;

/**
 * DAO to manage Doctor entities.
 * 
 */
public interface DoctorDAO extends JpaDao<Doctor> {

	/**
	 * JPQL Query - findDoctorByCodeKey
	 *
	 */
	public Set<Doctor> findDoctorByCodeKey(String codeKey) throws DataAccessException;

	/**
	 * JPQL Query - findDoctorByCodeKey
	 *
	 */
	public Set<Doctor> findDoctorByCodeKey(String codeKey, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findDoctorByHospitalContaining
	 *
	 */
	public Set<Doctor> findDoctorByHospitalContaining(String hospital) throws DataAccessException;

	/**
	 * JPQL Query - findDoctorByHospitalContaining
	 *
	 */
	public Set<Doctor> findDoctorByHospitalContaining(String hospital, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findDoctorByCodeKeyContaining
	 *
	 */
	public Set<Doctor> findDoctorByCodeKeyContaining(String codeKey_1) throws DataAccessException;

	/**
	 * JPQL Query - findDoctorByCodeKeyContaining
	 *
	 */
	public Set<Doctor> findDoctorByCodeKeyContaining(String codeKey_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findDoctorByPrimaryKey
	 *
	 */
	public Doctor findDoctorByPrimaryKey(Integer doctorId) throws DataAccessException;

	/**
	 * JPQL Query - findDoctorByPrimaryKey
	 *
	 */
	public Doctor findDoctorByPrimaryKey(Integer doctorId, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findDoctorByDoctorId
	 *
	 */
	public Doctor findDoctorByDoctorId(Integer doctorId_1) throws DataAccessException;

	/**
	 * JPQL Query - findDoctorByDoctorId
	 *
	 */
	public Doctor findDoctorByDoctorId(Integer doctorId_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findDoctorByHospital
	 *
	 */
	public Set<Doctor> findDoctorByHospital(String hospital_1) throws DataAccessException;

	/**
	 * JPQL Query - findDoctorByHospital
	 *
	 */
	public Set<Doctor> findDoctorByHospital(String hospital_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findAllDoctors
	 *
	 */
	public Set<Doctor> findAllDoctors() throws DataAccessException;

	/**
	 * JPQL Query - findAllDoctors
	 *
	 */
	public Set<Doctor> findAllDoctors(int startResult, int maxRows) throws DataAccessException;

}