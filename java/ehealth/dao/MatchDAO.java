
package ehealth.dao;

import ehealth.domain.Match;

import java.util.Calendar;
import java.util.Set;

import org.skyway.spring.util.dao.JpaDao;

import org.springframework.dao.DataAccessException;

/**
 * DAO to manage Match entities.
 * 
 */
public interface MatchDAO extends JpaDao<Match> {

	/**
	 * JPQL Query - findMatchByWinScore
	 *
	 */
	public Set<Match> findMatchByWinScore(String winScore) throws DataAccessException;

	/**
	 * JPQL Query - findMatchByWinScore
	 *
	 */
	public Set<Match> findMatchByWinScore(String winScore, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findMatchByMatchId
	 *
	 */
	public Match findMatchByMatchId(Integer matchId) throws DataAccessException;

	/**
	 * JPQL Query - findMatchByMatchId
	 *
	 */
	public Match findMatchByMatchId(Integer matchId, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findMatchByWinScoreContaining
	 *
	 */
	public Set<Match> findMatchByWinScoreContaining(String winScore_1) throws DataAccessException;

	/**
	 * JPQL Query - findMatchByWinScoreContaining
	 *
	 */
	public Set<Match> findMatchByWinScoreContaining(String winScore_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findAllMatchs
	 *
	 */
	public Set<Match> findAllMatchs() throws DataAccessException;

	/**
	 * JPQL Query - findAllMatchs
	 *
	 */
	public Set<Match> findAllMatchs(int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findMatchByDate
	 *
	 */
	public Set<Match> findMatchByDate(java.util.Calendar date) throws DataAccessException;

	/**
	 * JPQL Query - findMatchByDate
	 *
	 */
	public Set<Match> findMatchByDate(Calendar date, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findMatchByPrimaryKey
	 *
	 */
	public Match findMatchByPrimaryKey(Integer matchId_1) throws DataAccessException;

	/**
	 * JPQL Query - findMatchByPrimaryKey
	 *
	 */
	public Match findMatchByPrimaryKey(Integer matchId_1, int startResult, int maxRows) throws DataAccessException;

}