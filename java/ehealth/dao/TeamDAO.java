
package ehealth.dao;

import ehealth.domain.Team;

import java.util.Set;

import org.skyway.spring.util.dao.JpaDao;

import org.springframework.dao.DataAccessException;

/**
 * DAO to manage Team entities.
 * 
 */
public interface TeamDAO extends JpaDao<Team> {

	/**
	 * JPQL Query - findTeamByPrimaryKey
	 *
	 */
	public Team findTeamByPrimaryKey(Integer teamId) throws DataAccessException;

	/**
	 * JPQL Query - findTeamByPrimaryKey
	 *
	 */
	public Team findTeamByPrimaryKey(Integer teamId, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findTeamByTeamId
	 *
	 */
	public Team findTeamByTeamId(Integer teamId_1) throws DataAccessException;

	/**
	 * JPQL Query - findTeamByTeamId
	 *
	 */
	public Team findTeamByTeamId(Integer teamId_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findTeamByNumMember
	 *
	 */
	public Set<Team> findTeamByNumMember(Integer numMember) throws DataAccessException;

	/**
	 * JPQL Query - findTeamByNumMember
	 *
	 */
	public Set<Team> findTeamByNumMember(Integer numMember, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findTeamByNameContaining
	 *
	 */
	public Set<Team> findTeamByNameContaining(String name) throws DataAccessException;

	/**
	 * JPQL Query - findTeamByNameContaining
	 *
	 */
	public Set<Team> findTeamByNameContaining(String name, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findTeamByName
	 *
	 */
	public Set<Team> findTeamByName(String name_1) throws DataAccessException;

	/**
	 * JPQL Query - findTeamByName
	 *
	 */
	public Set<Team> findTeamByName(String name_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findAllTeams
	 *
	 */
	public Set<Team> findAllTeams() throws DataAccessException;

	/**
	 * JPQL Query - findAllTeams
	 *
	 */
	public Set<Team> findAllTeams(int startResult, int maxRows) throws DataAccessException;

}