
package ehealth.dao;

import ehealth.domain.Bonus;

import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.skyway.spring.util.dao.AbstractJpaDao;

import org.springframework.dao.DataAccessException;

import org.springframework.stereotype.Repository;

import org.springframework.transaction.annotation.Transactional;

/**
 * DAO to manage Bonus entities.
 * 
 */
@Repository("BonusDAO")

@Transactional
public class BonusDAOImpl extends AbstractJpaDao<Bonus> implements BonusDAO {

	/**
	 * Set of entity classes managed by this DAO.  Typically a DAO manages a single entity.
	 *
	 */
	private final static Set<Class<?>> dataTypes = new HashSet<Class<?>>(Arrays.asList(new Class<?>[] { Bonus.class }));

	/**
	 * EntityManager injected by Spring for persistence unit dbdriver
	 *
	 */
	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Instantiates a new BonusDAOImpl
	 *
	 */
	public BonusDAOImpl() {
		super();
	}

	/**
	 * Get the entity manager that manages persistence unit 
	 *
	 */
	public EntityManager getEntityManager() {
		return entityManager;
	}

	/**
	 * Returns the set of entity classes managed by this DAO.
	 *
	 */
	public Set<Class<?>> getTypes() {
		return dataTypes;
	}

	/**
	 * JPQL Query - findBonusByActivityTypeContaining
	 *
	 */
	@Transactional
	public Set<Bonus> findBonusByActivityTypeContaining(String activityType) throws DataAccessException {

		return findBonusByActivityTypeContaining(activityType, -1, -1);
	}

	/**
	 * JPQL Query - findBonusByActivityTypeContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<Bonus> findBonusByActivityTypeContaining(String activityType, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findBonusByActivityTypeContaining", startResult, maxRows, activityType);
		return new LinkedHashSet<Bonus>(query.getResultList());
	}

	/**
	 * JPQL Query - findAllBonuss
	 *
	 */
	@Transactional
	public Set<Bonus> findAllBonuss() throws DataAccessException {

		return findAllBonuss(-1, -1);
	}

	/**
	 * JPQL Query - findAllBonuss
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<Bonus> findAllBonuss(int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findAllBonuss", startResult, maxRows);
		return new LinkedHashSet<Bonus>(query.getResultList());
	}

	/**
	 * JPQL Query - findBonusByActivityType
	 *
	 */
	@Transactional
	public Set<Bonus> findBonusByActivityType(String activityType) throws DataAccessException {

		return findBonusByActivityType(activityType, -1, -1);
	}

	/**
	 * JPQL Query - findBonusByActivityType
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<Bonus> findBonusByActivityType(String activityType, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findBonusByActivityType", startResult, maxRows, activityType);
		return new LinkedHashSet<Bonus>(query.getResultList());
	}

	/**
	 * JPQL Query - findBonusByPrimaryKey
	 *
	 */
	@Transactional
	public Bonus findBonusByPrimaryKey(Integer bonusId) throws DataAccessException {

		return findBonusByPrimaryKey(bonusId, -1, -1);
	}

	/**
	 * JPQL Query - findBonusByPrimaryKey
	 *
	 */

	@Transactional
	public Bonus findBonusByPrimaryKey(Integer bonusId, int startResult, int maxRows) throws DataAccessException {
		try {
			Query query = createNamedQuery("findBonusByPrimaryKey", startResult, maxRows, bonusId);
			return (ehealth.domain.Bonus) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * JPQL Query - findBonusByPoint
	 *
	 */
	@Transactional
	public Set<Bonus> findBonusByPoint(Integer point) throws DataAccessException {

		return findBonusByPoint(point, -1, -1);
	}

	/**
	 * JPQL Query - findBonusByPoint
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<Bonus> findBonusByPoint(Integer point, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findBonusByPoint", startResult, maxRows, point);
		return new LinkedHashSet<Bonus>(query.getResultList());
	}

	/**
	 * JPQL Query - findBonusByBonusId
	 *
	 */
	@Transactional
	public Bonus findBonusByBonusId(Integer bonusId) throws DataAccessException {

		return findBonusByBonusId(bonusId, -1, -1);
	}

	/**
	 * JPQL Query - findBonusByBonusId
	 *
	 */

	@Transactional
	public Bonus findBonusByBonusId(Integer bonusId, int startResult, int maxRows) throws DataAccessException {
		try {
			Query query = createNamedQuery("findBonusByBonusId", startResult, maxRows, bonusId);
			return (ehealth.domain.Bonus) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * Used to determine whether or not to merge the entity or persist the entity when calling Store
	 * @see store
	 * 
	 *
	 */
	public boolean canBeMerged(Bonus entity) {
		return true;
	}
}
