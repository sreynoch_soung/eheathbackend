
package ehealth.dao;

import ehealth.domain.KidMeasures;

import java.util.Set;

import org.skyway.spring.util.dao.JpaDao;

import org.springframework.dao.DataAccessException;

/**
 * DAO to manage KidMeasures entities.
 * 
 */
public interface KidMeasuresDAO extends JpaDao<KidMeasures> {

	/**
	 * JPQL Query - findAllKidMeasuress
	 *
	 */
	public Set<KidMeasures> findAllKidMeasuress() throws DataAccessException;

	/**
	 * JPQL Query - findAllKidMeasuress
	 *
	 */
	public Set<KidMeasures> findAllKidMeasuress(int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findKidMeasuresByMets
	 *
	 */
	public Set<KidMeasures> findKidMeasuresByMets(Integer mets) throws DataAccessException;

	/**
	 * JPQL Query - findKidMeasuresByMets
	 *
	 */
	public Set<KidMeasures> findKidMeasuresByMets(Integer mets, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findKidMeasuresByHabitContaining
	 *
	 */
	public Set<KidMeasures> findKidMeasuresByHabitContaining(String habit) throws DataAccessException;

	/**
	 * JPQL Query - findKidMeasuresByHabitContaining
	 *
	 */
	public Set<KidMeasures> findKidMeasuresByHabitContaining(String habit, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findKidMeasuresByBloodSugarLevel
	 *
	 */
	public Set<KidMeasures> findKidMeasuresByBloodSugarLevel(Integer bloodSugarLevel) throws DataAccessException;

	/**
	 * JPQL Query - findKidMeasuresByBloodSugarLevel
	 *
	 */
	public Set<KidMeasures> findKidMeasuresByBloodSugarLevel(Integer bloodSugarLevel, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findKidMeasuresByKidMeasuresId
	 *
	 */
	public KidMeasures findKidMeasuresByKidMeasuresId(Integer kidMeasuresId) throws DataAccessException;

	/**
	 * JPQL Query - findKidMeasuresByKidMeasuresId
	 *
	 */
	public KidMeasures findKidMeasuresByKidMeasuresId(Integer kidMeasuresId, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findKidMeasuresByHabit
	 *
	 */
	public Set<KidMeasures> findKidMeasuresByHabit(String habit_1) throws DataAccessException;

	/**
	 * JPQL Query - findKidMeasuresByHabit
	 *
	 */
	public Set<KidMeasures> findKidMeasuresByHabit(String habit_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findKidMeasuresByGlucoseLevel
	 *
	 */
	public Set<KidMeasures> findKidMeasuresByGlucoseLevel(Integer glucoseLevel) throws DataAccessException;

	/**
	 * JPQL Query - findKidMeasuresByGlucoseLevel
	 *
	 */
	public Set<KidMeasures> findKidMeasuresByGlucoseLevel(Integer glucoseLevel, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findKidMeasuresByPrimaryKey
	 *
	 */
	public KidMeasures findKidMeasuresByPrimaryKey(Integer kidMeasuresId_1) throws DataAccessException;

	/**
	 * JPQL Query - findKidMeasuresByPrimaryKey
	 *
	 */
	public KidMeasures findKidMeasuresByPrimaryKey(Integer kidMeasuresId_1, int startResult, int maxRows) throws DataAccessException;

}