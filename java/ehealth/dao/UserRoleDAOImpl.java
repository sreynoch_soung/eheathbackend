
package ehealth.dao;

import ehealth.domain.UserRole;

import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.skyway.spring.util.dao.AbstractJpaDao;

import org.springframework.dao.DataAccessException;

import org.springframework.stereotype.Repository;

import org.springframework.transaction.annotation.Transactional;

/**
 * DAO to manage UserRole entities.
 * 
 */
@Repository("UserRoleDAO")

@Transactional
public class UserRoleDAOImpl extends AbstractJpaDao<UserRole> implements UserRoleDAO {

	/**
	 * Set of entity classes managed by this DAO.  Typically a DAO manages a single entity.
	 *
	 */
	private final static Set<Class<?>> dataTypes = new HashSet<Class<?>>(Arrays.asList(new Class<?>[] {
			UserRole.class }));

	/**
	 * EntityManager injected by Spring for persistence unit dbdriver
	 *
	 */
	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Instantiates a new UserRoleDAOImpl
	 *
	 */
	public UserRoleDAOImpl() {
		super();
	}

	/**
	 * Get the entity manager that manages persistence unit 
	 *
	 */
	public EntityManager getEntityManager() {
		return entityManager;
	}

	/**
	 * Returns the set of entity classes managed by this DAO.
	 *
	 */
	public Set<Class<?>> getTypes() {
		return dataTypes;
	}

	/**
	 * JPQL Query - findUserRoleByRoleTitle
	 *
	 */
	@Transactional
	public Set<UserRole> findUserRoleByRoleTitle(String roleTitle) throws DataAccessException {

		return findUserRoleByRoleTitle(roleTitle, -1, -1);
	}

	/**
	 * JPQL Query - findUserRoleByRoleTitle
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<UserRole> findUserRoleByRoleTitle(String roleTitle, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findUserRoleByRoleTitle", startResult, maxRows, roleTitle);
		return new LinkedHashSet<UserRole>(query.getResultList());
	}

	/**
	 * JPQL Query - findUserRoleByPrimaryKey
	 *
	 */
	@Transactional
	public UserRole findUserRoleByPrimaryKey(Integer userRoleId) throws DataAccessException {

		return findUserRoleByPrimaryKey(userRoleId, -1, -1);
	}

	/**
	 * JPQL Query - findUserRoleByPrimaryKey
	 *
	 */

	@Transactional
	public UserRole findUserRoleByPrimaryKey(Integer userRoleId, int startResult, int maxRows) throws DataAccessException {
		try {
			Query query = createNamedQuery("findUserRoleByPrimaryKey", startResult, maxRows, userRoleId);
			return (ehealth.domain.UserRole) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * JPQL Query - findUserRoleByRoleTitleContaining
	 *
	 */
	@Transactional
	public Set<UserRole> findUserRoleByRoleTitleContaining(String roleTitle) throws DataAccessException {

		return findUserRoleByRoleTitleContaining(roleTitle, -1, -1);
	}

	/**
	 * JPQL Query - findUserRoleByRoleTitleContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<UserRole> findUserRoleByRoleTitleContaining(String roleTitle, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findUserRoleByRoleTitleContaining", startResult, maxRows, roleTitle);
		return new LinkedHashSet<UserRole>(query.getResultList());
	}

	/**
	 * JPQL Query - findAllUserRoles
	 *
	 */
	@Transactional
	public Set<UserRole> findAllUserRoles() throws DataAccessException {

		return findAllUserRoles(-1, -1);
	}

	/**
	 * JPQL Query - findAllUserRoles
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<UserRole> findAllUserRoles(int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findAllUserRoles", startResult, maxRows);
		return new LinkedHashSet<UserRole>(query.getResultList());
	}

	/**
	 * JPQL Query - findUserRoleByUserRoleId
	 *
	 */
	@Transactional
	public UserRole findUserRoleByUserRoleId(Integer userRoleId) throws DataAccessException {

		return findUserRoleByUserRoleId(userRoleId, -1, -1);
	}

	/**
	 * JPQL Query - findUserRoleByUserRoleId
	 *
	 */

	@Transactional
	public UserRole findUserRoleByUserRoleId(Integer userRoleId, int startResult, int maxRows) throws DataAccessException {
		try {
			Query query = createNamedQuery("findUserRoleByUserRoleId", startResult, maxRows, userRoleId);
			return (ehealth.domain.UserRole) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * Used to determine whether or not to merge the entity or persist the entity when calling Store
	 * @see store
	 * 
	 *
	 */
	public boolean canBeMerged(UserRole entity) {
		return true;
	}
}
