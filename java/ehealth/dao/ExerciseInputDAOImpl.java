
package ehealth.dao;

import ehealth.domain.ExerciseInput;

import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.skyway.spring.util.dao.AbstractJpaDao;

import org.springframework.dao.DataAccessException;

import org.springframework.stereotype.Repository;

import org.springframework.transaction.annotation.Transactional;

/**
 * DAO to manage ExerciseInput entities.
 * 
 */
@Repository("ExerciseInputDAO")

@Transactional
public class ExerciseInputDAOImpl extends AbstractJpaDao<ExerciseInput> implements ExerciseInputDAO {

	/**
	 * Set of entity classes managed by this DAO.  Typically a DAO manages a single entity.
	 *
	 */
	private final static Set<Class<?>> dataTypes = new HashSet<Class<?>>(Arrays.asList(new Class<?>[] {
			ExerciseInput.class }));

	/**
	 * EntityManager injected by Spring for persistence unit dbdriver
	 *
	 */
	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Instantiates a new ExerciseInputDAOImpl
	 *
	 */
	public ExerciseInputDAOImpl() {
		super();
	}

	/**
	 * Get the entity manager that manages persistence unit 
	 *
	 */
	public EntityManager getEntityManager() {
		return entityManager;
	}

	/**
	 * Returns the set of entity classes managed by this DAO.
	 *
	 */
	public Set<Class<?>> getTypes() {
		return dataTypes;
	}

	/**
	 * JPQL Query - findExerciseInputByFinalValidationValue
	 *
	 */
	@Transactional
	public Set<ExerciseInput> findExerciseInputByFinalValidationValue(java.math.BigDecimal finalValidationValue) throws DataAccessException {

		return findExerciseInputByFinalValidationValue(finalValidationValue, -1, -1);
	}

	/**
	 * JPQL Query - findExerciseInputByFinalValidationValue
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ExerciseInput> findExerciseInputByFinalValidationValue(java.math.BigDecimal finalValidationValue, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findExerciseInputByFinalValidationValue", startResult, maxRows, finalValidationValue);
		return new LinkedHashSet<ExerciseInput>(query.getResultList());
	}

	/**
	 * JPQL Query - findExerciseInputByExerciseInputId
	 *
	 */
	@Transactional
	public ExerciseInput findExerciseInputByExerciseInputId(Integer exerciseInputId) throws DataAccessException {

		return findExerciseInputByExerciseInputId(exerciseInputId, -1, -1);
	}

	/**
	 * JPQL Query - findExerciseInputByExerciseInputId
	 *
	 */

	@Transactional
	public ExerciseInput findExerciseInputByExerciseInputId(Integer exerciseInputId, int startResult, int maxRows) throws DataAccessException {
		try {
			Query query = createNamedQuery("findExerciseInputByExerciseInputId", startResult, maxRows, exerciseInputId);
			return (ehealth.domain.ExerciseInput) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * JPQL Query - findAllExerciseInputs
	 *
	 */
	@Transactional
	public Set<ExerciseInput> findAllExerciseInputs() throws DataAccessException {

		return findAllExerciseInputs(-1, -1);
	}

	/**
	 * JPQL Query - findAllExerciseInputs
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ExerciseInput> findAllExerciseInputs(int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findAllExerciseInputs", startResult, maxRows);
		return new LinkedHashSet<ExerciseInput>(query.getResultList());
	}

	/**
	 * JPQL Query - findExerciseInputByValue
	 *
	 */
	@Transactional
	public Set<ExerciseInput> findExerciseInputByValue(java.math.BigDecimal value) throws DataAccessException {

		return findExerciseInputByValue(value, -1, -1);
	}

	/**
	 * JPQL Query - findExerciseInputByValue
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ExerciseInput> findExerciseInputByValue(java.math.BigDecimal value, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findExerciseInputByValue", startResult, maxRows, value);
		return new LinkedHashSet<ExerciseInput>(query.getResultList());
	}

	/**
	 * JPQL Query - findExerciseInputByPrimaryKey
	 *
	 */
	@Transactional
	public ExerciseInput findExerciseInputByPrimaryKey(Integer exerciseInputId) throws DataAccessException {

		return findExerciseInputByPrimaryKey(exerciseInputId, -1, -1);
	}

	/**
	 * JPQL Query - findExerciseInputByPrimaryKey
	 *
	 */

	@Transactional
	public ExerciseInput findExerciseInputByPrimaryKey(Integer exerciseInputId, int startResult, int maxRows) throws DataAccessException {
		try {
			Query query = createNamedQuery("findExerciseInputByPrimaryKey", startResult, maxRows, exerciseInputId);
			return (ehealth.domain.ExerciseInput) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * JPQL Query - findExerciseInputByDate
	 *
	 */
	@Transactional
	public Set<ExerciseInput> findExerciseInputByDate(java.util.Calendar date) throws DataAccessException {

		return findExerciseInputByDate(date, -1, -1);
	}

	/**
	 * JPQL Query - findExerciseInputByDate
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<ExerciseInput> findExerciseInputByDate(java.util.Calendar date, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findExerciseInputByDate", startResult, maxRows, date);
		return new LinkedHashSet<ExerciseInput>(query.getResultList());
	}

	/**
	 * Used to determine whether or not to merge the entity or persist the entity when calling Store
	 * @see store
	 * 
	 *
	 */
	public boolean canBeMerged(ExerciseInput entity) {
		return true;
	}
}
