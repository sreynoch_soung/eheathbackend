
package ehealth.dao;

import ehealth.domain.Match;

import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.skyway.spring.util.dao.AbstractJpaDao;

import org.springframework.dao.DataAccessException;

import org.springframework.stereotype.Repository;

import org.springframework.transaction.annotation.Transactional;

/**
 * DAO to manage Match entities.
 * 
 */
@Repository("MatchDAO")

@Transactional
public class MatchDAOImpl extends AbstractJpaDao<Match> implements MatchDAO {

	/**
	 * Set of entity classes managed by this DAO.  Typically a DAO manages a single entity.
	 *
	 */
	private final static Set<Class<?>> dataTypes = new HashSet<Class<?>>(Arrays.asList(new Class<?>[] { Match.class }));

	/**
	 * EntityManager injected by Spring for persistence unit dbdriver
	 *
	 */
	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Instantiates a new MatchDAOImpl
	 *
	 */
	public MatchDAOImpl() {
		super();
	}

	/**
	 * Get the entity manager that manages persistence unit 
	 *
	 */
	public EntityManager getEntityManager() {
		return entityManager;
	}

	/**
	 * Returns the set of entity classes managed by this DAO.
	 *
	 */
	public Set<Class<?>> getTypes() {
		return dataTypes;
	}

	/**
	 * JPQL Query - findMatchByWinScore
	 *
	 */
	@Transactional
	public Set<Match> findMatchByWinScore(String winScore) throws DataAccessException {

		return findMatchByWinScore(winScore, -1, -1);
	}

	/**
	 * JPQL Query - findMatchByWinScore
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<Match> findMatchByWinScore(String winScore, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findMatchByWinScore", startResult, maxRows, winScore);
		return new LinkedHashSet<Match>(query.getResultList());
	}

	/**
	 * JPQL Query - findMatchByMatchId
	 *
	 */
	@Transactional
	public Match findMatchByMatchId(Integer matchId) throws DataAccessException {

		return findMatchByMatchId(matchId, -1, -1);
	}

	/**
	 * JPQL Query - findMatchByMatchId
	 *
	 */

	@Transactional
	public Match findMatchByMatchId(Integer matchId, int startResult, int maxRows) throws DataAccessException {
		try {
			Query query = createNamedQuery("findMatchByMatchId", startResult, maxRows, matchId);
			return (ehealth.domain.Match) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * JPQL Query - findMatchByWinScoreContaining
	 *
	 */
	@Transactional
	public Set<Match> findMatchByWinScoreContaining(String winScore) throws DataAccessException {

		return findMatchByWinScoreContaining(winScore, -1, -1);
	}

	/**
	 * JPQL Query - findMatchByWinScoreContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<Match> findMatchByWinScoreContaining(String winScore, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findMatchByWinScoreContaining", startResult, maxRows, winScore);
		return new LinkedHashSet<Match>(query.getResultList());
	}

	/**
	 * JPQL Query - findAllMatchs
	 *
	 */
	@Transactional
	public Set<Match> findAllMatchs() throws DataAccessException {

		return findAllMatchs(-1, -1);
	}

	/**
	 * JPQL Query - findAllMatchs
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<Match> findAllMatchs(int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findAllMatchs", startResult, maxRows);
		return new LinkedHashSet<Match>(query.getResultList());
	}

	/**
	 * JPQL Query - findMatchByDate
	 *
	 */
	@Transactional
	public Set<Match> findMatchByDate(java.util.Calendar date) throws DataAccessException {

		return findMatchByDate(date, -1, -1);
	}

	/**
	 * JPQL Query - findMatchByDate
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<Match> findMatchByDate(java.util.Calendar date, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findMatchByDate", startResult, maxRows, date);
		return new LinkedHashSet<Match>(query.getResultList());
	}

	/**
	 * JPQL Query - findMatchByPrimaryKey
	 *
	 */
	@Transactional
	public Match findMatchByPrimaryKey(Integer matchId) throws DataAccessException {

		return findMatchByPrimaryKey(matchId, -1, -1);
	}

	/**
	 * JPQL Query - findMatchByPrimaryKey
	 *
	 */

	@Transactional
	public Match findMatchByPrimaryKey(Integer matchId, int startResult, int maxRows) throws DataAccessException {
		try {
			Query query = createNamedQuery("findMatchByPrimaryKey", startResult, maxRows, matchId);
			return (ehealth.domain.Match) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * Used to determine whether or not to merge the entity or persist the entity when calling Store
	 * @see store
	 * 
	 *
	 */
	public boolean canBeMerged(Match entity) {
		return true;
	}
}
