
package ehealth.dao;

import ehealth.domain.ExerciseInput;

import java.math.BigDecimal;

import java.util.Calendar;
import java.util.Set;

import org.skyway.spring.util.dao.JpaDao;

import org.springframework.dao.DataAccessException;

/**
 * DAO to manage ExerciseInput entities.
 * 
 */
public interface ExerciseInputDAO extends JpaDao<ExerciseInput> {

	/**
	 * JPQL Query - findExerciseInputByFinalValidationValue
	 *
	 */
	public Set<ExerciseInput> findExerciseInputByFinalValidationValue(java.math.BigDecimal finalValidationValue) throws DataAccessException;

	/**
	 * JPQL Query - findExerciseInputByFinalValidationValue
	 *
	 */
	public Set<ExerciseInput> findExerciseInputByFinalValidationValue(BigDecimal finalValidationValue, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findExerciseInputByExerciseInputId
	 *
	 */
	public ExerciseInput findExerciseInputByExerciseInputId(Integer exerciseInputId) throws DataAccessException;

	/**
	 * JPQL Query - findExerciseInputByExerciseInputId
	 *
	 */
	public ExerciseInput findExerciseInputByExerciseInputId(Integer exerciseInputId, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findAllExerciseInputs
	 *
	 */
	public Set<ExerciseInput> findAllExerciseInputs() throws DataAccessException;

	/**
	 * JPQL Query - findAllExerciseInputs
	 *
	 */
	public Set<ExerciseInput> findAllExerciseInputs(int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findExerciseInputByValue
	 *
	 */
	public Set<ExerciseInput> findExerciseInputByValue(java.math.BigDecimal value) throws DataAccessException;

	/**
	 * JPQL Query - findExerciseInputByValue
	 *
	 */
	public Set<ExerciseInput> findExerciseInputByValue(BigDecimal value, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findExerciseInputByPrimaryKey
	 *
	 */
	public ExerciseInput findExerciseInputByPrimaryKey(Integer exerciseInputId_1) throws DataAccessException;

	/**
	 * JPQL Query - findExerciseInputByPrimaryKey
	 *
	 */
	public ExerciseInput findExerciseInputByPrimaryKey(Integer exerciseInputId_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findExerciseInputByDate
	 *
	 */
	public Set<ExerciseInput> findExerciseInputByDate(java.util.Calendar date) throws DataAccessException;

	/**
	 * JPQL Query - findExerciseInputByDate
	 *
	 */
	public Set<ExerciseInput> findExerciseInputByDate(Calendar date, int startResult, int maxRows) throws DataAccessException;

}