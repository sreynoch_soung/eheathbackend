
package ehealth.dao;

import ehealth.domain.ExerciseType;

import java.util.Set;

import org.skyway.spring.util.dao.JpaDao;

import org.springframework.dao.DataAccessException;

/**
 * DAO to manage ExerciseType entities.
 * 
 */
public interface ExerciseTypeDAO extends JpaDao<ExerciseType> {

	/**
	 * JPQL Query - findExerciseTypeByTypeName
	 *
	 */
	public Set<ExerciseType> findExerciseTypeByTypeName(String typeName) throws DataAccessException;

	/**
	 * JPQL Query - findExerciseTypeByTypeName
	 *
	 */
	public Set<ExerciseType> findExerciseTypeByTypeName(String typeName, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findAllExerciseTypes
	 *
	 */
	public Set<ExerciseType> findAllExerciseTypes() throws DataAccessException;

	/**
	 * JPQL Query - findAllExerciseTypes
	 *
	 */
	public Set<ExerciseType> findAllExerciseTypes(int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findExerciseTypeByPrimaryKey
	 *
	 */
	public ExerciseType findExerciseTypeByPrimaryKey(Integer exerciseTypeId) throws DataAccessException;

	/**
	 * JPQL Query - findExerciseTypeByPrimaryKey
	 *
	 */
	public ExerciseType findExerciseTypeByPrimaryKey(Integer exerciseTypeId, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findExerciseTypeByExerciseTypeId
	 *
	 */
	public ExerciseType findExerciseTypeByExerciseTypeId(Integer exerciseTypeId_1) throws DataAccessException;

	/**
	 * JPQL Query - findExerciseTypeByExerciseTypeId
	 *
	 */
	public ExerciseType findExerciseTypeByExerciseTypeId(Integer exerciseTypeId_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findExerciseTypeByTypeNameContaining
	 *
	 */
	public Set<ExerciseType> findExerciseTypeByTypeNameContaining(String typeName_1) throws DataAccessException;

	/**
	 * JPQL Query - findExerciseTypeByTypeNameContaining
	 *
	 */
	public Set<ExerciseType> findExerciseTypeByTypeNameContaining(String typeName_1, int startResult, int maxRows) throws DataAccessException;

}