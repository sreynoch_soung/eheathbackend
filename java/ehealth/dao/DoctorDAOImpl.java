
package ehealth.dao;

import ehealth.domain.Doctor;

import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.skyway.spring.util.dao.AbstractJpaDao;

import org.springframework.dao.DataAccessException;

import org.springframework.stereotype.Repository;

import org.springframework.transaction.annotation.Transactional;

/**
 * DAO to manage Doctor entities.
 * 
 */
@Repository("DoctorDAO")

@Transactional
public class DoctorDAOImpl extends AbstractJpaDao<Doctor> implements DoctorDAO {

	/**
	 * Set of entity classes managed by this DAO.  Typically a DAO manages a single entity.
	 *
	 */
	private final static Set<Class<?>> dataTypes = new HashSet<Class<?>>(Arrays.asList(new Class<?>[] {
			Doctor.class }));

	/**
	 * EntityManager injected by Spring for persistence unit dbdriver
	 *
	 */
	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Instantiates a new DoctorDAOImpl
	 *
	 */
	public DoctorDAOImpl() {
		super();
	}

	/**
	 * Get the entity manager that manages persistence unit 
	 *
	 */
	public EntityManager getEntityManager() {
		return entityManager;
	}

	/**
	 * Returns the set of entity classes managed by this DAO.
	 *
	 */
	public Set<Class<?>> getTypes() {
		return dataTypes;
	}

	/**
	 * JPQL Query - findDoctorByCodeKey
	 *
	 */
	@Transactional
	public Set<Doctor> findDoctorByCodeKey(String codeKey) throws DataAccessException {
		return findDoctorByCodeKey(codeKey, -1, -1);
	}

	/**
	 * JPQL Query - findDoctorByCodeKey
	 *
	 */
	@SuppressWarnings("unchecked")
	@Transactional
	public Set<Doctor> findDoctorByCodeKey(String codeKey, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findDoctorByCodeKey", startResult, maxRows, codeKey);
		return new LinkedHashSet<Doctor>(query.getResultList());
	}

	/**
	 * JPQL Query - findDoctorByHospitalContaining
	 *
	 */
	@Transactional
	public Set<Doctor> findDoctorByHospitalContaining(String hospital) throws DataAccessException {

		return findDoctorByHospitalContaining(hospital, -1, -1);
	}

	/**
	 * JPQL Query - findDoctorByHospitalContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<Doctor> findDoctorByHospitalContaining(String hospital, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findDoctorByHospitalContaining", startResult, maxRows, hospital);
		return new LinkedHashSet<Doctor>(query.getResultList());
	}

	/**
	 * JPQL Query - findDoctorByCodeKeyContaining
	 *
	 */
	@Transactional
	public Set<Doctor> findDoctorByCodeKeyContaining(String codeKey) throws DataAccessException {

		return findDoctorByCodeKeyContaining(codeKey, -1, -1);
	}

	/**
	 * JPQL Query - findDoctorByCodeKeyContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<Doctor> findDoctorByCodeKeyContaining(String codeKey, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findDoctorByCodeKeyContaining", startResult, maxRows, codeKey);
		return new LinkedHashSet<Doctor>(query.getResultList());
	}

	/**
	 * JPQL Query - findDoctorByPrimaryKey
	 *
	 */
	@Transactional
	public Doctor findDoctorByPrimaryKey(Integer doctorId) throws DataAccessException {

		return findDoctorByPrimaryKey(doctorId, -1, -1);
	}

	/**
	 * JPQL Query - findDoctorByPrimaryKey
	 *
	 */

	@Transactional
	public Doctor findDoctorByPrimaryKey(Integer doctorId, int startResult, int maxRows) throws DataAccessException {
		try {
			Query query = createNamedQuery("findDoctorByPrimaryKey", startResult, maxRows, doctorId);
			return (ehealth.domain.Doctor) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * JPQL Query - findDoctorByDoctorId
	 *
	 */
	@Transactional
	public Doctor findDoctorByDoctorId(Integer doctorId) throws DataAccessException {

		return findDoctorByDoctorId(doctorId, -1, -1);
	}

	/**
	 * JPQL Query - findDoctorByDoctorId
	 *
	 */

	@Transactional
	public Doctor findDoctorByDoctorId(Integer doctorId, int startResult, int maxRows) throws DataAccessException {
		try {
			Query query = createNamedQuery("findDoctorByDoctorId", startResult, maxRows, doctorId);
			return (ehealth.domain.Doctor) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * JPQL Query - findDoctorByHospital
	 *
	 */
	@Transactional
	public Set<Doctor> findDoctorByHospital(String hospital) throws DataAccessException {

		return findDoctorByHospital(hospital, -1, -1);
	}

	/**
	 * JPQL Query - findDoctorByHospital
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<Doctor> findDoctorByHospital(String hospital, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findDoctorByHospital", startResult, maxRows, hospital);
		return new LinkedHashSet<Doctor>(query.getResultList());
	}

	/**
	 * JPQL Query - findAllDoctors
	 *
	 */
	@Transactional
	public Set<Doctor> findAllDoctors() throws DataAccessException {

		return findAllDoctors(-1, -1);
	}

	/**
	 * JPQL Query - findAllDoctors
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<Doctor> findAllDoctors(int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findAllDoctors", startResult, maxRows);
		return new LinkedHashSet<Doctor>(query.getResultList());
	}

	/**
	 * Used to determine whether or not to merge the entity or persist the entity when calling Store
	 * @see store
	 * 
	 *
	 */
	public boolean canBeMerged(Doctor entity) {
		return true;
	}
}
