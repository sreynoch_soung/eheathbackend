
package ehealth.dao;

import ehealth.domain.FoodTageInput;

import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.skyway.spring.util.dao.AbstractJpaDao;

import org.springframework.dao.DataAccessException;

import org.springframework.stereotype.Repository;

import org.springframework.transaction.annotation.Transactional;

/**
 * DAO to manage FoodTageInput entities.
 * 
 */
@Repository("FoodTageInputDAO")

@Transactional
public class FoodTageInputDAOImpl extends AbstractJpaDao<FoodTageInput> implements FoodTageInputDAO {

	/**
	 * Set of entity classes managed by this DAO.  Typically a DAO manages a single entity.
	 *
	 */
	private final static Set<Class<?>> dataTypes = new HashSet<Class<?>>(Arrays.asList(new Class<?>[] {
			FoodTageInput.class }));

	/**
	 * EntityManager injected by Spring for persistence unit dbdriver
	 *
	 */
	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Instantiates a new FoodTageInputDAOImpl
	 *
	 */
	public FoodTageInputDAOImpl() {
		super();
	}

	/**
	 * Get the entity manager that manages persistence unit 
	 *
	 */
	public EntityManager getEntityManager() {
		return entityManager;
	}

	/**
	 * Returns the set of entity classes managed by this DAO.
	 *
	 */
	public Set<Class<?>> getTypes() {
		return dataTypes;
	}

	/**
	 * JPQL Query - findFoodTageInputByValue
	 *
	 */
	@Transactional
	public Set<FoodTageInput> findFoodTageInputByValue(Integer value) throws DataAccessException {

		return findFoodTageInputByValue(value, -1, -1);
	}

	/**
	 * JPQL Query - findFoodTageInputByValue
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<FoodTageInput> findFoodTageInputByValue(Integer value, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findFoodTageInputByValue", startResult, maxRows, value);
		return new LinkedHashSet<FoodTageInput>(query.getResultList());
	}

	/**
	 * JPQL Query - findFoodTageInputByPrimaryKey
	 *
	 */
	@Transactional
	public FoodTageInput findFoodTageInputByPrimaryKey(Integer foodTageInputId) throws DataAccessException {

		return findFoodTageInputByPrimaryKey(foodTageInputId, -1, -1);
	}

	/**
	 * JPQL Query - findFoodTageInputByPrimaryKey
	 *
	 */

	@Transactional
	public FoodTageInput findFoodTageInputByPrimaryKey(Integer foodTageInputId, int startResult, int maxRows) throws DataAccessException {
		try {
			Query query = createNamedQuery("findFoodTageInputByPrimaryKey", startResult, maxRows, foodTageInputId);
			return (ehealth.domain.FoodTageInput) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * JPQL Query - findFoodTageInputByFoodTageInputId
	 *
	 */
	@Transactional
	public FoodTageInput findFoodTageInputByFoodTageInputId(Integer foodTageInputId) throws DataAccessException {

		return findFoodTageInputByFoodTageInputId(foodTageInputId, -1, -1);
	}

	/**
	 * JPQL Query - findFoodTageInputByFoodTageInputId
	 *
	 */

	@Transactional
	public FoodTageInput findFoodTageInputByFoodTageInputId(Integer foodTageInputId, int startResult, int maxRows) throws DataAccessException {
		try {
			Query query = createNamedQuery("findFoodTageInputByFoodTageInputId", startResult, maxRows, foodTageInputId);
			return (ehealth.domain.FoodTageInput) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * JPQL Query - findAllFoodTageInputs
	 *
	 */
	@Transactional
	public Set<FoodTageInput> findAllFoodTageInputs() throws DataAccessException {

		return findAllFoodTageInputs(-1, -1);
	}

	/**
	 * JPQL Query - findAllFoodTageInputs
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<FoodTageInput> findAllFoodTageInputs(int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findAllFoodTageInputs", startResult, maxRows);
		return new LinkedHashSet<FoodTageInput>(query.getResultList());
	}

	/**
	 * Used to determine whether or not to merge the entity or persist the entity when calling Store
	 * @see store
	 * 
	 *
	 */
	public boolean canBeMerged(FoodTageInput entity) {
		return true;
	}
}
