
package ehealth.dao;

import ehealth.domain.City;

import java.util.Set;

import org.skyway.spring.util.dao.JpaDao;

import org.springframework.dao.DataAccessException;

/**
 * DAO to manage City entities.
 * 
 */
public interface CityDAO extends JpaDao<City> {

	/**
	 * JPQL Query - findCityByNameContaining
	 *
	 */
	public Set<City> findCityByNameContaining(String name) throws DataAccessException;

	/**
	 * JPQL Query - findCityByNameContaining
	 *
	 */
	public Set<City> findCityByNameContaining(String name, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findCityByCityId
	 *
	 */
	public City findCityByCityId(Integer cityId) throws DataAccessException;

	/**
	 * JPQL Query - findCityByCityId
	 *
	 */
	public City findCityByCityId(Integer cityId, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findCityByName
	 *
	 */
	public Set<City> findCityByName(String name_1) throws DataAccessException;

	/**
	 * JPQL Query - findCityByName
	 *
	 */
	public Set<City> findCityByName(String name_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findAllCitys
	 *
	 */
	public Set<City> findAllCitys() throws DataAccessException;

	/**
	 * JPQL Query - findAllCitys
	 *
	 */
	public Set<City> findAllCitys(int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findCityByPrimaryKey
	 *
	 */
	public City findCityByPrimaryKey(Integer cityId_1) throws DataAccessException;

	/**
	 * JPQL Query - findCityByPrimaryKey
	 *
	 */
	public City findCityByPrimaryKey(Integer cityId_1, int startResult, int maxRows) throws DataAccessException;

}