
package ehealth.dao;

import ehealth.domain.Parent;

import java.util.Set;

import org.skyway.spring.util.dao.JpaDao;

import org.springframework.dao.DataAccessException;

/**
 * DAO to manage Parent entities.
 * 
 */
public interface ParentDAO extends JpaDao<Parent> {

	/**
	 * JPQL Query - findParentByPrimaryKey
	 *
	 */
	public Parent findParentByPrimaryKey(Integer parentId) throws DataAccessException;

	/**
	 * JPQL Query - findParentByPrimaryKey
	 *
	 */
	public Parent findParentByPrimaryKey(Integer parentId, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findParentByAddressContaining
	 *
	 */
	public Set<Parent> findParentByAddressContaining(String address) throws DataAccessException;

	/**
	 * JPQL Query - findParentByAddressContaining
	 *
	 */
	public Set<Parent> findParentByAddressContaining(String address, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findParentByAddress
	 *
	 */
	public Set<Parent> findParentByAddress(String address_1) throws DataAccessException;

	/**
	 * JPQL Query - findParentByAddress
	 *
	 */
	public Set<Parent> findParentByAddress(String address_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findParentByOccupation
	 *
	 */
	public Set<Parent> findParentByOccupation(String occupation) throws DataAccessException;

	/**
	 * JPQL Query - findParentByOccupation
	 *
	 */
	public Set<Parent> findParentByOccupation(String occupation, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findAllParents
	 *
	 */
	public Set<Parent> findAllParents() throws DataAccessException;

	/**
	 * JPQL Query - findAllParents
	 *
	 */
	public Set<Parent> findAllParents(int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findParentByOccupationContaining
	 *
	 */
	public Set<Parent> findParentByOccupationContaining(String occupation_1) throws DataAccessException;

	/**
	 * JPQL Query - findParentByOccupationContaining
	 *
	 */
	public Set<Parent> findParentByOccupationContaining(String occupation_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findParentByParentId
	 *
	 */
	public Parent findParentByParentId(Integer parentId_1) throws DataAccessException;

	/**
	 * JPQL Query - findParentByParentId
	 *
	 */
	public Parent findParentByParentId(Integer parentId_1, int startResult, int maxRows) throws DataAccessException;

}