
package ehealth.dao;

import ehealth.domain.Grad;

import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.skyway.spring.util.dao.AbstractJpaDao;

import org.springframework.dao.DataAccessException;

import org.springframework.stereotype.Repository;

import org.springframework.transaction.annotation.Transactional;

/**
 * DAO to manage Grad entities.
 * 
 */
@Repository("GradDAO")

@Transactional
public class GradDAOImpl extends AbstractJpaDao<Grad> implements GradDAO {

	/**
	 * Set of entity classes managed by this DAO.  Typically a DAO manages a single entity.
	 *
	 */
	private final static Set<Class<?>> dataTypes = new HashSet<Class<?>>(Arrays.asList(new Class<?>[] { Grad.class }));

	/**
	 * EntityManager injected by Spring for persistence unit dbdriver
	 *
	 */
	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Instantiates a new GradDAOImpl
	 *
	 */
	public GradDAOImpl() {
		super();
	}

	/**
	 * Get the entity manager that manages persistence unit 
	 *
	 */
	public EntityManager getEntityManager() {
		return entityManager;
	}

	/**
	 * Returns the set of entity classes managed by this DAO.
	 *
	 */
	public Set<Class<?>> getTypes() {
		return dataTypes;
	}

	/**
	 * JPQL Query - findGradByDate
	 *
	 */
	@Transactional
	public Set<Grad> findGradByDate(java.util.Calendar date) throws DataAccessException {

		return findGradByDate(date, -1, -1);
	}

	/**
	 * JPQL Query - findGradByDate
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<Grad> findGradByDate(java.util.Calendar date, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findGradByDate", startResult, maxRows, date);
		return new LinkedHashSet<Grad>(query.getResultList());
	}

	/**
	 * JPQL Query - findGradByValueContaining
	 *
	 */
	@Transactional
	public Set<Grad> findGradByValueContaining(String value) throws DataAccessException {

		return findGradByValueContaining(value, -1, -1);
	}

	/**
	 * JPQL Query - findGradByValueContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<Grad> findGradByValueContaining(String value, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findGradByValueContaining", startResult, maxRows, value);
		return new LinkedHashSet<Grad>(query.getResultList());
	}

	/**
	 * JPQL Query - findGradByPrimaryKey
	 *
	 */
	@Transactional
	public Grad findGradByPrimaryKey(Integer gradId) throws DataAccessException {

		return findGradByPrimaryKey(gradId, -1, -1);
	}

	/**
	 * JPQL Query - findGradByPrimaryKey
	 *
	 */

	@Transactional
	public Grad findGradByPrimaryKey(Integer gradId, int startResult, int maxRows) throws DataAccessException {
		try {
			Query query = createNamedQuery("findGradByPrimaryKey", startResult, maxRows, gradId);
			return (ehealth.domain.Grad) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * JPQL Query - findGradByValue
	 *
	 */
	@Transactional
	public Set<Grad> findGradByValue(String value) throws DataAccessException {

		return findGradByValue(value, -1, -1);
	}

	/**
	 * JPQL Query - findGradByValue
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<Grad> findGradByValue(String value, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findGradByValue", startResult, maxRows, value);
		return new LinkedHashSet<Grad>(query.getResultList());
	}

	/**
	 * JPQL Query - findAllGrads
	 *
	 */
	@Transactional
	public Set<Grad> findAllGrads() throws DataAccessException {

		return findAllGrads(-1, -1);
	}

	/**
	 * JPQL Query - findAllGrads
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<Grad> findAllGrads(int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findAllGrads", startResult, maxRows);
		return new LinkedHashSet<Grad>(query.getResultList());
	}

	/**
	 * JPQL Query - findGradByGradId
	 *
	 */
	@Transactional
	public Grad findGradByGradId(Integer gradId) throws DataAccessException {

		return findGradByGradId(gradId, -1, -1);
	}

	/**
	 * JPQL Query - findGradByGradId
	 *
	 */

	@Transactional
	public Grad findGradByGradId(Integer gradId, int startResult, int maxRows) throws DataAccessException {
		try {
			Query query = createNamedQuery("findGradByGradId", startResult, maxRows, gradId);
			return (ehealth.domain.Grad) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * Used to determine whether or not to merge the entity or persist the entity when calling Store
	 * @see store
	 * 
	 *
	 */
	public boolean canBeMerged(Grad entity) {
		return true;
	}
}
