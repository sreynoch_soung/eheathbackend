
package ehealth.dao;

import ehealth.domain.User;

import java.util.Set;

import org.skyway.spring.util.dao.JpaDao;

import org.springframework.dao.DataAccessException;

/**
 * DAO to manage User entities.
 * 
 */
public interface UserDAO extends JpaDao<User> {

	/**
	 * JPQL Query - findUserByPassword
	 *
	 */
	public Set<User> findUserByPassword(String password) throws DataAccessException;

	/**
	 * JPQL Query - findUserByPassword
	 *
	 */
	public Set<User> findUserByPassword(String password, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findUserByUsernameContaining
	 *
	 */
	public Set<User> findUserByUsernameContaining(String username) throws DataAccessException;

	/**
	 * JPQL Query - findUserByUsernameContaining
	 *
	 */
	public Set<User> findUserByUsernameContaining(String username, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findUserByAge
	 *
	 */
	public Set<User> findUserByAge(Integer age) throws DataAccessException;

	/**
	 * JPQL Query - findUserByAge
	 *
	 */
	public Set<User> findUserByAge(Integer age, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findAllUsers
	 *
	 */
	public Set<User> findAllUsers() throws DataAccessException;

	/**
	 * JPQL Query - findAllUsers
	 *
	 */
	public Set<User> findAllUsers(int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findUserBySexContaining
	 *
	 */
	public Set<User> findUserBySexContaining(String sex) throws DataAccessException;

	/**
	 * JPQL Query - findUserBySexContaining
	 *
	 */
	public Set<User> findUserBySexContaining(String sex, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findUserByName
	 *
	 */
	public Set<User> findUserByName(String name) throws DataAccessException;

	/**
	 * JPQL Query - findUserByName
	 *
	 */
	public Set<User> findUserByName(String name, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findUserByTelephoneContaining
	 *
	 */
	public Set<User> findUserByTelephoneContaining(String telephone) throws DataAccessException;

	/**
	 * JPQL Query - findUserByTelephoneContaining
	 *
	 */
	public Set<User> findUserByTelephoneContaining(String telephone, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findUserBySurenameContaining
	 *
	 */
	public Set<User> findUserBySurenameContaining(String surename) throws DataAccessException;

	/**
	 * JPQL Query - findUserBySurenameContaining
	 *
	 */
	public Set<User> findUserBySurenameContaining(String surename, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findUserByNameContaining
	 *
	 */
	public Set<User> findUserByNameContaining(String name_1) throws DataAccessException;

	/**
	 * JPQL Query - findUserByNameContaining
	 *
	 */
	public Set<User> findUserByNameContaining(String name_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findUserByPasswordContaining
	 *
	 */
	public Set<User> findUserByPasswordContaining(String password_1) throws DataAccessException;

	/**
	 * JPQL Query - findUserByPasswordContaining
	 *
	 */
	public Set<User> findUserByPasswordContaining(String password_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findUserByEnabled
	 *
	 */
	public Set<User> findUserByEnabled(Boolean enabled) throws DataAccessException;

	/**
	 * JPQL Query - findUserByEnabled
	 *
	 */
	public Set<User> findUserByEnabled(Boolean enabled, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findUserBySex
	 *
	 */
	public Set<User> findUserBySex(String sex_1) throws DataAccessException;

	/**
	 * JPQL Query - findUserBySex
	 *
	 */
	public Set<User> findUserBySex(String sex_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findUserByUsername
	 *
	 */
	public User findUserByUsername(String username_1) throws DataAccessException;

	/**
	 * JPQL Query - findUserByUsername
	 *
	 */
	public User findUserByUsername(String username_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findUserByTelephone
	 *
	 */
	public Set<User> findUserByTelephone(String telephone_1) throws DataAccessException;

	/**
	 * JPQL Query - findUserByTelephone
	 *
	 */
	public Set<User> findUserByTelephone(String telephone_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findUserByPrimaryKey
	 *
	 */
	public User findUserByPrimaryKey(Integer userId) throws DataAccessException;

	/**
	 * JPQL Query - findUserByPrimaryKey
	 *
	 */
	public User findUserByPrimaryKey(Integer userId, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findUserByUserId
	 *
	 */
	public User findUserByUserId(Integer userId_1) throws DataAccessException;

	/**
	 * JPQL Query - findUserByUserId
	 *
	 */
	public User findUserByUserId(Integer userId_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findUserByEmail
	 *
	 */
	public Set<User> findUserByEmail(String email) throws DataAccessException;

	/**
	 * JPQL Query - findUserByEmail
	 *
	 */
	public Set<User> findUserByEmail(String email, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findUserByEmailContaining
	 *
	 */
	public Set<User> findUserByEmailContaining(String email_1) throws DataAccessException;

	/**
	 * JPQL Query - findUserByEmailContaining
	 *
	 */
	public Set<User> findUserByEmailContaining(String email_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findUserBySurename
	 *
	 */
	public Set<User> findUserBySurename(String surename_1) throws DataAccessException;

	/**
	 * JPQL Query - findUserBySurename
	 *
	 */
	public Set<User> findUserBySurename(String surename_1, int startResult, int maxRows) throws DataAccessException;

}