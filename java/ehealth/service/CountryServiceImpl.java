package ehealth.service;

import ehealth.dao.CityDAO;
import ehealth.dao.CountryDAO;

import ehealth.domain.City;
import ehealth.domain.Country;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;

import org.springframework.transaction.annotation.Transactional;

/**
 * Spring service that handles CRUD requests for Country entities
 * 
 */

@Service("CountryService")

@Transactional
public class CountryServiceImpl implements CountryService {

	/**
	 * DAO injected by Spring that manages City entities
	 * 
	 */
	@Autowired
	private CityDAO cityDAO;

	/**
	 * DAO injected by Spring that manages Country entities
	 * 
	 */
	@Autowired
	private CountryDAO countryDAO;

	/**
	 * Instantiates a new CountryServiceImpl.
	 *
	 */
	public CountryServiceImpl() {
	}

	/**
	 * Delete an existing City entity
	 * 
	 */
	@Transactional
	public Country deleteCountryCities(Integer country_countryId, Integer related_cities_cityId) {
		City related_cities = cityDAO.findCityByPrimaryKey(related_cities_cityId, -1, -1);

		Country country = countryDAO.findCountryByPrimaryKey(country_countryId, -1, -1);

		related_cities.setCountry(null);
		country.getCities().remove(related_cities);

		cityDAO.remove(related_cities);
		cityDAO.flush();

		return country;
	}

	/**
	 * Save an existing City entity
	 * 
	 */
	@Transactional
	public Country saveCountryCities(Integer countryId, City related_cities) {
		Country country = countryDAO.findCountryByPrimaryKey(countryId, -1, -1);
		City existingcities = cityDAO.findCityByPrimaryKey(related_cities.getCityId());

		// copy into the existing record to preserve existing relationships
		if (existingcities != null) {
			existingcities.setCityId(related_cities.getCityId());
			existingcities.setName(related_cities.getName());
			related_cities = existingcities;
		}

		related_cities.setCountry(country);
		country.getCities().add(related_cities);
		related_cities = cityDAO.store(related_cities);
		cityDAO.flush();

		country = countryDAO.store(country);
		countryDAO.flush();

		return country;
	}

	/**
	 * Return all Country entity
	 * 
	 */
	@Transactional
	public List<Country> findAllCountrys(Integer startResult, Integer maxRows) {
		return new java.util.ArrayList<Country>(countryDAO.findAllCountrys(startResult, maxRows));
	}

	/**
	 * Save an existing Country entity
	 * 
	 */
	@Transactional
	public Country saveCountry(Country country) {
		Country existingCountry = countryDAO.findCountryByPrimaryKey(country.getCountryId());

		if (existingCountry != null) {
			if (existingCountry != country) {
				existingCountry.setCountryId(country.getCountryId());
				existingCountry.setName(country.getName());
			}
			country = countryDAO.store(existingCountry);
		} else {
			country = countryDAO.store(country);
		}
		countryDAO.flush();
		return country;
	}

	/**
	 * Return a count of all Country entity
	 * 
	 */
	@Transactional
	public Integer countCountrys() {
		return ((Long) countryDAO.createQuerySingleResult("select count(o) from Country o").getSingleResult()).intValue();
	}

	/**
	 * Delete an existing Country entity
	 * 
	 */
	@Transactional
	public void deleteCountry(Country country) {
		countryDAO.remove(country);
		countryDAO.flush();
	}

	/**
	 * Load an existing Country entity
	 * 
	 */
	@Transactional
	public Set<Country> loadCountrys() {
		return countryDAO.findAllCountrys();
	}

	/**
	 */
	@Transactional
	public Country findCountryByPrimaryKey(Integer countryId) {
		return countryDAO.findCountryByPrimaryKey(countryId);
	}
}
