
package ehealth.service;

import ehealth.domain.EffortType;
import ehealth.domain.ExerciseInput;
import ehealth.domain.ExerciseType;
import ehealth.domain.Kid;

import java.util.List;
import java.util.Set;

/**
 * Spring service that handles CRUD requests for ExerciseInput entities
 * 
 */
public interface ExerciseInputService {

	/**
	* Return all ExerciseInput entity
	* 
	 */
	public List<ExerciseInput> findAllExerciseInputs(Integer startResult, Integer maxRows);

	/**
	* Save an existing ExerciseInput entity
	* 
	 */
	public ExerciseInput saveExerciseInput(ExerciseInput exerciseinput);

	/**
	* Delete an existing ExerciseType entity
	* 
	 */
	public ExerciseInput deleteExerciseInputExerciseType(Integer exerciseinput_exerciseInputId, Integer related_exercisetype_exerciseTypeId);

	/**
	* Save an existing EffortType entity
	* 
	 */
	public ExerciseInput saveExerciseInputEffortType(Integer exerciseInputId, EffortType related_efforttype);

	/**
	 */
	public ExerciseInput findExerciseInputByPrimaryKey(Integer exerciseInputId_1);

	/**
	* Delete an existing EffortType entity
	* 
	 */
	public ExerciseInput deleteExerciseInputEffortType(Integer exerciseinput_exerciseInputId_1, Integer related_efforttype_effortTypeId);

	/**
	* Delete an existing Kid entity
	* 
	 */
	public ExerciseInput deleteExerciseInputKid(Integer exerciseinput_exerciseInputId_2, Integer related_kid_kidId);

	/**
	* Save an existing ExerciseType entity
	* 
	 */
	public ExerciseInput saveExerciseInputExerciseType(Integer exerciseInputId_2, ExerciseType related_exercisetype);

	/**
	* Load an existing ExerciseInput entity
	* 
	 */
	public Set<ExerciseInput> loadExerciseInputs();

	/**
	* Delete an existing ExerciseInput entity
	* 
	 */
	public void deleteExerciseInput(ExerciseInput exerciseinput_1);

	/**
	* Save an existing Kid entity
	* 
	 */
	public ExerciseInput saveExerciseInputKid(Integer exerciseInputId_3, Kid related_kid);

	/**
	* Return a count of all ExerciseInput entity
	* 
	 */
	public Integer countExerciseInputs();
}