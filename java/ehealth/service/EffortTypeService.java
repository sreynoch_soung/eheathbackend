
package ehealth.service;

import ehealth.domain.EffortType;
import ehealth.domain.ExerciseInput;

import java.util.List;
import java.util.Set;

/**
 * Spring service that handles CRUD requests for EffortType entities
 * 
 */
public interface EffortTypeService {

	/**
	* Save an existing ExerciseInput entity
	* 
	 */
	public EffortType saveEffortTypeExerciseInputs(Integer effortTypeId, ExerciseInput related_exerciseinputs);

	/**
	* Return all EffortType entity
	* 
	 */
	public List<EffortType> findAllEffortTypes(Integer startResult, Integer maxRows);

	/**
	* Delete an existing EffortType entity
	* 
	 */
	public void deleteEffortType(EffortType efforttype);

	/**
	* Delete an existing ExerciseInput entity
	* 
	 */
	public EffortType deleteEffortTypeExerciseInputs(Integer efforttype_effortTypeId, Integer related_exerciseinputs_exerciseInputId);

	/**
	* Return a count of all EffortType entity
	* 
	 */
	public Integer countEffortTypes();

	/**
	* Load an existing EffortType entity
	* 
	 */
	public Set<EffortType> loadEffortTypes();

	/**
	* Save an existing EffortType entity
	* 
	 */
	public EffortType saveEffortType(EffortType efforttype_1);

	/**
	 */
	public EffortType findEffortTypeByPrimaryKey(Integer effortTypeId_1);
}