package ehealth.service;

import ehealth.dao.FoodPhotoDAO;
import ehealth.dao.FoodValidationDAO;
import ehealth.dao.UserDAO;

import ehealth.domain.FoodPhoto;
import ehealth.domain.FoodValidation;
import ehealth.domain.User;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;

import org.springframework.transaction.annotation.Transactional;

/**
 * Spring service that handles CRUD requests for FoodValidation entities
 * 
 */

@Service("FoodValidationService")

@Transactional
public class FoodValidationServiceImpl implements FoodValidationService {

	/**
	 * DAO injected by Spring that manages FoodPhoto entities
	 * 
	 */
	@Autowired
	private FoodPhotoDAO foodPhotoDAO;

	/**
	 * DAO injected by Spring that manages FoodValidation entities
	 * 
	 */
	@Autowired
	private FoodValidationDAO foodValidationDAO;

	/**
	 * DAO injected by Spring that manages User entities
	 * 
	 */
	@Autowired
	private UserDAO userDAO;

	/**
	 * Instantiates a new FoodValidationServiceImpl.
	 *
	 */
	public FoodValidationServiceImpl() {
	}

	/**
	 */
	@Transactional
	public FoodValidation findFoodValidationByPrimaryKey(Integer foodValidationId) {
		return foodValidationDAO.findFoodValidationByPrimaryKey(foodValidationId);
	}

	/**
	 * Return all FoodValidation entity
	 * 
	 */
	@Transactional
	public List<FoodValidation> findAllFoodValidations(Integer startResult, Integer maxRows) {
		return new java.util.ArrayList<FoodValidation>(foodValidationDAO.findAllFoodValidations(startResult, maxRows));
	}

	/**
	 * Delete an existing FoodValidation entity
	 * 
	 */
	@Transactional
	public void deleteFoodValidation(FoodValidation foodvalidation) {
		foodValidationDAO.remove(foodvalidation);
		foodValidationDAO.flush();
	}

	/**
	 * Delete an existing FoodPhoto entity
	 * 
	 */
	@Transactional
	public FoodValidation deleteFoodValidationFoodPhoto(Integer foodvalidation_foodValidationId, Integer related_foodphoto_foodPhotoId) {
		FoodValidation foodvalidation = foodValidationDAO.findFoodValidationByPrimaryKey(foodvalidation_foodValidationId, -1, -1);
		FoodPhoto related_foodphoto = foodPhotoDAO.findFoodPhotoByPrimaryKey(related_foodphoto_foodPhotoId, -1, -1);

		foodvalidation.setFoodPhoto(null);
		related_foodphoto.getFoodValidations().remove(foodvalidation);
		foodvalidation = foodValidationDAO.store(foodvalidation);
		foodValidationDAO.flush();

		related_foodphoto = foodPhotoDAO.store(related_foodphoto);
		foodPhotoDAO.flush();

		foodPhotoDAO.remove(related_foodphoto);
		foodPhotoDAO.flush();

		return foodvalidation;
	}

	/**
	 * Return a count of all FoodValidation entity
	 * 
	 */
	@Transactional
	public Integer countFoodValidations() {
		return ((Long) foodValidationDAO.createQuerySingleResult("select count(o) from FoodValidation o").getSingleResult()).intValue();
	}

	/**
	 * Save an existing FoodValidation entity
	 * 
	 */
	@Transactional
	public FoodValidation saveFoodValidation(FoodValidation foodvalidation) {
		FoodValidation existingFoodValidation = foodValidationDAO.findFoodValidationByPrimaryKey(foodvalidation.getFoodValidationId());

		if (existingFoodValidation != null) {
			if (existingFoodValidation != foodvalidation) {
				existingFoodValidation.setFoodValidationId(foodvalidation.getFoodValidationId());
				existingFoodValidation.setValue(foodvalidation.getValue());
				existingFoodValidation.setTime(foodvalidation.getTime());
			}
			foodvalidation = foodValidationDAO.store(existingFoodValidation);
		} else {
			foodvalidation = foodValidationDAO.store(foodvalidation);
		}
		foodValidationDAO.flush();
		return foodvalidation;
	}

	/**
	 * Save an existing User entity
	 * 
	 */
	@Transactional
	public FoodValidation saveFoodValidationUser(Integer foodValidationId, User related_user) {
		FoodValidation foodvalidation = foodValidationDAO.findFoodValidationByPrimaryKey(foodValidationId, -1, -1);
		User existinguser = userDAO.findUserByPrimaryKey(related_user.getUserId());

		// copy into the existing record to preserve existing relationships
		if (existinguser != null) {
			existinguser.setUserId(related_user.getUserId());
			existinguser.setUsername(related_user.getUsername());
			existinguser.setName(related_user.getName());
			existinguser.setSurename(related_user.getSurename());
			existinguser.setPassword(related_user.getPassword());
			existinguser.setEmail(related_user.getEmail());
			existinguser.setTelephone(related_user.getTelephone());
			existinguser.setAge(related_user.getAge());
			existinguser.setSex(related_user.getSex());
			related_user = existinguser;
		}

		foodvalidation.setUser(related_user);
		related_user.getFoodValidations().add(foodvalidation);
		foodvalidation = foodValidationDAO.store(foodvalidation);
		foodValidationDAO.flush();

		related_user = userDAO.store(related_user);
		userDAO.flush();

		return foodvalidation;
	}

	/**
	 * Load an existing FoodValidation entity
	 * 
	 */
	@Transactional
	public Set<FoodValidation> loadFoodValidations() {
		return foodValidationDAO.findAllFoodValidations();
	}

	/**
	 * Delete an existing User entity
	 * 
	 */
	@Transactional
	public FoodValidation deleteFoodValidationUser(Integer foodvalidation_foodValidationId, Integer related_user_userId) {
		FoodValidation foodvalidation = foodValidationDAO.findFoodValidationByPrimaryKey(foodvalidation_foodValidationId, -1, -1);
		User related_user = userDAO.findUserByPrimaryKey(related_user_userId, -1, -1);

		foodvalidation.setUser(null);
		related_user.getFoodValidations().remove(foodvalidation);
		foodvalidation = foodValidationDAO.store(foodvalidation);
		foodValidationDAO.flush();

		related_user = userDAO.store(related_user);
		userDAO.flush();

		userDAO.remove(related_user);
		userDAO.flush();

		return foodvalidation;
	}

	/**
	 * Save an existing FoodPhoto entity
	 * 
	 */
	@Transactional
	public FoodValidation saveFoodValidationFoodPhoto(Integer foodValidationId, FoodPhoto related_foodphoto) {
		FoodValidation foodvalidation = foodValidationDAO.findFoodValidationByPrimaryKey(foodValidationId, -1, -1);
		FoodPhoto existingfoodPhoto = foodPhotoDAO.findFoodPhotoByPrimaryKey(related_foodphoto.getFoodPhotoId());

		// copy into the existing record to preserve existing relationships
		if (existingfoodPhoto != null) {
			existingfoodPhoto.setFoodPhotoId(related_foodphoto.getFoodPhotoId());
			existingfoodPhoto.setUrl(related_foodphoto.getUrl());
			existingfoodPhoto.setFinalValidationValue(related_foodphoto.getFinalValidationValue());
			related_foodphoto = existingfoodPhoto;
		}

		foodvalidation.setFoodPhoto(related_foodphoto);
		related_foodphoto.getFoodValidations().add(foodvalidation);
		foodvalidation = foodValidationDAO.store(foodvalidation);
		foodValidationDAO.flush();

		related_foodphoto = foodPhotoDAO.store(related_foodphoto);
		foodPhotoDAO.flush();

		return foodvalidation;
	}
}
