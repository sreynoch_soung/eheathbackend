package ehealth.service;

import ehealth.dao.GradDAO;
import ehealth.dao.KidDAO;

import ehealth.domain.Grad;
import ehealth.domain.Kid;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;

import org.springframework.transaction.annotation.Transactional;

/**
 * Spring service that handles CRUD requests for Grad entities
 * 
 */

@Service("GradService")

@Transactional
public class GradServiceImpl implements GradService {

	/**
	 * DAO injected by Spring that manages Grad entities
	 * 
	 */
	@Autowired
	private GradDAO gradDAO;

	/**
	 * DAO injected by Spring that manages Kid entities
	 * 
	 */
	@Autowired
	private KidDAO kidDAO;

	/**
	 * Instantiates a new GradServiceImpl.
	 *
	 */
	public GradServiceImpl() {
	}

	/**
	 * Save an existing Kid entity
	 * 
	 */
	@Transactional
	public Grad saveGradKid(Integer gradId, Kid related_kid) {
		Grad grad = gradDAO.findGradByPrimaryKey(gradId, -1, -1);
		Kid existingkid = kidDAO.findKidByPrimaryKey(related_kid.getKidId());

		// copy into the existing record to preserve existing relationships
		if (existingkid != null) {
			existingkid.setKidId(related_kid.getKidId());
			existingkid.setHigh(related_kid.getHigh());
			existingkid.setWeight(related_kid.getWeight());
			existingkid.setAddress(related_kid.getAddress());
			existingkid.setCodeKey(related_kid.getCodeKey());
			related_kid = existingkid;
		}

		grad.setKid(related_kid);
		related_kid.getGrads().add(grad);
		grad = gradDAO.store(grad);
		gradDAO.flush();

		related_kid = kidDAO.store(related_kid);
		kidDAO.flush();

		return grad;
	}

	/**
	 * Delete an existing Kid entity
	 * 
	 */
	@Transactional
	public Grad deleteGradKid(Integer grad_gradId, Integer related_kid_kidId) {
		Grad grad = gradDAO.findGradByPrimaryKey(grad_gradId, -1, -1);
		Kid related_kid = kidDAO.findKidByPrimaryKey(related_kid_kidId, -1, -1);

		grad.setKid(null);
		related_kid.getGrads().remove(grad);
		grad = gradDAO.store(grad);
		gradDAO.flush();

		related_kid = kidDAO.store(related_kid);
		kidDAO.flush();

		kidDAO.remove(related_kid);
		kidDAO.flush();

		return grad;
	}

	/**
	 * Save an existing Grad entity
	 * 
	 */
	@Transactional
	public Grad saveGrad(Grad grad) {
		Grad existingGrad = gradDAO.findGradByPrimaryKey(grad.getGradId());

		if (existingGrad != null) {
			if (existingGrad != grad) {
				existingGrad.setGradId(grad.getGradId());
				existingGrad.setValue(grad.getValue());
				existingGrad.setDate(grad.getDate());
			}
			grad = gradDAO.store(existingGrad);
		} else {
			grad = gradDAO.store(grad);
		}
		gradDAO.flush();
		return grad;
	}

	/**
	 * Load an existing Grad entity
	 * 
	 */
	@Transactional
	public Set<Grad> loadGrads() {
		return gradDAO.findAllGrads();
	}

	/**
	 */
	@Transactional
	public Grad findGradByPrimaryKey(Integer gradId) {
		return gradDAO.findGradByPrimaryKey(gradId);
	}

	/**
	 * Delete an existing Grad entity
	 * 
	 */
	@Transactional
	public void deleteGrad(Grad grad) {
		gradDAO.remove(grad);
		gradDAO.flush();
	}

	/**
	 * Return a count of all Grad entity
	 * 
	 */
	@Transactional
	public Integer countGrads() {
		return ((Long) gradDAO.createQuerySingleResult("select count(o) from Grad o").getSingleResult()).intValue();
	}

	/**
	 * Return all Grad entity
	 * 
	 */
	@Transactional
	public List<Grad> findAllGrads(Integer startResult, Integer maxRows) {
		return new java.util.ArrayList<Grad>(gradDAO.findAllGrads(startResult, maxRows));
	}
}
