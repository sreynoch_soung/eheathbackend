package ehealth.service;

import ehealth.dao.AccountSettingDAO;
import ehealth.dao.CityDAO;
import ehealth.dao.ParentDAO;
import ehealth.dao.ParentKidDAO;
import ehealth.dao.UserDAO;

import ehealth.domain.AccountSetting;
import ehealth.domain.City;
import ehealth.domain.Parent;
import ehealth.domain.ParentKid;
import ehealth.domain.User;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;

import org.springframework.transaction.annotation.Transactional;

/**
 * Spring service that handles CRUD requests for Parent entities
 * 
 */

@Service("ParentService")

@Transactional
public class ParentServiceImpl implements ParentService {

	/**
	 * DAO injected by Spring that manages AccountSetting entities
	 * 
	 */
	@Autowired
	private AccountSettingDAO accountSettingDAO;

	/**
	 * DAO injected by Spring that manages City entities
	 * 
	 */
	@Autowired
	private CityDAO cityDAO;

	/**
	 * DAO injected by Spring that manages Parent entities
	 * 
	 */
	@Autowired
	private ParentDAO parentDAO;

	/**
	 * DAO injected by Spring that manages ParentKid entities
	 * 
	 */
	@Autowired
	private ParentKidDAO parentKidDAO;

	/**
	 * DAO injected by Spring that manages User entities
	 * 
	 */
	@Autowired
	private UserDAO userDAO;

	/**
	 * Instantiates a new ParentServiceImpl.
	 *
	 */
	public ParentServiceImpl() {
	}

	/**
	 */
	@Transactional
	public Parent findParentByPrimaryKey(Integer parentId) {
		return parentDAO.findParentByPrimaryKey(parentId);
	}

	/**
	 * Delete an existing City entity
	 * 
	 */
	@Transactional
	public Parent deleteParentCity(Integer parent_parentId, Integer related_city_cityId) {
		Parent parent = parentDAO.findParentByPrimaryKey(parent_parentId, -1, -1);
		City related_city = cityDAO.findCityByPrimaryKey(related_city_cityId, -1, -1);

		parent.setCity(null);
		related_city.getParents().remove(parent);
		parent = parentDAO.store(parent);
		parentDAO.flush();

		related_city = cityDAO.store(related_city);
		cityDAO.flush();

		cityDAO.remove(related_city);
		cityDAO.flush();

		return parent;
	}

	/**
	 * Return all Parent entity
	 * 
	 */
	@Transactional
	public List<Parent> findAllParents(Integer startResult, Integer maxRows) {
		return new java.util.ArrayList<Parent>(parentDAO.findAllParents(startResult, maxRows));
	}

	/**
	 * Delete an existing ParentKid entity
	 * 
	 */
	@Transactional
	public Parent deleteParentParentKids(Integer parent_parentId, Integer related_parentkids_parentKidId) {
		ParentKid related_parentkids = parentKidDAO.findParentKidByPrimaryKey(related_parentkids_parentKidId, -1, -1);

		Parent parent = parentDAO.findParentByPrimaryKey(parent_parentId, -1, -1);

		related_parentkids.setParent(null);
		parent.getParentKids().remove(related_parentkids);

		parentKidDAO.remove(related_parentkids);
		parentKidDAO.flush();

		return parent;
	}

	/**
	 * Save an existing ParentKid entity
	 * 
	 */
	@Transactional
	public Parent saveParentParentKids(Integer parentId, ParentKid related_parentkids) {
		Parent parent = parentDAO.findParentByPrimaryKey(parentId, -1, -1);
		ParentKid existingparentKids = parentKidDAO.findParentKidByPrimaryKey(related_parentkids.getParentKidId());

		// copy into the existing record to preserve existing relationships
		if (existingparentKids != null) {
			existingparentKids.setParentKidId(related_parentkids.getParentKidId());
			related_parentkids = existingparentKids;
		}

		related_parentkids.setParent(parent);
		parent.getParentKids().add(related_parentkids);
		related_parentkids = parentKidDAO.store(related_parentkids);
		parentKidDAO.flush();

		parent = parentDAO.store(parent);
		parentDAO.flush();

		return parent;
	}

	/**
	 * Load an existing Parent entity
	 * 
	 */
	@Transactional
	public Set<Parent> loadParents() {
		return parentDAO.findAllParents();
	}

	/**
	 * Return a count of all Parent entity
	 * 
	 */
	@Transactional
	public Integer countParents() {
		return ((Long) parentDAO.createQuerySingleResult("select count(o) from Parent o").getSingleResult()).intValue();
	}

	/**
	 * Save an existing AccountSetting entity
	 * 
	 */
	@Transactional
	public Parent saveParentAccountSetting(Integer parentId, AccountSetting related_accountsetting) {
		Parent parent = parentDAO.findParentByPrimaryKey(parentId, -1, -1);
		AccountSetting existingaccountSetting = accountSettingDAO.findAccountSettingByPrimaryKey(related_accountsetting.getAccountSettingId());

		// copy into the existing record to preserve existing relationships
		if (existingaccountSetting != null) {
			existingaccountSetting.setAccountSettingId(related_accountsetting.getAccountSettingId());
			existingaccountSetting.setNumPhotoValidateGroup(related_accountsetting.getNumPhotoValidateGroup());
			related_accountsetting = existingaccountSetting;
		} else {
			related_accountsetting = accountSettingDAO.store(related_accountsetting);
			accountSettingDAO.flush();
		}

		parent.setAccountSetting(related_accountsetting);
		related_accountsetting.getParents().add(parent);
		parent = parentDAO.store(parent);
		parentDAO.flush();

		related_accountsetting = accountSettingDAO.store(related_accountsetting);
		accountSettingDAO.flush();

		return parent;
	}

	/**
	 * Save an existing Parent entity
	 * 
	 */
	@Transactional
	public Parent saveParent(Parent parent) {
		Parent existingParent = parentDAO.findParentByPrimaryKey(parent.getParentId());

		if (existingParent != null) {
			if (existingParent != parent) {
				existingParent.setParentId(parent.getParentId());
				existingParent.setOccupation(parent.getOccupation());
				existingParent.setAddress(parent.getAddress());
			}
			parent = parentDAO.store(existingParent);
		} else {
			parent = parentDAO.store(parent);
		}
		parentDAO.flush();
		return parent;
	}

	/**
	 * Delete an existing User entity
	 * 
	 */
	@Transactional
	public Parent deleteParentUser(Integer parent_parentId, Integer related_user_userId) {
		Parent parent = parentDAO.findParentByPrimaryKey(parent_parentId, -1, -1);
		User related_user = userDAO.findUserByPrimaryKey(related_user_userId, -1, -1);

		parent.setUser(null);
		related_user.getParents().remove(parent);
		parent = parentDAO.store(parent);
		parentDAO.flush();

		related_user = userDAO.store(related_user);
		userDAO.flush();

		userDAO.remove(related_user);
		userDAO.flush();

		return parent;
	}

	/**
	 * Save an existing User entity
	 * 
	 */
	@Transactional
	public Parent saveParentUser(Integer parentId, User related_user) {
		Parent parent = parentDAO.findParentByPrimaryKey(parentId, -1, -1);
		User existinguser = userDAO.findUserByPrimaryKey(related_user.getUserId());

		// copy into the existing record to preserve existing relationships
		if (existinguser != null) {
			existinguser.setUserId(related_user.getUserId());
			existinguser.setUsername(related_user.getUsername());
			existinguser.setName(related_user.getName());
			existinguser.setSurename(related_user.getSurename());
			existinguser.setPassword(related_user.getPassword());
			existinguser.setEmail(related_user.getEmail());
			existinguser.setTelephone(related_user.getTelephone());
			existinguser.setAge(related_user.getAge());
			existinguser.setSex(related_user.getSex());
			related_user = existinguser;
		}

		parent.setUser(related_user);
		related_user.getParents().add(parent);
		parent = parentDAO.store(parent);
		parentDAO.flush();

		related_user = userDAO.store(related_user);
		userDAO.flush();

		return parent;
	}

	/**
	 * Save an existing City entity
	 * 
	 */
	@Transactional
	public Parent saveParentCity(Integer parentId, City related_city) {
		Parent parent = parentDAO.findParentByPrimaryKey(parentId, -1, -1);
		City existingcity = cityDAO.findCityByPrimaryKey(related_city.getCityId());

		// copy into the existing record to preserve existing relationships
		if (existingcity != null) {
			existingcity.setCityId(related_city.getCityId());
			existingcity.setName(related_city.getName());
			related_city = existingcity;
		}

		parent.setCity(related_city);
		related_city.getParents().add(parent);
		parent = parentDAO.store(parent);
		parentDAO.flush();

		related_city = cityDAO.store(related_city);
		cityDAO.flush();

		return parent;
	}

	/**
	 * Delete an existing AccountSetting entity
	 * 
	 */
	@Transactional
	public Parent deleteParentAccountSetting(Integer parent_parentId, Integer related_accountsetting_accountSettingId) {
		Parent parent = parentDAO.findParentByPrimaryKey(parent_parentId, -1, -1);
		AccountSetting related_accountsetting = accountSettingDAO.findAccountSettingByPrimaryKey(related_accountsetting_accountSettingId, -1, -1);

		parent.setAccountSetting(null);
		related_accountsetting.getParents().remove(parent);
		parent = parentDAO.store(parent);
		parentDAO.flush();

		related_accountsetting = accountSettingDAO.store(related_accountsetting);
		accountSettingDAO.flush();

		accountSettingDAO.remove(related_accountsetting);
		accountSettingDAO.flush();

		return parent;
	}

	/**
	 * Delete an existing Parent entity
	 * 
	 */
	@Transactional
	public void deleteParent(Parent parent) {
		parentDAO.remove(parent);
		parentDAO.flush();
	}
}
