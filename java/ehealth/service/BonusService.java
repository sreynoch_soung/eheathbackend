
package ehealth.service;

import ehealth.domain.Bonus;
import ehealth.domain.Kid;

import java.util.List;
import java.util.Set;

/**
 * Spring service that handles CRUD requests for Bonus entities
 * 
 */
public interface BonusService {

	/**
	* Return all Bonus entity
	* 
	 */
	public List<Bonus> findAllBonuss(Integer startResult, Integer maxRows);

	/**
	* Load an existing Bonus entity
	* 
	 */
	public Set<Bonus> loadBonuss();

	/**
	 */
	public Bonus findBonusByPrimaryKey(Integer bonusId);

	/**
	* Return a count of all Bonus entity
	* 
	 */
	public Integer countBonuss();

	/**
	* Delete an existing Bonus entity
	* 
	 */
	public void deleteBonus(Bonus bonus);

	/**
	* Save an existing Bonus entity
	* 
	 */
	public Bonus saveBonus(Bonus bonus_1);

	/**
	* Delete an existing Kid entity
	* 
	 */
	public Bonus deleteBonusKid(Integer bonus_bonusId, Integer related_kid_kidId);

	/**
	* Save an existing Kid entity
	* 
	 */
	public Bonus saveBonusKid(Integer bonusId_1, Kid related_kid);
}