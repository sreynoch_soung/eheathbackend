
package ehealth.service;

import ehealth.domain.FoodPhoto;
import ehealth.domain.FoodValidation;
import ehealth.domain.User;

import java.util.List;
import java.util.Set;

/**
 * Spring service that handles CRUD requests for FoodValidation entities
 * 
 */
public interface FoodValidationService {

	/**
	 */
	public FoodValidation findFoodValidationByPrimaryKey(Integer foodValidationId);

	/**
	* Return all FoodValidation entity
	* 
	 */
	public List<FoodValidation> findAllFoodValidations(Integer startResult, Integer maxRows);

	/**
	* Delete an existing FoodValidation entity
	* 
	 */
	public void deleteFoodValidation(FoodValidation foodvalidation);

	/**
	* Delete an existing FoodPhoto entity
	* 
	 */
	public FoodValidation deleteFoodValidationFoodPhoto(Integer foodvalidation_foodValidationId, Integer related_foodphoto_foodPhotoId);

	/**
	* Return a count of all FoodValidation entity
	* 
	 */
	public Integer countFoodValidations();

	/**
	* Save an existing FoodValidation entity
	* 
	 */
	public FoodValidation saveFoodValidation(FoodValidation foodvalidation_1);

	/**
	* Save an existing User entity
	* 
	 */
	public FoodValidation saveFoodValidationUser(Integer foodValidationId_1, User related_user);

	/**
	* Load an existing FoodValidation entity
	* 
	 */
	public Set<FoodValidation> loadFoodValidations();

	/**
	* Delete an existing User entity
	* 
	 */
	public FoodValidation deleteFoodValidationUser(Integer foodvalidation_foodValidationId_1, Integer related_user_userId);

	/**
	* Save an existing FoodPhoto entity
	* 
	 */
	public FoodValidation saveFoodValidationFoodPhoto(Integer foodValidationId_2, FoodPhoto related_foodphoto);
}