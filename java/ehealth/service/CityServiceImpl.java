package ehealth.service;

import ehealth.dao.CityDAO;
import ehealth.dao.CountryDAO;
import ehealth.dao.KidDAO;
import ehealth.dao.ParentDAO;

import ehealth.domain.City;
import ehealth.domain.Country;
import ehealth.domain.Kid;
import ehealth.domain.Parent;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;

import org.springframework.transaction.annotation.Transactional;

/**
 * Spring service that handles CRUD requests for City entities
 * 
 */

@Service("CityService")

@Transactional
public class CityServiceImpl implements CityService {

	/**
	 * DAO injected by Spring that manages City entities
	 * 
	 */
	@Autowired
	private CityDAO cityDAO;

	/**
	 * DAO injected by Spring that manages Country entities
	 * 
	 */
	@Autowired
	private CountryDAO countryDAO;

	/**
	 * DAO injected by Spring that manages Kid entities
	 * 
	 */
	@Autowired
	private KidDAO kidDAO;

	/**
	 * DAO injected by Spring that manages Parent entities
	 * 
	 */
	@Autowired
	private ParentDAO parentDAO;

	/**
	 * Instantiates a new CityServiceImpl.
	 *
	 */
	public CityServiceImpl() {
	}

	/**
	 */
	@Transactional
	public City findCityByPrimaryKey(Integer cityId) {
		return cityDAO.findCityByPrimaryKey(cityId);
	}

	/**
	 * Delete an existing Parent entity
	 * 
	 */
	@Transactional
	public City deleteCityParents(Integer city_cityId, Integer related_parents_parentId) {
		Parent related_parents = parentDAO.findParentByPrimaryKey(related_parents_parentId, -1, -1);

		City city = cityDAO.findCityByPrimaryKey(city_cityId, -1, -1);

		related_parents.setCity(null);
		city.getParents().remove(related_parents);

		parentDAO.remove(related_parents);
		parentDAO.flush();

		return city;
	}

	/**
	 * Return a count of all City entity
	 * 
	 */
	@Transactional
	public Integer countCitys() {
		return ((Long) cityDAO.createQuerySingleResult("select count(o) from City o").getSingleResult()).intValue();
	}

	/**
	 * Save an existing Country entity
	 * 
	 */
	@Transactional
	public City saveCityCountry(Integer cityId, Country related_country) {
		City city = cityDAO.findCityByPrimaryKey(cityId, -1, -1);
		Country existingcountry = countryDAO.findCountryByPrimaryKey(related_country.getCountryId());

		// copy into the existing record to preserve existing relationships
		if (existingcountry != null) {
			existingcountry.setCountryId(related_country.getCountryId());
			existingcountry.setName(related_country.getName());
			related_country = existingcountry;
		}

		city.setCountry(related_country);
		related_country.getCities().add(city);
		city = cityDAO.store(city);
		cityDAO.flush();

		related_country = countryDAO.store(related_country);
		countryDAO.flush();

		return city;
	}

	/**
	 * Delete an existing Country entity
	 * 
	 */
	@Transactional
	public City deleteCityCountry(Integer city_cityId, Integer related_country_countryId) {
		City city = cityDAO.findCityByPrimaryKey(city_cityId, -1, -1);
		Country related_country = countryDAO.findCountryByPrimaryKey(related_country_countryId, -1, -1);

		city.setCountry(null);
		related_country.getCities().remove(city);
		city = cityDAO.store(city);
		cityDAO.flush();

		related_country = countryDAO.store(related_country);
		countryDAO.flush();

		countryDAO.remove(related_country);
		countryDAO.flush();

		return city;
	}

	/**
	 * Delete an existing City entity
	 * 
	 */
	@Transactional
	public void deleteCity(City city) {
		cityDAO.remove(city);
		cityDAO.flush();
	}

	/**
	 * Delete an existing Kid entity
	 * 
	 */
	@Transactional
	public City deleteCityKids(Integer city_cityId, Integer related_kids_kidId) {
		Kid related_kids = kidDAO.findKidByPrimaryKey(related_kids_kidId, -1, -1);

		City city = cityDAO.findCityByPrimaryKey(city_cityId, -1, -1);

		related_kids.setCity(null);
		city.getKids().remove(related_kids);

		kidDAO.remove(related_kids);
		kidDAO.flush();

		return city;
	}

	/**
	 * Save an existing Parent entity
	 * 
	 */
	@Transactional
	public City saveCityParents(Integer cityId, Parent related_parents) {
		City city = cityDAO.findCityByPrimaryKey(cityId, -1, -1);
		Parent existingparents = parentDAO.findParentByPrimaryKey(related_parents.getParentId());

		// copy into the existing record to preserve existing relationships
		if (existingparents != null) {
			existingparents.setParentId(related_parents.getParentId());
			existingparents.setOccupation(related_parents.getOccupation());
			existingparents.setAddress(related_parents.getAddress());
			related_parents = existingparents;
		}

		related_parents.setCity(city);
		city.getParents().add(related_parents);
		related_parents = parentDAO.store(related_parents);
		parentDAO.flush();

		city = cityDAO.store(city);
		cityDAO.flush();

		return city;
	}

	/**
	 * Load an existing City entity
	 * 
	 */
	@Transactional
	public Set<City> loadCitys() {
		return cityDAO.findAllCitys();
	}

	/**
	 * Return all City entity
	 * 
	 */
	@Transactional
	public List<City> findAllCitys(Integer startResult, Integer maxRows) {
		return new java.util.ArrayList<City>(cityDAO.findAllCitys(startResult, maxRows));
	}

	/**
	 * Save an existing Kid entity
	 * 
	 */
	@Transactional
	public City saveCityKids(Integer cityId, Kid related_kids) {
		City city = cityDAO.findCityByPrimaryKey(cityId, -1, -1);
		Kid existingkids = kidDAO.findKidByPrimaryKey(related_kids.getKidId());

		// copy into the existing record to preserve existing relationships
		if (existingkids != null) {
			existingkids.setKidId(related_kids.getKidId());
			existingkids.setHigh(related_kids.getHigh());
			existingkids.setWeight(related_kids.getWeight());
			existingkids.setAddress(related_kids.getAddress());
			existingkids.setCodeKey(related_kids.getCodeKey());
			related_kids = existingkids;
		}

		related_kids.setCity(city);
		city.getKids().add(related_kids);
		related_kids = kidDAO.store(related_kids);
		kidDAO.flush();

		city = cityDAO.store(city);
		cityDAO.flush();

		return city;
	}

	/**
	 * Save an existing City entity
	 * 
	 */
	@Transactional
	public City saveCity(City city) {
		City existingCity = cityDAO.findCityByPrimaryKey(city.getCityId());

		if (existingCity != null) {
			if (existingCity != city) {
				existingCity.setCityId(city.getCityId());
				existingCity.setName(city.getName());
			}
			city = cityDAO.store(existingCity);
		} else {
			city = cityDAO.store(city);
		}
		cityDAO.flush();
		return city;
	}
}
