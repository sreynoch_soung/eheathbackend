package ehealth.service;

import ehealth.dao.DoctorDAO;
import ehealth.dao.FoodTageTypeDAO;
import ehealth.dao.KidDAO;
import ehealth.dao.NutritionTargetDAO;

import ehealth.domain.Doctor;
import ehealth.domain.FoodTageType;
import ehealth.domain.Kid;
import ehealth.domain.NutritionTarget;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;

import org.springframework.transaction.annotation.Transactional;

/**
 * Spring service that handles CRUD requests for NutritionTarget entities
 * 
 */

@Service("NutritionTargetService")

@Transactional
public class NutritionTargetServiceImpl implements NutritionTargetService {

	/**
	 * DAO injected by Spring that manages Doctor entities
	 * 
	 */
	@Autowired
	private DoctorDAO doctorDAO;

	/**
	 * DAO injected by Spring that manages FoodTageType entities
	 * 
	 */
	@Autowired
	private FoodTageTypeDAO foodTageTypeDAO;

	/**
	 * DAO injected by Spring that manages Kid entities
	 * 
	 */
	@Autowired
	private KidDAO kidDAO;

	/**
	 * DAO injected by Spring that manages NutritionTarget entities
	 * 
	 */
	@Autowired
	private NutritionTargetDAO nutritionTargetDAO;

	/**
	 * Instantiates a new NutritionTargetServiceImpl.
	 *
	 */
	public NutritionTargetServiceImpl() {
	}

	/**
	 * Save an existing Kid entity
	 * 
	 */
	@Transactional
	public NutritionTarget saveNutritionTargetKid(Integer nutritionTargetId, Kid related_kid) {
		NutritionTarget nutritiontarget = nutritionTargetDAO.findNutritionTargetByPrimaryKey(nutritionTargetId, -1, -1);
		Kid existingkid = kidDAO.findKidByPrimaryKey(related_kid.getKidId());

		// copy into the existing record to preserve existing relationships
		if (existingkid != null) {
			existingkid.setKidId(related_kid.getKidId());
			existingkid.setHigh(related_kid.getHigh());
			existingkid.setWeight(related_kid.getWeight());
			existingkid.setAddress(related_kid.getAddress());
			existingkid.setCodeKey(related_kid.getCodeKey());
			related_kid = existingkid;
		}

		nutritiontarget.setKid(related_kid);
		related_kid.getNutritionTargets().add(nutritiontarget);
		nutritiontarget = nutritionTargetDAO.store(nutritiontarget);
		nutritionTargetDAO.flush();

		related_kid = kidDAO.store(related_kid);
		kidDAO.flush();

		return nutritiontarget;
	}

	/**
	 * Save an existing Doctor entity
	 * 
	 */
	@Transactional
	public NutritionTarget saveNutritionTargetDoctor(Integer nutritionTargetId, Doctor related_doctor) {
		NutritionTarget nutritiontarget = nutritionTargetDAO.findNutritionTargetByPrimaryKey(nutritionTargetId, -1, -1);
		Doctor existingdoctor = doctorDAO.findDoctorByPrimaryKey(related_doctor.getDoctorId());

		// copy into the existing record to preserve existing relationships
		if (existingdoctor != null) {
			existingdoctor.setDoctorId(related_doctor.getDoctorId());
			existingdoctor.setHospital(related_doctor.getHospital());
			existingdoctor.setCodeKey(related_doctor.getCodeKey());
			related_doctor = existingdoctor;
		}

		nutritiontarget.setDoctor(related_doctor);
		related_doctor.getNutritionTargets().add(nutritiontarget);
		nutritiontarget = nutritionTargetDAO.store(nutritiontarget);
		nutritionTargetDAO.flush();

		related_doctor = doctorDAO.store(related_doctor);
		doctorDAO.flush();

		return nutritiontarget;
	}

	/**
	 * Save an existing FoodTageType entity
	 * 
	 */
	@Transactional
	public NutritionTarget saveNutritionTargetFoodTageType(Integer nutritionTargetId, FoodTageType related_foodtagetype) {
		NutritionTarget nutritiontarget = nutritionTargetDAO.findNutritionTargetByPrimaryKey(nutritionTargetId, -1, -1);
		FoodTageType existingfoodTageType = foodTageTypeDAO.findFoodTageTypeByPrimaryKey(related_foodtagetype.getFoodTageTypeId());

		// copy into the existing record to preserve existing relationships
		if (existingfoodTageType != null) {
			existingfoodTageType.setFoodTageTypeId(related_foodtagetype.getFoodTageTypeId());
			existingfoodTageType.setTypeName(related_foodtagetype.getTypeName());
			related_foodtagetype = existingfoodTageType;
		}

		nutritiontarget.setFoodTageType(related_foodtagetype);
		related_foodtagetype.getNutritionTargets().add(nutritiontarget);
		nutritiontarget = nutritionTargetDAO.store(nutritiontarget);
		nutritionTargetDAO.flush();

		related_foodtagetype = foodTageTypeDAO.store(related_foodtagetype);
		foodTageTypeDAO.flush();

		return nutritiontarget;
	}

	/**
	 * Return all NutritionTarget entity
	 * 
	 */
	@Transactional
	public List<NutritionTarget> findAllNutritionTargets(Integer startResult, Integer maxRows) {
		return new java.util.ArrayList<NutritionTarget>(nutritionTargetDAO.findAllNutritionTargets(startResult, maxRows));
	}

	/**
	 * Delete an existing Doctor entity
	 * 
	 */
	@Transactional
	public NutritionTarget deleteNutritionTargetDoctor(Integer nutritiontarget_nutritionTargetId, Integer related_doctor_doctorId) {
		NutritionTarget nutritiontarget = nutritionTargetDAO.findNutritionTargetByPrimaryKey(nutritiontarget_nutritionTargetId, -1, -1);
		Doctor related_doctor = doctorDAO.findDoctorByPrimaryKey(related_doctor_doctorId, -1, -1);

		nutritiontarget.setDoctor(null);
		related_doctor.getNutritionTargets().remove(nutritiontarget);
		nutritiontarget = nutritionTargetDAO.store(nutritiontarget);
		nutritionTargetDAO.flush();

		related_doctor = doctorDAO.store(related_doctor);
		doctorDAO.flush();

		doctorDAO.remove(related_doctor);
		doctorDAO.flush();

		return nutritiontarget;
	}

	/**
	 * Delete an existing Kid entity
	 * 
	 */
	@Transactional
	public NutritionTarget deleteNutritionTargetKid(Integer nutritiontarget_nutritionTargetId, Integer related_kid_kidId) {
		NutritionTarget nutritiontarget = nutritionTargetDAO.findNutritionTargetByPrimaryKey(nutritiontarget_nutritionTargetId, -1, -1);
		Kid related_kid = kidDAO.findKidByPrimaryKey(related_kid_kidId, -1, -1);

		nutritiontarget.setKid(null);
		related_kid.getNutritionTargets().remove(nutritiontarget);
		nutritiontarget = nutritionTargetDAO.store(nutritiontarget);
		nutritionTargetDAO.flush();

		related_kid = kidDAO.store(related_kid);
		kidDAO.flush();

		kidDAO.remove(related_kid);
		kidDAO.flush();

		return nutritiontarget;
	}

	/**
	 * Save an existing NutritionTarget entity
	 * 
	 */
	@Transactional
	public NutritionTarget saveNutritionTarget(NutritionTarget nutritiontarget) {
		NutritionTarget existingNutritionTarget = nutritionTargetDAO.findNutritionTargetByPrimaryKey(nutritiontarget.getNutritionTargetId());

		if (existingNutritionTarget != null) {
			if (existingNutritionTarget != nutritiontarget) {
				existingNutritionTarget.setNutritionTargetId(nutritiontarget.getNutritionTargetId());
				existingNutritionTarget.setDate(nutritiontarget.getDate());
			}
			nutritiontarget = nutritionTargetDAO.store(existingNutritionTarget);
		} else {
			nutritiontarget = nutritionTargetDAO.store(nutritiontarget);
		}
		nutritionTargetDAO.flush();
		return nutritiontarget;
	}

	/**
	 * Delete an existing FoodTageType entity
	 * 
	 */
	@Transactional
	public NutritionTarget deleteNutritionTargetFoodTageType(Integer nutritiontarget_nutritionTargetId, Integer related_foodtagetype_foodTageTypeId) {
		NutritionTarget nutritiontarget = nutritionTargetDAO.findNutritionTargetByPrimaryKey(nutritiontarget_nutritionTargetId, -1, -1);
		FoodTageType related_foodtagetype = foodTageTypeDAO.findFoodTageTypeByPrimaryKey(related_foodtagetype_foodTageTypeId, -1, -1);

		nutritiontarget.setFoodTageType(null);
		related_foodtagetype.getNutritionTargets().remove(nutritiontarget);
		nutritiontarget = nutritionTargetDAO.store(nutritiontarget);
		nutritionTargetDAO.flush();

		related_foodtagetype = foodTageTypeDAO.store(related_foodtagetype);
		foodTageTypeDAO.flush();

		foodTageTypeDAO.remove(related_foodtagetype);
		foodTageTypeDAO.flush();

		return nutritiontarget;
	}

	/**
	 * Return a count of all NutritionTarget entity
	 * 
	 */
	@Transactional
	public Integer countNutritionTargets() {
		return ((Long) nutritionTargetDAO.createQuerySingleResult("select count(o) from NutritionTarget o").getSingleResult()).intValue();
	}

	/**
	 * Delete an existing NutritionTarget entity
	 * 
	 */
	@Transactional
	public void deleteNutritionTarget(NutritionTarget nutritiontarget) {
		nutritionTargetDAO.remove(nutritiontarget);
		nutritionTargetDAO.flush();
	}

	/**
	 * Load an existing NutritionTarget entity
	 * 
	 */
	@Transactional
	public Set<NutritionTarget> loadNutritionTargets() {
		return nutritionTargetDAO.findAllNutritionTargets();
	}

	/**
	 */
	@Transactional
	public NutritionTarget findNutritionTargetByPrimaryKey(Integer nutritionTargetId) {
		return nutritionTargetDAO.findNutritionTargetByPrimaryKey(nutritionTargetId);
	}
}
