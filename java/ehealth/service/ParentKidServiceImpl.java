package ehealth.service;

import ehealth.dao.KidDAO;
import ehealth.dao.ParentDAO;
import ehealth.dao.ParentKidDAO;

import ehealth.domain.Kid;
import ehealth.domain.Parent;
import ehealth.domain.ParentKid;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;

import org.springframework.transaction.annotation.Transactional;

/**
 * Spring service that handles CRUD requests for ParentKid entities
 * 
 */

@Service("ParentKidService")

@Transactional
public class ParentKidServiceImpl implements ParentKidService {

	/**
	 * DAO injected by Spring that manages Kid entities
	 * 
	 */
	@Autowired
	private KidDAO kidDAO;

	/**
	 * DAO injected by Spring that manages Parent entities
	 * 
	 */
	@Autowired
	private ParentDAO parentDAO;

	/**
	 * DAO injected by Spring that manages ParentKid entities
	 * 
	 */
	@Autowired
	private ParentKidDAO parentKidDAO;

	/**
	 * Instantiates a new ParentKidServiceImpl.
	 *
	 */
	public ParentKidServiceImpl() {
	}

	/**
	 */
	@Transactional
	public ParentKid findParentKidByPrimaryKey(Integer parentKidId) {
		return parentKidDAO.findParentKidByPrimaryKey(parentKidId);
	}

	/**
	 * Save an existing Parent entity
	 * 
	 */
	@Transactional
	public ParentKid saveParentKidParent(Integer parentKidId, Parent related_parent) {
		ParentKid parentkid = parentKidDAO.findParentKidByPrimaryKey(parentKidId, -1, -1);
		Parent existingparent = parentDAO.findParentByPrimaryKey(related_parent.getParentId());

		// copy into the existing record to preserve existing relationships
		if (existingparent != null) {
			existingparent.setParentId(related_parent.getParentId());
			existingparent.setOccupation(related_parent.getOccupation());
			existingparent.setAddress(related_parent.getAddress());
			related_parent = existingparent;
		}

		parentkid.setParent(related_parent);
		related_parent.getParentKids().add(parentkid);
		parentkid = parentKidDAO.store(parentkid);
		parentKidDAO.flush();

		related_parent = parentDAO.store(related_parent);
		parentDAO.flush();

		return parentkid;
	}

	/**
	 * Return all ParentKid entity
	 * 
	 */
	@Transactional
	public List<ParentKid> findAllParentKids(Integer startResult, Integer maxRows) {
		return new java.util.ArrayList<ParentKid>(parentKidDAO.findAllParentKids(startResult, maxRows));
	}

	/**
	 * Return a count of all ParentKid entity
	 * 
	 */
	@Transactional
	public Integer countParentKids() {
		return ((Long) parentKidDAO.createQuerySingleResult("select count(o) from ParentKid o").getSingleResult()).intValue();
	}

	/**
	 * Delete an existing Parent entity
	 * 
	 */
	@Transactional
	public ParentKid deleteParentKidParent(Integer parentkid_parentKidId, Integer related_parent_parentId) {
		ParentKid parentkid = parentKidDAO.findParentKidByPrimaryKey(parentkid_parentKidId, -1, -1);
		Parent related_parent = parentDAO.findParentByPrimaryKey(related_parent_parentId, -1, -1);

		parentkid.setParent(null);
		related_parent.getParentKids().remove(parentkid);
		parentkid = parentKidDAO.store(parentkid);
		parentKidDAO.flush();

		related_parent = parentDAO.store(related_parent);
		parentDAO.flush();

		parentDAO.remove(related_parent);
		parentDAO.flush();

		return parentkid;
	}

	/**
	 * Save an existing Kid entity
	 * 
	 */
	@Transactional
	public ParentKid saveParentKidKid(Integer parentKidId, Kid related_kid) {
		ParentKid parentkid = parentKidDAO.findParentKidByPrimaryKey(parentKidId, -1, -1);
		Kid existingkid = kidDAO.findKidByPrimaryKey(related_kid.getKidId());

		// copy into the existing record to preserve existing relationships
		if (existingkid != null) {
			existingkid.setKidId(related_kid.getKidId());
			existingkid.setHigh(related_kid.getHigh());
			existingkid.setWeight(related_kid.getWeight());
			existingkid.setAddress(related_kid.getAddress());
			existingkid.setCodeKey(related_kid.getCodeKey());
			related_kid = existingkid;
		}

		parentkid.setKid(related_kid);
		related_kid.getParentKids().add(parentkid);
		parentkid = parentKidDAO.store(parentkid);
		parentKidDAO.flush();

		related_kid = kidDAO.store(related_kid);
		kidDAO.flush();

		return parentkid;
	}

	/**
	 * Delete an existing ParentKid entity
	 * 
	 */
	@Transactional
	public void deleteParentKid(ParentKid parentkid) {
		parentKidDAO.remove(parentkid);
		parentKidDAO.flush();
	}

	/**
	 * Delete an existing Kid entity
	 * 
	 */
	@Transactional
	public ParentKid deleteParentKidKid(Integer parentkid_parentKidId, Integer related_kid_kidId) {
		ParentKid parentkid = parentKidDAO.findParentKidByPrimaryKey(parentkid_parentKidId, -1, -1);
		Kid related_kid = kidDAO.findKidByPrimaryKey(related_kid_kidId, -1, -1);

		parentkid.setKid(null);
		related_kid.getParentKids().remove(parentkid);
		parentkid = parentKidDAO.store(parentkid);
		parentKidDAO.flush();

		related_kid = kidDAO.store(related_kid);
		kidDAO.flush();

		kidDAO.remove(related_kid);
		kidDAO.flush();

		return parentkid;
	}

	/**
	 * Save an existing ParentKid entity
	 * 
	 */
	@Transactional
	public ParentKid saveParentKid(ParentKid parentkid) {
		ParentKid existingParentKid = parentKidDAO.findParentKidByPrimaryKey(parentkid.getParentKidId());

		if (existingParentKid != null) {
			if (existingParentKid != parentkid) {
				existingParentKid.setParentKidId(parentkid.getParentKidId());
			}
			parentkid = parentKidDAO.store(existingParentKid);
		} else {
			parentkid = parentKidDAO.store(parentkid);
		}
		parentKidDAO.flush();
		return parentkid;
	}

	/**
	 * Load an existing ParentKid entity
	 * 
	 */
	@Transactional
	public Set<ParentKid> loadParentKids() {
		return parentKidDAO.findAllParentKids();
	}
}
