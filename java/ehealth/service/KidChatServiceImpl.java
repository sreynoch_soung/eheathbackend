package ehealth.service;

import ehealth.dao.ChatRecordDAO;
import ehealth.dao.KidChatDAO;
import ehealth.dao.KidDAO;

import ehealth.domain.ChatRecord;
import ehealth.domain.Kid;
import ehealth.domain.KidChat;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;

import org.springframework.transaction.annotation.Transactional;

/**
 * Spring service that handles CRUD requests for KidChat entities
 * 
 */

@Service("KidChatService")

@Transactional
public class KidChatServiceImpl implements KidChatService {

	/**
	 * DAO injected by Spring that manages ChatRecord entities
	 * 
	 */
	@Autowired
	private ChatRecordDAO chatRecordDAO;

	/**
	 * DAO injected by Spring that manages KidChat entities
	 * 
	 */
	@Autowired
	private KidChatDAO kidChatDAO;

	/**
	 * DAO injected by Spring that manages Kid entities
	 * 
	 */
	@Autowired
	private KidDAO kidDAO;

	/**
	 * Instantiates a new KidChatServiceImpl.
	 *
	 */
	public KidChatServiceImpl() {
	}

	/**
	 * Save an existing Kid entity
	 * 
	 */
	@Transactional
	public KidChat saveKidChatKidByKidId1(Integer kidChatId, Kid related_kidbykidid1) {
		KidChat kidchat = kidChatDAO.findKidChatByPrimaryKey(kidChatId, -1, -1);
		Kid existingkidByKidId1 = kidDAO.findKidByPrimaryKey(related_kidbykidid1.getKidId());

		// copy into the existing record to preserve existing relationships
		if (existingkidByKidId1 != null) {
			existingkidByKidId1.setKidId(related_kidbykidid1.getKidId());
			existingkidByKidId1.setHigh(related_kidbykidid1.getHigh());
			existingkidByKidId1.setWeight(related_kidbykidid1.getWeight());
			existingkidByKidId1.setAddress(related_kidbykidid1.getAddress());
			existingkidByKidId1.setCodeKey(related_kidbykidid1.getCodeKey());
			related_kidbykidid1 = existingkidByKidId1;
		}

		kidchat.setKidByKidId1(related_kidbykidid1);
		related_kidbykidid1.getKidChatsForKidId1().add(kidchat);
		kidchat = kidChatDAO.store(kidchat);
		kidChatDAO.flush();

		related_kidbykidid1 = kidDAO.store(related_kidbykidid1);
		kidDAO.flush();

		return kidchat;
	}

	/**
	 */
	@Transactional
	public KidChat findKidChatByPrimaryKey(Integer kidChatId) {
		return kidChatDAO.findKidChatByPrimaryKey(kidChatId);
	}

	/**
	 * Load an existing KidChat entity
	 * 
	 */
	@Transactional
	public Set<KidChat> loadKidChats() {
		return kidChatDAO.findAllKidChats();
	}

	/**
	 * Delete an existing KidChat entity
	 * 
	 */
	@Transactional
	public void deleteKidChat(KidChat kidchat) {
		kidChatDAO.remove(kidchat);
		kidChatDAO.flush();
	}

	/**
	 * Delete an existing ChatRecord entity
	 * 
	 */
	@Transactional
	public KidChat deleteKidChatChatRecord(Integer kidchat_kidChatId, Integer related_chatrecord_chatRecordId) {
		KidChat kidchat = kidChatDAO.findKidChatByPrimaryKey(kidchat_kidChatId, -1, -1);
		ChatRecord related_chatrecord = chatRecordDAO.findChatRecordByPrimaryKey(related_chatrecord_chatRecordId, -1, -1);

		kidchat.setChatRecord(null);
		related_chatrecord.getKidChats().remove(kidchat);
		kidchat = kidChatDAO.store(kidchat);
		kidChatDAO.flush();

		related_chatrecord = chatRecordDAO.store(related_chatrecord);
		chatRecordDAO.flush();

		chatRecordDAO.remove(related_chatrecord);
		chatRecordDAO.flush();

		return kidchat;
	}

	/**
	 * Save an existing KidChat entity
	 * 
	 */
	@Transactional
	public KidChat saveKidChat(KidChat kidchat) {
		KidChat existingKidChat = kidChatDAO.findKidChatByPrimaryKey(kidchat.getKidChatId());

		if (existingKidChat != null) {
			if (existingKidChat != kidchat) {
				existingKidChat.setKidChatId(kidchat.getKidChatId());
			}
			kidchat = kidChatDAO.store(existingKidChat);
		} else {
			kidchat = kidChatDAO.store(kidchat);
		}
		kidChatDAO.flush();
		return existingKidChat;
	}

	/**
	 * Delete an existing Kid entity
	 * 
	 */
	@Transactional
	public KidChat deleteKidChatKidByKidId2(Integer kidchat_kidChatId, Integer related_kidbykidid2_kidId) {
		KidChat kidchat = kidChatDAO.findKidChatByPrimaryKey(kidchat_kidChatId, -1, -1);
		Kid related_kidbykidid2 = kidDAO.findKidByPrimaryKey(related_kidbykidid2_kidId, -1, -1);

		kidchat.setKidByKidId2(null);
		related_kidbykidid2.getKidChatsForKidId2().remove(kidchat);
		kidchat = kidChatDAO.store(kidchat);
		kidChatDAO.flush();

		related_kidbykidid2 = kidDAO.store(related_kidbykidid2);
		kidDAO.flush();

		kidDAO.remove(related_kidbykidid2);
		kidDAO.flush();

		return kidchat;
	}

	/**
	 * Return all KidChat entity
	 * 
	 */
	@Transactional
	public List<KidChat> findAllKidChats(Integer startResult, Integer maxRows) {
		return new java.util.ArrayList<KidChat>(kidChatDAO.findAllKidChats(startResult, maxRows));
	}

	/**
	 * Delete an existing Kid entity
	 * 
	 */
	@Transactional
	public KidChat deleteKidChatKidByKidId1(Integer kidchat_kidChatId, Integer related_kidbykidid1_kidId) {
		KidChat kidchat = kidChatDAO.findKidChatByPrimaryKey(kidchat_kidChatId, -1, -1);
		Kid related_kidbykidid1 = kidDAO.findKidByPrimaryKey(related_kidbykidid1_kidId, -1, -1);

		kidchat.setKidByKidId1(null);
		related_kidbykidid1.getKidChatsForKidId1().remove(kidchat);
		kidchat = kidChatDAO.store(kidchat);
		kidChatDAO.flush();

		related_kidbykidid1 = kidDAO.store(related_kidbykidid1);
		kidDAO.flush();

		kidDAO.remove(related_kidbykidid1);
		kidDAO.flush();

		return kidchat;
	}

	/**
	 * Save an existing Kid entity
	 * 
	 */
	@Transactional
	public KidChat saveKidChatKidByKidId2(Integer kidChatId, Kid related_kidbykidid2) {
		KidChat kidchat = kidChatDAO.findKidChatByPrimaryKey(kidChatId, -1, -1);
		Kid existingkidByKidId2 = kidDAO.findKidByPrimaryKey(related_kidbykidid2.getKidId());

		// copy into the existing record to preserve existing relationships
		if (existingkidByKidId2 != null) {
			existingkidByKidId2.setKidId(related_kidbykidid2.getKidId());
			existingkidByKidId2.setHigh(related_kidbykidid2.getHigh());
			existingkidByKidId2.setWeight(related_kidbykidid2.getWeight());
			existingkidByKidId2.setAddress(related_kidbykidid2.getAddress());
			existingkidByKidId2.setCodeKey(related_kidbykidid2.getCodeKey());
			related_kidbykidid2 = existingkidByKidId2;
		}

		kidchat.setKidByKidId2(related_kidbykidid2);
		related_kidbykidid2.getKidChatsForKidId2().add(kidchat);
		kidchat = kidChatDAO.store(kidchat);
		kidChatDAO.flush();

		related_kidbykidid2 = kidDAO.store(related_kidbykidid2);
		kidDAO.flush();

		return kidchat;
	}

	/**
	 * Save an existing ChatRecord entity
	 * 
	 */
	@Transactional
	public KidChat saveKidChatChatRecord(Integer kidChatId, ChatRecord related_chatrecord) {
		KidChat kidchat = kidChatDAO.findKidChatByPrimaryKey(kidChatId, -1, -1);
		ChatRecord existingchatRecord = chatRecordDAO.findChatRecordByPrimaryKey(related_chatrecord.getChatRecordId());

		// copy into the existing record to preserve existing relationships
		if (existingchatRecord != null) {
			existingchatRecord.setChatRecordId(related_chatrecord.getChatRecordId());
			existingchatRecord.setDate(related_chatrecord.getDate());
			existingchatRecord.setMessage(related_chatrecord.getMessage());
			related_chatrecord = existingchatRecord;
		}

		kidchat.setChatRecord(related_chatrecord);
		related_chatrecord.getKidChats().add(kidchat);
		kidchat = kidChatDAO.store(kidchat);
		kidChatDAO.flush();

		related_chatrecord = chatRecordDAO.store(related_chatrecord);
		chatRecordDAO.flush();

		return kidchat;
	}

	/**
	 * Return a count of all KidChat entity
	 * 
	 */
	@Transactional
	public Integer countKidChats() {
		return ((Long) kidChatDAO.createQuerySingleResult("select count(o) from KidChat o").getSingleResult()).intValue();
	}
}
