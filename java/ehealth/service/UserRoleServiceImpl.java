package ehealth.service;

import ehealth.dao.UserDAO;
import ehealth.dao.UserRoleDAO;

import ehealth.domain.User;
import ehealth.domain.UserRole;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;

import org.springframework.transaction.annotation.Transactional;

/**
 * Spring service that handles CRUD requests for UserRole entities
 * 
 */

@Service("UserRoleService")

@Transactional
public class UserRoleServiceImpl implements UserRoleService {

	/**
	 * DAO injected by Spring that manages User entities
	 * 
	 */
	@Autowired
	private UserDAO userDAO;

	/**
	 * DAO injected by Spring that manages UserRole entities
	 * 
	 */
	@Autowired
	private UserRoleDAO userRoleDAO;

	/**
	 * Instantiates a new UserRoleServiceImpl.
	 *
	 */
	public UserRoleServiceImpl() {
	}

	/**
	 * Delete an existing User entity
	 * 
	 */
	@Transactional
	public UserRole deleteUserRoleUsers(Integer userrole_userRoleId, Integer related_users_userId) {
		User related_users = userDAO.findUserByPrimaryKey(related_users_userId, -1, -1);

		UserRole userrole = userRoleDAO.findUserRoleByPrimaryKey(userrole_userRoleId, -1, -1);

		related_users.setUserRole(null);
		userrole.getUsers().remove(related_users);

		userDAO.remove(related_users);
		userDAO.flush();

		return userrole;
	}

	/**
	 */
	@Transactional
	public UserRole findUserRoleByPrimaryKey(Integer userRoleId) {
		return userRoleDAO.findUserRoleByPrimaryKey(userRoleId);
	}

	/**
	 * Load an existing UserRole entity
	 * 
	 */
	@Transactional
	public Set<UserRole> loadUserRoles() {
		return userRoleDAO.findAllUserRoles();
	}

	/**
	 * Delete an existing UserRole entity
	 * 
	 */
	@Transactional
	public void deleteUserRole(UserRole userrole) {
		userRoleDAO.remove(userrole);
		userRoleDAO.flush();
	}

	/**
	 * Save an existing UserRole entity
	 * 
	 */
	@Transactional
	public void saveUserRole(UserRole userrole) {
		UserRole existingUserRole = userRoleDAO.findUserRoleByPrimaryKey(userrole.getUserRoleId());

		if (existingUserRole != null) {
			if (existingUserRole != userrole) {
				existingUserRole.setUserRoleId(userrole.getUserRoleId());
				existingUserRole.setRoleTitle(userrole.getRoleTitle());
			}
			userrole = userRoleDAO.store(existingUserRole);
		} else {
			userrole = userRoleDAO.store(userrole);
		}
		userRoleDAO.flush();
	}

	/**
	 * Return all UserRole entity
	 * 
	 */
	@Transactional
	public List<UserRole> findAllUserRoles(Integer startResult, Integer maxRows) {
		return new java.util.ArrayList<UserRole>(userRoleDAO.findAllUserRoles(startResult, maxRows));
	}

	/**
	 * Save an existing User entity
	 * 
	 */
	@Transactional
	public UserRole saveUserRoleUsers(Integer userRoleId, User related_users) {
		UserRole userrole = userRoleDAO.findUserRoleByPrimaryKey(userRoleId, -1, -1);
		User existingusers = userDAO.findUserByPrimaryKey(related_users.getUserId());

		// copy into the existing record to preserve existing relationships
		if (existingusers != null) {
			existingusers.setUserId(related_users.getUserId());
			existingusers.setUsername(related_users.getUsername());
			existingusers.setName(related_users.getName());
			existingusers.setSurename(related_users.getSurename());
			existingusers.setPassword(related_users.getPassword());
			existingusers.setEmail(related_users.getEmail());
			existingusers.setTelephone(related_users.getTelephone());
			existingusers.setAge(related_users.getAge());
			existingusers.setSex(related_users.getSex());
			existingusers.setEnabled(related_users.getEnabled());
			related_users = existingusers;
		}

		related_users.setUserRole(userrole);
		userrole.getUsers().add(related_users);
		related_users = userDAO.store(related_users);
		userDAO.flush();

		userrole = userRoleDAO.store(userrole);
		userRoleDAO.flush();

		return userrole;
	}

	/**
	 * Return a count of all UserRole entity
	 * 
	 */
	@Transactional
	public Integer countUserRoles() {
		return ((Long) userRoleDAO.createQuerySingleResult("select count(o) from UserRole o").getSingleResult()).intValue();
	}
}
