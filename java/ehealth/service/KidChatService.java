
package ehealth.service;

import ehealth.domain.ChatRecord;
import ehealth.domain.Kid;
import ehealth.domain.KidChat;

import java.util.List;
import java.util.Set;

/**
 * Spring service that handles CRUD requests for KidChat entities
 * 
 */
public interface KidChatService {

	/**
	* Save an existing Kid entity
	* 
	 */
	public KidChat saveKidChatKidByKidId1(Integer kidChatId, Kid related_kidbykidid1);

	/**
	 */
	public KidChat findKidChatByPrimaryKey(Integer kidChatId_1);

	/**
	* Load an existing KidChat entity
	* 
	 */
	public Set<KidChat> loadKidChats();

	/**
	* Delete an existing KidChat entity
	* 
	 */
	public void deleteKidChat(KidChat kidchat);

	/**
	* Delete an existing ChatRecord entity
	* 
	 */
	public KidChat deleteKidChatChatRecord(Integer kidchat_kidChatId, Integer related_chatrecord_chatRecordId);

	/**
	* Save an existing KidChat entity
	* 
	 */
	public KidChat saveKidChat(KidChat kidchat_1);

	/**
	* Delete an existing Kid entity
	* 
	 */
	public KidChat deleteKidChatKidByKidId2(Integer kidchat_kidChatId_1, Integer related_kidbykidid2_kidId);

	/**
	* Return all KidChat entity
	* 
	 */
	public List<KidChat> findAllKidChats(Integer startResult, Integer maxRows);

	/**
	* Delete an existing Kid entity
	* 
	 */
	public KidChat deleteKidChatKidByKidId1(Integer kidchat_kidChatId_2, Integer related_kidbykidid1_kidId);

	/**
	* Save an existing Kid entity
	* 
	 */
	public KidChat saveKidChatKidByKidId2(Integer kidChatId_2, Kid related_kidbykidid2);

	/**
	* Save an existing ChatRecord entity
	* 
	 */
	public KidChat saveKidChatChatRecord(Integer kidChatId_3, ChatRecord related_chatrecord);

	/**
	* Return a count of all KidChat entity
	* 
	 */
	public Integer countKidChats();
}