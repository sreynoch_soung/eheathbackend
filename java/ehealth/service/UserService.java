
package ehealth.service;

import java.util.List;
import java.util.Set;

import ehealth.domain.Doctor;
import ehealth.domain.FoodValidation;
import ehealth.domain.Kid;
import ehealth.domain.Parent;
import ehealth.domain.Timestamp;
import ehealth.domain.User;
import ehealth.domain.UserRole;

/**
 * Spring service that handles CRUD requests for User entities
 * 
 */
public interface UserService {

	/**
	* Load an existing User entity
	* 
	 */
	public Set<User> loadUsers();

	/**
	* Delete an existing Parent entity
	* 
	 */
	public User deleteUserParents(Integer user_userId, Integer related_parents_parentId);

	/**
	* Save an existing Kid entity
	* 
	 */
	public User saveUserKids(Integer userId, Kid related_kids);

	/**
	* Save an existing User entity
	* 
	 */
	public User saveUser(User user);

	/**
	* Delete an existing User entity
	* 
	 */
	public void deleteUser(User user_1);

	/**
	* Return all User entity
	* 
	 */
	public List<User> findAllUsers(Integer startResult, Integer maxRows);

	/**
	* Save an existing UserRole entity
	* 
	 */
	public User saveUserUserRole(Integer userId_1, UserRole related_userrole);

	/**
	* Delete an existing Timestamp entity
	* 
	 */
	public User deleteUserTimestamp(Integer user_userId_1, Integer related_timestamp_timestampId);

	/**
	* Save an existing FoodValidation entity
	* 
	 */
	public User saveUserFoodValidations(Integer userId_2, FoodValidation related_foodvalidations);

	/**
	* Delete an existing UserRole entity
	* 
	 */
	public User deleteUserUserRole(Integer user_userId_2, Integer related_userrole_userRoleId);

	/**
	* Save an existing Doctor entity
	* 
	 */
	public User saveUserDoctors(Integer userId_3, Doctor related_doctors);

	/**
	* Delete an existing Kid entity
	* 
	 */
	public User deleteUserKids(Integer user_userId_3, Integer related_kids_kidId);

	/**
	* Delete an existing FoodValidation entity
	* 
	 */
	public User deleteUserFoodValidations(Integer user_userId_4, Integer related_foodvalidations_foodValidationId);

	/**
	* Save an existing Timestamp entity
	* 
	 */
	public User saveUserTimestamp(Integer userId_4, Timestamp related_timestamp);

	/**
	* Save an existing Parent entity
	* 
	 */
	public User saveUserParents(Integer userId_5, Parent related_parents);

	/**
	* Return a count of all User entity
	* 
	 */
	public Integer countUsers();

	/**
	 */
	public User findUserByPrimaryKey(Integer userId_6);

	/**
	* Delete an existing Doctor entity
	* 
	 */
	public User deleteUserDoctors(Integer user_userId_5, Integer related_doctors_doctorId);
}