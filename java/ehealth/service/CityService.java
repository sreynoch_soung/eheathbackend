
package ehealth.service;

import ehealth.domain.City;
import ehealth.domain.Country;
import ehealth.domain.Kid;
import ehealth.domain.Parent;

import java.util.List;
import java.util.Set;

/**
 * Spring service that handles CRUD requests for City entities
 * 
 */
public interface CityService {

	/**
	 */
	public City findCityByPrimaryKey(Integer cityId);

	/**
	* Delete an existing Parent entity
	* 
	 */
	public City deleteCityParents(Integer city_cityId, Integer related_parents_parentId);

	/**
	* Return a count of all City entity
	* 
	 */
	public Integer countCitys();

	/**
	* Save an existing Country entity
	* 
	 */
	public City saveCityCountry(Integer cityId_1, Country related_country);

	/**
	* Delete an existing Country entity
	* 
	 */
	public City deleteCityCountry(Integer city_cityId_1, Integer related_country_countryId);

	/**
	* Delete an existing City entity
	* 
	 */
	public void deleteCity(City city);

	/**
	* Delete an existing Kid entity
	* 
	 */
	public City deleteCityKids(Integer city_cityId_2, Integer related_kids_kidId);

	/**
	* Save an existing Parent entity
	* 
	 */
	public City saveCityParents(Integer cityId_2, Parent related_parents);

	/**
	* Load an existing City entity
	* 
	 */
	public Set<City> loadCitys();

	/**
	* Return all City entity
	* 
	 */
	public List<City> findAllCitys(Integer startResult, Integer maxRows);

	/**
	* Save an existing Kid entity
	* 
	 */
	public City saveCityKids(Integer cityId_3, Kid related_kids);

	/**
	* Save an existing City entity
	* 
	 */
	public City saveCity(City city_1);
}