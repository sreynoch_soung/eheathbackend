
package ehealth.service;

import ehealth.domain.Doctor;
import ehealth.domain.ExerciseTarget;
import ehealth.domain.Kid;

import java.util.List;
import java.util.Set;

/**
 * Spring service that handles CRUD requests for ExerciseTarget entities
 * 
 */
public interface ExerciseTargetService {

	/**
	* Delete an existing Kid entity
	* 
	 */
	public ExerciseTarget deleteExerciseTargetKid(Integer exercisetarget_exerciseTargetId, Integer related_kid_kidId);

	/**
	* Delete an existing Doctor entity
	* 
	 */
	public ExerciseTarget deleteExerciseTargetDoctor(Integer exercisetarget_exerciseTargetId_1, Integer related_doctor_doctorId);

	/**
	* Return all ExerciseTarget entity
	* 
	 */
	public List<ExerciseTarget> findAllExerciseTargets(Integer startResult, Integer maxRows);

	/**
	* Save an existing ExerciseTarget entity
	* 
	 */
	public ExerciseTarget saveExerciseTarget(ExerciseTarget exercisetarget);

	/**
	* Load an existing ExerciseTarget entity
	* 
	 */
	public Set<ExerciseTarget> loadExerciseTargets();

	/**
	* Delete an existing ExerciseTarget entity
	* 
	 */
	public void deleteExerciseTarget(ExerciseTarget exercisetarget_1);

	/**
	* Save an existing Kid entity
	* 
	 */
	public ExerciseTarget saveExerciseTargetKid(Integer exerciseTargetId, Kid related_kid);

	/**
	* Return a count of all ExerciseTarget entity
	* 
	 */
	public Integer countExerciseTargets();

	/**
	 */
	public ExerciseTarget findExerciseTargetByPrimaryKey(Integer exerciseTargetId_1);

	/**
	* Save an existing Doctor entity
	* 
	 */
	public ExerciseTarget saveExerciseTargetDoctor(Integer exerciseTargetId_2, Doctor related_doctor);
}