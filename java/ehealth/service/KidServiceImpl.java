package ehealth.service;

import ehealth.dao.AccountSettingDAO;
import ehealth.dao.BonusDAO;
import ehealth.dao.CityDAO;
import ehealth.dao.DoctorDAO;
import ehealth.dao.ExerciseInputDAO;
import ehealth.dao.ExerciseTargetDAO;
import ehealth.dao.GradDAO;
import ehealth.dao.KidChatDAO;
import ehealth.dao.KidDAO;
import ehealth.dao.KidMeasuresDAO;
import ehealth.dao.MealDAO;
import ehealth.dao.NutritionTargetDAO;
import ehealth.dao.ParentKidDAO;
import ehealth.dao.TeamDAO;
import ehealth.dao.UserDAO;

import ehealth.domain.AccountSetting;
import ehealth.domain.Bonus;
import ehealth.domain.City;
import ehealth.domain.Doctor;
import ehealth.domain.ExerciseInput;
import ehealth.domain.ExerciseTarget;
import ehealth.domain.Grad;
import ehealth.domain.Kid;
import ehealth.domain.KidChat;
import ehealth.domain.KidMeasures;
import ehealth.domain.Meal;
import ehealth.domain.NutritionTarget;
import ehealth.domain.ParentKid;
import ehealth.domain.Team;
import ehealth.domain.User;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;

import org.springframework.transaction.annotation.Transactional;

/**
 * Spring service that handles CRUD requests for Kid entities
 * 
 */

@Service("KidService")

@Transactional
public class KidServiceImpl implements KidService {

	/**
	 * DAO injected by Spring that manages AccountSetting entities
	 * 
	 */
	@Autowired
	private AccountSettingDAO accountSettingDAO;

	/**
	 * DAO injected by Spring that manages Bonus entities
	 * 
	 */
	@Autowired
	private BonusDAO bonusDAO;

	/**
	 * DAO injected by Spring that manages City entities
	 * 
	 */
	@Autowired
	private CityDAO cityDAO;

	/**
	 * DAO injected by Spring that manages Doctor entities
	 * 
	 */
	@Autowired
	private DoctorDAO doctorDAO;

	/**
	 * DAO injected by Spring that manages ExerciseInput entities
	 * 
	 */
	@Autowired
	private ExerciseInputDAO exerciseInputDAO;

	/**
	 * DAO injected by Spring that manages ExerciseTarget entities
	 * 
	 */
	@Autowired
	private ExerciseTargetDAO exerciseTargetDAO;

	/**
	 * DAO injected by Spring that manages Grad entities
	 * 
	 */
	@Autowired
	private GradDAO gradDAO;

	/**
	 * DAO injected by Spring that manages KidChat entities
	 * 
	 */
	@Autowired
	private KidChatDAO kidChatDAO;

	/**
	 * DAO injected by Spring that manages Kid entities
	 * 
	 */
	@Autowired
	private KidDAO kidDAO;

	/**
	 * DAO injected by Spring that manages KidMeasures entities
	 * 
	 */
	@Autowired
	private KidMeasuresDAO kidMeasuresDAO;

	/**
	 * DAO injected by Spring that manages Meal entities
	 * 
	 */
	@Autowired
	private MealDAO mealDAO;

	/**
	 * DAO injected by Spring that manages NutritionTarget entities
	 * 
	 */
	@Autowired
	private NutritionTargetDAO nutritionTargetDAO;

	/**
	 * DAO injected by Spring that manages ParentKid entities
	 * 
	 */
	@Autowired
	private ParentKidDAO parentKidDAO;

	/**
	 * DAO injected by Spring that manages Team entities
	 * 
	 */
	@Autowired
	private TeamDAO teamDAO;

	/**
	 * DAO injected by Spring that manages User entities
	 * 
	 */
	@Autowired
	private UserDAO userDAO;

	/**
	 * Instantiates a new KidServiceImpl.
	 *
	 */
	public KidServiceImpl() {
	}

	/**
	 * Save an existing NutritionTarget entity
	 * 
	 */
	@Transactional
	public Kid saveKidNutritionTargets(Integer kidId, NutritionTarget related_nutritiontargets) {
		Kid kid = kidDAO.findKidByPrimaryKey(kidId, -1, -1);
		NutritionTarget existingnutritionTargets = nutritionTargetDAO.findNutritionTargetByPrimaryKey(related_nutritiontargets.getNutritionTargetId());

		// copy into the existing record to preserve existing relationships
		if (existingnutritionTargets != null) {
			existingnutritionTargets.setNutritionTargetId(related_nutritiontargets.getNutritionTargetId());
			existingnutritionTargets.setDate(related_nutritiontargets.getDate());
			related_nutritiontargets = existingnutritionTargets;
		}

		related_nutritiontargets.setKid(kid);
		kid.getNutritionTargets().add(related_nutritiontargets);
		related_nutritiontargets = nutritionTargetDAO.store(related_nutritiontargets);
		nutritionTargetDAO.flush();

		kid = kidDAO.store(kid);
		kidDAO.flush();

		return kid;
	}

	/**
	 * Save an existing Meal entity
	 * 
	 */
	@Transactional
	public Kid saveKidMeals(Integer kidId, Meal related_meals) {
		Kid kid = kidDAO.findKidByPrimaryKey(kidId, -1, -1);
		Meal existingmeals = mealDAO.findMealByPrimaryKey(related_meals.getMealId());

		// copy into the existing record to preserve existing relationships
		if (existingmeals != null) {
			existingmeals.setMealId(related_meals.getMealId());
			existingmeals.setDate(related_meals.getDate());
			related_meals = existingmeals;
		}

		related_meals.setKid(kid);
		kid.getMeals().add(related_meals);
		related_meals = mealDAO.store(related_meals);
		mealDAO.flush();

		kid = kidDAO.store(kid);
		kidDAO.flush();

		return kid;
	}

	/**
	 * Delete an existing KidChat entity
	 * 
	 */
	@Transactional
	public Kid deleteKidKidChatsForKidId2(Integer kid_kidId, Integer related_kidchatsforkidid2_kidChatId) {
		KidChat related_kidchatsforkidid2 = kidChatDAO.findKidChatByPrimaryKey(related_kidchatsforkidid2_kidChatId, -1, -1);

		Kid kid = kidDAO.findKidByPrimaryKey(kid_kidId, -1, -1);

		related_kidchatsforkidid2.setKidByKidId2(null);
		kid.getKidChatsForKidId2().remove(related_kidchatsforkidid2);

		kidChatDAO.remove(related_kidchatsforkidid2);
		kidChatDAO.flush();

		return kid;
	}

	/**
	 * Delete an existing Grad entity
	 * 
	 */
	@Transactional
	public Kid deleteKidGrads(Integer kid_kidId, Integer related_grads_gradId) {
		Grad related_grads = gradDAO.findGradByPrimaryKey(related_grads_gradId, -1, -1);

		Kid kid = kidDAO.findKidByPrimaryKey(kid_kidId, -1, -1);

		related_grads.setKid(null);
		kid.getGrads().remove(related_grads);

		gradDAO.remove(related_grads);
		gradDAO.flush();

		return kid;
	}

	/**
	 * Delete an existing AccountSetting entity
	 * 
	 */
	@Transactional
	public Kid deleteKidAccountSetting(Integer kid_kidId, Integer related_accountsetting_accountSettingId) {
		Kid kid = kidDAO.findKidByPrimaryKey(kid_kidId, -1, -1);
		AccountSetting related_accountsetting = accountSettingDAO.findAccountSettingByPrimaryKey(related_accountsetting_accountSettingId, -1, -1);

		kid.setAccountSetting(null);
		related_accountsetting.getKids().remove(kid);
		kid = kidDAO.store(kid);
		kidDAO.flush();

		related_accountsetting = accountSettingDAO.store(related_accountsetting);
		accountSettingDAO.flush();

		accountSettingDAO.remove(related_accountsetting);
		accountSettingDAO.flush();

		return kid;
	}

	/**
	 * Delete an existing ExerciseTarget entity
	 * 
	 */
	@Transactional
	public Kid deleteKidExerciseTargets(Integer kid_kidId, Integer related_exercisetargets_exerciseTargetId) {
		ExerciseTarget related_exercisetargets = exerciseTargetDAO.findExerciseTargetByPrimaryKey(related_exercisetargets_exerciseTargetId, -1, -1);

		Kid kid = kidDAO.findKidByPrimaryKey(kid_kidId, -1, -1);

		related_exercisetargets.setKid(null);
		kid.getExerciseTargets().remove(related_exercisetargets);

		exerciseTargetDAO.remove(related_exercisetargets);
		exerciseTargetDAO.flush();

		return kid;
	}

	/**
	 * Delete an existing ParentKid entity
	 * 
	 */
	@Transactional
	public Kid deleteKidParentKids(Integer kid_kidId, Integer related_parentkids_parentKidId) {
		ParentKid related_parentkids = parentKidDAO.findParentKidByPrimaryKey(related_parentkids_parentKidId, -1, -1);

		Kid kid = kidDAO.findKidByPrimaryKey(kid_kidId, -1, -1);

		related_parentkids.setKid(null);
		kid.getParentKids().remove(related_parentkids);

		parentKidDAO.remove(related_parentkids);
		parentKidDAO.flush();

		return kid;
	}

	/**
	 * Save an existing ExerciseTarget entity
	 * 
	 */
	@Transactional
	public Kid saveKidExerciseTargets(Integer kidId, ExerciseTarget related_exercisetargets) {
		Kid kid = kidDAO.findKidByPrimaryKey(kidId, -1, -1);
		ExerciseTarget existingexerciseTargets = exerciseTargetDAO.findExerciseTargetByPrimaryKey(related_exercisetargets.getExerciseTargetId());

		// copy into the existing record to preserve existing relationships
		if (existingexerciseTargets != null) {
			existingexerciseTargets.setExerciseTargetId(related_exercisetargets.getExerciseTargetId());
			existingexerciseTargets.setValue(related_exercisetargets.getValue());
			existingexerciseTargets.setDate(related_exercisetargets.getDate());
			related_exercisetargets = existingexerciseTargets;
		}

		related_exercisetargets.setKid(kid);
		kid.getExerciseTargets().add(related_exercisetargets);
		related_exercisetargets = exerciseTargetDAO.store(related_exercisetargets);
		exerciseTargetDAO.flush();

		kid = kidDAO.store(kid);
		kidDAO.flush();

		return kid;
	}

	/**
	 * Delete an existing Bonus entity
	 * 
	 */
	@Transactional
	public Kid deleteKidBonuses(Integer kid_kidId, Integer related_bonuses_bonusId) {
		Bonus related_bonuses = bonusDAO.findBonusByPrimaryKey(related_bonuses_bonusId, -1, -1);

		Kid kid = kidDAO.findKidByPrimaryKey(kid_kidId, -1, -1);

		related_bonuses.setKid(null);
		kid.getBonuses().remove(related_bonuses);

		bonusDAO.remove(related_bonuses);
		bonusDAO.flush();

		return kid;
	}

	/**
	 * Save an existing User entity
	 * 
	 */
	@Transactional
	public Kid saveKidUser(Integer kidId, User related_user) {
		Kid kid = kidDAO.findKidByPrimaryKey(kidId, -1, -1);
		User existinguser = userDAO.findUserByPrimaryKey(related_user.getUserId());

		// copy into the existing record to preserve existing relationships
		if (existinguser != null) {
			existinguser.setUserId(related_user.getUserId());
			existinguser.setUsername(related_user.getUsername());
			existinguser.setName(related_user.getName());
			existinguser.setSurename(related_user.getSurename());
			existinguser.setPassword(related_user.getPassword());
			existinguser.setEmail(related_user.getEmail());
			existinguser.setTelephone(related_user.getTelephone());
			existinguser.setAge(related_user.getAge());
			existinguser.setSex(related_user.getSex());
			related_user = existinguser;
		}

		kid.setUser(related_user);
		related_user.getKids().add(kid);
		kid = kidDAO.store(kid);
		kidDAO.flush();

		related_user = userDAO.store(related_user);
		userDAO.flush();

		return kid;
	}

	/**
	 * Delete an existing NutritionTarget entity
	 * 
	 */
	@Transactional
	public Kid deleteKidNutritionTargets(Integer kid_kidId, Integer related_nutritiontargets_nutritionTargetId) {
		NutritionTarget related_nutritiontargets = nutritionTargetDAO.findNutritionTargetByPrimaryKey(related_nutritiontargets_nutritionTargetId, -1, -1);

		Kid kid = kidDAO.findKidByPrimaryKey(kid_kidId, -1, -1);

		related_nutritiontargets.setKid(null);
		kid.getNutritionTargets().remove(related_nutritiontargets);

		nutritionTargetDAO.remove(related_nutritiontargets);
		nutritionTargetDAO.flush();

		return kid;
	}

	/**
	 * Delete an existing Meal entity
	 * 
	 */
	@Transactional
	public Kid deleteKidMeals(Integer kid_kidId, Integer related_meals_mealId) {
		Meal related_meals = mealDAO.findMealByPrimaryKey(related_meals_mealId, -1, -1);

		Kid kid = kidDAO.findKidByPrimaryKey(kid_kidId, -1, -1);

		related_meals.setKid(null);
		kid.getMeals().remove(related_meals);

		mealDAO.remove(related_meals);
		mealDAO.flush();

		return kid;
	}

	/**
	 * Delete an existing Doctor entity
	 * 
	 */
	@Transactional
	public Kid deleteKidDoctor(Integer kid_kidId, Integer related_doctor_doctorId) {
		Kid kid = kidDAO.findKidByPrimaryKey(kid_kidId, -1, -1);
		Doctor related_doctor = doctorDAO.findDoctorByPrimaryKey(related_doctor_doctorId, -1, -1);

		kid.setDoctor(null);
		related_doctor.getKids().remove(kid);
		kid = kidDAO.store(kid);
		kidDAO.flush();

		related_doctor = doctorDAO.store(related_doctor);
		doctorDAO.flush();

		doctorDAO.remove(related_doctor);
		doctorDAO.flush();

		return kid;
	}

	/**
	 * Return all Kid entity
	 * 
	 */
	@Transactional
	public List<Kid> findAllKids(Integer startResult, Integer maxRows) {
		return new java.util.ArrayList<Kid>(kidDAO.findAllKids(startResult, maxRows));
	}

	/**
	 * Save an existing AccountSetting entity
	 * 
	 */
	@Transactional
	public Kid saveKidAccountSetting(Integer kidId, AccountSetting related_accountsetting) {
		Kid kid = kidDAO.findKidByPrimaryKey(kidId, -1, -1);
		AccountSetting existingaccountSetting = accountSettingDAO.findAccountSettingByPrimaryKey(related_accountsetting.getAccountSettingId());

		// copy into the existing record to preserve existing relationships
		if (existingaccountSetting != null) {
			existingaccountSetting.setAccountSettingId(related_accountsetting.getAccountSettingId());
			existingaccountSetting.setNumPhotoValidateGroup(related_accountsetting.getNumPhotoValidateGroup());
			related_accountsetting = existingaccountSetting;
		} else {
			related_accountsetting = accountSettingDAO.store(related_accountsetting);
			accountSettingDAO.flush();
		}

		kid.setAccountSetting(related_accountsetting);
		related_accountsetting.getKids().add(kid);
		kid = kidDAO.store(kid);
		kidDAO.flush();

		related_accountsetting = accountSettingDAO.store(related_accountsetting);
		accountSettingDAO.flush();

		return kid;
	}

	/**
	 * Delete an existing KidChat entity
	 * 
	 */
	@Transactional
	public Kid deleteKidKidChatsForKidId1(Integer kid_kidId, Integer related_kidchatsforkidid1_kidChatId) {
		KidChat related_kidchatsforkidid1 = kidChatDAO.findKidChatByPrimaryKey(related_kidchatsforkidid1_kidChatId, -1, -1);

		Kid kid = kidDAO.findKidByPrimaryKey(kid_kidId, -1, -1);

		related_kidchatsforkidid1.setKidByKidId2(null);
		kid.getKidChatsForKidId2().remove(related_kidchatsforkidid1);

		kidChatDAO.remove(related_kidchatsforkidid1);
		kidChatDAO.flush();

		return kid;
	}

	/**
	 * Save an existing ExerciseInput entity
	 * 
	 */
	@Transactional
	public Kid saveKidExerciseInputs(Integer kidId, ExerciseInput related_exerciseinputs) {
		Kid kid = kidDAO.findKidByPrimaryKey(kidId, -1, -1);
		ExerciseInput existingexerciseInputs = exerciseInputDAO.findExerciseInputByPrimaryKey(related_exerciseinputs.getExerciseInputId());

		// copy into the existing record to preserve existing relationships
		if (existingexerciseInputs != null) {
			existingexerciseInputs.setExerciseInputId(related_exerciseinputs.getExerciseInputId());
			existingexerciseInputs.setDate(related_exerciseinputs.getDate());
			existingexerciseInputs.setValue(related_exerciseinputs.getValue());
			existingexerciseInputs.setFinalValidationValue(related_exerciseinputs.getFinalValidationValue());
			related_exerciseinputs = existingexerciseInputs;
		}

		related_exerciseinputs.setKid(kid);
		kid.getExerciseInputs().add(related_exerciseinputs);
		related_exerciseinputs = exerciseInputDAO.store(related_exerciseinputs);
		exerciseInputDAO.flush();

		kid = kidDAO.store(kid);
		kidDAO.flush();

		return kid;
	}

	/**
	 * Load an existing Kid entity
	 * 
	 */
	@Transactional
	public Set<Kid> loadKids() {
		return kidDAO.findAllKids();
	}

	/**
	 * Save an existing KidChat entity
	 * 
	 */
	@Transactional
	public Kid saveKidKidChatsForKidId2(Integer kidId, KidChat related_kidchatsforkidid2) {
		Kid kid = kidDAO.findKidByPrimaryKey(kidId, -1, -1);
		KidChat existingkidChatsForKidId2 = kidChatDAO.findKidChatByPrimaryKey(related_kidchatsforkidid2.getKidChatId());

		// copy into the existing record to preserve existing relationships
		if (existingkidChatsForKidId2 != null) {
			existingkidChatsForKidId2.setKidChatId(related_kidchatsforkidid2.getKidChatId());
			related_kidchatsforkidid2 = existingkidChatsForKidId2;
		}

		related_kidchatsforkidid2.setKidByKidId2(kid);
		kid.getKidChatsForKidId2().add(related_kidchatsforkidid2);
		related_kidchatsforkidid2 = kidChatDAO.store(related_kidchatsforkidid2);
		kidChatDAO.flush();

		kid = kidDAO.store(kid);
		kidDAO.flush();

		return kid;
	}

	/**
	 * Save an existing Bonus entity
	 * 
	 */
	@Transactional
	public Kid saveKidBonuses(Integer kidId, Bonus related_bonuses) {
		Kid kid = kidDAO.findKidByPrimaryKey(kidId, -1, -1);
		Bonus existingbonuses = bonusDAO.findBonusByPrimaryKey(related_bonuses.getBonusId());

		// copy into the existing record to preserve existing relationships
		if (existingbonuses != null) {
			existingbonuses.setBonusId(related_bonuses.getBonusId());
			existingbonuses.setPoint(related_bonuses.getPoint());
			existingbonuses.setActivityType(related_bonuses.getActivityType());
			related_bonuses = existingbonuses;
		}

		related_bonuses.setKid(kid);
		kid.getBonuses().add(related_bonuses);
		related_bonuses = bonusDAO.store(related_bonuses);
		bonusDAO.flush();

		kid = kidDAO.store(kid);
		kidDAO.flush();

		return kid;
	}

	/**
	 * Return a count of all Kid entity
	 * 
	 */
	@Transactional
	public Integer countKids() {
		return ((Long) kidDAO.createQuerySingleResult("select count(o) from Kid o").getSingleResult()).intValue();
	}

	/**
	 * Save an existing Team entity
	 * 
	 */
	@Transactional
	public Kid saveKidTeam(Integer kidId, Team related_team) {
		Kid kid = kidDAO.findKidByPrimaryKey(kidId, -1, -1);
		Team existingteam = teamDAO.findTeamByPrimaryKey(related_team.getTeamId());

		// copy into the existing record to preserve existing relationships
		if (existingteam != null) {
			existingteam.setTeamId(related_team.getTeamId());
			existingteam.setName(related_team.getName());
			existingteam.setNumMember(related_team.getNumMember());
			related_team = existingteam;
		} else {
			related_team = teamDAO.store(related_team);
			teamDAO.flush();
		}

		kid.setTeam(related_team);
		related_team.getKids().add(kid);
		kid = kidDAO.store(kid);
		kidDAO.flush();

		related_team = teamDAO.store(related_team);
		teamDAO.flush();

		return kid;
	}

	/**
	 * Find kid by Primary key
	 */
	@Transactional
	public Kid findKidByPrimaryKey(Integer kidId) {
		return kidDAO.findKidByPrimaryKey(kidId);
	}
	
	/**
	 * Find kid by CodeKey
	 */
	@Transactional
	public Set<Kid> findKidByCodeKey(String codeKey) {
		return kidDAO.findKidByCodeKey(codeKey);
	}

	/**
	 * Delete an existing Kid entity
	 * 
	 */
	@Transactional
	public void deleteKid(Kid kid) {
		kidDAO.remove(kid);
		kidDAO.flush();
	}

	/**
	 * Save an existing Kid entity
	 * 
	 */
	@Transactional
	public Kid saveKid(Kid kid) {
		Kid existingKid = kidDAO.findKidByPrimaryKey(kid.getKidId());

		if (existingKid != null) {
			if (existingKid != kid) {
				existingKid.setKidId(kid.getKidId());
				existingKid.setHigh(kid.getHigh());
				existingKid.setWeight(kid.getWeight());
				existingKid.setAddress(kid.getAddress());
				existingKid.setCodeKey(kid.getCodeKey());
			}
			kid = kidDAO.store(existingKid);
		} else {
			kid = kidDAO.store(kid);
		}
		kidDAO.flush();
		return kid;
	}

	/**
	 * Save an existing KidChat entity
	 * 
	 */
	@Transactional
	public Kid saveKidKidChatsForKidId1(Integer kidId, KidChat related_kidchatsforkidid1) {
		Kid kid = kidDAO.findKidByPrimaryKey(kidId, -1, -1);
		KidChat existingkidChatsForKidId1 = kidChatDAO.findKidChatByPrimaryKey(related_kidchatsforkidid1.getKidChatId());

		// copy into the existing record to preserve existing relationships
		if (existingkidChatsForKidId1 != null) {
			existingkidChatsForKidId1.setKidChatId(related_kidchatsforkidid1.getKidChatId());
			related_kidchatsforkidid1 = existingkidChatsForKidId1;
		}

		related_kidchatsforkidid1.setKidByKidId2(kid);
		kid.getKidChatsForKidId2().add(related_kidchatsforkidid1);
		related_kidchatsforkidid1 = kidChatDAO.store(related_kidchatsforkidid1);
		kidChatDAO.flush();

		kid = kidDAO.store(kid);
		kidDAO.flush();

		return kid;
	}

	/**
	 * Delete an existing ExerciseInput entity
	 * 
	 */
	@Transactional
	public Kid deleteKidExerciseInputs(Integer kid_kidId, Integer related_exerciseinputs_exerciseInputId) {
		ExerciseInput related_exerciseinputs = exerciseInputDAO.findExerciseInputByPrimaryKey(related_exerciseinputs_exerciseInputId, -1, -1);

		Kid kid = kidDAO.findKidByPrimaryKey(kid_kidId, -1, -1);

		related_exerciseinputs.setKid(null);
		kid.getExerciseInputs().remove(related_exerciseinputs);

		exerciseInputDAO.remove(related_exerciseinputs);
		exerciseInputDAO.flush();

		return kid;
	}

	/**
	 * Save an existing Doctor entity
	 * 
	 */
	@Transactional
	public Kid saveKidDoctor(Integer kidId, Doctor related_doctor) {
		Kid kid = kidDAO.findKidByPrimaryKey(kidId, -1, -1);
		Doctor existingdoctor = doctorDAO.findDoctorByPrimaryKey(related_doctor.getDoctorId());

		// copy into the existing record to preserve existing relationships
		if (existingdoctor != null) {
			existingdoctor.setDoctorId(related_doctor.getDoctorId());
			existingdoctor.setHospital(related_doctor.getHospital());
			existingdoctor.setCodeKey(related_doctor.getCodeKey());
			related_doctor = existingdoctor;
		} else {
			related_doctor = doctorDAO.store(related_doctor);
			doctorDAO.flush();
		}

		kid.setDoctor(related_doctor);
		related_doctor.getKids().add(kid);
		kid = kidDAO.store(kid);
		kidDAO.flush();

		related_doctor = doctorDAO.store(related_doctor);
		doctorDAO.flush();

		return kid;
	}

	/**
	 * Save an existing City entity
	 * 
	 */
	@Transactional
	public Kid saveKidCity(Integer kidId, City related_city) {
		Kid kid = kidDAO.findKidByPrimaryKey(kidId, -1, -1);
		City existingcity = cityDAO.findCityByPrimaryKey(related_city.getCityId());

		// copy into the existing record to preserve existing relationships
		if (existingcity != null) {
			existingcity.setCityId(related_city.getCityId());
			existingcity.setName(related_city.getName());
			related_city = existingcity;
		}

		kid.setCity(related_city);
		related_city.getKids().add(kid);
		kid = kidDAO.store(kid);
		kidDAO.flush();

		related_city = cityDAO.store(related_city);
		cityDAO.flush();

		return kid;
	}

	/**
	 * Save an existing Grad entity
	 * 
	 */
	@Transactional
	public Kid saveKidGrads(Integer kidId, Grad related_grads) {
		Kid kid = kidDAO.findKidByPrimaryKey(kidId, -1, -1);
		Grad existinggrads = gradDAO.findGradByPrimaryKey(related_grads.getGradId());

		// copy into the existing record to preserve existing relationships
		if (existinggrads != null) {
			existinggrads.setGradId(related_grads.getGradId());
			existinggrads.setValue(related_grads.getValue());
			existinggrads.setDate(related_grads.getDate());
			related_grads = existinggrads;
		}

		related_grads.setKid(kid);
		kid.getGrads().add(related_grads);
		related_grads = gradDAO.store(related_grads);
		gradDAO.flush();

		kid = kidDAO.store(kid);
		kidDAO.flush();

		return kid;
	}

	/**
	 * Save an existing KidMeasures entity
	 * 
	 */
	@Transactional
	public Kid saveKidKidMeasures(Integer kidId, KidMeasures related_kidmeasures) {
		Kid kid = kidDAO.findKidByPrimaryKey(kidId, -1, -1);
		KidMeasures existingkidMeasures = kidMeasuresDAO.findKidMeasuresByPrimaryKey(related_kidmeasures.getKidMeasuresId());

		// copy into the existing record to preserve existing relationships
		if (existingkidMeasures != null) {
			existingkidMeasures.setKidMeasuresId(related_kidmeasures.getKidMeasuresId());
			existingkidMeasures.setGlucoseLevel(related_kidmeasures.getGlucoseLevel());
			existingkidMeasures.setBloodSugarLevel(related_kidmeasures.getBloodSugarLevel());
			existingkidMeasures.setHabit(related_kidmeasures.getHabit());
			existingkidMeasures.setMets(related_kidmeasures.getMets());
			related_kidmeasures = existingkidMeasures;
		} else {
			related_kidmeasures = kidMeasuresDAO.store(related_kidmeasures);
			kidMeasuresDAO.flush();
		}

		kid.setKidMeasures(related_kidmeasures);
		related_kidmeasures.getKids().add(kid);
		kid = kidDAO.store(kid);
		kidDAO.flush();

		related_kidmeasures = kidMeasuresDAO.store(related_kidmeasures);
		kidMeasuresDAO.flush();

		return kid;
	}

	/**
	 * Save an existing ParentKid entity
	 * 
	 */
	@Transactional
	public Kid saveKidParentKids(Integer kidId, ParentKid related_parentkids) {
		Kid kid = kidDAO.findKidByPrimaryKey(kidId, -1, -1);
		ParentKid existingparentKids = parentKidDAO.findParentKidByPrimaryKey(related_parentkids.getParentKidId());

		// copy into the existing record to preserve existing relationships
		if (existingparentKids != null) {
			existingparentKids.setParentKidId(related_parentkids.getParentKidId());
			related_parentkids = existingparentKids;
		}

		related_parentkids.setKid(kid);
		kid.getParentKids().add(related_parentkids);
		related_parentkids = parentKidDAO.store(related_parentkids);
		parentKidDAO.flush();

		kid = kidDAO.store(kid);
		kidDAO.flush();

		return kid;
	}

	/**
	 * Delete an existing City entity
	 * 
	 */
	@Transactional
	public Kid deleteKidCity(Integer kid_kidId, Integer related_city_cityId) {
		Kid kid = kidDAO.findKidByPrimaryKey(kid_kidId, -1, -1);
		City related_city = cityDAO.findCityByPrimaryKey(related_city_cityId, -1, -1);

		kid.setCity(null);
		related_city.getKids().remove(kid);
		kid = kidDAO.store(kid);
		kidDAO.flush();

		related_city = cityDAO.store(related_city);
		cityDAO.flush();

		cityDAO.remove(related_city);
		cityDAO.flush();

		return kid;
	}

	/**
	 * Delete an existing KidMeasures entity
	 * 
	 */
	@Transactional
	public Kid deleteKidKidMeasures(Integer kid_kidId, Integer related_kidmeasures_kidMeasuresId) {
		Kid kid = kidDAO.findKidByPrimaryKey(kid_kidId, -1, -1);
		KidMeasures related_kidmeasures = kidMeasuresDAO.findKidMeasuresByPrimaryKey(related_kidmeasures_kidMeasuresId, -1, -1);

		kid.setKidMeasures(null);
		related_kidmeasures.getKids().remove(kid);
		kid = kidDAO.store(kid);
		kidDAO.flush();

		related_kidmeasures = kidMeasuresDAO.store(related_kidmeasures);
		kidMeasuresDAO.flush();

		kidMeasuresDAO.remove(related_kidmeasures);
		kidMeasuresDAO.flush();

		return kid;
	}

	/**
	 * Delete an existing User entity
	 * 
	 */
	@Transactional
	public Kid deleteKidUser(Integer kid_kidId, Integer related_user_userId) {
		Kid kid = kidDAO.findKidByPrimaryKey(kid_kidId, -1, -1);
		User related_user = userDAO.findUserByPrimaryKey(related_user_userId, -1, -1);

		kid.setUser(null);
		related_user.getKids().remove(kid);
		kid = kidDAO.store(kid);
		kidDAO.flush();

		related_user = userDAO.store(related_user);
		userDAO.flush();

		userDAO.remove(related_user);
		userDAO.flush();

		return kid;
	}

	/**
	 * Delete an existing Team entity
	 * 
	 */
	@Transactional
	public Kid deleteKidTeam(Integer kid_kidId, Integer related_team_teamId) {
		Kid kid = kidDAO.findKidByPrimaryKey(kid_kidId, -1, -1);
		Team related_team = teamDAO.findTeamByPrimaryKey(related_team_teamId, -1, -1);

		kid.setTeam(null);
		related_team.getKids().remove(kid);
		kid = kidDAO.store(kid);
		kidDAO.flush();

		related_team = teamDAO.store(related_team);
		teamDAO.flush();

		teamDAO.remove(related_team);
		teamDAO.flush();

		return kid;
	}
}
