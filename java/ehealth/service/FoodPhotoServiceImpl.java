package ehealth.service;

import ehealth.dao.FoodPhotoDAO;
import ehealth.dao.FoodTageInputDAO;
import ehealth.dao.FoodValidationDAO;
import ehealth.dao.MealDAO;

import ehealth.domain.FoodPhoto;
import ehealth.domain.FoodTageInput;
import ehealth.domain.FoodValidation;
import ehealth.domain.Meal;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;

import org.springframework.transaction.annotation.Transactional;

/**
 * Spring service that handles CRUD requests for FoodPhoto entities
 * 
 */

@Service("FoodPhotoService")

@Transactional
public class FoodPhotoServiceImpl implements FoodPhotoService {

	/**
	 * DAO injected by Spring that manages FoodPhoto entities
	 * 
	 */
	@Autowired
	private FoodPhotoDAO foodPhotoDAO;

	/**
	 * DAO injected by Spring that manages FoodTageInput entities
	 * 
	 */
	@Autowired
	private FoodTageInputDAO foodTageInputDAO;

	/**
	 * DAO injected by Spring that manages FoodValidation entities
	 * 
	 */
	@Autowired
	private FoodValidationDAO foodValidationDAO;

	/**
	 * DAO injected by Spring that manages Meal entities
	 * 
	 */
	@Autowired
	private MealDAO mealDAO;

	/**
	 * Instantiates a new FoodPhotoServiceImpl.
	 *
	 */
	public FoodPhotoServiceImpl() {
	}

	/**
	 * Delete an existing Meal entity
	 * 
	 */
	@Transactional
	public FoodPhoto deleteFoodPhotoMeal(Integer foodphoto_foodPhotoId, Integer related_meal_mealId) {
		FoodPhoto foodphoto = foodPhotoDAO.findFoodPhotoByPrimaryKey(foodphoto_foodPhotoId, -1, -1);
		Meal related_meal = mealDAO.findMealByPrimaryKey(related_meal_mealId, -1, -1);

		foodphoto.setMeal(null);
		related_meal.getFoodPhotos().remove(foodphoto);
		foodphoto = foodPhotoDAO.store(foodphoto);
		foodPhotoDAO.flush();

		related_meal = mealDAO.store(related_meal);
		mealDAO.flush();

		mealDAO.remove(related_meal);
		mealDAO.flush();

		return foodphoto;
	}

	/**
	 * Delete an existing FoodPhoto entity
	 * 
	 */
	@Transactional
	public void deleteFoodPhoto(FoodPhoto foodphoto) {
		foodPhotoDAO.remove(foodphoto);
		foodPhotoDAO.flush();
	}

	/**
	 * Return a count of all FoodPhoto entity
	 * 
	 */
	@Transactional
	public Integer countFoodPhotos() {
		return ((Long) foodPhotoDAO.createQuerySingleResult("select count(o) from FoodPhoto o").getSingleResult()).intValue();
	}

	/**
	 * Save an existing FoodTageInput entity
	 * 
	 */
	@Transactional
	public FoodPhoto saveFoodPhotoFoodTageInputs(Integer foodPhotoId, FoodTageInput related_foodtageinputs) {
		FoodPhoto foodphoto = foodPhotoDAO.findFoodPhotoByPrimaryKey(foodPhotoId, -1, -1);
		FoodTageInput existingfoodTageInputs = foodTageInputDAO.findFoodTageInputByPrimaryKey(related_foodtageinputs.getFoodTageInputId());

		// copy into the existing record to preserve existing relationships
		if (existingfoodTageInputs != null) {
			existingfoodTageInputs.setFoodTageInputId(related_foodtageinputs.getFoodTageInputId());
			existingfoodTageInputs.setValue(related_foodtageinputs.getValue());
			related_foodtageinputs = existingfoodTageInputs;
		}

		related_foodtageinputs.setFoodPhoto(foodphoto);
		foodphoto.getFoodTageInputs().add(related_foodtageinputs);
		related_foodtageinputs = foodTageInputDAO.store(related_foodtageinputs);
		foodTageInputDAO.flush();

		foodphoto = foodPhotoDAO.store(foodphoto);
		foodPhotoDAO.flush();

		return foodphoto;
	}

	/**
	 * Return all FoodPhoto entity
	 * 
	 */
	@Transactional
	public List<FoodPhoto> findAllFoodPhotos(Integer startResult, Integer maxRows) {
		return new java.util.ArrayList<FoodPhoto>(foodPhotoDAO.findAllFoodPhotos(startResult, maxRows));
	}

	/**
	 * Delete an existing FoodValidation entity
	 * 
	 */
	@Transactional
	public FoodPhoto deleteFoodPhotoFoodValidations(Integer foodphoto_foodPhotoId, Integer related_foodvalidations_foodValidationId) {
		FoodValidation related_foodvalidations = foodValidationDAO.findFoodValidationByPrimaryKey(related_foodvalidations_foodValidationId, -1, -1);

		FoodPhoto foodphoto = foodPhotoDAO.findFoodPhotoByPrimaryKey(foodphoto_foodPhotoId, -1, -1);

		related_foodvalidations.setFoodPhoto(null);
		foodphoto.getFoodValidations().remove(related_foodvalidations);

		foodValidationDAO.remove(related_foodvalidations);
		foodValidationDAO.flush();

		return foodphoto;
	}

	/**
	 * Delete an existing FoodTageInput entity
	 * 
	 */
	@Transactional
	public FoodPhoto deleteFoodPhotoFoodTageInputs(Integer foodphoto_foodPhotoId, Integer related_foodtageinputs_foodTageInputId) {
		FoodTageInput related_foodtageinputs = foodTageInputDAO.findFoodTageInputByPrimaryKey(related_foodtageinputs_foodTageInputId, -1, -1);

		FoodPhoto foodphoto = foodPhotoDAO.findFoodPhotoByPrimaryKey(foodphoto_foodPhotoId, -1, -1);

		related_foodtageinputs.setFoodPhoto(null);
		foodphoto.getFoodTageInputs().remove(related_foodtageinputs);

		foodTageInputDAO.remove(related_foodtageinputs);
		foodTageInputDAO.flush();

		return foodphoto;
	}

	/**
	 * Save an existing Meal entity
	 * 
	 */
	@Transactional
	public FoodPhoto saveFoodPhotoMeal(Integer foodPhotoId, Meal related_meal) {
		FoodPhoto foodphoto = foodPhotoDAO.findFoodPhotoByPrimaryKey(foodPhotoId, -1, -1);
		Meal existingmeal = mealDAO.findMealByPrimaryKey(related_meal.getMealId());

		// copy into the existing record to preserve existing relationships
		if (existingmeal != null) {
			existingmeal.setMealId(related_meal.getMealId());
			existingmeal.setDate(related_meal.getDate());
			related_meal = existingmeal;
		}

		foodphoto.setMeal(related_meal);
		related_meal.getFoodPhotos().add(foodphoto);
		foodphoto = foodPhotoDAO.store(foodphoto);
		foodPhotoDAO.flush();

		related_meal = mealDAO.store(related_meal);
		mealDAO.flush();

		return foodphoto;
	}

	/**
	 * Load an existing FoodPhoto entity
	 * 
	 */
	@Transactional
	public Set<FoodPhoto> loadFoodPhotos() {
		return foodPhotoDAO.findAllFoodPhotos();
	}

	/**
	 * Save an existing FoodPhoto entity
	 * 
	 */
	@Transactional
	public FoodPhoto saveFoodPhoto(FoodPhoto foodphoto) {
		FoodPhoto existingFoodPhoto = foodPhotoDAO.findFoodPhotoByPrimaryKey(foodphoto.getFoodPhotoId());

		if (existingFoodPhoto != null) {
			if (existingFoodPhoto != foodphoto) {
				existingFoodPhoto.setFoodPhotoId(foodphoto.getFoodPhotoId());
				existingFoodPhoto.setUrl(foodphoto.getUrl());
				existingFoodPhoto.setFinalValidationValue(foodphoto.getFinalValidationValue());
			}
			foodphoto = foodPhotoDAO.store(existingFoodPhoto);
		} else {
			foodphoto = foodPhotoDAO.store(foodphoto);
		}
		foodPhotoDAO.flush();
		return foodphoto;
	}

	/**
	 * Save an existing FoodValidation entity
	 * 
	 */
	@Transactional
	public FoodPhoto saveFoodPhotoFoodValidations(Integer foodPhotoId, FoodValidation related_foodvalidations) {
		FoodPhoto foodphoto = foodPhotoDAO.findFoodPhotoByPrimaryKey(foodPhotoId, -1, -1);
		FoodValidation existingfoodValidations = foodValidationDAO.findFoodValidationByPrimaryKey(related_foodvalidations.getFoodValidationId());

		// copy into the existing record to preserve existing relationships
		if (existingfoodValidations != null) {
			existingfoodValidations.setFoodValidationId(related_foodvalidations.getFoodValidationId());
			existingfoodValidations.setValue(related_foodvalidations.getValue());
			existingfoodValidations.setTime(related_foodvalidations.getTime());
			related_foodvalidations = existingfoodValidations;
		}

		related_foodvalidations.setFoodPhoto(foodphoto);
		foodphoto.getFoodValidations().add(related_foodvalidations);
		related_foodvalidations = foodValidationDAO.store(related_foodvalidations);
		foodValidationDAO.flush();

		foodphoto = foodPhotoDAO.store(foodphoto);
		foodPhotoDAO.flush();

		return foodphoto;
	}

	/**
	 */
	@Transactional
	public FoodPhoto findFoodPhotoByPrimaryKey(Integer foodPhotoId) {
		return foodPhotoDAO.findFoodPhotoByPrimaryKey(foodPhotoId);
	}
}
