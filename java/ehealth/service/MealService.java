
package ehealth.service;

import ehealth.domain.FoodPhoto;
import ehealth.domain.Kid;
import ehealth.domain.Meal;

import java.util.List;
import java.util.Set;

/**
 * Spring service that handles CRUD requests for Meal entities
 * 
 */
public interface MealService {

	/**
	* Return all Meal entity
	* 
	 */
	public List<Meal> findAllMeals(Integer startResult, Integer maxRows);

	/**
	* Delete an existing Meal entity
	* 
	 */
	public void deleteMeal(Meal meal);

	/**
	* Delete an existing FoodPhoto entity
	* 
	 */
	public Meal deleteMealFoodPhotos(Integer meal_mealId, Integer related_foodphotos_foodPhotoId);

	/**
	* Delete an existing Kid entity
	* 
	 */
	public Meal deleteMealKid(Integer meal_mealId_1, Integer related_kid_kidId);

	/**
	* Load an existing Meal entity
	* 
	 */
	public Set<Meal> loadMeals();

	/**
	* Save an existing Meal entity
	* 
	 */
	public Meal saveMeal(Meal meal_1);

	/**
	* Return a count of all Meal entity
	* 
	 */
	public Integer countMeals();

	/**
	* Save an existing Kid entity
	* 
	 */
	public Meal saveMealKid(Integer mealId, Kid related_kid);

	/**
	 */
	public Meal findMealByPrimaryKey(Integer mealId_1);

	/**
	* Save an existing FoodPhoto entity
	* 
	 */
	public Meal saveMealFoodPhotos(Integer mealId_2, FoodPhoto related_foodphotos);
}