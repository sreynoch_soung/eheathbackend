package ehealth.service;

import ehealth.dao.DoctorDAO;
import ehealth.dao.ExerciseTargetDAO;
import ehealth.dao.KidDAO;

import ehealth.domain.Doctor;
import ehealth.domain.ExerciseTarget;
import ehealth.domain.Kid;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;

import org.springframework.transaction.annotation.Transactional;

/**
 * Spring service that handles CRUD requests for ExerciseTarget entities
 * 
 */

@Service("ExerciseTargetService")

@Transactional
public class ExerciseTargetServiceImpl implements ExerciseTargetService {

	/**
	 * DAO injected by Spring that manages Doctor entities
	 * 
	 */
	@Autowired
	private DoctorDAO doctorDAO;

	/**
	 * DAO injected by Spring that manages ExerciseTarget entities
	 * 
	 */
	@Autowired
	private ExerciseTargetDAO exerciseTargetDAO;

	/**
	 * DAO injected by Spring that manages Kid entities
	 * 
	 */
	@Autowired
	private KidDAO kidDAO;

	/**
	 * Instantiates a new ExerciseTargetServiceImpl.
	 *
	 */
	public ExerciseTargetServiceImpl() {
	}

	/**
	 * Delete an existing Kid entity
	 * 
	 */
	@Transactional
	public ExerciseTarget deleteExerciseTargetKid(Integer exercisetarget_exerciseTargetId, Integer related_kid_kidId) {
		ExerciseTarget exercisetarget = exerciseTargetDAO.findExerciseTargetByPrimaryKey(exercisetarget_exerciseTargetId, -1, -1);
		Kid related_kid = kidDAO.findKidByPrimaryKey(related_kid_kidId, -1, -1);

		exercisetarget.setKid(null);
		related_kid.getExerciseTargets().remove(exercisetarget);
		exercisetarget = exerciseTargetDAO.store(exercisetarget);
		exerciseTargetDAO.flush();

		related_kid = kidDAO.store(related_kid);
		kidDAO.flush();

		kidDAO.remove(related_kid);
		kidDAO.flush();

		return exercisetarget;
	}

	/**
	 * Delete an existing Doctor entity
	 * 
	 */
	@Transactional
	public ExerciseTarget deleteExerciseTargetDoctor(Integer exercisetarget_exerciseTargetId, Integer related_doctor_doctorId) {
		ExerciseTarget exercisetarget = exerciseTargetDAO.findExerciseTargetByPrimaryKey(exercisetarget_exerciseTargetId, -1, -1);
		Doctor related_doctor = doctorDAO.findDoctorByPrimaryKey(related_doctor_doctorId, -1, -1);

		exercisetarget.setDoctor(null);
		related_doctor.getExerciseTargets().remove(exercisetarget);
		exercisetarget = exerciseTargetDAO.store(exercisetarget);
		exerciseTargetDAO.flush();

		related_doctor = doctorDAO.store(related_doctor);
		doctorDAO.flush();

		doctorDAO.remove(related_doctor);
		doctorDAO.flush();

		return exercisetarget;
	}

	/**
	 * Return all ExerciseTarget entity
	 * 
	 */
	@Transactional
	public List<ExerciseTarget> findAllExerciseTargets(Integer startResult, Integer maxRows) {
		return new java.util.ArrayList<ExerciseTarget>(exerciseTargetDAO.findAllExerciseTargets(startResult, maxRows));
	}

	/**
	 * Save an existing ExerciseTarget entity
	 * 
	 */
	@Transactional
	public ExerciseTarget saveExerciseTarget(ExerciseTarget exercisetarget) {
		ExerciseTarget existingExerciseTarget = exerciseTargetDAO.findExerciseTargetByPrimaryKey(exercisetarget.getExerciseTargetId());

		if (existingExerciseTarget != null) {
			if (existingExerciseTarget != exercisetarget) {
				existingExerciseTarget.setExerciseTargetId(exercisetarget.getExerciseTargetId());
				existingExerciseTarget.setValue(exercisetarget.getValue());
				existingExerciseTarget.setDate(exercisetarget.getDate());
			}
			exercisetarget = exerciseTargetDAO.store(existingExerciseTarget);
		} else {
			exercisetarget = exerciseTargetDAO.store(exercisetarget);
		}
		exerciseTargetDAO.flush();
		return exercisetarget;
	}

	/**
	 * Load an existing ExerciseTarget entity
	 * 
	 */
	@Transactional
	public Set<ExerciseTarget> loadExerciseTargets() {
		return exerciseTargetDAO.findAllExerciseTargets();
	}

	/**
	 * Delete an existing ExerciseTarget entity
	 * 
	 */
	@Transactional
	public void deleteExerciseTarget(ExerciseTarget exercisetarget) {
		exerciseTargetDAO.remove(exercisetarget);
		exerciseTargetDAO.flush();
	}

	/**
	 * Save an existing Kid entity
	 * 
	 */
	@Transactional
	public ExerciseTarget saveExerciseTargetKid(Integer exerciseTargetId, Kid related_kid) {
		ExerciseTarget exercisetarget = exerciseTargetDAO.findExerciseTargetByPrimaryKey(exerciseTargetId, -1, -1);
		Kid existingkid = kidDAO.findKidByPrimaryKey(related_kid.getKidId());

		// copy into the existing record to preserve existing relationships
		if (existingkid != null) {
			existingkid.setKidId(related_kid.getKidId());
			existingkid.setHigh(related_kid.getHigh());
			existingkid.setWeight(related_kid.getWeight());
			existingkid.setAddress(related_kid.getAddress());
			existingkid.setCodeKey(related_kid.getCodeKey());
			related_kid = existingkid;
		}

		exercisetarget.setKid(related_kid);
		related_kid.getExerciseTargets().add(exercisetarget);
		exercisetarget = exerciseTargetDAO.store(exercisetarget);
		exerciseTargetDAO.flush();

		related_kid = kidDAO.store(related_kid);
		kidDAO.flush();

		return exercisetarget;
	}

	/**
	 * Return a count of all ExerciseTarget entity
	 * 
	 */
	@Transactional
	public Integer countExerciseTargets() {
		return ((Long) exerciseTargetDAO.createQuerySingleResult("select count(o) from ExerciseTarget o").getSingleResult()).intValue();
	}

	/**
	 */
	@Transactional
	public ExerciseTarget findExerciseTargetByPrimaryKey(Integer exerciseTargetId) {
		return exerciseTargetDAO.findExerciseTargetByPrimaryKey(exerciseTargetId);
	}

	/**
	 * Save an existing Doctor entity
	 * 
	 */
	@Transactional
	public ExerciseTarget saveExerciseTargetDoctor(Integer exerciseTargetId, Doctor related_doctor) {
		ExerciseTarget exercisetarget = exerciseTargetDAO.findExerciseTargetByPrimaryKey(exerciseTargetId, -1, -1);
		Doctor existingdoctor = doctorDAO.findDoctorByPrimaryKey(related_doctor.getDoctorId());

		// copy into the existing record to preserve existing relationships
		if (existingdoctor != null) {
			existingdoctor.setDoctorId(related_doctor.getDoctorId());
			existingdoctor.setHospital(related_doctor.getHospital());
			existingdoctor.setCodeKey(related_doctor.getCodeKey());
			related_doctor = existingdoctor;
		}

		exercisetarget.setDoctor(related_doctor);
		related_doctor.getExerciseTargets().add(exercisetarget);
		exercisetarget = exerciseTargetDAO.store(exercisetarget);
		exerciseTargetDAO.flush();

		related_doctor = doctorDAO.store(related_doctor);
		doctorDAO.flush();

		return exercisetarget;
	}
}
