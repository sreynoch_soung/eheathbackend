
package ehealth.service;

import ehealth.domain.Kid;
import ehealth.domain.KidMeasures;

import java.util.List;
import java.util.Set;

/**
 * Spring service that handles CRUD requests for KidMeasures entities
 * 
 */
public interface KidMeasuresService {

	/**
	* Delete an existing Kid entity
	* 
	 */
	public KidMeasures deleteKidMeasuresKids(Integer kidmeasures_kidMeasuresId, Integer related_kids_kidId);

	/**
	* Delete an existing KidMeasures entity
	* 
	 */
	public void deleteKidMeasures(KidMeasures kidmeasures);

	/**
	* Return all KidMeasures entity
	* 
	 */
	public List<KidMeasures> findAllKidMeasuress(Integer startResult, Integer maxRows);

	/**
	* Save an existing KidMeasures entity
	* 
	 */
	public KidMeasures saveKidMeasures(KidMeasures kidmeasures_1);

	/**
	* Return a count of all KidMeasures entity
	* 
	 */
	public Integer countKidMeasuress();

	/**
	* Load an existing KidMeasures entity
	* 
	 */
	public Set<KidMeasures> loadKidMeasuress();

	/**
	* Save an existing Kid entity
	* 
	 */
	public KidMeasures saveKidMeasuresKids(Integer kidMeasuresId, Kid related_kids);

	/**
	 */
	public KidMeasures findKidMeasuresByPrimaryKey(Integer kidMeasuresId_1);
}