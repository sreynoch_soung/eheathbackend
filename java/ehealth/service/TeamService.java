
package ehealth.service;

import ehealth.domain.Kid;
import ehealth.domain.League;
import ehealth.domain.Match;
import ehealth.domain.Team;

import java.util.List;
import java.util.Set;

/**
 * Spring service that handles CRUD requests for Team entities
 * 
 */
public interface TeamService {

	/**
	* Save an existing League entity
	* 
	 */
	public Team saveTeamLeagues(Integer teamId, League related_leagues);

	/**
	* Return a count of all Team entity
	* 
	 */
	public Integer countTeams();

	/**
	* Save an existing Kid entity
	* 
	 */
	public Team saveTeamKids(Integer teamId_1, Kid related_kids);

	/**
	* Load an existing Team entity
	* 
	 */
	public Set<Team> loadTeams();

	/**
	* Delete an existing Match entity
	* 
	 */
	public Team deleteTeamMatchsForTeamId1(Integer team_teamId, Integer related_matchsforteamid1_matchId);

	/**
	* Save an existing Match entity
	* 
	 */
	public Team saveTeamMatchsForTeamId2(Integer teamId_2, Match related_matchsforteamid2);

	/**
	* Save an existing Match entity
	* 
	 */
	public Team saveTeamMatchsForTeamIdWin(Integer teamId_3, Match related_matchsforteamidwin);

	/**
	 */
	public Team findTeamByPrimaryKey(Integer teamId_4);

	/**
	* Delete an existing Kid entity
	* 
	 */
	public Team deleteTeamKids(Integer team_teamId_1, Integer related_kids_kidId);

	/**
	* Save an existing Team entity
	* 
	 */
	public Team saveTeam(Team team);

	/**
	* Return all Team entity
	* 
	 */
	public List<Team> findAllTeams(Integer startResult, Integer maxRows);

	/**
	* Delete an existing League entity
	* 
	 */
	public Team deleteTeamLeagues(Integer team_teamId_2, Integer related_leagues_leagueId);

	/**
	* Delete an existing Match entity
	* 
	 */
	public Team deleteTeamMatchsForTeamIdWin(Integer team_teamId_3, Integer related_matchsforteamidwin_matchId);

	/**
	* Save an existing Match entity
	* 
	 */
	public Team saveTeamMatchsForTeamId1(Integer teamId_5, Match related_matchsforteamid1);

	/**
	* Delete an existing Match entity
	* 
	 */
	public Team deleteTeamMatchsForTeamId2(Integer team_teamId_4, Integer related_matchsforteamid2_matchId);

	/**
	* Delete an existing Team entity
	* 
	 */
	public void deleteTeam(Team team_1);
}