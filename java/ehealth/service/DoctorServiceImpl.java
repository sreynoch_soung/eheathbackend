package ehealth.service;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ehealth.dao.DoctorDAO;
import ehealth.dao.ExerciseTargetDAO;
import ehealth.dao.KidDAO;
import ehealth.dao.NutritionTargetDAO;
import ehealth.dao.UserDAO;
import ehealth.domain.Doctor;
import ehealth.domain.ExerciseTarget;
import ehealth.domain.Kid;
import ehealth.domain.NutritionTarget;
import ehealth.domain.User;

/**
 * Spring service that handles CRUD requests for Doctor entities
 * 
 */

@Service("DoctorService")

@Transactional
public class DoctorServiceImpl implements DoctorService {

	/**
	 * DAO injected by Spring that manages Doctor entities
	 * 
	 */
	@Autowired
	private DoctorDAO doctorDAO;

	/**
	 * DAO injected by Spring that manages ExerciseTarget entities
	 * 
	 */
	@Autowired
	private ExerciseTargetDAO exerciseTargetDAO;

	/**
	 * DAO injected by Spring that manages Kid entities
	 * 
	 */
	@Autowired
	private KidDAO kidDAO;

	/**
	 * DAO injected by Spring that manages NutritionTarget entities
	 * 
	 */
	@Autowired
	private NutritionTargetDAO nutritionTargetDAO;

	/**
	 * DAO injected by Spring that manages User entities
	 * 
	 */
	@Autowired
	private UserDAO userDAO;

	/**
	 * Instantiates a new DoctorServiceImpl.
	 *
	 */
	public DoctorServiceImpl() {
	}

	/**
	 * Save an existing NutritionTarget entity
	 * 
	 */
	@Transactional
	public Doctor saveDoctorNutritionTargets(Integer doctorId, NutritionTarget related_nutritiontargets) {
		Doctor doctor = doctorDAO.findDoctorByPrimaryKey(doctorId, -1, -1);
		NutritionTarget existingnutritionTargets = nutritionTargetDAO.findNutritionTargetByPrimaryKey(related_nutritiontargets.getNutritionTargetId());

		// copy into the existing record to preserve existing relationships
		if (existingnutritionTargets != null) {
			existingnutritionTargets.setNutritionTargetId(related_nutritiontargets.getNutritionTargetId());
			existingnutritionTargets.setDate(related_nutritiontargets.getDate());
			related_nutritiontargets = existingnutritionTargets;
		}

		related_nutritiontargets.setDoctor(doctor);
		doctor.getNutritionTargets().add(related_nutritiontargets);
		related_nutritiontargets = nutritionTargetDAO.store(related_nutritiontargets);
		nutritionTargetDAO.flush();

		doctor = doctorDAO.store(doctor);
		doctorDAO.flush();

		return doctor;
	}

	/**
	 * Delete an existing Kid entity
	 * 
	 */
	@Transactional
//	@PreAuthorize("hasRole('ROLE_USER')")
	public Doctor deleteDoctorKids(Integer doctor_doctorId, Integer related_kids_kidId) {
		Kid related_kids = kidDAO.findKidByPrimaryKey(related_kids_kidId, -1, -1);

		Doctor doctor = doctorDAO.findDoctorByPrimaryKey(doctor_doctorId, -1, -1);

		related_kids.setDoctor(null);
		doctor.getKids().remove(related_kids);

		kidDAO.remove(related_kids);
		kidDAO.flush();

		return doctor;
	}

	/**
	 */
	@Transactional
	public Doctor findDoctorByPrimaryKey(Integer doctorId) {
		return doctorDAO.findDoctorByPrimaryKey(doctorId);
	}

	/**
	 */
	@Transactional
	public Set<Doctor> findDoctorByCodeKey(String doctorCodeKey) {
		return doctorDAO.findDoctorByCodeKey(doctorCodeKey);
	}

	
	/**
	 * Return a count of all Doctor entity
	 * 
	 */
	@Transactional
	public Integer countDoctors() {
		return ((Long) doctorDAO.createQuerySingleResult("select count(o) from Doctor o").getSingleResult()).intValue();
	}

	/**
	 * Save an existing User entity
	 * 
	 */
	@Transactional
	public Doctor saveDoctorUser(Integer doctorId, User related_user) {
		Doctor doctor = doctorDAO.findDoctorByPrimaryKey(doctorId, -1, -1);
		User existinguser = userDAO.findUserByPrimaryKey(related_user.getUserId());

		// copy into the existing record to preserve existing relationships
		if (existinguser != null) {
			existinguser.setUserId(related_user.getUserId());
			existinguser.setUsername(related_user.getUsername());
			existinguser.setName(related_user.getName());
			existinguser.setSurename(related_user.getSurename());
			existinguser.setPassword(related_user.getPassword());
			existinguser.setEmail(related_user.getEmail());
			existinguser.setTelephone(related_user.getTelephone());
			existinguser.setAge(related_user.getAge());
			existinguser.setSex(related_user.getSex());
			related_user = existinguser;
		}

		doctor.setUser(related_user);
		related_user.getDoctors().add(doctor);
		doctor = doctorDAO.store(doctor);
		doctorDAO.flush();

		related_user = userDAO.store(related_user);
		userDAO.flush();

		return doctor;
	}

	/**
	 * Load an existing Doctor entity
	 * 
	 */
	@Transactional
	public Set<Doctor> loadDoctors() {
		return doctorDAO.findAllDoctors();
	}

	/**
	 * Delete an existing NutritionTarget entity
	 * 
	 */
	@Transactional
	public Doctor deleteDoctorNutritionTargets(Integer doctor_doctorId, Integer related_nutritiontargets_nutritionTargetId) {
		NutritionTarget related_nutritiontargets = nutritionTargetDAO.findNutritionTargetByPrimaryKey(related_nutritiontargets_nutritionTargetId, -1, -1);

		Doctor doctor = doctorDAO.findDoctorByPrimaryKey(doctor_doctorId, -1, -1);

		related_nutritiontargets.setDoctor(null);
		doctor.getNutritionTargets().remove(related_nutritiontargets);

		nutritionTargetDAO.remove(related_nutritiontargets);
		nutritionTargetDAO.flush();

		return doctor;
	}

	/**
	 * Delete an existing User entity
	 * 
	 */
	@Transactional
	public Doctor deleteDoctorUser(Integer doctor_doctorId, Integer related_user_userId) {
		Doctor doctor = doctorDAO.findDoctorByPrimaryKey(doctor_doctorId, -1, -1);
		User related_user = userDAO.findUserByPrimaryKey(related_user_userId, -1, -1);

		doctor.setUser(null);
		related_user.getDoctors().remove(doctor);
		doctor = doctorDAO.store(doctor);
		doctorDAO.flush();

		related_user = userDAO.store(related_user);
		userDAO.flush();

		userDAO.remove(related_user);
		userDAO.flush();

		return doctor;
	}

	/**
	 * Delete an existing ExerciseTarget entity
	 * 
	 */
	@Transactional
	public Doctor deleteDoctorExerciseTargets(Integer doctor_doctorId, Integer related_exercisetargets_exerciseTargetId) {
		ExerciseTarget related_exercisetargets = exerciseTargetDAO.findExerciseTargetByPrimaryKey(related_exercisetargets_exerciseTargetId, -1, -1);

		Doctor doctor = doctorDAO.findDoctorByPrimaryKey(doctor_doctorId, -1, -1);

		related_exercisetargets.setDoctor(null);
		doctor.getExerciseTargets().remove(related_exercisetargets);

		exerciseTargetDAO.remove(related_exercisetargets);
		exerciseTargetDAO.flush();

		return doctor;
	}

	/**
	 * Return all Doctor entity
	 * 
	 */
	@Transactional
	public List<Doctor> findAllDoctors(Integer startResult, Integer maxRows) {
		return new java.util.ArrayList<Doctor>(doctorDAO.findAllDoctors(startResult, maxRows));
	}

	/**
	 * Delete an existing Doctor entity
	 * 
	 */
	@Transactional
	public void deleteDoctor(Doctor doctor) {
		doctorDAO.remove(doctor);
		doctorDAO.flush();
	}

	/**
	 * Save an existing ExerciseTarget entity
	 * 
	 */
	@Transactional
	public Doctor saveDoctorExerciseTargets(Integer doctorId, ExerciseTarget related_exercisetargets) {
		Doctor doctor = doctorDAO.findDoctorByPrimaryKey(doctorId, -1, -1);
		ExerciseTarget existingexerciseTargets = exerciseTargetDAO.findExerciseTargetByPrimaryKey(related_exercisetargets.getExerciseTargetId());

		// copy into the existing record to preserve existing relationships
		if (existingexerciseTargets != null) {
			existingexerciseTargets.setExerciseTargetId(related_exercisetargets.getExerciseTargetId());
			existingexerciseTargets.setValue(related_exercisetargets.getValue());
			existingexerciseTargets.setDate(related_exercisetargets.getDate());
			related_exercisetargets = existingexerciseTargets;
		}

		related_exercisetargets.setDoctor(doctor);
		doctor.getExerciseTargets().add(related_exercisetargets);
		related_exercisetargets = exerciseTargetDAO.store(related_exercisetargets);
		exerciseTargetDAO.flush();

		doctor = doctorDAO.store(doctor);
		doctorDAO.flush();

		return doctor;
	}

	/**
	 * Save an existing Kid entity
	 * 
	 */
	@Transactional
	public Doctor saveDoctorKids(Integer doctorId, Kid related_kids) {
		Doctor doctor = doctorDAO.findDoctorByPrimaryKey(doctorId, -1, -1);
		Kid existingkids = kidDAO.findKidByPrimaryKey(related_kids.getKidId());

		// copy into the existing record to preserve existing relationships
		if (existingkids != null) {
			existingkids.setKidId(related_kids.getKidId());
			existingkids.setHigh(related_kids.getHigh());
			existingkids.setWeight(related_kids.getWeight());
			existingkids.setAddress(related_kids.getAddress());
			existingkids.setCodeKey(related_kids.getCodeKey());
			related_kids = existingkids;
		} else {
			related_kids = kidDAO.store(related_kids);
			kidDAO.flush();
		}

		related_kids.setDoctor(doctor);
		doctor.getKids().add(related_kids);
		related_kids = kidDAO.store(related_kids);
		kidDAO.flush();

		doctor = doctorDAO.store(doctor);
		doctorDAO.flush();

		return doctor;
	}

	/**
	 * Save an existing Doctor entity
	 * 
	 */
	@Transactional
	public Doctor saveDoctor(Doctor doctor) {
		Doctor existingDoctor = doctorDAO.findDoctorByPrimaryKey(doctor.getDoctorId());

		if (existingDoctor != null) {
			if (existingDoctor != doctor) {
				existingDoctor.setDoctorId(doctor.getDoctorId());
				existingDoctor.setHospital(doctor.getHospital());
				existingDoctor.setCodeKey(doctor.getCodeKey());
			}
			doctor = doctorDAO.store(existingDoctor);
		} else {
			doctor = doctorDAO.store(doctor);
		}
		doctorDAO.flush();
		return doctor;
	}
}
