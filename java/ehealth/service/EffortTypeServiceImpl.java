package ehealth.service;

import ehealth.dao.EffortTypeDAO;
import ehealth.dao.ExerciseInputDAO;

import ehealth.domain.EffortType;
import ehealth.domain.ExerciseInput;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;

import org.springframework.transaction.annotation.Transactional;

/**
 * Spring service that handles CRUD requests for EffortType entities
 * 
 */

@Service("EffortTypeService")

@Transactional
public class EffortTypeServiceImpl implements EffortTypeService {

	/**
	 * DAO injected by Spring that manages EffortType entities
	 * 
	 */
	@Autowired
	private EffortTypeDAO effortTypeDAO;

	/**
	 * DAO injected by Spring that manages ExerciseInput entities
	 * 
	 */
	@Autowired
	private ExerciseInputDAO exerciseInputDAO;

	/**
	 * Instantiates a new EffortTypeServiceImpl.
	 *
	 */
	public EffortTypeServiceImpl() {
	}

	/**
	 * Save an existing ExerciseInput entity
	 * 
	 */
	@Transactional
	public EffortType saveEffortTypeExerciseInputs(Integer effortTypeId, ExerciseInput related_exerciseinputs) {
		EffortType efforttype = effortTypeDAO.findEffortTypeByPrimaryKey(effortTypeId, -1, -1);
		ExerciseInput existingexerciseInputs = exerciseInputDAO.findExerciseInputByPrimaryKey(related_exerciseinputs.getExerciseInputId());

		// copy into the existing record to preserve existing relationships
		if (existingexerciseInputs != null) {
			existingexerciseInputs.setExerciseInputId(related_exerciseinputs.getExerciseInputId());
			existingexerciseInputs.setDate(related_exerciseinputs.getDate());
			existingexerciseInputs.setValue(related_exerciseinputs.getValue());
			existingexerciseInputs.setFinalValidationValue(related_exerciseinputs.getFinalValidationValue());
			related_exerciseinputs = existingexerciseInputs;
		}

		related_exerciseinputs.setEffortType(efforttype);
		efforttype.getExerciseInputs().add(related_exerciseinputs);
		related_exerciseinputs = exerciseInputDAO.store(related_exerciseinputs);
		exerciseInputDAO.flush();

		efforttype = effortTypeDAO.store(efforttype);
		effortTypeDAO.flush();

		return efforttype;
	}

	/**
	 * Return all EffortType entity
	 * 
	 */
	@Transactional
	public List<EffortType> findAllEffortTypes(Integer startResult, Integer maxRows) {
		return new java.util.ArrayList<EffortType>(effortTypeDAO.findAllEffortTypes(startResult, maxRows));
	}

	/**
	 * Delete an existing EffortType entity
	 * 
	 */
	@Transactional
	public void deleteEffortType(EffortType efforttype) {
		effortTypeDAO.remove(efforttype);
		effortTypeDAO.flush();
	}

	/**
	 * Delete an existing ExerciseInput entity
	 * 
	 */
	@Transactional
	public EffortType deleteEffortTypeExerciseInputs(Integer efforttype_effortTypeId, Integer related_exerciseinputs_exerciseInputId) {
		ExerciseInput related_exerciseinputs = exerciseInputDAO.findExerciseInputByPrimaryKey(related_exerciseinputs_exerciseInputId, -1, -1);

		EffortType efforttype = effortTypeDAO.findEffortTypeByPrimaryKey(efforttype_effortTypeId, -1, -1);

		related_exerciseinputs.setEffortType(null);
		efforttype.getExerciseInputs().remove(related_exerciseinputs);

		exerciseInputDAO.remove(related_exerciseinputs);
		exerciseInputDAO.flush();

		return efforttype;
	}

	/**
	 * Return a count of all EffortType entity
	 * 
	 */
	@Transactional
	public Integer countEffortTypes() {
		return ((Long) effortTypeDAO.createQuerySingleResult("select count(o) from EffortType o").getSingleResult()).intValue();
	}

	/**
	 * Load an existing EffortType entity
	 * 
	 */
	@Transactional
	public Set<EffortType> loadEffortTypes() {
		return effortTypeDAO.findAllEffortTypes();
	}

	/**
	 * Save an existing EffortType entity
	 * 
	 */
	@Transactional
	public EffortType saveEffortType(EffortType efforttype) {
		EffortType existingEffortType = effortTypeDAO.findEffortTypeByPrimaryKey(efforttype.getEffortTypeId());

		if (existingEffortType != null) {
			if (existingEffortType != efforttype) {
				existingEffortType.setEffortTypeId(efforttype.getEffortTypeId());
				existingEffortType.setTitle(efforttype.getTitle());
			}
			efforttype = effortTypeDAO.store(existingEffortType);
		} else {
			efforttype = effortTypeDAO.store(efforttype);
		}
		effortTypeDAO.flush();
		return efforttype;
	}

	/**
	 */
	@Transactional
	public EffortType findEffortTypeByPrimaryKey(Integer effortTypeId) {
		return effortTypeDAO.findEffortTypeByPrimaryKey(effortTypeId);
	}
}
