
package ehealth.service;

import ehealth.domain.AccountSetting;
import ehealth.domain.Bonus;
import ehealth.domain.City;
import ehealth.domain.Doctor;
import ehealth.domain.ExerciseInput;
import ehealth.domain.ExerciseTarget;
import ehealth.domain.Grad;
import ehealth.domain.Kid;
import ehealth.domain.KidChat;
import ehealth.domain.KidMeasures;
import ehealth.domain.Meal;
import ehealth.domain.NutritionTarget;
import ehealth.domain.ParentKid;
import ehealth.domain.Team;
import ehealth.domain.User;

import java.util.List;
import java.util.Set;

/**
 * Spring service that handles CRUD requests for Kid entities
 * 
 */
public interface KidService {

	/**
	* Save an existing NutritionTarget entity
	* 
	 */
	public Kid saveKidNutritionTargets(Integer kidId, NutritionTarget related_nutritiontargets);

	/**
	* Save an existing Meal entity
	* 
	 */
	public Kid saveKidMeals(Integer kidId_1, Meal related_meals);

	/**
	* Delete an existing KidChat entity
	* 
	 */
	public Kid deleteKidKidChatsForKidId2(Integer kid_kidId, Integer related_kidchatsforkidid2_kidChatId);

	/**
	* Delete an existing Grad entity
	* 
	 */
	public Kid deleteKidGrads(Integer kid_kidId_1, Integer related_grads_gradId);

	/**
	* Delete an existing AccountSetting entity
	* 
	 */
	public Kid deleteKidAccountSetting(Integer kid_kidId_2, Integer related_accountsetting_accountSettingId);

	/**
	* Delete an existing ExerciseTarget entity
	* 
	 */
	public Kid deleteKidExerciseTargets(Integer kid_kidId_3, Integer related_exercisetargets_exerciseTargetId);

	/**
	* Delete an existing ParentKid entity
	* 
	 */
	public Kid deleteKidParentKids(Integer kid_kidId_4, Integer related_parentkids_parentKidId);

	/**
	* Save an existing ExerciseTarget entity
	* 
	 */
	public Kid saveKidExerciseTargets(Integer kidId_2, ExerciseTarget related_exercisetargets);

	/**
	* Delete an existing Bonus entity
	* 
	 */
	public Kid deleteKidBonuses(Integer kid_kidId_5, Integer related_bonuses_bonusId);

	/**
	* Save an existing User entity
	* 
	 */
	public Kid saveKidUser(Integer kidId_3, User related_user);

	/**
	* Delete an existing NutritionTarget entity
	* 
	 */
	public Kid deleteKidNutritionTargets(Integer kid_kidId_6, Integer related_nutritiontargets_nutritionTargetId);

	/**
	* Delete an existing Meal entity
	* 
	 */
	public Kid deleteKidMeals(Integer kid_kidId_7, Integer related_meals_mealId);

	/**
	* Delete an existing Doctor entity
	* 
	 */
	public Kid deleteKidDoctor(Integer kid_kidId_8, Integer related_doctor_doctorId);

	/**
	* Return all Kid entity
	* 
	 */
	public List<Kid> findAllKids(Integer startResult, Integer maxRows);

	/**
	* Save an existing AccountSetting entity
	* 
	 */
	public Kid saveKidAccountSetting(Integer kidId_4, AccountSetting related_accountsetting);

	/**
	* Delete an existing KidChat entity
	* 
	 */
	public Kid deleteKidKidChatsForKidId1(Integer kid_kidId_9, Integer related_kidchatsforkidid1_kidChatId);

	/**
	* Save an existing ExerciseInput entity
	* 
	 */
	public Kid saveKidExerciseInputs(Integer kidId_5, ExerciseInput related_exerciseinputs);

	/**
	* Load an existing Kid entity
	* 
	 */
	public Set<Kid> loadKids();

	/**
	* Save an existing KidChat entity
	* 
	 */
	public Kid saveKidKidChatsForKidId2(Integer kidId_6, KidChat related_kidchatsforkidid2);

	/**
	* Save an existing Bonus entity
	* 
	 */
	public Kid saveKidBonuses(Integer kidId_7, Bonus related_bonuses);

	/**
	* Return a count of all Kid entity
	* 
	 */
	public Integer countKids();

	/**
	* Save an existing Team entity
	* 
	 */
	public Kid saveKidTeam(Integer kidId_8, Team related_team);

	/**
	 * Find kid by Primary key
	 */
	public Kid findKidByPrimaryKey(Integer kidId_9);
	/**
	 * Find kid by CodeKey
	 */
	public Set<Kid> findKidByCodeKey(String codeKey);

	/**
	* Delete an existing Kid entity
	* 
	 */
	public void deleteKid(Kid kid);

	/**
	* Save an existing Kid entity
	* 
	 */
	public Kid saveKid(Kid kid_1);

	/**
	* Save an existing KidChat entity
	* 
	 */
	public Kid saveKidKidChatsForKidId1(Integer kidId_10, KidChat related_kidchatsforkidid1);

	/**
	* Delete an existing ExerciseInput entity
	* 
	 */
	public Kid deleteKidExerciseInputs(Integer kid_kidId_10, Integer related_exerciseinputs_exerciseInputId);

	/**
	* Save an existing Doctor entity
	* 
	 */
	public Kid saveKidDoctor(Integer kidId_11, Doctor related_doctor);

	/**
	* Save an existing City entity
	* 
	 */
	public Kid saveKidCity(Integer kidId_12, City related_city);

	/**
	* Save an existing Grad entity
	* 
	 */
	public Kid saveKidGrads(Integer kidId_13, Grad related_grads);

	/**
	* Save an existing KidMeasures entity
	* 
	 */
	public Kid saveKidKidMeasures(Integer kidId_14, KidMeasures related_kidmeasures);

	/**
	* Save an existing ParentKid entity
	* 
	 */
	public Kid saveKidParentKids(Integer kidId_15, ParentKid related_parentkids);

	/**
	* Delete an existing City entity
	* 
	 */
	public Kid deleteKidCity(Integer kid_kidId_11, Integer related_city_cityId);

	/**
	* Delete an existing KidMeasures entity
	* 
	 */
	public Kid deleteKidKidMeasures(Integer kid_kidId_12, Integer related_kidmeasures_kidMeasuresId);

	/**
	* Delete an existing User entity
	* 
	 */
	public Kid deleteKidUser(Integer kid_kidId_13, Integer related_user_userId);

	/**
	* Delete an existing Team entity
	* 
	 */
	public Kid deleteKidTeam(Integer kid_kidId_14, Integer related_team_teamId);
}