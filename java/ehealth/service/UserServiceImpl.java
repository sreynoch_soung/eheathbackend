package ehealth.service;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ehealth.dao.DoctorDAO;
import ehealth.dao.FoodValidationDAO;
import ehealth.dao.KidDAO;
import ehealth.dao.ParentDAO;
import ehealth.dao.TimestampDAO;
import ehealth.dao.UserDAO;
import ehealth.dao.UserRoleDAO;
import ehealth.domain.Doctor;
import ehealth.domain.FoodValidation;
import ehealth.domain.Kid;
import ehealth.domain.Parent;
import ehealth.domain.Timestamp;
import ehealth.domain.User;
import ehealth.domain.UserRole;

/**
 * Spring service that handles CRUD requests for User entities
 * 
 */

@Service("UserService")

@Transactional
public class UserServiceImpl implements UserService {

	/**
	 * DAO injected by Spring that manages Doctor entities
	 * 
	 */
	@Autowired
	private DoctorDAO doctorDAO;

	/**
	 * DAO injected by Spring that manages FoodValidation entities
	 * 
	 */
	@Autowired
	private FoodValidationDAO foodValidationDAO;

	/**
	 * DAO injected by Spring that manages Kid entities
	 * 
	 */
	@Autowired
	private KidDAO kidDAO;

	/**
	 * DAO injected by Spring that manages Parent entities
	 * 
	 */
	@Autowired
	private ParentDAO parentDAO;

	/**
	 * DAO injected by Spring that manages Timestamp entities
	 * 
	 */
	@Autowired
	private TimestampDAO timestampDAO;

	/**
	 * DAO injected by Spring that manages User entities
	 * 
	 */
	@Autowired
	private UserDAO userDAO;

	/**
	 * DAO injected by Spring that manages UserRole entities
	 * 
	 */
	@Autowired
	private UserRoleDAO userRoleDAO;

	/**
	 * Instantiates a new UserServiceImpl.
	 *
	 */
	public UserServiceImpl() {
	}

	/**
	 * Load an existing User entity
	 * 
	 */
	@Transactional
	public Set<User> loadUsers() {
		return userDAO.findAllUsers();
	}

	/**
	 * Delete an existing Parent entity
	 * 
	 */
	@Transactional
	public User deleteUserParents(Integer user_userId, Integer related_parents_parentId) {
		Parent related_parents = parentDAO.findParentByPrimaryKey(related_parents_parentId, -1, -1);

		User user = userDAO.findUserByPrimaryKey(user_userId, -1, -1);

		related_parents.setUser(null);
		user.getParents().remove(related_parents);

		parentDAO.remove(related_parents);
		parentDAO.flush();

		return user;
	}

	/**
	 * Save an existing Kid entity
	 * 
	 */
	@Transactional
	public User saveUserKids(Integer userId, Kid related_kids) {
		User user = userDAO.findUserByPrimaryKey(userId, -1, -1);
		Kid existingkids = kidDAO.findKidByPrimaryKey(related_kids.getKidId());

		// copy into the existing record to preserve existing relationships
		if (existingkids != null) {
			existingkids.setKidId(related_kids.getKidId());
			existingkids.setHigh(related_kids.getHigh());
			existingkids.setWeight(related_kids.getWeight());
			existingkids.setAddress(related_kids.getAddress());
			existingkids.setCodeKey(related_kids.getCodeKey());
			related_kids = existingkids;
		}

		related_kids.setUser(user);
		user.getKids().add(related_kids);
		related_kids = kidDAO.store(related_kids);
		kidDAO.flush();

		user = userDAO.store(user);
		userDAO.flush();

		return user;
	}

	/**
	 * Save an existing User entity
	 * 
	 */
	@Transactional
	public User saveUser(User user) {
		User existingUser = userDAO.findUserByPrimaryKey(user.getUserId());
		
		BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
		user.setPassword(passwordEncoder.encode(user.getPassword()));

		if (existingUser != null) {
			if (existingUser != user) {
				existingUser.setUserId(user.getUserId());
				existingUser.setUsername(user.getUsername());
				existingUser.setName(user.getName());
				existingUser.setSurename(user.getSurename());
				existingUser.setPassword(user.getPassword());
				existingUser.setEmail(user.getEmail());
				existingUser.setTelephone(user.getTelephone());
				existingUser.setAge(user.getAge());
				existingUser.setSex(user.getSex());
				existingUser.setEnabled(user.getEnabled());
			}
			user = userDAO.store(existingUser);
		} else {
			user = userDAO.store(user);
		}
		userDAO.flush();
		return user;
	}

	/**
	 * Delete an existing User entity
	 * 
	 */
	@Transactional
	public void deleteUser(User user) {
		userDAO.remove(user);
		userDAO.flush();
	}

	/**
	 * Return all User entity
	 * 
	 */
	@Transactional
	public List<User> findAllUsers(Integer startResult, Integer maxRows) {
		return new java.util.ArrayList<User>(userDAO.findAllUsers(startResult, maxRows));
	}

	/**
	 * Save an existing UserRole entity
	 * 
	 */
	@Transactional
	public User saveUserUserRole(Integer userId, UserRole related_userrole) {
		User user = userDAO.findUserByPrimaryKey(userId, -1, -1);
		UserRole existinguserRole = userRoleDAO.findUserRoleByPrimaryKey(related_userrole.getUserRoleId());

		// copy into the existing record to preserve existing relationships
		if (existinguserRole != null) {
			existinguserRole.setUserRoleId(related_userrole.getUserRoleId());
			existinguserRole.setRoleTitle(related_userrole.getRoleTitle());
			related_userrole = existinguserRole;
		}

		user.setUserRole(related_userrole);
		related_userrole.getUsers().add(user);
		user = userDAO.store(user);
		userDAO.flush();

		related_userrole = userRoleDAO.store(related_userrole);
		userRoleDAO.flush();

		return user;
	}

	/**
	 * Delete an existing Timestamp entity
	 * 
	 */
	@Transactional
	public User deleteUserTimestamp(Integer user_userId, Integer related_timestamp_timestampId) {
		User user = userDAO.findUserByPrimaryKey(user_userId, -1, -1);
		Timestamp related_timestamp = timestampDAO.findTimestampByPrimaryKey(related_timestamp_timestampId, -1, -1);

		user.setTimestamp(null);
		related_timestamp.getUsers().remove(user);
		user = userDAO.store(user);
		userDAO.flush();

		related_timestamp = timestampDAO.store(related_timestamp);
		timestampDAO.flush();

		timestampDAO.remove(related_timestamp);
		timestampDAO.flush();

		return user;
	}

	/**
	 * Save an existing FoodValidation entity
	 * 
	 */
	@Transactional
	public User saveUserFoodValidations(Integer userId, FoodValidation related_foodvalidations) {
		User user = userDAO.findUserByPrimaryKey(userId, -1, -1);
		FoodValidation existingfoodValidations = foodValidationDAO.findFoodValidationByPrimaryKey(related_foodvalidations.getFoodValidationId());

		// copy into the existing record to preserve existing relationships
		if (existingfoodValidations != null) {
			existingfoodValidations.setFoodValidationId(related_foodvalidations.getFoodValidationId());
			existingfoodValidations.setValue(related_foodvalidations.getValue());
			existingfoodValidations.setTime(related_foodvalidations.getTime());
			related_foodvalidations = existingfoodValidations;
		}

		related_foodvalidations.setUser(user);
		user.getFoodValidations().add(related_foodvalidations);
		related_foodvalidations = foodValidationDAO.store(related_foodvalidations);
		foodValidationDAO.flush();

		user = userDAO.store(user);
		userDAO.flush();

		return user;
	}

	/**
	 * Delete an existing UserRole entity
	 * 
	 */
	@Transactional
	public User deleteUserUserRole(Integer user_userId, Integer related_userrole_userRoleId) {
		User user = userDAO.findUserByPrimaryKey(user_userId, -1, -1);
		UserRole related_userrole = userRoleDAO.findUserRoleByPrimaryKey(related_userrole_userRoleId, -1, -1);

		user.setUserRole(null);
		related_userrole.getUsers().remove(user);
		user = userDAO.store(user);
		userDAO.flush();

		related_userrole = userRoleDAO.store(related_userrole);
		userRoleDAO.flush();

		userRoleDAO.remove(related_userrole);
		userRoleDAO.flush();

		return user;
	}

	/**
	 * Save an existing Doctor entity
	 * 
	 */
	@Transactional
	public User saveUserDoctors(Integer userId, Doctor related_doctors) {
		User user = userDAO.findUserByPrimaryKey(userId, -1, -1);
		Doctor existingdoctors = doctorDAO.findDoctorByPrimaryKey(related_doctors.getDoctorId());

		// copy into the existing record to preserve existing relationships
		if (existingdoctors != null) {
			existingdoctors.setDoctorId(related_doctors.getDoctorId());
			existingdoctors.setHospital(related_doctors.getHospital());
			existingdoctors.setCodeKey(related_doctors.getCodeKey());
			related_doctors = existingdoctors;
		}

		related_doctors.setUser(user);
		user.getDoctors().add(related_doctors);
		related_doctors = doctorDAO.store(related_doctors);
		doctorDAO.flush();

		user = userDAO.store(user);
		userDAO.flush();

		return user;
	}

	/**
	 * Delete an existing Kid entity
	 * 
	 */
	@Transactional
	public User deleteUserKids(Integer user_userId, Integer related_kids_kidId) {
		Kid related_kids = kidDAO.findKidByPrimaryKey(related_kids_kidId, -1, -1);

		User user = userDAO.findUserByPrimaryKey(user_userId, -1, -1);

		related_kids.setUser(null);
		user.getKids().remove(related_kids);

		kidDAO.remove(related_kids);
		kidDAO.flush();

		return user;
	}

	/**
	 * Delete an existing FoodValidation entity
	 * 
	 */
	@Transactional
	public User deleteUserFoodValidations(Integer user_userId, Integer related_foodvalidations_foodValidationId) {
		FoodValidation related_foodvalidations = foodValidationDAO.findFoodValidationByPrimaryKey(related_foodvalidations_foodValidationId, -1, -1);

		User user = userDAO.findUserByPrimaryKey(user_userId, -1, -1);

		related_foodvalidations.setUser(null);
		user.getFoodValidations().remove(related_foodvalidations);

		foodValidationDAO.remove(related_foodvalidations);
		foodValidationDAO.flush();

		return user;
	}

	/**
	 * Save an existing Timestamp entity
	 * 
	 */
	@Transactional
	public User saveUserTimestamp(Integer userId, Timestamp related_timestamp) {
		User user = userDAO.findUserByPrimaryKey(userId, -1, -1);
		Timestamp existingtimestamp = timestampDAO.findTimestampByPrimaryKey(related_timestamp.getTimestampId());

		// copy into the existing record to preserve existing relationships
		if (existingtimestamp != null) {
			existingtimestamp.setTimestampId(related_timestamp.getTimestampId());
			existingtimestamp.setLoginDate(related_timestamp.getLoginDate());
			existingtimestamp.setLogoutDate(related_timestamp.getLogoutDate());
			existingtimestamp.setCreateDate(related_timestamp.getCreateDate());
			existingtimestamp.setUpdateDate(related_timestamp.getUpdateDate());
			existingtimestamp.setIsActive(related_timestamp.getIsActive());
			related_timestamp = existingtimestamp;
		}

		user.setTimestamp(related_timestamp);
		related_timestamp.getUsers().add(user);
		user = userDAO.store(user);
		userDAO.flush();

		related_timestamp = timestampDAO.store(related_timestamp);
		timestampDAO.flush();

		return user;
	}

	/**
	 * Save an existing Parent entity
	 * 
	 */
	@Transactional
	public User saveUserParents(Integer userId, Parent related_parents) {
		User user = userDAO.findUserByPrimaryKey(userId, -1, -1);
		Parent existingparents = parentDAO.findParentByPrimaryKey(related_parents.getParentId());

		// copy into the existing record to preserve existing relationships
		if (existingparents != null) {
			existingparents.setParentId(related_parents.getParentId());
			existingparents.setOccupation(related_parents.getOccupation());
			existingparents.setAddress(related_parents.getAddress());
			related_parents = existingparents;
		}

		related_parents.setUser(user);
		user.getParents().add(related_parents);
		related_parents = parentDAO.store(related_parents);
		parentDAO.flush();

		user = userDAO.store(user);
		userDAO.flush();

		return user;
	}

	/**
	 * Return a count of all User entity
	 * 
	 */
	@Transactional
	public Integer countUsers() {
		return ((Long) userDAO.createQuerySingleResult("select count(o) from User o").getSingleResult()).intValue();
	}

	/**
	 */
	@Transactional
	public User findUserByPrimaryKey(Integer userId) {
		return userDAO.findUserByPrimaryKey(userId);
	}

	/**
	 * Delete an existing Doctor entity
	 * 
	 */
	@Transactional
	public User deleteUserDoctors(Integer user_userId, Integer related_doctors_doctorId) {
		Doctor related_doctors = doctorDAO.findDoctorByPrimaryKey(related_doctors_doctorId, -1, -1);

		User user = userDAO.findUserByPrimaryKey(user_userId, -1, -1);

		related_doctors.setUser(null);
		user.getDoctors().remove(related_doctors);

		doctorDAO.remove(related_doctors);
		doctorDAO.flush();

		return user;
	}
}
