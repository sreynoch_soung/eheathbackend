package ehealth.service;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ehealth.dao.AccountSettingDAO;
import ehealth.dao.KidDAO;
import ehealth.dao.ParentDAO;
import ehealth.domain.AccountSetting;
import ehealth.domain.Kid;
import ehealth.domain.Parent;

/**
 * Spring service that handles CRUD requests for AccountSetting entities
 * 
 */

@Service("AccountSettingService")

@Transactional
public class AccountSettingServiceImpl implements AccountSettingService {

	/**
	 * DAO injected by Spring that manages AccountSetting entities
	 * 
	 */
	@Autowired
	private AccountSettingDAO accountSettingDAO;

	/**
	 * DAO injected by Spring that manages Kid entities
	 * 
	 */
	@Autowired
	private KidDAO kidDAO;

	/**
	 * DAO injected by Spring that manages Parent entities
	 * 
	 */
	@Autowired
	private ParentDAO parentDAO;

	/**
	 * Instantiates a new AccountSettingServiceImpl.
	 *
	 */
	public AccountSettingServiceImpl() {
	}

	/**
	 * Delete an existing Kid entity
	 * 
	 */
	@Transactional
	public AccountSetting deleteAccountSettingKids(Integer accountsetting_accountSettingId, Integer related_kids_kidId) {
		Kid related_kids = kidDAO.findKidByPrimaryKey(related_kids_kidId, -1, -1);

		AccountSetting accountsetting = accountSettingDAO.findAccountSettingByPrimaryKey(accountsetting_accountSettingId, -1, -1);

		related_kids.setAccountSetting(null);
		accountsetting.getKids().remove(related_kids);

		kidDAO.remove(related_kids);
		kidDAO.flush();

		return accountsetting;
	}

	/**
	 * Save an existing AccountSetting entity
	 * 
	 */
	@Transactional
	public AccountSetting saveAccountSetting(AccountSetting accountsetting) {
		AccountSetting existingAccountSetting = accountSettingDAO.findAccountSettingByPrimaryKey(accountsetting.getAccountSettingId());

		if (existingAccountSetting != null) {
			if (existingAccountSetting != accountsetting) {
				existingAccountSetting.setAccountSettingId(accountsetting.getAccountSettingId());
				existingAccountSetting.setNumPhotoValidateGroup(accountsetting.getNumPhotoValidateGroup());
			}
			accountsetting = accountSettingDAO.store(existingAccountSetting);
		} else {
			accountsetting = accountSettingDAO.store(accountsetting);
		}
		accountSettingDAO.flush();
		return accountsetting;
	}

	/**
	 * Delete an existing Parent entity
	 * 
	 */
	@Transactional
	public AccountSetting deleteAccountSettingParents(Integer accountsetting_accountSettingId, Integer related_parents_parentId) {
		Parent related_parents = parentDAO.findParentByPrimaryKey(related_parents_parentId, -1, -1);

		AccountSetting accountsetting = accountSettingDAO.findAccountSettingByPrimaryKey(accountsetting_accountSettingId, -1, -1);

		related_parents.setAccountSetting(null);
		accountsetting.getParents().remove(related_parents);

		parentDAO.remove(related_parents);
		parentDAO.flush();

		return accountsetting;
	}

	/**
	 * Delete an existing AccountSetting entity
	 * 
	 */
	@Transactional
	public void deleteAccountSetting(AccountSetting accountsetting) {
		accountSettingDAO.remove(accountsetting);
		accountSettingDAO.flush();
	}

	/**
	 * Load an existing AccountSetting entity
	 * 
	 */
	@Transactional
	public Set<AccountSetting> loadAccountSettings() {
		return accountSettingDAO.findAllAccountSettings();
	}

	/**
	 * Save an existing Parent entity
	 * 
	 */
	@Transactional
	public AccountSetting saveAccountSettingParents(Integer accountSettingId, Parent related_parents) {
		AccountSetting accountsetting = accountSettingDAO.findAccountSettingByPrimaryKey(accountSettingId, -1, -1);
		Parent existingparents = parentDAO.findParentByPrimaryKey(related_parents.getParentId());

		// copy into the existing record to preserve existing relationships
		if (existingparents != null) {
			existingparents.setParentId(related_parents.getParentId());
			existingparents.setOccupation(related_parents.getOccupation());
			existingparents.setAddress(related_parents.getAddress());
			related_parents = existingparents;
		} else {
			related_parents = parentDAO.store(related_parents);
			parentDAO.flush();
		}

		related_parents.setAccountSetting(accountsetting);
		accountsetting.getParents().add(related_parents);
		related_parents = parentDAO.store(related_parents);
		parentDAO.flush();

		accountsetting = accountSettingDAO.store(accountsetting);
		accountSettingDAO.flush();

		return accountsetting;
	}

	/**
	 * Save an existing Kid entity
	 * 
	 */
	@Transactional
	public AccountSetting saveAccountSettingKids(Integer accountSettingId, Kid related_kids) {
		AccountSetting accountsetting = accountSettingDAO.findAccountSettingByPrimaryKey(accountSettingId, -1, -1);
		Kid existingkids = kidDAO.findKidByPrimaryKey(related_kids.getKidId());

		// copy into the existing record to preserve existing relationships
		if (existingkids != null) {
			existingkids.setKidId(related_kids.getKidId());
			existingkids.setHigh(related_kids.getHigh());
			existingkids.setWeight(related_kids.getWeight());
			existingkids.setAddress(related_kids.getAddress());
			existingkids.setCodeKey(related_kids.getCodeKey());
			related_kids = existingkids;
		} else {
			related_kids = kidDAO.store(related_kids);
			kidDAO.flush();
		}

		related_kids.setAccountSetting(accountsetting);
		accountsetting.getKids().add(related_kids);
		related_kids = kidDAO.store(related_kids);
		kidDAO.flush();

		accountsetting = accountSettingDAO.store(accountsetting);
		accountSettingDAO.flush();

		return accountsetting;
	}

	/**
	 * Return all AccountSetting entity
	 * 
	 */
	@Transactional
	public List<AccountSetting> findAllAccountSettings(Integer startResult, Integer maxRows) {
		return new java.util.ArrayList<AccountSetting>(accountSettingDAO.findAllAccountSettings(startResult, maxRows));
	}

	/**
	 */
	@Transactional
	public AccountSetting findAccountSettingByPrimaryKey(Integer accountSettingId) {
		return accountSettingDAO.findAccountSettingByPrimaryKey(accountSettingId);
	}

	/**
	 * Return a count of all AccountSetting entity
	 * 
	 */
	@Transactional
	public Integer countAccountSettings() {
		return ((Long) accountSettingDAO.createQuerySingleResult("select count(o) from AccountSetting o").getSingleResult()).intValue();
	}
}
