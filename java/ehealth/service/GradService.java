
package ehealth.service;

import ehealth.domain.Grad;
import ehealth.domain.Kid;

import java.util.List;
import java.util.Set;

/**
 * Spring service that handles CRUD requests for Grad entities
 * 
 */
public interface GradService {

	/**
	* Save an existing Kid entity
	* 
	 */
	public Grad saveGradKid(Integer gradId, Kid related_kid);

	/**
	* Delete an existing Kid entity
	* 
	 */
	public Grad deleteGradKid(Integer grad_gradId, Integer related_kid_kidId);

	/**
	* Save an existing Grad entity
	* 
	 */
	public Grad saveGrad(Grad grad);

	/**
	* Load an existing Grad entity
	* 
	 */
	public Set<Grad> loadGrads();

	/**
	 */
	public Grad findGradByPrimaryKey(Integer gradId_1);

	/**
	* Delete an existing Grad entity
	* 
	 */
	public void deleteGrad(Grad grad_1);

	/**
	* Return a count of all Grad entity
	* 
	 */
	public Integer countGrads();

	/**
	* Return all Grad entity
	* 
	 */
	public List<Grad> findAllGrads(Integer startResult, Integer maxRows);
}