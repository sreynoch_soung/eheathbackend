
package ehealth.service;

import ehealth.domain.User;
import ehealth.domain.UserRole;

import java.util.List;
import java.util.Set;

/**
 * Spring service that handles CRUD requests for UserRole entities
 * 
 */
public interface UserRoleService {

	/**
	* Delete an existing User entity
	* 
	 */
	public UserRole deleteUserRoleUsers(Integer userrole_userRoleId, Integer related_users_userId);

	/**
	 */
	public UserRole findUserRoleByPrimaryKey(Integer userRoleId);

	/**
	* Load an existing UserRole entity
	* 
	 */
	public Set<UserRole> loadUserRoles();

	/**
	* Delete an existing UserRole entity
	* 
	 */
	public void deleteUserRole(UserRole userrole);

	/**
	* Save an existing UserRole entity
	* 
	 */
	public void saveUserRole(UserRole userrole_1);

	/**
	* Return all UserRole entity
	* 
	 */
	public List<UserRole> findAllUserRoles(Integer startResult, Integer maxRows);

	/**
	* Save an existing User entity
	* 
	 */
	public UserRole saveUserRoleUsers(Integer userRoleId_1, User related_users);

	/**
	* Return a count of all UserRole entity
	* 
	 */
	public Integer countUserRoles();
}