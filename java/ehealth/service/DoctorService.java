
package ehealth.service;

import ehealth.domain.Doctor;
import ehealth.domain.ExerciseTarget;
import ehealth.domain.Kid;
import ehealth.domain.NutritionTarget;
import ehealth.domain.User;

import java.util.List;
import java.util.Set;

/**
 * Spring service that handles CRUD requests for Doctor entities
 * 
 */
public interface DoctorService {

	/**
	* Save an existing NutritionTarget entity
	* 
	 */
	public Doctor saveDoctorNutritionTargets(Integer doctorId, NutritionTarget related_nutritiontargets);

	/**
	* Delete an existing Kid entity
	* 
	 */
	public Doctor deleteDoctorKids(Integer doctor_doctorId, Integer related_kids_kidId);

	/**
	 */
	public Doctor findDoctorByPrimaryKey(Integer doctorId_1);

	/**
	 */
	public Set<Doctor> findDoctorByCodeKey(String doctorCodeKey);

	/**
	* Return a count of all Doctor entity
	* 
	 */
	public Integer countDoctors();

	/**
	* Save an existing User entity
	* 
	 */
	public Doctor saveDoctorUser(Integer doctorId_2, User related_user);

	/**
	* Load an existing Doctor entity
	* 
	 */
	public Set<Doctor> loadDoctors();

	/**
	* Delete an existing NutritionTarget entity
	* 
	 */
	public Doctor deleteDoctorNutritionTargets(Integer doctor_doctorId_1, Integer related_nutritiontargets_nutritionTargetId);

	/**
	* Delete an existing User entity
	* 
	 */
	public Doctor deleteDoctorUser(Integer doctor_doctorId_2, Integer related_user_userId);

	/**
	* Delete an existing ExerciseTarget entity
	* 
	 */
	public Doctor deleteDoctorExerciseTargets(Integer doctor_doctorId_3, Integer related_exercisetargets_exerciseTargetId);

	/**
	* Return all Doctor entity
	* 
	 */
	public List<Doctor> findAllDoctors(Integer startResult, Integer maxRows);

	/**
	* Delete an existing Doctor entity
	* 
	 */
	public void deleteDoctor(Doctor doctor);

	/**
	* Save an existing ExerciseTarget entity
	* 
	 */
	public Doctor saveDoctorExerciseTargets(Integer doctorId_3, ExerciseTarget related_exercisetargets);

	/**
	* Save an existing Kid entity
	* 
	 */
	public Doctor saveDoctorKids(Integer doctorId_4, Kid related_kids);

	/**
	* Save an existing Doctor entity
	* 
	 */
	public Doctor saveDoctor(Doctor doctor_1);
}