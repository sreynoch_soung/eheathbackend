package ehealth.service;

import ehealth.dao.BonusDAO;
import ehealth.dao.KidDAO;

import ehealth.domain.Bonus;
import ehealth.domain.Kid;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;

import org.springframework.transaction.annotation.Transactional;

/**
 * Spring service that handles CRUD requests for Bonus entities
 * 
 */

@Service("BonusService")

@Transactional
public class BonusServiceImpl implements BonusService {

	/**
	 * DAO injected by Spring that manages Bonus entities
	 * 
	 */
	@Autowired
	private BonusDAO bonusDAO;

	/**
	 * DAO injected by Spring that manages Kid entities
	 * 
	 */
	@Autowired
	private KidDAO kidDAO;

	/**
	 * Instantiates a new BonusServiceImpl.
	 *
	 */
	public BonusServiceImpl() {
	}

	/**
	 * Return all Bonus entity
	 * 
	 */
	@Transactional
	public List<Bonus> findAllBonuss(Integer startResult, Integer maxRows) {
		return new java.util.ArrayList<Bonus>(bonusDAO.findAllBonuss(startResult, maxRows));
	}

	/**
	 * Load an existing Bonus entity
	 * 
	 */
	@Transactional
	public Set<Bonus> loadBonuss() {
		return bonusDAO.findAllBonuss();
	}

	/**
	 */
	@Transactional
	public Bonus findBonusByPrimaryKey(Integer bonusId) {
		return bonusDAO.findBonusByPrimaryKey(bonusId);
	}

	/**
	 * Return a count of all Bonus entity
	 * 
	 */
	@Transactional
	public Integer countBonuss() {
		return ((Long) bonusDAO.createQuerySingleResult("select count(o) from Bonus o").getSingleResult()).intValue();
	}

	/**
	 * Delete an existing Bonus entity
	 * 
	 */
	@Transactional
	public void deleteBonus(Bonus bonus) {
		bonusDAO.remove(bonus);
		bonusDAO.flush();
	}

	/**
	 * Save an existing Bonus entity
	 * 
	 */
	@Transactional
	public Bonus saveBonus(Bonus bonus) {
		Bonus existingBonus = bonusDAO.findBonusByPrimaryKey(bonus.getBonusId());

		if (existingBonus != null) {
			if (existingBonus != bonus) {
				existingBonus.setBonusId(bonus.getBonusId());
				existingBonus.setPoint(bonus.getPoint());
				existingBonus.setActivityType(bonus.getActivityType());
			}
			bonus = bonusDAO.store(existingBonus);
		} else {
			bonus = bonusDAO.store(bonus);
		}
		bonusDAO.flush();
		return bonus;
	}

	/**
	 * Delete an existing Kid entity
	 * 
	 */
	@Transactional
	public Bonus deleteBonusKid(Integer bonus_bonusId, Integer related_kid_kidId) {
		Bonus bonus = bonusDAO.findBonusByPrimaryKey(bonus_bonusId, -1, -1);
		Kid related_kid = kidDAO.findKidByPrimaryKey(related_kid_kidId, -1, -1);

		bonus.setKid(null);
		related_kid.getBonuses().remove(bonus);
		bonus = bonusDAO.store(bonus);
		bonusDAO.flush();

		related_kid = kidDAO.store(related_kid);
		kidDAO.flush();

		kidDAO.remove(related_kid);
		kidDAO.flush();

		return bonus;
	}

	/**
	 * Save an existing Kid entity
	 * 
	 */
	@Transactional
	public Bonus saveBonusKid(Integer bonusId, Kid related_kid) {
		Bonus bonus = bonusDAO.findBonusByPrimaryKey(bonusId, -1, -1);
		Kid existingkid = kidDAO.findKidByPrimaryKey(related_kid.getKidId());

		// copy into the existing record to preserve existing relationships
		if (existingkid != null) {
			existingkid.setKidId(related_kid.getKidId());
			existingkid.setHigh(related_kid.getHigh());
			existingkid.setWeight(related_kid.getWeight());
			existingkid.setAddress(related_kid.getAddress());
			existingkid.setCodeKey(related_kid.getCodeKey());
			related_kid = existingkid;
		}

		bonus.setKid(related_kid);
		related_kid.getBonuses().add(bonus);
		bonus = bonusDAO.store(bonus);
		bonusDAO.flush();

		related_kid = kidDAO.store(related_kid);
		kidDAO.flush();

		return bonus;
	}
}
