package ehealth.service;

import ehealth.dao.ExerciseInputDAO;
import ehealth.dao.ExerciseTypeDAO;

import ehealth.domain.ExerciseInput;
import ehealth.domain.ExerciseType;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;

import org.springframework.transaction.annotation.Transactional;

/**
 * Spring service that handles CRUD requests for ExerciseType entities
 * 
 */

@Service("ExerciseTypeService")

@Transactional
public class ExerciseTypeServiceImpl implements ExerciseTypeService {

	/**
	 * DAO injected by Spring that manages ExerciseInput entities
	 * 
	 */
	@Autowired
	private ExerciseInputDAO exerciseInputDAO;

	/**
	 * DAO injected by Spring that manages ExerciseType entities
	 * 
	 */
	@Autowired
	private ExerciseTypeDAO exerciseTypeDAO;

	/**
	 * Instantiates a new ExerciseTypeServiceImpl.
	 *
	 */
	public ExerciseTypeServiceImpl() {
	}

	/**
	 * Return a count of all ExerciseType entity
	 * 
	 */
	@Transactional
	public Integer countExerciseTypes() {
		return ((Long) exerciseTypeDAO.createQuerySingleResult("select count(o) from ExerciseType o").getSingleResult()).intValue();
	}

	/**
	 * Load an existing ExerciseType entity
	 * 
	 */
	@Transactional
	public Set<ExerciseType> loadExerciseTypes() {
		return exerciseTypeDAO.findAllExerciseTypes();
	}

	/**
	 * Delete an existing ExerciseInput entity
	 * 
	 */
	@Transactional
	public ExerciseType deleteExerciseTypeExerciseInputs(Integer exercisetype_exerciseTypeId, Integer related_exerciseinputs_exerciseInputId) {
		ExerciseInput related_exerciseinputs = exerciseInputDAO.findExerciseInputByPrimaryKey(related_exerciseinputs_exerciseInputId, -1, -1);

		ExerciseType exercisetype = exerciseTypeDAO.findExerciseTypeByPrimaryKey(exercisetype_exerciseTypeId, -1, -1);

		related_exerciseinputs.setExerciseType(null);
		exercisetype.getExerciseInputs().remove(related_exerciseinputs);

		exerciseInputDAO.remove(related_exerciseinputs);
		exerciseInputDAO.flush();

		return exercisetype;
	}

	/**
	 */
	@Transactional
	public ExerciseType findExerciseTypeByPrimaryKey(Integer exerciseTypeId) {
		return exerciseTypeDAO.findExerciseTypeByPrimaryKey(exerciseTypeId);
	}

	/**
	 * Save an existing ExerciseType entity
	 * 
	 */
	@Transactional
	public ExerciseType saveExerciseType(ExerciseType exercisetype) {
		ExerciseType existingExerciseType = exerciseTypeDAO.findExerciseTypeByPrimaryKey(exercisetype.getExerciseTypeId());

		if (existingExerciseType != null) {
			if (existingExerciseType != exercisetype) {
				existingExerciseType.setExerciseTypeId(exercisetype.getExerciseTypeId());
				existingExerciseType.setTypeName(exercisetype.getTypeName());
			}
			exercisetype = exerciseTypeDAO.store(existingExerciseType);
		} else {
			exercisetype = exerciseTypeDAO.store(exercisetype);
		}
		exerciseTypeDAO.flush();
		return exercisetype;
	}

	/**
	 * Delete an existing ExerciseType entity
	 * 
	 */
	@Transactional
	public void deleteExerciseType(ExerciseType exercisetype) {
		exerciseTypeDAO.remove(exercisetype);
		exerciseTypeDAO.flush();
	}

	/**
	 * Return all ExerciseType entity
	 * 
	 */
	@Transactional
	public List<ExerciseType> findAllExerciseTypes(Integer startResult, Integer maxRows) {
		return new java.util.ArrayList<ExerciseType>(exerciseTypeDAO.findAllExerciseTypes(startResult, maxRows));
	}

	/**
	 * Save an existing ExerciseInput entity
	 * 
	 */
	@Transactional
	public ExerciseType saveExerciseTypeExerciseInputs(Integer exerciseTypeId, ExerciseInput related_exerciseinputs) {
		ExerciseType exercisetype = exerciseTypeDAO.findExerciseTypeByPrimaryKey(exerciseTypeId, -1, -1);
		ExerciseInput existingexerciseInputs = exerciseInputDAO.findExerciseInputByPrimaryKey(related_exerciseinputs.getExerciseInputId());

		// copy into the existing record to preserve existing relationships
		if (existingexerciseInputs != null) {
			existingexerciseInputs.setExerciseInputId(related_exerciseinputs.getExerciseInputId());
			existingexerciseInputs.setDate(related_exerciseinputs.getDate());
			existingexerciseInputs.setValue(related_exerciseinputs.getValue());
			existingexerciseInputs.setFinalValidationValue(related_exerciseinputs.getFinalValidationValue());
			related_exerciseinputs = existingexerciseInputs;
		}

		related_exerciseinputs.setExerciseType(exercisetype);
		exercisetype.getExerciseInputs().add(related_exerciseinputs);
		related_exerciseinputs = exerciseInputDAO.store(related_exerciseinputs);
		exerciseInputDAO.flush();

		exercisetype = exerciseTypeDAO.store(exercisetype);
		exerciseTypeDAO.flush();

		return exercisetype;
	}
}
