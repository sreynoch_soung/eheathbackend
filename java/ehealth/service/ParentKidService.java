
package ehealth.service;

import ehealth.domain.Kid;
import ehealth.domain.Parent;
import ehealth.domain.ParentKid;

import java.util.List;
import java.util.Set;

/**
 * Spring service that handles CRUD requests for ParentKid entities
 * 
 */
public interface ParentKidService {

	/**
	 */
	public ParentKid findParentKidByPrimaryKey(Integer parentKidId);

	/**
	* Save an existing Parent entity
	* 
	 */
	public ParentKid saveParentKidParent(Integer parentKidId_1, Parent related_parent);

	/**
	* Return all ParentKid entity
	* 
	 */
	public List<ParentKid> findAllParentKids(Integer startResult, Integer maxRows);

	/**
	* Return a count of all ParentKid entity
	* 
	 */
	public Integer countParentKids();

	/**
	* Delete an existing Parent entity
	* 
	 */
	public ParentKid deleteParentKidParent(Integer parentkid_parentKidId, Integer related_parent_parentId);

	/**
	* Save an existing Kid entity
	* 
	 */
	public ParentKid saveParentKidKid(Integer parentKidId_2, Kid related_kid);

	/**
	* Delete an existing ParentKid entity
	* 
	 */
	public void deleteParentKid(ParentKid parentkid);

	/**
	* Delete an existing Kid entity
	* 
	 */
	public ParentKid deleteParentKidKid(Integer parentkid_parentKidId_1, Integer related_kid_kidId);

	/**
	* Save an existing ParentKid entity
	* 
	 */
	public ParentKid saveParentKid(ParentKid parentkid_1);

	/**
	* Load an existing ParentKid entity
	* 
	 */
	public Set<ParentKid> loadParentKids();
}