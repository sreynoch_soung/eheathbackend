
package ehealth.service;

import ehealth.domain.ChatRecord;
import ehealth.domain.KidChat;

import java.util.List;
import java.util.Set;

/**
 * Spring service that handles CRUD requests for ChatRecord entities
 * 
 */
public interface ChatRecordService {

	/**
	* Save an existing KidChat entity
	* 
	 */
	public ChatRecord saveChatRecordKidChats(Integer chatRecordId, KidChat related_kidchats);

	/**
	* Save an existing ChatRecord entity
	* 
	 */
	public ChatRecord saveChatRecord(ChatRecord chatrecord);

	/**
	* Return all ChatRecord entity
	* 
	 */
	public List<ChatRecord> findAllChatRecords(Integer startResult, Integer maxRows);

	/**
	 */
	public ChatRecord findChatRecordByPrimaryKey(Integer chatRecordId_1);

	/**
	* Return a count of all ChatRecord entity
	* 
	 */
	public Integer countChatRecords();

	/**
	* Delete an existing ChatRecord entity
	* 
	 */
	public void deleteChatRecord(ChatRecord chatrecord_1);

	/**
	* Delete an existing KidChat entity
	* 
	 */
	public ChatRecord deleteChatRecordKidChats(Integer chatrecord_chatRecordId, Integer related_kidchats_kidChatId);

	/**
	* Load an existing ChatRecord entity
	* 
	 */
	public Set<ChatRecord> loadChatRecords();
}