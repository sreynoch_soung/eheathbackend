
package ehealth.service;

import ehealth.domain.FoodPhoto;
import ehealth.domain.FoodTageInput;
import ehealth.domain.FoodTageType;

import java.util.List;
import java.util.Set;

/**
 * Spring service that handles CRUD requests for FoodTageInput entities
 * 
 */
public interface FoodTageInputService {

	/**
	* Save an existing FoodTageInput entity
	* 
	 */
	public FoodTageInput saveFoodTageInput(FoodTageInput foodtageinput);

	/**
	* Delete an existing FoodPhoto entity
	* 
	 */
	public FoodTageInput deleteFoodTageInputFoodPhoto(Integer foodtageinput_foodTageInputId, Integer related_foodphoto_foodPhotoId);

	/**
	* Save an existing FoodPhoto entity
	* 
	 */
	public FoodTageInput saveFoodTageInputFoodPhoto(Integer foodTageInputId, FoodPhoto related_foodphoto);

	/**
	 */
	public FoodTageInput findFoodTageInputByPrimaryKey(Integer foodTageInputId_1);

	/**
	* Return a count of all FoodTageInput entity
	* 
	 */
	public Integer countFoodTageInputs();

	/**
	* Load an existing FoodTageInput entity
	* 
	 */
	public Set<FoodTageInput> loadFoodTageInputs();

	/**
	* Delete an existing FoodTageType entity
	* 
	 */
	public FoodTageInput deleteFoodTageInputFoodTageType(Integer foodtageinput_foodTageInputId_1, Integer related_foodtagetype_foodTageTypeId);

	/**
	* Delete an existing FoodTageInput entity
	* 
	 */
	public void deleteFoodTageInput(FoodTageInput foodtageinput_1);

	/**
	* Save an existing FoodTageType entity
	* 
	 */
	public FoodTageInput saveFoodTageInputFoodTageType(Integer foodTageInputId_2, FoodTageType related_foodtagetype);

	/**
	* Return all FoodTageInput entity
	* 
	 */
	public List<FoodTageInput> findAllFoodTageInputs(Integer startResult, Integer maxRows);
}