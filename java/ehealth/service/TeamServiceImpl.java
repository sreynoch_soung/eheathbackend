package ehealth.service;

import ehealth.dao.KidDAO;
import ehealth.dao.LeagueDAO;
import ehealth.dao.MatchDAO;
import ehealth.dao.TeamDAO;

import ehealth.domain.Kid;
import ehealth.domain.League;
import ehealth.domain.Match;
import ehealth.domain.Team;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;

import org.springframework.transaction.annotation.Transactional;

/**
 * Spring service that handles CRUD requests for Team entities
 * 
 */

@Service("TeamService")

@Transactional
public class TeamServiceImpl implements TeamService {

	/**
	 * DAO injected by Spring that manages Kid entities
	 * 
	 */
	@Autowired
	private KidDAO kidDAO;

	/**
	 * DAO injected by Spring that manages League entities
	 * 
	 */
	@Autowired
	private LeagueDAO leagueDAO;

	/**
	 * DAO injected by Spring that manages Match entities
	 * 
	 */
	@Autowired
	private MatchDAO matchDAO;

	/**
	 * DAO injected by Spring that manages Team entities
	 * 
	 */
	@Autowired
	private TeamDAO teamDAO;

	/**
	 * Instantiates a new TeamServiceImpl.
	 *
	 */
	public TeamServiceImpl() {
	}

	/**
	 * Save an existing League entity
	 * 
	 */
	@Transactional
	public Team saveTeamLeagues(Integer teamId, League related_leagues) {
		Team team = teamDAO.findTeamByPrimaryKey(teamId, -1, -1);
		League existingleagues = leagueDAO.findLeagueByPrimaryKey(related_leagues.getLeagueId());

		// copy into the existing record to preserve existing relationships
		if (existingleagues != null) {
			existingleagues.setLeagueId(related_leagues.getLeagueId());
			existingleagues.setLeagueName(related_leagues.getLeagueName());
			existingleagues.setMatchDate(related_leagues.getMatchDate());
			existingleagues.setPoint(related_leagues.getPoint());
			related_leagues = existingleagues;
		}

		related_leagues.setTeam(team);
		team.getLeagues().add(related_leagues);
		related_leagues = leagueDAO.store(related_leagues);
		leagueDAO.flush();

		team = teamDAO.store(team);
		teamDAO.flush();

		return team;
	}

	/**
	 * Return a count of all Team entity
	 * 
	 */
	@Transactional
	public Integer countTeams() {
		return ((Long) teamDAO.createQuerySingleResult("select count(o) from Team o").getSingleResult()).intValue();
	}

	/**
	 * Save an existing Kid entity
	 * 
	 */
	@Transactional
	public Team saveTeamKids(Integer teamId, Kid related_kids) {
		Team team = teamDAO.findTeamByPrimaryKey(teamId, -1, -1);
		Kid existingkids = kidDAO.findKidByPrimaryKey(related_kids.getKidId());

		// copy into the existing record to preserve existing relationships
		if (existingkids != null) {
			existingkids.setKidId(related_kids.getKidId());
			existingkids.setHigh(related_kids.getHigh());
			existingkids.setWeight(related_kids.getWeight());
			existingkids.setAddress(related_kids.getAddress());
			existingkids.setCodeKey(related_kids.getCodeKey());
			related_kids = existingkids;
		} else {
			related_kids = kidDAO.store(related_kids);
			kidDAO.flush();
		}

		related_kids.setTeam(team);
		team.getKids().add(related_kids);
		related_kids = kidDAO.store(related_kids);
		kidDAO.flush();

		team = teamDAO.store(team);
		teamDAO.flush();

		return team;
	}

	/**
	 * Load an existing Team entity
	 * 
	 */
	@Transactional
	public Set<Team> loadTeams() {
		return teamDAO.findAllTeams();
	}

	/**
	 * Delete an existing Match entity
	 * 
	 */
	@Transactional
	public Team deleteTeamMatchsForTeamId1(Integer team_teamId, Integer related_matchsforteamid1_matchId) {
		Match related_matchsforteamid1 = matchDAO.findMatchByPrimaryKey(related_matchsforteamid1_matchId, -1, -1);

		Team team = teamDAO.findTeamByPrimaryKey(team_teamId, -1, -1);

		related_matchsforteamid1.setTeamByTeamId2(null);
		team.getMatchsForTeamId2().remove(related_matchsforteamid1);

		matchDAO.remove(related_matchsforteamid1);
		matchDAO.flush();

		return team;
	}

	/**
	 * Save an existing Match entity
	 * 
	 */
	@Transactional
	public Team saveTeamMatchsForTeamId2(Integer teamId, Match related_matchsforteamid2) {
		Team team = teamDAO.findTeamByPrimaryKey(teamId, -1, -1);
		Match existingmatchsForTeamId2 = matchDAO.findMatchByPrimaryKey(related_matchsforteamid2.getMatchId());

		// copy into the existing record to preserve existing relationships
		if (existingmatchsForTeamId2 != null) {
			existingmatchsForTeamId2.setMatchId(related_matchsforteamid2.getMatchId());
			existingmatchsForTeamId2.setDate(related_matchsforteamid2.getDate());
			existingmatchsForTeamId2.setWinScore(related_matchsforteamid2.getWinScore());
			related_matchsforteamid2 = existingmatchsForTeamId2;
		}

		related_matchsforteamid2.setTeamByTeamId2(team);
		team.getMatchsForTeamId2().add(related_matchsforteamid2);
		related_matchsforteamid2 = matchDAO.store(related_matchsforteamid2);
		matchDAO.flush();

		team = teamDAO.store(team);
		teamDAO.flush();

		return team;
	}

	/**
	 * Save an existing Match entity
	 * 
	 */
	@Transactional
	public Team saveTeamMatchsForTeamIdWin(Integer teamId, Match related_matchsforteamidwin) {
		Team team = teamDAO.findTeamByPrimaryKey(teamId, -1, -1);
		Match existingmatchsForTeamIdWin = matchDAO.findMatchByPrimaryKey(related_matchsforteamidwin.getMatchId());

		// copy into the existing record to preserve existing relationships
		if (existingmatchsForTeamIdWin != null) {
			existingmatchsForTeamIdWin.setMatchId(related_matchsforteamidwin.getMatchId());
			existingmatchsForTeamIdWin.setDate(related_matchsforteamidwin.getDate());
			existingmatchsForTeamIdWin.setWinScore(related_matchsforteamidwin.getWinScore());
			related_matchsforteamidwin = existingmatchsForTeamIdWin;
		}

		related_matchsforteamidwin.setTeamByTeamId2(team);
		team.getMatchsForTeamId2().add(related_matchsforteamidwin);
		related_matchsforteamidwin = matchDAO.store(related_matchsforteamidwin);
		matchDAO.flush();

		team = teamDAO.store(team);
		teamDAO.flush();

		return team;
	}

	/**
	 */
	@Transactional
	public Team findTeamByPrimaryKey(Integer teamId) {
		return teamDAO.findTeamByPrimaryKey(teamId);
	}

	/**
	 * Delete an existing Kid entity
	 * 
	 */
	@Transactional
	public Team deleteTeamKids(Integer team_teamId, Integer related_kids_kidId) {
		Kid related_kids = kidDAO.findKidByPrimaryKey(related_kids_kidId, -1, -1);

		Team team = teamDAO.findTeamByPrimaryKey(team_teamId, -1, -1);

		related_kids.setTeam(null);
		team.getKids().remove(related_kids);

		kidDAO.remove(related_kids);
		kidDAO.flush();

		return team;
	}

	/**
	 * Save an existing Team entity
	 * 
	 */
	@Transactional
	public Team saveTeam(Team team) {
		Team existingTeam = teamDAO.findTeamByPrimaryKey(team.getTeamId());

		if (existingTeam != null) {
			if (existingTeam != team) {
				existingTeam.setTeamId(team.getTeamId());
				existingTeam.setName(team.getName());
				existingTeam.setNumMember(team.getNumMember());
			}
			team = teamDAO.store(existingTeam);
		} else {
			team = teamDAO.store(team);
		}
		teamDAO.flush();
		return team;
	}

	/**
	 * Return all Team entity
	 * 
	 */
	@Transactional
	public List<Team> findAllTeams(Integer startResult, Integer maxRows) {
		return new java.util.ArrayList<Team>(teamDAO.findAllTeams(startResult, maxRows));
	}

	/**
	 * Delete an existing League entity
	 * 
	 */
	@Transactional
	public Team deleteTeamLeagues(Integer team_teamId, Integer related_leagues_leagueId) {
		League related_leagues = leagueDAO.findLeagueByPrimaryKey(related_leagues_leagueId, -1, -1);

		Team team = teamDAO.findTeamByPrimaryKey(team_teamId, -1, -1);

		related_leagues.setTeam(null);
		team.getLeagues().remove(related_leagues);

		leagueDAO.remove(related_leagues);
		leagueDAO.flush();

		return team;
	}

	/**
	 * Delete an existing Match entity
	 * 
	 */
	@Transactional
	public Team deleteTeamMatchsForTeamIdWin(Integer team_teamId, Integer related_matchsforteamidwin_matchId) {
		Match related_matchsforteamidwin = matchDAO.findMatchByPrimaryKey(related_matchsforteamidwin_matchId, -1, -1);

		Team team = teamDAO.findTeamByPrimaryKey(team_teamId, -1, -1);

		related_matchsforteamidwin.setTeamByTeamId2(null);
		team.getMatchsForTeamId2().remove(related_matchsforteamidwin);

		matchDAO.remove(related_matchsforteamidwin);
		matchDAO.flush();

		return team;
	}

	/**
	 * Save an existing Match entity
	 * 
	 */
	@Transactional
	public Team saveTeamMatchsForTeamId1(Integer teamId, Match related_matchsforteamid1) {
		Team team = teamDAO.findTeamByPrimaryKey(teamId, -1, -1);
		Match existingmatchsForTeamId1 = matchDAO.findMatchByPrimaryKey(related_matchsforteamid1.getMatchId());

		// copy into the existing record to preserve existing relationships
		if (existingmatchsForTeamId1 != null) {
			existingmatchsForTeamId1.setMatchId(related_matchsforteamid1.getMatchId());
			existingmatchsForTeamId1.setDate(related_matchsforteamid1.getDate());
			existingmatchsForTeamId1.setWinScore(related_matchsforteamid1.getWinScore());
			related_matchsforteamid1 = existingmatchsForTeamId1;
		}

		related_matchsforteamid1.setTeamByTeamId2(team);
		team.getMatchsForTeamId2().add(related_matchsforteamid1);
		related_matchsforteamid1 = matchDAO.store(related_matchsforteamid1);
		matchDAO.flush();

		team = teamDAO.store(team);
		teamDAO.flush();

		return team;
	}

	/**
	 * Delete an existing Match entity
	 * 
	 */
	@Transactional
	public Team deleteTeamMatchsForTeamId2(Integer team_teamId, Integer related_matchsforteamid2_matchId) {
		Match related_matchsforteamid2 = matchDAO.findMatchByPrimaryKey(related_matchsforteamid2_matchId, -1, -1);

		Team team = teamDAO.findTeamByPrimaryKey(team_teamId, -1, -1);

		related_matchsforteamid2.setTeamByTeamId2(null);
		team.getMatchsForTeamId2().remove(related_matchsforteamid2);

		matchDAO.remove(related_matchsforteamid2);
		matchDAO.flush();

		return team;
	}

	/**
	 * Delete an existing Team entity
	 * 
	 */
	@Transactional
	public void deleteTeam(Team team) {
		teamDAO.remove(team);
		teamDAO.flush();
	}
}
