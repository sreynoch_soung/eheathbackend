package ehealth.service;

import ehealth.dao.EffortTypeDAO;
import ehealth.dao.ExerciseInputDAO;
import ehealth.dao.ExerciseTypeDAO;
import ehealth.dao.KidDAO;

import ehealth.domain.EffortType;
import ehealth.domain.ExerciseInput;
import ehealth.domain.ExerciseType;
import ehealth.domain.Kid;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;

import org.springframework.transaction.annotation.Transactional;

/**
 * Spring service that handles CRUD requests for ExerciseInput entities
 * 
 */

@Service("ExerciseInputService")

@Transactional
public class ExerciseInputServiceImpl implements ExerciseInputService {

	/**
	 * DAO injected by Spring that manages EffortType entities
	 * 
	 */
	@Autowired
	private EffortTypeDAO effortTypeDAO;

	/**
	 * DAO injected by Spring that manages ExerciseInput entities
	 * 
	 */
	@Autowired
	private ExerciseInputDAO exerciseInputDAO;

	/**
	 * DAO injected by Spring that manages ExerciseType entities
	 * 
	 */
	@Autowired
	private ExerciseTypeDAO exerciseTypeDAO;

	/**
	 * DAO injected by Spring that manages Kid entities
	 * 
	 */
	@Autowired
	private KidDAO kidDAO;

	/**
	 * Instantiates a new ExerciseInputServiceImpl.
	 *
	 */
	public ExerciseInputServiceImpl() {
	}

	/**
	 * Return all ExerciseInput entity
	 * 
	 */
	@Transactional
	public List<ExerciseInput> findAllExerciseInputs(Integer startResult, Integer maxRows) {
		return new java.util.ArrayList<ExerciseInput>(exerciseInputDAO.findAllExerciseInputs(startResult, maxRows));
	}

	/**
	 * Save an existing ExerciseInput entity
	 * 
	 */
	@Transactional
	public ExerciseInput saveExerciseInput(ExerciseInput exerciseinput) {
		ExerciseInput existingExerciseInput = exerciseInputDAO.findExerciseInputByPrimaryKey(exerciseinput.getExerciseInputId());

		if (existingExerciseInput != null) {
			if (existingExerciseInput != exerciseinput) {
				existingExerciseInput.setExerciseInputId(exerciseinput.getExerciseInputId());
				existingExerciseInput.setDate(exerciseinput.getDate());
				existingExerciseInput.setValue(exerciseinput.getValue());
				existingExerciseInput.setFinalValidationValue(exerciseinput.getFinalValidationValue());
			}
			exerciseinput = exerciseInputDAO.store(existingExerciseInput);
		} else {
			exerciseinput = exerciseInputDAO.store(exerciseinput);
		}
		exerciseInputDAO.flush();
		return exerciseinput;
	}

	/**
	 * Delete an existing ExerciseType entity
	 * 
	 */
	@Transactional
	public ExerciseInput deleteExerciseInputExerciseType(Integer exerciseinput_exerciseInputId, Integer related_exercisetype_exerciseTypeId) {
		ExerciseInput exerciseinput = exerciseInputDAO.findExerciseInputByPrimaryKey(exerciseinput_exerciseInputId, -1, -1);
		ExerciseType related_exercisetype = exerciseTypeDAO.findExerciseTypeByPrimaryKey(related_exercisetype_exerciseTypeId, -1, -1);

		exerciseinput.setExerciseType(null);
		related_exercisetype.getExerciseInputs().remove(exerciseinput);
		exerciseinput = exerciseInputDAO.store(exerciseinput);
		exerciseInputDAO.flush();

		related_exercisetype = exerciseTypeDAO.store(related_exercisetype);
		exerciseTypeDAO.flush();

		exerciseTypeDAO.remove(related_exercisetype);
		exerciseTypeDAO.flush();

		return exerciseinput;
	}

	/**
	 * Save an existing EffortType entity
	 * 
	 */
	@Transactional
	public ExerciseInput saveExerciseInputEffortType(Integer exerciseInputId, EffortType related_efforttype) {
		ExerciseInput exerciseinput = exerciseInputDAO.findExerciseInputByPrimaryKey(exerciseInputId, -1, -1);
		EffortType existingeffortType = effortTypeDAO.findEffortTypeByPrimaryKey(related_efforttype.getEffortTypeId());

		// copy into the existing record to preserve existing relationships
		if (existingeffortType != null) {
			existingeffortType.setEffortTypeId(related_efforttype.getEffortTypeId());
			existingeffortType.setTitle(related_efforttype.getTitle());
			related_efforttype = existingeffortType;
		}

		exerciseinput.setEffortType(related_efforttype);
		related_efforttype.getExerciseInputs().add(exerciseinput);
		exerciseinput = exerciseInputDAO.store(exerciseinput);
		exerciseInputDAO.flush();

		related_efforttype = effortTypeDAO.store(related_efforttype);
		effortTypeDAO.flush();

		return exerciseinput;
	}

	/**
	 */
	@Transactional
	public ExerciseInput findExerciseInputByPrimaryKey(Integer exerciseInputId) {
		return exerciseInputDAO.findExerciseInputByPrimaryKey(exerciseInputId);
	}

	/**
	 * Delete an existing EffortType entity
	 * 
	 */
	@Transactional
	public ExerciseInput deleteExerciseInputEffortType(Integer exerciseinput_exerciseInputId, Integer related_efforttype_effortTypeId) {
		ExerciseInput exerciseinput = exerciseInputDAO.findExerciseInputByPrimaryKey(exerciseinput_exerciseInputId, -1, -1);
		EffortType related_efforttype = effortTypeDAO.findEffortTypeByPrimaryKey(related_efforttype_effortTypeId, -1, -1);

		exerciseinput.setEffortType(null);
		related_efforttype.getExerciseInputs().remove(exerciseinput);
		exerciseinput = exerciseInputDAO.store(exerciseinput);
		exerciseInputDAO.flush();

		related_efforttype = effortTypeDAO.store(related_efforttype);
		effortTypeDAO.flush();

		effortTypeDAO.remove(related_efforttype);
		effortTypeDAO.flush();

		return exerciseinput;
	}

	/**
	 * Delete an existing Kid entity
	 * 
	 */
	@Transactional
	public ExerciseInput deleteExerciseInputKid(Integer exerciseinput_exerciseInputId, Integer related_kid_kidId) {
		ExerciseInput exerciseinput = exerciseInputDAO.findExerciseInputByPrimaryKey(exerciseinput_exerciseInputId, -1, -1);
		Kid related_kid = kidDAO.findKidByPrimaryKey(related_kid_kidId, -1, -1);

		exerciseinput.setKid(null);
		related_kid.getExerciseInputs().remove(exerciseinput);
		exerciseinput = exerciseInputDAO.store(exerciseinput);
		exerciseInputDAO.flush();

		related_kid = kidDAO.store(related_kid);
		kidDAO.flush();

		kidDAO.remove(related_kid);
		kidDAO.flush();

		return exerciseinput;
	}

	/**
	 * Save an existing ExerciseType entity
	 * 
	 */
	@Transactional
	public ExerciseInput saveExerciseInputExerciseType(Integer exerciseInputId, ExerciseType related_exercisetype) {
		ExerciseInput exerciseinput = exerciseInputDAO.findExerciseInputByPrimaryKey(exerciseInputId, -1, -1);
		ExerciseType existingexerciseType = exerciseTypeDAO.findExerciseTypeByPrimaryKey(related_exercisetype.getExerciseTypeId());

		// copy into the existing record to preserve existing relationships
		if (existingexerciseType != null) {
			existingexerciseType.setExerciseTypeId(related_exercisetype.getExerciseTypeId());
			existingexerciseType.setTypeName(related_exercisetype.getTypeName());
			related_exercisetype = existingexerciseType;
		}

		exerciseinput.setExerciseType(related_exercisetype);
		related_exercisetype.getExerciseInputs().add(exerciseinput);
		exerciseinput = exerciseInputDAO.store(exerciseinput);
		exerciseInputDAO.flush();

		related_exercisetype = exerciseTypeDAO.store(related_exercisetype);
		exerciseTypeDAO.flush();

		return exerciseinput;
	}

	/**
	 * Load an existing ExerciseInput entity
	 * 
	 */
	@Transactional
	public Set<ExerciseInput> loadExerciseInputs() {
		return exerciseInputDAO.findAllExerciseInputs();
	}

	/**
	 * Delete an existing ExerciseInput entity
	 * 
	 */
	@Transactional
	public void deleteExerciseInput(ExerciseInput exerciseinput) {
		exerciseInputDAO.remove(exerciseinput);
		exerciseInputDAO.flush();
	}

	/**
	 * Save an existing Kid entity
	 * 
	 */
	@Transactional
	public ExerciseInput saveExerciseInputKid(Integer exerciseInputId, Kid related_kid) {
		ExerciseInput exerciseinput = exerciseInputDAO.findExerciseInputByPrimaryKey(exerciseInputId, -1, -1);
		Kid existingkid = kidDAO.findKidByPrimaryKey(related_kid.getKidId());

		// copy into the existing record to preserve existing relationships
		if (existingkid != null) {
			existingkid.setKidId(related_kid.getKidId());
			existingkid.setHigh(related_kid.getHigh());
			existingkid.setWeight(related_kid.getWeight());
			existingkid.setAddress(related_kid.getAddress());
			existingkid.setCodeKey(related_kid.getCodeKey());
			related_kid = existingkid;
		}

		exerciseinput.setKid(related_kid);
		related_kid.getExerciseInputs().add(exerciseinput);
		exerciseinput = exerciseInputDAO.store(exerciseinput);
		exerciseInputDAO.flush();

		related_kid = kidDAO.store(related_kid);
		kidDAO.flush();

		return exerciseinput;
	}

	/**
	 * Return a count of all ExerciseInput entity
	 * 
	 */
	@Transactional
	public Integer countExerciseInputs() {
		return ((Long) exerciseInputDAO.createQuerySingleResult("select count(o) from ExerciseInput o").getSingleResult()).intValue();
	}
}
