package ehealth.service;

import ehealth.dao.FoodTageInputDAO;
import ehealth.dao.FoodTageTypeDAO;
import ehealth.dao.NutritionTargetDAO;

import ehealth.domain.FoodTageInput;
import ehealth.domain.FoodTageType;
import ehealth.domain.NutritionTarget;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;

import org.springframework.transaction.annotation.Transactional;

/**
 * Spring service that handles CRUD requests for FoodTageType entities
 * 
 */

@Service("FoodTageTypeService")

@Transactional
public class FoodTageTypeServiceImpl implements FoodTageTypeService {

	/**
	 * DAO injected by Spring that manages FoodTageInput entities
	 * 
	 */
	@Autowired
	private FoodTageInputDAO foodTageInputDAO;

	/**
	 * DAO injected by Spring that manages FoodTageType entities
	 * 
	 */
	@Autowired
	private FoodTageTypeDAO foodTageTypeDAO;

	/**
	 * DAO injected by Spring that manages NutritionTarget entities
	 * 
	 */
	@Autowired
	private NutritionTargetDAO nutritionTargetDAO;

	/**
	 * Instantiates a new FoodTageTypeServiceImpl.
	 *
	 */
	public FoodTageTypeServiceImpl() {
	}

	/**
	 * Return all FoodTageType entity
	 * 
	 */
	@Transactional
	public List<FoodTageType> findAllFoodTageTypes(Integer startResult, Integer maxRows) {
		return new java.util.ArrayList<FoodTageType>(foodTageTypeDAO.findAllFoodTageTypes(startResult, maxRows));
	}

	/**
	 */
	@Transactional
	public FoodTageType findFoodTageTypeByPrimaryKey(Integer foodTageTypeId) {
		return foodTageTypeDAO.findFoodTageTypeByPrimaryKey(foodTageTypeId);
	}

	/**
	 * Load an existing FoodTageType entity
	 * 
	 */
	@Transactional
	public Set<FoodTageType> loadFoodTageTypes() {
		return foodTageTypeDAO.findAllFoodTageTypes();
	}

	/**
	 * Save an existing NutritionTarget entity
	 * 
	 */
	@Transactional
	public FoodTageType saveFoodTageTypeNutritionTargets(Integer foodTageTypeId, NutritionTarget related_nutritiontargets) {
		FoodTageType foodtagetype = foodTageTypeDAO.findFoodTageTypeByPrimaryKey(foodTageTypeId, -1, -1);
		NutritionTarget existingnutritionTargets = nutritionTargetDAO.findNutritionTargetByPrimaryKey(related_nutritiontargets.getNutritionTargetId());

		// copy into the existing record to preserve existing relationships
		if (existingnutritionTargets != null) {
			existingnutritionTargets.setNutritionTargetId(related_nutritiontargets.getNutritionTargetId());
			existingnutritionTargets.setDate(related_nutritiontargets.getDate());
			related_nutritiontargets = existingnutritionTargets;
		}

		related_nutritiontargets.setFoodTageType(foodtagetype);
		foodtagetype.getNutritionTargets().add(related_nutritiontargets);
		related_nutritiontargets = nutritionTargetDAO.store(related_nutritiontargets);
		nutritionTargetDAO.flush();

		foodtagetype = foodTageTypeDAO.store(foodtagetype);
		foodTageTypeDAO.flush();

		return foodtagetype;
	}

	/**
	 * Delete an existing FoodTageInput entity
	 * 
	 */
	@Transactional
	public FoodTageType deleteFoodTageTypeFoodTageInputs(Integer foodtagetype_foodTageTypeId, Integer related_foodtageinputs_foodTageInputId) {
		FoodTageInput related_foodtageinputs = foodTageInputDAO.findFoodTageInputByPrimaryKey(related_foodtageinputs_foodTageInputId, -1, -1);

		FoodTageType foodtagetype = foodTageTypeDAO.findFoodTageTypeByPrimaryKey(foodtagetype_foodTageTypeId, -1, -1);

		related_foodtageinputs.setFoodTageType(null);
		foodtagetype.getFoodTageInputs().remove(related_foodtageinputs);

		foodTageInputDAO.remove(related_foodtageinputs);
		foodTageInputDAO.flush();

		return foodtagetype;
	}

	/**
	 * Return a count of all FoodTageType entity
	 * 
	 */
	@Transactional
	public Integer countFoodTageTypes() {
		return ((Long) foodTageTypeDAO.createQuerySingleResult("select count(o) from FoodTageType o").getSingleResult()).intValue();
	}

	/**
	 * Save an existing FoodTageType entity
	 * 
	 */
	@Transactional
	public FoodTageType saveFoodTageType(FoodTageType foodtagetype) {
		FoodTageType existingFoodTageType = foodTageTypeDAO.findFoodTageTypeByPrimaryKey(foodtagetype.getFoodTageTypeId());

		if (existingFoodTageType != null) {
			if (existingFoodTageType != foodtagetype) {
				existingFoodTageType.setFoodTageTypeId(foodtagetype.getFoodTageTypeId());
				existingFoodTageType.setTypeName(foodtagetype.getTypeName());
			}
			foodtagetype = foodTageTypeDAO.store(existingFoodTageType);
		} else {
			foodtagetype = foodTageTypeDAO.store(foodtagetype);
		}
		foodTageTypeDAO.flush();
		return foodtagetype;
	}

	/**
	 * Delete an existing NutritionTarget entity
	 * 
	 */
	@Transactional
	public FoodTageType deleteFoodTageTypeNutritionTargets(Integer foodtagetype_foodTageTypeId, Integer related_nutritiontargets_nutritionTargetId) {
		NutritionTarget related_nutritiontargets = nutritionTargetDAO.findNutritionTargetByPrimaryKey(related_nutritiontargets_nutritionTargetId, -1, -1);

		FoodTageType foodtagetype = foodTageTypeDAO.findFoodTageTypeByPrimaryKey(foodtagetype_foodTageTypeId, -1, -1);

		related_nutritiontargets.setFoodTageType(null);
		foodtagetype.getNutritionTargets().remove(related_nutritiontargets);

		nutritionTargetDAO.remove(related_nutritiontargets);
		nutritionTargetDAO.flush();

		return foodtagetype;
	}

	/**
	 * Save an existing FoodTageInput entity
	 * 
	 */
	@Transactional
	public FoodTageType saveFoodTageTypeFoodTageInputs(Integer foodTageTypeId, FoodTageInput related_foodtageinputs) {
		FoodTageType foodtagetype = foodTageTypeDAO.findFoodTageTypeByPrimaryKey(foodTageTypeId, -1, -1);
		FoodTageInput existingfoodTageInputs = foodTageInputDAO.findFoodTageInputByPrimaryKey(related_foodtageinputs.getFoodTageInputId());

		// copy into the existing record to preserve existing relationships
		if (existingfoodTageInputs != null) {
			existingfoodTageInputs.setFoodTageInputId(related_foodtageinputs.getFoodTageInputId());
			existingfoodTageInputs.setValue(related_foodtageinputs.getValue());
			related_foodtageinputs = existingfoodTageInputs;
		}

		related_foodtageinputs.setFoodTageType(foodtagetype);
		foodtagetype.getFoodTageInputs().add(related_foodtageinputs);
		related_foodtageinputs = foodTageInputDAO.store(related_foodtageinputs);
		foodTageInputDAO.flush();

		foodtagetype = foodTageTypeDAO.store(foodtagetype);
		foodTageTypeDAO.flush();

		return foodtagetype;
	}

	/**
	 * Delete an existing FoodTageType entity
	 * 
	 */
	@Transactional
	public void deleteFoodTageType(FoodTageType foodtagetype) {
		foodTageTypeDAO.remove(foodtagetype);
		foodTageTypeDAO.flush();
	}
}
