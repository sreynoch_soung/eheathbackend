package ehealth.service;

import ehealth.dao.TimestampDAO;
import ehealth.dao.UserDAO;

import ehealth.domain.Timestamp;
import ehealth.domain.User;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;

import org.springframework.transaction.annotation.Transactional;

/**
 * Spring service that handles CRUD requests for Timestamp entities
 * 
 */

@Service("TimestampService")

@Transactional
public class TimestampServiceImpl implements TimestampService {

	/**
	 * DAO injected by Spring that manages Timestamp entities
	 * 
	 */
	@Autowired
	private TimestampDAO timestampDAO;

	/**
	 * DAO injected by Spring that manages User entities
	 * 
	 */
	@Autowired
	private UserDAO userDAO;

	/**
	 * Instantiates a new TimestampServiceImpl.
	 *
	 */
	public TimestampServiceImpl() {
	}

	/**
	 * Load an existing Timestamp entity
	 * 
	 */
	@Transactional
	public Set<Timestamp> loadTimestamps() {
		return timestampDAO.findAllTimestamps();
	}

	/**
	 * Save an existing User entity
	 * 
	 */
	@Transactional
	public Timestamp saveTimestampUsers(Integer timestampId, User related_users) {
		Timestamp timestamp = timestampDAO.findTimestampByPrimaryKey(timestampId, -1, -1);
		User existingusers = userDAO.findUserByPrimaryKey(related_users.getUserId());

		// copy into the existing record to preserve existing relationships
		if (existingusers != null) {
			existingusers.setUserId(related_users.getUserId());
			existingusers.setUsername(related_users.getUsername());
			existingusers.setName(related_users.getName());
			existingusers.setSurename(related_users.getSurename());
			existingusers.setPassword(related_users.getPassword());
			existingusers.setEmail(related_users.getEmail());
			existingusers.setTelephone(related_users.getTelephone());
			existingusers.setAge(related_users.getAge());
			existingusers.setSex(related_users.getSex());
			related_users = existingusers;
		}

		related_users.setTimestamp(timestamp);
		timestamp.getUsers().add(related_users);
		related_users = userDAO.store(related_users);
		userDAO.flush();

		timestamp = timestampDAO.store(timestamp);
		timestampDAO.flush();

		return timestamp;
	}

	/**
	 */
	@Transactional
	public Timestamp findTimestampByPrimaryKey(Integer timestampId) {
		return timestampDAO.findTimestampByPrimaryKey(timestampId);
	}

	/**
	 * Delete an existing Timestamp entity
	 * 
	 */
	@Transactional
	public void deleteTimestamp(Timestamp timestamp) {
		timestampDAO.remove(timestamp);
		timestampDAO.flush();
	}

	/**
	 * Delete an existing User entity
	 * 
	 */
	@Transactional
	public Timestamp deleteTimestampUsers(Integer timestamp_timestampId, Integer related_users_userId) {
		User related_users = userDAO.findUserByPrimaryKey(related_users_userId, -1, -1);

		Timestamp timestamp = timestampDAO.findTimestampByPrimaryKey(timestamp_timestampId, -1, -1);

		related_users.setTimestamp(null);
		timestamp.getUsers().remove(related_users);

		userDAO.remove(related_users);
		userDAO.flush();

		return timestamp;
	}

	/**
	 * Return a count of all Timestamp entity
	 * 
	 */
	@Transactional
	public Integer countTimestamps() {
		return ((Long) timestampDAO.createQuerySingleResult("select count(o) from Timestamp o").getSingleResult()).intValue();
	}

	/**
	 * Save an existing Timestamp entity
	 * 
	 */
	@Transactional
	public Timestamp saveTimestamp(Timestamp timestamp) {
		Timestamp existingTimestamp = timestampDAO.findTimestampByPrimaryKey(timestamp.getTimestampId());

		if (existingTimestamp != null) {
			if (existingTimestamp != timestamp) {
				existingTimestamp.setTimestampId(timestamp.getTimestampId());
				existingTimestamp.setLoginDate(timestamp.getLoginDate());
				existingTimestamp.setLogoutDate(timestamp.getLogoutDate());
				existingTimestamp.setCreateDate(timestamp.getCreateDate());
				existingTimestamp.setUpdateDate(timestamp.getUpdateDate());
				existingTimestamp.setIsActive(timestamp.getIsActive());
			}
			timestamp = timestampDAO.store(existingTimestamp);
		} else {
			timestamp = timestampDAO.store(timestamp);
		}
		timestampDAO.flush();
		return timestamp;
	}

	/**
	 * Return all Timestamp entity
	 * 
	 */
	@Transactional
	public List<Timestamp> findAllTimestamps(Integer startResult, Integer maxRows) {
		return new java.util.ArrayList<Timestamp>(timestampDAO.findAllTimestamps(startResult, maxRows));
	}
}
