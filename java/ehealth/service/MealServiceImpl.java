package ehealth.service;

import ehealth.dao.FoodPhotoDAO;
import ehealth.dao.KidDAO;
import ehealth.dao.MealDAO;

import ehealth.domain.FoodPhoto;
import ehealth.domain.Kid;
import ehealth.domain.Meal;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;

import org.springframework.transaction.annotation.Transactional;

/**
 * Spring service that handles CRUD requests for Meal entities
 * 
 */

@Service("MealService")

@Transactional
public class MealServiceImpl implements MealService {

	/**
	 * DAO injected by Spring that manages FoodPhoto entities
	 * 
	 */
	@Autowired
	private FoodPhotoDAO foodPhotoDAO;

	/**
	 * DAO injected by Spring that manages Kid entities
	 * 
	 */
	@Autowired
	private KidDAO kidDAO;

	/**
	 * DAO injected by Spring that manages Meal entities
	 * 
	 */
	@Autowired
	private MealDAO mealDAO;

	/**
	 * Instantiates a new MealServiceImpl.
	 *
	 */
	public MealServiceImpl() {
	}

	/**
	 * Return all Meal entity
	 * 
	 */
	@Transactional
	public List<Meal> findAllMeals(Integer startResult, Integer maxRows) {
		return new java.util.ArrayList<Meal>(mealDAO.findAllMeals(startResult, maxRows));
	}

	/**
	 * Delete an existing Meal entity
	 * 
	 */
	@Transactional
	public void deleteMeal(Meal meal) {
		mealDAO.remove(meal);
		mealDAO.flush();
	}

	/**
	 * Delete an existing FoodPhoto entity
	 * 
	 */
	@Transactional
	public Meal deleteMealFoodPhotos(Integer meal_mealId, Integer related_foodphotos_foodPhotoId) {
		FoodPhoto related_foodphotos = foodPhotoDAO.findFoodPhotoByPrimaryKey(related_foodphotos_foodPhotoId, -1, -1);

		Meal meal = mealDAO.findMealByPrimaryKey(meal_mealId, -1, -1);

		related_foodphotos.setMeal(null);
		meal.getFoodPhotos().remove(related_foodphotos);

		foodPhotoDAO.remove(related_foodphotos);
		foodPhotoDAO.flush();

		return meal;
	}

	/**
	 * Delete an existing Kid entity
	 * 
	 */
	@Transactional
	public Meal deleteMealKid(Integer meal_mealId, Integer related_kid_kidId) {
		Meal meal = mealDAO.findMealByPrimaryKey(meal_mealId, -1, -1);
		Kid related_kid = kidDAO.findKidByPrimaryKey(related_kid_kidId, -1, -1);

		meal.setKid(null);
		related_kid.getMeals().remove(meal);
		meal = mealDAO.store(meal);
		mealDAO.flush();

		related_kid = kidDAO.store(related_kid);
		kidDAO.flush();

		kidDAO.remove(related_kid);
		kidDAO.flush();

		return meal;
	}

	/**
	 * Load an existing Meal entity
	 * 
	 */
	@Transactional
	public Set<Meal> loadMeals() {
		return mealDAO.findAllMeals();
	}

	/**
	 * Save an existing Meal entity
	 * 
	 */
	@Transactional
	public Meal saveMeal(Meal meal) {
		Meal existingMeal = mealDAO.findMealByPrimaryKey(meal.getMealId());

		if (existingMeal != null) {
			if (existingMeal != meal) {
				existingMeal.setMealId(meal.getMealId());
				existingMeal.setDate(meal.getDate());
			}
			meal = mealDAO.store(existingMeal);
		} else {
			meal = mealDAO.store(meal);
		}
		mealDAO.flush();
		return meal;
	}

	/**
	 * Return a count of all Meal entity
	 * 
	 */
	@Transactional
	public Integer countMeals() {
		return ((Long) mealDAO.createQuerySingleResult("select count(o) from Meal o").getSingleResult()).intValue();
	}

	/**
	 * Save an existing Kid entity
	 * 
	 */
	@Transactional
	public Meal saveMealKid(Integer mealId, Kid related_kid) {
		Meal meal = mealDAO.findMealByPrimaryKey(mealId, -1, -1);
		Kid existingkid = kidDAO.findKidByPrimaryKey(related_kid.getKidId());

		// copy into the existing record to preserve existing relationships
		if (existingkid != null) {
			existingkid.setKidId(related_kid.getKidId());
			existingkid.setHigh(related_kid.getHigh());
			existingkid.setWeight(related_kid.getWeight());
			existingkid.setAddress(related_kid.getAddress());
			existingkid.setCodeKey(related_kid.getCodeKey());
			related_kid = existingkid;
		}

		meal.setKid(related_kid);
		related_kid.getMeals().add(meal);
		meal = mealDAO.store(meal);
		mealDAO.flush();

		related_kid = kidDAO.store(related_kid);
		kidDAO.flush();

		return meal;
	}

	/**
	 */
	@Transactional
	public Meal findMealByPrimaryKey(Integer mealId) {
		return mealDAO.findMealByPrimaryKey(mealId);
	}

	/**
	 * Save an existing FoodPhoto entity
	 * 
	 */
	@Transactional
	public Meal saveMealFoodPhotos(Integer mealId, FoodPhoto related_foodphotos) {
		Meal meal = mealDAO.findMealByPrimaryKey(mealId, -1, -1);
		FoodPhoto existingfoodPhotos = foodPhotoDAO.findFoodPhotoByPrimaryKey(related_foodphotos.getFoodPhotoId());

		// copy into the existing record to preserve existing relationships
		if (existingfoodPhotos != null) {
			existingfoodPhotos.setFoodPhotoId(related_foodphotos.getFoodPhotoId());
			existingfoodPhotos.setUrl(related_foodphotos.getUrl());
			existingfoodPhotos.setFinalValidationValue(related_foodphotos.getFinalValidationValue());
			related_foodphotos = existingfoodPhotos;
		}

		related_foodphotos.setMeal(meal);
		meal.getFoodPhotos().add(related_foodphotos);
		related_foodphotos = foodPhotoDAO.store(related_foodphotos);
		foodPhotoDAO.flush();

		meal = mealDAO.store(meal);
		mealDAO.flush();

		return meal;
	}
}
