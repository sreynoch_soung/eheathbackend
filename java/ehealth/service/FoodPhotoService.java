
package ehealth.service;

import ehealth.domain.FoodPhoto;
import ehealth.domain.FoodTageInput;
import ehealth.domain.FoodValidation;
import ehealth.domain.Meal;

import java.util.List;
import java.util.Set;

/**
 * Spring service that handles CRUD requests for FoodPhoto entities
 * 
 */
public interface FoodPhotoService {

	/**
	* Delete an existing Meal entity
	* 
	 */
	public FoodPhoto deleteFoodPhotoMeal(Integer foodphoto_foodPhotoId, Integer related_meal_mealId);

	/**
	* Delete an existing FoodPhoto entity
	* 
	 */
	public void deleteFoodPhoto(FoodPhoto foodphoto);

	/**
	* Return a count of all FoodPhoto entity
	* 
	 */
	public Integer countFoodPhotos();

	/**
	* Save an existing FoodTageInput entity
	* 
	 */
	public FoodPhoto saveFoodPhotoFoodTageInputs(Integer foodPhotoId, FoodTageInput related_foodtageinputs);

	/**
	* Return all FoodPhoto entity
	* 
	 */
	public List<FoodPhoto> findAllFoodPhotos(Integer startResult, Integer maxRows);

	/**
	* Delete an existing FoodValidation entity
	* 
	 */
	public FoodPhoto deleteFoodPhotoFoodValidations(Integer foodphoto_foodPhotoId_1, Integer related_foodvalidations_foodValidationId);

	/**
	* Delete an existing FoodTageInput entity
	* 
	 */
	public FoodPhoto deleteFoodPhotoFoodTageInputs(Integer foodphoto_foodPhotoId_2, Integer related_foodtageinputs_foodTageInputId);

	/**
	* Save an existing Meal entity
	* 
	 */
	public FoodPhoto saveFoodPhotoMeal(Integer foodPhotoId_1, Meal related_meal);

	/**
	* Load an existing FoodPhoto entity
	* 
	 */
	public Set<FoodPhoto> loadFoodPhotos();

	/**
	* Save an existing FoodPhoto entity
	* 
	 */
	public FoodPhoto saveFoodPhoto(FoodPhoto foodphoto_1);

	/**
	* Save an existing FoodValidation entity
	* 
	 */
	public FoodPhoto saveFoodPhotoFoodValidations(Integer foodPhotoId_2, FoodValidation related_foodvalidations);

	/**
	 */
	public FoodPhoto findFoodPhotoByPrimaryKey(Integer foodPhotoId_3);
}