
package ehealth.service;

import ehealth.domain.Doctor;
import ehealth.domain.FoodTageType;
import ehealth.domain.Kid;
import ehealth.domain.NutritionTarget;

import java.util.List;
import java.util.Set;

/**
 * Spring service that handles CRUD requests for NutritionTarget entities
 * 
 */
public interface NutritionTargetService {

	/**
	* Save an existing Kid entity
	* 
	 */
	public NutritionTarget saveNutritionTargetKid(Integer nutritionTargetId, Kid related_kid);

	/**
	* Save an existing Doctor entity
	* 
	 */
	public NutritionTarget saveNutritionTargetDoctor(Integer nutritionTargetId_1, Doctor related_doctor);

	/**
	* Save an existing FoodTageType entity
	* 
	 */
	public NutritionTarget saveNutritionTargetFoodTageType(Integer nutritionTargetId_2, FoodTageType related_foodtagetype);

	/**
	* Return all NutritionTarget entity
	* 
	 */
	public List<NutritionTarget> findAllNutritionTargets(Integer startResult, Integer maxRows);

	/**
	* Delete an existing Doctor entity
	* 
	 */
	public NutritionTarget deleteNutritionTargetDoctor(Integer nutritiontarget_nutritionTargetId, Integer related_doctor_doctorId);

	/**
	* Delete an existing Kid entity
	* 
	 */
	public NutritionTarget deleteNutritionTargetKid(Integer nutritiontarget_nutritionTargetId_1, Integer related_kid_kidId);

	/**
	* Save an existing NutritionTarget entity
	* 
	 */
	public NutritionTarget saveNutritionTarget(NutritionTarget nutritiontarget);

	/**
	* Delete an existing FoodTageType entity
	* 
	 */
	public NutritionTarget deleteNutritionTargetFoodTageType(Integer nutritiontarget_nutritionTargetId_2, Integer related_foodtagetype_foodTageTypeId);

	/**
	* Return a count of all NutritionTarget entity
	* 
	 */
	public Integer countNutritionTargets();

	/**
	* Delete an existing NutritionTarget entity
	* 
	 */
	public void deleteNutritionTarget(NutritionTarget nutritiontarget_1);

	/**
	* Load an existing NutritionTarget entity
	* 
	 */
	public Set<NutritionTarget> loadNutritionTargets();

	/**
	 */
	public NutritionTarget findNutritionTargetByPrimaryKey(Integer nutritionTargetId_3);
}