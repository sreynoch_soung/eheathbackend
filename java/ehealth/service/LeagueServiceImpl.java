package ehealth.service;

import ehealth.dao.LeagueDAO;
import ehealth.dao.MatchDAO;
import ehealth.dao.TeamDAO;

import ehealth.domain.League;
import ehealth.domain.Match;
import ehealth.domain.Team;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;

import org.springframework.transaction.annotation.Transactional;

/**
 * Spring service that handles CRUD requests for League entities
 * 
 */

@Service("LeagueService")

@Transactional
public class LeagueServiceImpl implements LeagueService {

	/**
	 * DAO injected by Spring that manages League entities
	 * 
	 */
	@Autowired
	private LeagueDAO leagueDAO;

	/**
	 * DAO injected by Spring that manages Match entities
	 * 
	 */
	@Autowired
	private MatchDAO matchDAO;

	/**
	 * DAO injected by Spring that manages Team entities
	 * 
	 */
	@Autowired
	private TeamDAO teamDAO;

	/**
	 * Instantiates a new LeagueServiceImpl.
	 *
	 */
	public LeagueServiceImpl() {
	}

	/**
	 * Save an existing Match entity
	 * 
	 */
	@Transactional
	public League saveLeagueMatch(Integer leagueId, Match related_match) {
		League league = leagueDAO.findLeagueByPrimaryKey(leagueId, -1, -1);
		Match existingmatch = matchDAO.findMatchByPrimaryKey(related_match.getMatchId());

		// copy into the existing record to preserve existing relationships
		if (existingmatch != null) {
			existingmatch.setMatchId(related_match.getMatchId());
			existingmatch.setDate(related_match.getDate());
			existingmatch.setWinScore(related_match.getWinScore());
			related_match = existingmatch;
		}

		league.setMatch(related_match);
		related_match.getLeagues().add(league);
		league = leagueDAO.store(league);
		leagueDAO.flush();

		related_match = matchDAO.store(related_match);
		matchDAO.flush();

		return league;
	}

	/**
	 * Save an existing League entity
	 * 
	 */
	@Transactional
	public League saveLeague(League league) {
		League existingLeague = leagueDAO.findLeagueByPrimaryKey(league.getLeagueId());

		if (existingLeague != null) {
			if (existingLeague != league) {
				existingLeague.setLeagueId(league.getLeagueId());
				existingLeague.setLeagueName(league.getLeagueName());
				existingLeague.setMatchDate(league.getMatchDate());
				existingLeague.setPoint(league.getPoint());
			}
			league = leagueDAO.store(existingLeague);
		} else {
			league = leagueDAO.store(league);
		}
		leagueDAO.flush();
		return league;
	}

	/**
	 * Delete an existing Match entity
	 * 
	 */
	@Transactional
	public League deleteLeagueMatch(Integer league_leagueId, Integer related_match_matchId) {
		League league = leagueDAO.findLeagueByPrimaryKey(league_leagueId, -1, -1);
		Match related_match = matchDAO.findMatchByPrimaryKey(related_match_matchId, -1, -1);

		league.setMatch(null);
		related_match.getLeagues().remove(league);
		league = leagueDAO.store(league);
		leagueDAO.flush();

		related_match = matchDAO.store(related_match);
		matchDAO.flush();

		matchDAO.remove(related_match);
		matchDAO.flush();

		return league;
	}

	/**
	 * Delete an existing Team entity
	 * 
	 */
	@Transactional
	public League deleteLeagueTeam(Integer league_leagueId, Integer related_team_teamId) {
		League league = leagueDAO.findLeagueByPrimaryKey(league_leagueId, -1, -1);
		Team related_team = teamDAO.findTeamByPrimaryKey(related_team_teamId, -1, -1);

		league.setTeam(null);
		related_team.getLeagues().remove(league);
		league = leagueDAO.store(league);
		leagueDAO.flush();

		related_team = teamDAO.store(related_team);
		teamDAO.flush();

		teamDAO.remove(related_team);
		teamDAO.flush();

		return league;
	}

	/**
	 * Return a count of all League entity
	 * 
	 */
	@Transactional
	public Integer countLeagues() {
		return ((Long) leagueDAO.createQuerySingleResult("select count(o) from League o").getSingleResult()).intValue();
	}

	/**
	 * Return all League entity
	 * 
	 */
	@Transactional
	public List<League> findAllLeagues(Integer startResult, Integer maxRows) {
		return new java.util.ArrayList<League>(leagueDAO.findAllLeagues(startResult, maxRows));
	}

	/**
	 * Load an existing League entity
	 * 
	 */
	@Transactional
	public Set<League> loadLeagues() {
		return leagueDAO.findAllLeagues();
	}

	/**
	 * Save an existing Team entity
	 * 
	 */
	@Transactional
	public League saveLeagueTeam(Integer leagueId, Team related_team) {
		League league = leagueDAO.findLeagueByPrimaryKey(leagueId, -1, -1);
		Team existingteam = teamDAO.findTeamByPrimaryKey(related_team.getTeamId());

		// copy into the existing record to preserve existing relationships
		if (existingteam != null) {
			existingteam.setTeamId(related_team.getTeamId());
			existingteam.setName(related_team.getName());
			existingteam.setNumMember(related_team.getNumMember());
			related_team = existingteam;
		}

		league.setTeam(related_team);
		related_team.getLeagues().add(league);
		league = leagueDAO.store(league);
		leagueDAO.flush();

		related_team = teamDAO.store(related_team);
		teamDAO.flush();

		return league;
	}

	/**
	 * Delete an existing League entity
	 * 
	 */
	@Transactional
	public void deleteLeague(League league) {
		leagueDAO.remove(league);
		leagueDAO.flush();
	}

	/**
	 */
	@Transactional
	public League findLeagueByPrimaryKey(Integer leagueId) {
		return leagueDAO.findLeagueByPrimaryKey(leagueId);
	}
}
