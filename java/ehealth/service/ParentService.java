
package ehealth.service;

import ehealth.domain.AccountSetting;
import ehealth.domain.City;
import ehealth.domain.Parent;
import ehealth.domain.ParentKid;
import ehealth.domain.User;

import java.util.List;
import java.util.Set;

/**
 * Spring service that handles CRUD requests for Parent entities
 * 
 */
public interface ParentService {

	/**
	 */
	public Parent findParentByPrimaryKey(Integer parentId);

	/**
	* Delete an existing City entity
	* 
	 */
	public Parent deleteParentCity(Integer parent_parentId, Integer related_city_cityId);

	/**
	* Return all Parent entity
	* 
	 */
	public List<Parent> findAllParents(Integer startResult, Integer maxRows);

	/**
	* Delete an existing ParentKid entity
	* 
	 */
	public Parent deleteParentParentKids(Integer parent_parentId_1, Integer related_parentkids_parentKidId);

	/**
	* Save an existing ParentKid entity
	* 
	 */
	public Parent saveParentParentKids(Integer parentId_1, ParentKid related_parentkids);

	/**
	* Load an existing Parent entity
	* 
	 */
	public Set<Parent> loadParents();

	/**
	* Return a count of all Parent entity
	* 
	 */
	public Integer countParents();

	/**
	* Save an existing AccountSetting entity
	* 
	 */
	public Parent saveParentAccountSetting(Integer parentId_2, AccountSetting related_accountsetting);

	/**
	* Save an existing Parent entity
	* 
	 */
	public Parent saveParent(Parent parent);

	/**
	* Delete an existing User entity
	* 
	 */
	public Parent deleteParentUser(Integer parent_parentId_2, Integer related_user_userId);

	/**
	* Save an existing User entity
	* 
	 */
	public Parent saveParentUser(Integer parentId_3, User related_user);

	/**
	* Save an existing City entity
	* 
	 */
	public Parent saveParentCity(Integer parentId_4, City related_city);

	/**
	* Delete an existing AccountSetting entity
	* 
	 */
	public Parent deleteParentAccountSetting(Integer parent_parentId_3, Integer related_accountsetting_accountSettingId);

	/**
	* Delete an existing Parent entity
	* 
	 */
	public void deleteParent(Parent parent_1);
}