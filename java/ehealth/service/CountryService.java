
package ehealth.service;

import ehealth.domain.City;
import ehealth.domain.Country;

import java.util.List;
import java.util.Set;

/**
 * Spring service that handles CRUD requests for Country entities
 * 
 */
public interface CountryService {

	/**
	* Delete an existing City entity
	* 
	 */
	public Country deleteCountryCities(Integer country_countryId, Integer related_cities_cityId);

	/**
	* Save an existing City entity
	* 
	 */
	public Country saveCountryCities(Integer countryId, City related_cities);

	/**
	* Return all Country entity
	* 
	 */
	public List<Country> findAllCountrys(Integer startResult, Integer maxRows);

	/**
	* Save an existing Country entity
	* 
	 */
	public Country saveCountry(Country country);

	/**
	* Return a count of all Country entity
	* 
	 */
	public Integer countCountrys();

	/**
	* Delete an existing Country entity
	* 
	 */
	public void deleteCountry(Country country_1);

	/**
	* Load an existing Country entity
	* 
	 */
	public Set<Country> loadCountrys();

	/**
	 */
	public Country findCountryByPrimaryKey(Integer countryId_1);
}