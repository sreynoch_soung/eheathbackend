
package ehealth.service;

import ehealth.domain.FoodTageInput;
import ehealth.domain.FoodTageType;
import ehealth.domain.NutritionTarget;

import java.util.List;
import java.util.Set;

/**
 * Spring service that handles CRUD requests for FoodTageType entities
 * 
 */
public interface FoodTageTypeService {

	/**
	* Return all FoodTageType entity
	* 
	 */
	public List<FoodTageType> findAllFoodTageTypes(Integer startResult, Integer maxRows);

	/**
	 */
	public FoodTageType findFoodTageTypeByPrimaryKey(Integer foodTageTypeId);

	/**
	* Load an existing FoodTageType entity
	* 
	 */
	public Set<FoodTageType> loadFoodTageTypes();

	/**
	* Save an existing NutritionTarget entity
	* 
	 */
	public FoodTageType saveFoodTageTypeNutritionTargets(Integer foodTageTypeId_1, NutritionTarget related_nutritiontargets);

	/**
	* Delete an existing FoodTageInput entity
	* 
	 */
	public FoodTageType deleteFoodTageTypeFoodTageInputs(Integer foodtagetype_foodTageTypeId, Integer related_foodtageinputs_foodTageInputId);

	/**
	* Return a count of all FoodTageType entity
	* 
	 */
	public Integer countFoodTageTypes();

	/**
	* Save an existing FoodTageType entity
	* 
	 */
	public FoodTageType saveFoodTageType(FoodTageType foodtagetype);

	/**
	* Delete an existing NutritionTarget entity
	* 
	 */
	public FoodTageType deleteFoodTageTypeNutritionTargets(Integer foodtagetype_foodTageTypeId_1, Integer related_nutritiontargets_nutritionTargetId);

	/**
	* Save an existing FoodTageInput entity
	* 
	 */
	public FoodTageType saveFoodTageTypeFoodTageInputs(Integer foodTageTypeId_2, FoodTageInput related_foodtageinputs);

	/**
	* Delete an existing FoodTageType entity
	* 
	 */
	public void deleteFoodTageType(FoodTageType foodtagetype_1);
}