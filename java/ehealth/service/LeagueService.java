
package ehealth.service;

import ehealth.domain.League;
import ehealth.domain.Match;
import ehealth.domain.Team;

import java.util.List;
import java.util.Set;

/**
 * Spring service that handles CRUD requests for League entities
 * 
 */
public interface LeagueService {

	/**
	* Save an existing Match entity
	* 
	 */
	public League saveLeagueMatch(Integer leagueId, Match related_match);

	/**
	* Save an existing League entity
	* 
	 */
	public League saveLeague(League league);

	/**
	* Delete an existing Match entity
	* 
	 */
	public League deleteLeagueMatch(Integer league_leagueId, Integer related_match_matchId);

	/**
	* Delete an existing Team entity
	* 
	 */
	public League deleteLeagueTeam(Integer league_leagueId_1, Integer related_team_teamId);

	/**
	* Return a count of all League entity
	* 
	 */
	public Integer countLeagues();

	/**
	* Return all League entity
	* 
	 */
	public List<League> findAllLeagues(Integer startResult, Integer maxRows);

	/**
	* Load an existing League entity
	* 
	 */
	public Set<League> loadLeagues();

	/**
	* Save an existing Team entity
	* 
	 */
	public League saveLeagueTeam(Integer leagueId_1, Team related_team);

	/**
	* Delete an existing League entity
	* 
	 */
	public void deleteLeague(League league_1);

	/**
	 */
	public League findLeagueByPrimaryKey(Integer leagueId_2);
}