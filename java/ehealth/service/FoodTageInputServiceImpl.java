package ehealth.service;

import ehealth.dao.FoodPhotoDAO;
import ehealth.dao.FoodTageInputDAO;
import ehealth.dao.FoodTageTypeDAO;

import ehealth.domain.FoodPhoto;
import ehealth.domain.FoodTageInput;
import ehealth.domain.FoodTageType;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;

import org.springframework.transaction.annotation.Transactional;

/**
 * Spring service that handles CRUD requests for FoodTageInput entities
 * 
 */

@Service("FoodTageInputService")

@Transactional
public class FoodTageInputServiceImpl implements FoodTageInputService {

	/**
	 * DAO injected by Spring that manages FoodPhoto entities
	 * 
	 */
	@Autowired
	private FoodPhotoDAO foodPhotoDAO;

	/**
	 * DAO injected by Spring that manages FoodTageInput entities
	 * 
	 */
	@Autowired
	private FoodTageInputDAO foodTageInputDAO;

	/**
	 * DAO injected by Spring that manages FoodTageType entities
	 * 
	 */
	@Autowired
	private FoodTageTypeDAO foodTageTypeDAO;

	/**
	 * Instantiates a new FoodTageInputServiceImpl.
	 *
	 */
	public FoodTageInputServiceImpl() {
	}

	/**
	 * Save an existing FoodTageInput entity
	 * 
	 */
	@Transactional
	public FoodTageInput saveFoodTageInput(FoodTageInput foodtageinput) {
		FoodTageInput existingFoodTageInput = foodTageInputDAO.findFoodTageInputByPrimaryKey(foodtageinput.getFoodTageInputId());

		if (existingFoodTageInput != null) {
			if (existingFoodTageInput != foodtageinput) {
				existingFoodTageInput.setFoodTageInputId(foodtageinput.getFoodTageInputId());
				existingFoodTageInput.setValue(foodtageinput.getValue());
			}
			foodtageinput = foodTageInputDAO.store(existingFoodTageInput);
		} else {
			foodtageinput = foodTageInputDAO.store(foodtageinput);
		}
		foodTageInputDAO.flush();
		return foodtageinput;
	}

	/**
	 * Delete an existing FoodPhoto entity
	 * 
	 */
	@Transactional
	public FoodTageInput deleteFoodTageInputFoodPhoto(Integer foodtageinput_foodTageInputId, Integer related_foodphoto_foodPhotoId) {
		FoodTageInput foodtageinput = foodTageInputDAO.findFoodTageInputByPrimaryKey(foodtageinput_foodTageInputId, -1, -1);
		FoodPhoto related_foodphoto = foodPhotoDAO.findFoodPhotoByPrimaryKey(related_foodphoto_foodPhotoId, -1, -1);

		foodtageinput.setFoodPhoto(null);
		related_foodphoto.getFoodTageInputs().remove(foodtageinput);
		foodtageinput = foodTageInputDAO.store(foodtageinput);
		foodTageInputDAO.flush();

		related_foodphoto = foodPhotoDAO.store(related_foodphoto);
		foodPhotoDAO.flush();

		foodPhotoDAO.remove(related_foodphoto);
		foodPhotoDAO.flush();

		return foodtageinput;
	}

	/**
	 * Save an existing FoodPhoto entity
	 * 
	 */
	@Transactional
	public FoodTageInput saveFoodTageInputFoodPhoto(Integer foodTageInputId, FoodPhoto related_foodphoto) {
		FoodTageInput foodtageinput = foodTageInputDAO.findFoodTageInputByPrimaryKey(foodTageInputId, -1, -1);
		FoodPhoto existingfoodPhoto = foodPhotoDAO.findFoodPhotoByPrimaryKey(related_foodphoto.getFoodPhotoId());

		// copy into the existing record to preserve existing relationships
		if (existingfoodPhoto != null) {
			existingfoodPhoto.setFoodPhotoId(related_foodphoto.getFoodPhotoId());
			existingfoodPhoto.setUrl(related_foodphoto.getUrl());
			existingfoodPhoto.setFinalValidationValue(related_foodphoto.getFinalValidationValue());
			related_foodphoto = existingfoodPhoto;
		}

		foodtageinput.setFoodPhoto(related_foodphoto);
		related_foodphoto.getFoodTageInputs().add(foodtageinput);
		foodtageinput = foodTageInputDAO.store(foodtageinput);
		foodTageInputDAO.flush();

		related_foodphoto = foodPhotoDAO.store(related_foodphoto);
		foodPhotoDAO.flush();

		return foodtageinput;
	}

	/**
	 */
	@Transactional
	public FoodTageInput findFoodTageInputByPrimaryKey(Integer foodTageInputId) {
		return foodTageInputDAO.findFoodTageInputByPrimaryKey(foodTageInputId);
	}

	/**
	 * Return a count of all FoodTageInput entity
	 * 
	 */
	@Transactional
	public Integer countFoodTageInputs() {
		return ((Long) foodTageInputDAO.createQuerySingleResult("select count(o) from FoodTageInput o").getSingleResult()).intValue();
	}

	/**
	 * Load an existing FoodTageInput entity
	 * 
	 */
	@Transactional
	public Set<FoodTageInput> loadFoodTageInputs() {
		return foodTageInputDAO.findAllFoodTageInputs();
	}

	/**
	 * Delete an existing FoodTageType entity
	 * 
	 */
	@Transactional
	public FoodTageInput deleteFoodTageInputFoodTageType(Integer foodtageinput_foodTageInputId, Integer related_foodtagetype_foodTageTypeId) {
		FoodTageInput foodtageinput = foodTageInputDAO.findFoodTageInputByPrimaryKey(foodtageinput_foodTageInputId, -1, -1);
		FoodTageType related_foodtagetype = foodTageTypeDAO.findFoodTageTypeByPrimaryKey(related_foodtagetype_foodTageTypeId, -1, -1);

		foodtageinput.setFoodTageType(null);
		related_foodtagetype.getFoodTageInputs().remove(foodtageinput);
		foodtageinput = foodTageInputDAO.store(foodtageinput);
		foodTageInputDAO.flush();

		related_foodtagetype = foodTageTypeDAO.store(related_foodtagetype);
		foodTageTypeDAO.flush();

		foodTageTypeDAO.remove(related_foodtagetype);
		foodTageTypeDAO.flush();

		return foodtageinput;
	}

	/**
	 * Delete an existing FoodTageInput entity
	 * 
	 */
	@Transactional
	public void deleteFoodTageInput(FoodTageInput foodtageinput) {
		foodTageInputDAO.remove(foodtageinput);
		foodTageInputDAO.flush();
	}

	/**
	 * Save an existing FoodTageType entity
	 * 
	 */
	@Transactional
	public FoodTageInput saveFoodTageInputFoodTageType(Integer foodTageInputId, FoodTageType related_foodtagetype) {
		FoodTageInput foodtageinput = foodTageInputDAO.findFoodTageInputByPrimaryKey(foodTageInputId, -1, -1);
		FoodTageType existingfoodTageType = foodTageTypeDAO.findFoodTageTypeByPrimaryKey(related_foodtagetype.getFoodTageTypeId());

		// copy into the existing record to preserve existing relationships
		if (existingfoodTageType != null) {
			existingfoodTageType.setFoodTageTypeId(related_foodtagetype.getFoodTageTypeId());
			existingfoodTageType.setTypeName(related_foodtagetype.getTypeName());
			related_foodtagetype = existingfoodTageType;
		}

		foodtageinput.setFoodTageType(related_foodtagetype);
		related_foodtagetype.getFoodTageInputs().add(foodtageinput);
		foodtageinput = foodTageInputDAO.store(foodtageinput);
		foodTageInputDAO.flush();

		related_foodtagetype = foodTageTypeDAO.store(related_foodtagetype);
		foodTageTypeDAO.flush();

		return foodtageinput;
	}

	/**
	 * Return all FoodTageInput entity
	 * 
	 */
	@Transactional
	public List<FoodTageInput> findAllFoodTageInputs(Integer startResult, Integer maxRows) {
		return new java.util.ArrayList<FoodTageInput>(foodTageInputDAO.findAllFoodTageInputs(startResult, maxRows));
	}
}
