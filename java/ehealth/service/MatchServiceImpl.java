package ehealth.service;

import ehealth.dao.LeagueDAO;
import ehealth.dao.MatchDAO;
import ehealth.dao.TeamDAO;

import ehealth.domain.League;
import ehealth.domain.Match;
import ehealth.domain.Team;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;

import org.springframework.transaction.annotation.Transactional;

/**
 * Spring service that handles CRUD requests for Match entities
 * 
 */

@Service("MatchService")

@Transactional
public class MatchServiceImpl implements MatchService {

	/**
	 * DAO injected by Spring that manages League entities
	 * 
	 */
	@Autowired
	private LeagueDAO leagueDAO;

	/**
	 * DAO injected by Spring that manages Match entities
	 * 
	 */
	@Autowired
	private MatchDAO matchDAO;

	/**
	 * DAO injected by Spring that manages Team entities
	 * 
	 */
	@Autowired
	private TeamDAO teamDAO;

	/**
	 * Instantiates a new MatchServiceImpl.
	 *
	 */
	public MatchServiceImpl() {
	}

	/**
	 */
	@Transactional
	public Match findMatchByPrimaryKey(Integer matchId) {
		return matchDAO.findMatchByPrimaryKey(matchId);
	}

	/**
	 * Delete an existing League entity
	 * 
	 */
	@Transactional
	public Match deleteMatchLeagues(Integer match_matchId, Integer related_leagues_leagueId) {
		League related_leagues = leagueDAO.findLeagueByPrimaryKey(related_leagues_leagueId, -1, -1);

		Match match = matchDAO.findMatchByPrimaryKey(match_matchId, -1, -1);

		related_leagues.setMatch(null);
		match.getLeagues().remove(related_leagues);

		leagueDAO.remove(related_leagues);
		leagueDAO.flush();

		return match;
	}

	/**
	 * Delete an existing Team entity
	 * 
	 */
	@Transactional
	public Match deleteMatchTeamByTeamId2(Integer match_matchId, Integer related_teambyteamid2_teamId) {
		Match match = matchDAO.findMatchByPrimaryKey(match_matchId, -1, -1);
		Team related_teambyteamid2 = teamDAO.findTeamByPrimaryKey(related_teambyteamid2_teamId, -1, -1);

		match.setTeamByTeamId2(null);
		related_teambyteamid2.getMatchsForTeamId2().remove(match);
		match = matchDAO.store(match);
		matchDAO.flush();

		related_teambyteamid2 = teamDAO.store(related_teambyteamid2);
		teamDAO.flush();

		teamDAO.remove(related_teambyteamid2);
		teamDAO.flush();

		return match;
	}

	/**
	 * Save an existing Match entity
	 * 
	 */
	@Transactional
	public Match saveMatch(Match match) {
		Match existingMatch = matchDAO.findMatchByPrimaryKey(match.getMatchId());

		if (existingMatch != null) {
			if (existingMatch != match) {
				existingMatch.setMatchId(match.getMatchId());
				existingMatch.setDate(match.getDate());
				existingMatch.setWinScore(match.getWinScore());
			}
			match = matchDAO.store(existingMatch);
		} else {
			match = matchDAO.store(match);
		}
		matchDAO.flush();
		return match;
	}

	/**
	 * Return a count of all Match entity
	 * 
	 */
	@Transactional
	public Integer countMatchs() {
		return ((Long) matchDAO.createQuerySingleResult("select count(o) from Match o").getSingleResult()).intValue();
	}

	/**
	 * Save an existing Team entity
	 * 
	 */
	@Transactional
	public Match saveMatchTeamByTeamId1(Integer matchId, Team related_teambyteamid1) {
		Match match = matchDAO.findMatchByPrimaryKey(matchId, -1, -1);
		Team existingteamByTeamId1 = teamDAO.findTeamByPrimaryKey(related_teambyteamid1.getTeamId());

		// copy into the existing record to preserve existing relationships
		if (existingteamByTeamId1 != null) {
			existingteamByTeamId1.setTeamId(related_teambyteamid1.getTeamId());
			existingteamByTeamId1.setName(related_teambyteamid1.getName());
			existingteamByTeamId1.setNumMember(related_teambyteamid1.getNumMember());
			related_teambyteamid1 = existingteamByTeamId1;
		}

		match.setTeamByTeamId1(related_teambyteamid1);
		related_teambyteamid1.getMatchsForTeamId1().add(match);
		match = matchDAO.store(match);
		matchDAO.flush();

		related_teambyteamid1 = teamDAO.store(related_teambyteamid1);
		teamDAO.flush();

		return match;
	}

	/**
	 * Return all Match entity
	 * 
	 */
	@Transactional
	public List<Match> findAllMatchs(Integer startResult, Integer maxRows) {
		return new java.util.ArrayList<Match>(matchDAO.findAllMatchs(startResult, maxRows));
	}

	/**
	 * Save an existing League entity
	 * 
	 */
	@Transactional
	public Match saveMatchLeagues(Integer matchId, League related_leagues) {
		Match match = matchDAO.findMatchByPrimaryKey(matchId, -1, -1);
		League existingleagues = leagueDAO.findLeagueByPrimaryKey(related_leagues.getLeagueId());

		// copy into the existing record to preserve existing relationships
		if (existingleagues != null) {
			existingleagues.setLeagueId(related_leagues.getLeagueId());
			existingleagues.setLeagueName(related_leagues.getLeagueName());
			existingleagues.setMatchDate(related_leagues.getMatchDate());
			existingleagues.setPoint(related_leagues.getPoint());
			related_leagues = existingleagues;
		}

		related_leagues.setMatch(match);
		match.getLeagues().add(related_leagues);
		related_leagues = leagueDAO.store(related_leagues);
		leagueDAO.flush();

		match = matchDAO.store(match);
		matchDAO.flush();

		return match;
	}

	/**
	 * Load an existing Match entity
	 * 
	 */
	@Transactional
	public Set<Match> loadMatchs() {
		return matchDAO.findAllMatchs();
	}

	/**
	 * Delete an existing Team entity
	 * 
	 */
	@Transactional
	public Match deleteMatchTeamByTeamId1(Integer match_matchId, Integer related_teambyteamid1_teamId) {
		Match match = matchDAO.findMatchByPrimaryKey(match_matchId, -1, -1);
		Team related_teambyteamid1 = teamDAO.findTeamByPrimaryKey(related_teambyteamid1_teamId, -1, -1);

		match.setTeamByTeamId1(null);
		related_teambyteamid1.getMatchsForTeamId1().remove(match);
		match = matchDAO.store(match);
		matchDAO.flush();

		related_teambyteamid1 = teamDAO.store(related_teambyteamid1);
		teamDAO.flush();

		teamDAO.remove(related_teambyteamid1);
		teamDAO.flush();

		return match;
	}

	/**
	 * Delete an existing Match entity
	 * 
	 */
	@Transactional
	public void deleteMatch(Match match) {
		matchDAO.remove(match);
		matchDAO.flush();
	}

	/**
	 * Delete an existing Team entity
	 * 
	 */
	@Transactional
	public Match deleteMatchTeamByTeamIdWin(Integer match_matchId, Integer related_teambyteamidwin_teamId) {
		Match match = matchDAO.findMatchByPrimaryKey(match_matchId, -1, -1);
		Team related_teambyteamidwin = teamDAO.findTeamByPrimaryKey(related_teambyteamidwin_teamId, -1, -1);

		match.setTeamByTeamIdWin(null);
		related_teambyteamidwin.getMatchsForTeamIdWin().remove(match);
		match = matchDAO.store(match);
		matchDAO.flush();

		related_teambyteamidwin = teamDAO.store(related_teambyteamidwin);
		teamDAO.flush();

		teamDAO.remove(related_teambyteamidwin);
		teamDAO.flush();

		return match;
	}

	/**
	 * Save an existing Team entity
	 * 
	 */
	@Transactional
	public Match saveMatchTeamByTeamId2(Integer matchId, Team related_teambyteamid2) {
		Match match = matchDAO.findMatchByPrimaryKey(matchId, -1, -1);
		Team existingteamByTeamId2 = teamDAO.findTeamByPrimaryKey(related_teambyteamid2.getTeamId());

		// copy into the existing record to preserve existing relationships
		if (existingteamByTeamId2 != null) {
			existingteamByTeamId2.setTeamId(related_teambyteamid2.getTeamId());
			existingteamByTeamId2.setName(related_teambyteamid2.getName());
			existingteamByTeamId2.setNumMember(related_teambyteamid2.getNumMember());
			related_teambyteamid2 = existingteamByTeamId2;
		}

		match.setTeamByTeamId2(related_teambyteamid2);
		related_teambyteamid2.getMatchsForTeamId2().add(match);
		match = matchDAO.store(match);
		matchDAO.flush();

		related_teambyteamid2 = teamDAO.store(related_teambyteamid2);
		teamDAO.flush();

		return match;
	}

	/**
	 * Save an existing Team entity
	 * 
	 */
	@Transactional
	public Match saveMatchTeamByTeamIdWin(Integer matchId, Team related_teambyteamidwin) {
		Match match = matchDAO.findMatchByPrimaryKey(matchId, -1, -1);
		Team existingteamByTeamIdWin = teamDAO.findTeamByPrimaryKey(related_teambyteamidwin.getTeamId());

		// copy into the existing record to preserve existing relationships
		if (existingteamByTeamIdWin != null) {
			existingteamByTeamIdWin.setTeamId(related_teambyteamidwin.getTeamId());
			existingteamByTeamIdWin.setName(related_teambyteamidwin.getName());
			existingteamByTeamIdWin.setNumMember(related_teambyteamidwin.getNumMember());
			related_teambyteamidwin = existingteamByTeamIdWin;
		} else {
			related_teambyteamidwin = teamDAO.store(related_teambyteamidwin);
			teamDAO.flush();
		}

		match.setTeamByTeamIdWin(related_teambyteamidwin);
		related_teambyteamidwin.getMatchsForTeamIdWin().add(match);
		match = matchDAO.store(match);
		matchDAO.flush();

		related_teambyteamidwin = teamDAO.store(related_teambyteamidwin);
		teamDAO.flush();

		return match;
	}
}
