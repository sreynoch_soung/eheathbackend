
package ehealth.service;

import ehealth.domain.Timestamp;
import ehealth.domain.User;

import java.util.List;
import java.util.Set;

/**
 * Spring service that handles CRUD requests for Timestamp entities
 * 
 */
public interface TimestampService {

	/**
	* Load an existing Timestamp entity
	* 
	 */
	public Set<Timestamp> loadTimestamps();

	/**
	* Save an existing User entity
	* 
	 */
	public Timestamp saveTimestampUsers(Integer timestampId, User related_users);

	/**
	 */
	public Timestamp findTimestampByPrimaryKey(Integer timestampId_1);

	/**
	* Delete an existing Timestamp entity
	* 
	 */
	public void deleteTimestamp(Timestamp timestamp);

	/**
	* Delete an existing User entity
	* 
	 */
	public Timestamp deleteTimestampUsers(Integer timestamp_timestampId, Integer related_users_userId);

	/**
	* Return a count of all Timestamp entity
	* 
	 */
	public Integer countTimestamps();

	/**
	* Save an existing Timestamp entity
	* 
	 */
	public Timestamp saveTimestamp(Timestamp timestamp_1);

	/**
	* Return all Timestamp entity
	* 
	 */
	public List<Timestamp> findAllTimestamps(Integer startResult, Integer maxRows);
}