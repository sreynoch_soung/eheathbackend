<!DOCTYPE HTML>
<%@ page language="java" isELIgnored="false" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %> 

<fmt:setBundle basename="bundles.user-resources" />

<html>
<head>
<title>New Target</title>
<link href="<c:url value='/resources/bootstrap-3.3.6-dist/css/bootstrap.min.css'/>" rel="stylesheet" type="text/css" />
<link href="<c:url value='/resources/css/header.css'/>" rel="stylesheet" type="text/css" />
<link href="<c:url value='/resources/css/target/newTarget.css'/>" rel="stylesheet" type="text/css" />
<link href="<c:url value='/resources/datepicker/daterangepicker.css'/>" rel="stylesheet" type="text/css" />


<script src="<c:url value='/resources/js/jquery-1.12.3.min.js'/>" /></script>
<script src="<c:url value='/resources/bootstrap-3.3.6-dist/js/bootstrap.min.js'/>" /></script>
<script src="<c:url value='/resources/datepicker/moment.min.js'/>" /></script>
<script src="<c:url value='/resources/datepicker/daterangepicker.js'/>" /></script>


</head>
<body>
	
	<nav class="navbar navbar-default">
	  <div class="container-fluid">
	     <!-- Brand and toggle get grouped for better mobile display -->
		    <div class="navbar-header">
		      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
		        <span class="sr-only">Toggle navigation</span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		      </button>
		      <a class="navbar-brand" href="${pageContext.request.contextPath}/Kid_ListKids?username=${pageContext.request.userPrincipal.name}">e-Health <span class="glyphicon glyphicon-home"></span></a>
		      
		    </div>
		 	
	    <!-- Collect the nav links, forms, and other content for toggling -->
	    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
	        <c:url value="/logout" var="logoutUrl" />
			<form action="${logoutUrl}" method="post" id="logoutForm">
				<input type="hidden" name="${_csrf.parameterName}"
					value="${_csrf.token}" />
			</form>
		      <ul class="nav navbar-nav navbar-right">
		       <li class="dropdown">
			     <a class="dropdown-toggle" data-toggle="dropdown">${kid.doctor.user.name}<b class="caret"></b></a>
			        <ul class="dropdown-menu">
			            <li><a href="${pageContext.request.contextPath}/Doctor_ProfileDoctor?username=${pageContext.request.userPrincipal.name}"><i class="icon-arrow-up"></i> <fmt:message key="profile" /> <span class="glyphicon glyphicon-user"></span></a></li>
			            <li role="separator" class="divider"></li>
			            <li><a href="javascript:formSubmit()"><i class="icon-arrow-down"></i> <fmt:message key="signout" /><span class="glyphicon glyphicon-off"></span></a></li>
			        </ul>
			    </li>
		      </ul>
	    </div><!-- /.navbar-collapse -->
	  </div><!-- /.container-fluid -->
	</nav>
	<div id="contentarea">
		<div class="title"><p><fmt:message key="food" /></p></div>
		<%-- <form:form method="POST" action="/NutritionTargetInput"> --%>
			<table class="contexttable" id="table_food">
				<%-- <tr>
					<td><p><fmt:message key="carbohydrate" />:</p></td>
					<td><form:input type="number" path="nutritiontargetinput.foodtagetypes[0].value"></form:input></td>
					<td><p><fmt:message key="meatfish" />:</p></td>
					<td><form:input type="number" path="nutritiontargetinput.foodtagetypes[1].value"></form:input></td>
				</tr>
				<tr>
					<td><p><fmt:message key="fruitvegetable" />:</p></td>
					<td><form:input type="number" path="nutritiontargetinput.foodtagetypes[2].value"></form:input></td>
				</tr> --%>
				<tr>
					<td><p><fmt:message key="carbohydrate" />:</p></td>
					<td><input type="number" /></td>
					<td><p><fmt:message key="meatfish" />:</p></td>
					<td><input type="number" /></td>
				</tr>
				<tr>
					<td><p><fmt:message key="fruitvegetable" />:</p></td>
					<td><input type="number" /></td>
				</tr>
				<tr>
					<td><p><fmt:message key="duration" />:</p></td>
					<td colspan="3">
			            <div class="input-group date " >
						    <input type="text"  id="daterange-food" ><span class="add-on"><span class="glyphicon glyphicon-calendar"></span></span>
						</div>
					</td>
				</tr>
				<tr>
					<td colspan="4">
						<button  class="btn pull-right" id="saveFoodBtn" ><fmt:message key="savebtn" /></button>
					</td>
				</tr>
			</table>
		<%-- </form:form>	 --%>
		<hr>
		<div class="title"><p><fmt:message key="exercise" /></p></div>
		<table class="contexttable" id="table_exercise">
			<tr>
				<td><p><fmt:message key="hardexertion" />:</p></td>
				<td><input type="number"></td>
				<td><p><fmt:message key="lightexertion" />:</p></td>
				<td><input type="number"></td>
			</tr>
			<tr>
				<td><p><fmt:message key="duration" />:</p></td>
				<td colspan="3">
		            <div class="input-group date " >
					    <input type="text"  id="daterange_exercise"><span class="add-on"><span class="glyphicon glyphicon-calendar"></span></span>
					</div>
				</td>
			</tr>
			<tr>
				<td colspan="4">
					<button  class="btn pull-right" id="saveExerciseBtn" type="submit"><fmt:message key="savebtn" /></button>
				</td>
			</tr>
		</table>
	</div>	
		

	
    <script type="text/javascript">
    var endDate_food;
    var startDate_food;
    var endDate_food;
    var startDate_food;
    
     $('#saveFoodBtn').click(function(){
   	  	endDate = $('#daterange-food').data('daterangepicker').endDate._d;
   	  	startDate = $('#daterange-food').data('daterangepicker').startDate;
   	 	/* alert(startDate); */
    });
     
     $('#saveExerciseBtn').click(function(){
    	  	endDate = $('#daterange_exercise').data('daterangepicker').endDate._d;
    	  	startDate = $('#daterange_exercise').data('daterangepicker').startDate;
    	 	/* alert(startDate); */
     });
    
      $(function() {
        $('#daterange-food').daterangepicker({
        	/* timePicker: true, 
            timePickerIncrement: 30,*/
            locale: {
                format: 'DD/MM/YYYY'
            }
        });
      });
      
      $(function() {
          $('#daterange_exercise').daterangepicker({
        	  locale: {
                  format: 'DD/MM/YYYY'
              }
          });
        });
      
    </script>

</body>


</html>