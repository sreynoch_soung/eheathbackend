<!DOCTYPE HTML>
<%@ page language="java" isELIgnored="false" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<fmt:setBundle basename="bundles.user-resources" />

<html>
<head>
<title>List targets</title>
<link href="<c:url value='/resources/bootstrap-3.3.6-dist/css/bootstrap.min.css'/>" rel="stylesheet" type="text/css" />
<link href="<c:url value='/resources/css/target/listTargets.css'/>" rel="stylesheet" type="text/css" />
<link href="<c:url value='/resources/css/header.css'/>" rel="stylesheet" type="text/css" />
<script src="<c:url value='/resources/js/jquery-1.12.3.min.js'/>" /></script>
<script src="<c:url value='/resources/js/bootstrap-paginator.js'/>" /></script>
<script src="<c:url value='/resources/bootstrap-3.3.6-dist/js/bootstrap.min.js'/>" /></script>
</head>
<body>
	<nav class="navbar navbar-default">
	  <div class="container-fluid">
	     <!-- Brand and toggle get grouped for better mobile display -->
		    <div class="navbar-header">
		      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
		        <span class="sr-only">Toggle navigation</span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		      </button>
		      <a class="navbar-brand" href="${pageContext.request.contextPath}/Kid_ListKids?username=${pageContext.request.userPrincipal.name}">e-Health <span class="glyphicon glyphicon-home"></span></a>
		      
		    </div>
		 	
	    <!-- Collect the nav links, forms, and other content for toggling -->
	    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
	        <c:url value="/logout" var="logoutUrl" />
			<form action="${logoutUrl}" method="post" id="logoutForm">
				<input type="hidden" name="${_csrf.parameterName}"
					value="${_csrf.token}" />
			</form>
		      <ul class="nav navbar-nav navbar-right">
		       <li class="dropdown">
			     <a class="dropdown-toggle" data-toggle="dropdown">${kid.doctor.user.name}<b class="caret"></b></a>
			        <ul class="dropdown-menu">
			            <li><a href="${pageContext.request.contextPath}/Doctor_ProfileDoctor?username=${pageContext.request.userPrincipal.name}"><i class="icon-arrow-up"></i> <fmt:message key="profile" /> <span class="glyphicon glyphicon-user"></span></a></li>
			            <li role="separator" class="divider"></li>
			            <li><a href="javascript:formSubmit()"><i class="icon-arrow-down"></i> <fmt:message key="signout" /><span class="glyphicon glyphicon-off"></span></a></li>
			        </ul>
			    </li>
		      </ul>
	    </div><!-- /.navbar-collapse -->
	  </div><!-- /.container-fluid -->
	</nav>
	
	
	
	<div id="contentarea">
		<div id="content">
		    <ul id="tabs" class="nav nav-tabs" data-tabs="tabs">
		        <li class="active"><a href="#tab_food" data-toggle="tab"><fmt:message key="food" /></a></li>
		        <li><a href="#tab_exercise" data-toggle="tab"><fmt:message key="exercise" /></a></li>
		    </ul>
		    <div id="my-tab-content" class="tab-content">
		        <div class="tab-pane active" id="tab_food">
		            <h1>Red</h1>
		           	<div id="tablewrapper">
						<table id="listFoodTable" class="table table-hover results">
							<tbody>
							<%-- 
								<c:forEach items="${kids}" var="current" varStatus="i">
									<c:choose>
										<c:when test="${(i.count) % 2 == 0}">
											<c:set var="rowclass" value="rowtwo" />
										</c:when>
										<c:otherwise>
											<c:set var="rowclass" value="rowone" />
										</c:otherwise>
									</c:choose> --%>
									<tr class="${rowclass}">
										<td></td>
										<td>
											<table>
												<tr>
													<td>Food Tag Type</td>
													<td>Value</td>
												</tr>
												<tr>
													<td>From Date</td>
													<td>To Date</td>
												</tr>
											</table>
										</td>
										<td>
											<div class="btn-group">
									            <button data-toggle="dropdown" class="btn btn-xs dropdown-toggle" data-original-title="" title="">
									              Action 
									              <span class="caret">
									              </span>
									            </button>
									            <ul class="dropdown-menu pull-right">
									              <li>
									                <a href="#">Edit</a>
									              </li>
									              <li>
									                <a href="#">Delete</a>
									              </li>
									            </ul>
									          </div>
										</td>
									</tr>
								<%-- </c:forEach> --%>
								
							</tbody>
						</table>
					</div>
		        </div>
		        <div class="tab-pane" id="tab_exercise">
		            <h1>Orange</h1>
		            <div id="tablewrapper">
						<table id="listExerciseTable" class="table table-hover results">
							<tbody>
							
								<c:forEach items="${kids}" var="current" varStatus="i">
									<c:choose>
										<c:when test="${(i.count) % 2 == 0}">
											<c:set var="rowclass" value="rowtwo" />
										</c:when>
										<c:otherwise>
											<c:set var="rowclass" value="rowone" />
										</c:otherwise>
									</c:choose>
									<tr class="${rowclass}">
										<td></td>
										<td>
											<table>
												<tr>
													<td>Exercise Tag Type</td>
													<td>Value</td>
												</tr>
												<tr>
													<td>From Date</td>
													<td>To Date</td>
												</tr>
											</table>
										</td>
										<td>
											<div class="btn-group">
									            <button data-toggle="dropdown" class="btn btn-xs dropdown-toggle" data-original-title="" title="">
									              Action 
									              <span class="caret">
									              </span>
									            </button>
									            <ul class="dropdown-menu pull-right">
									              <li>
									                <a href="#">Edit</a>
									              </li>
									              <li>
									                <a href="#">Delete</a>
									              </li>
									            </ul>
									          </div>
										</td>
									</tr>
								</c:forEach>
								
							</tbody>
						</table>
					</div>
		        </div>
		    </div>
		</div>
	
	
		
		
		<div id="footer">
			
		</div>
	</div>
	
	
	<script type='text/javascript'>
        var options = {
            currentPage: 1,
            totalPages: 2
        }

        $('#footer').bootstrapPaginator(options);
  	</script>
  	
  	<script>
			function formSubmit() {
				document.getElementById("logoutForm").submit();
			}
	</script>
            

</body>


</html>