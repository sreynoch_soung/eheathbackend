<!DOCTYPE HTML>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page session="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<fmt:setBundle basename="bundles.user-resources" />

<html>
<head>
<title>Login Page</title>
<link href="<c:url value='/resources/css/login.css'/>" rel="stylesheet" type="text/css"/>
<link href="<c:url value='/resources/css/normalize.css'/>" rel="stylesheet" type="text/css"/>
<link href="<c:url value='/resources/css/header.css'/>" rel="stylesheet" type="text/css" />
<script src="<c:url value='/resources/js/prefixfree.min.js'/>"/></script>
</head>
<body onload='document.loginForm.username.focus();'>
	<div class="login">
		<div id="title">
			<h1>e-Health</h1>
			<c:if test="${not empty error}">
				<div class="error">${error}</div>
			</c:if>
			<c:if test="${not empty msg}">
				<div class="msg">${msg}</div>
			</c:if>
		</div>
		<form name='loginForm' action="<c:url value='/login' />" method='POST'>
			<input type="text" name="username" placeholder="Username" required="required" />
			<input type="password" name="password" placeholder="Password" required="required" />
			<button type="submit" class="btn">Sign in</button>
			<input type="hidden" name="${_csrf.parameterName}"
				value="${_csrf.token}" />
		</form>
		${_csrf.parameterName}
		${_csrf.token}
			
	</div>
</body>
</html>