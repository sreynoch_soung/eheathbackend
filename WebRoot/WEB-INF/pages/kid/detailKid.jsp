<!DOCTYPE HTML>
<%@ page language="java" isELIgnored="false" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %> 

<fmt:setBundle basename="bundles.user-resources" />

<html>
<head>
<title>Detail</title>
<link href="<c:url value='/resources/bootstrap-3.3.6-dist/css/bootstrap.min.css'/>" rel="stylesheet" type="text/css" />
<link href="<c:url value='/resources/css/header.css'/>" rel="stylesheet" type="text/css" />
<link href="<c:url value='/resources/css/kid/detailKid.css'/>" rel="stylesheet" type="text/css" />


<script src="<c:url value='/resources/js/jquery-1.12.3.min.js'/>" /></script>
<script src="<c:url value='/resources/bootstrap-3.3.6-dist/js/bootstrap.min.js'/>" /></script>

<script src="<c:url value='/resources/js/highchart/highcharts.js'/>"/></script>
<script src="<c:url value='/resources/js/highchart/exporting.js'/>"/></script>

</head>
<body>
	
	<nav class="navbar navbar-default">
	  <div class="container-fluid">
	     <!-- Brand and toggle get grouped for better mobile display -->
		    <div class="navbar-header">
		      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
		        <span class="sr-only">Toggle navigation</span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		      </button>
		      <a class="navbar-brand" href="${pageContext.request.contextPath}/Kid_ListKids?username=${pageContext.request.userPrincipal.name}">e-Health <span class="glyphicon glyphicon-home"></span></a>
		      
		    </div>
		 	
	    <!-- Collect the nav links, forms, and other content for toggling -->
	    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
	        <c:url value="/logout" var="logoutUrl" />
			<form action="${logoutUrl}" method="post" id="logoutForm">
				<input type="hidden" name="${_csrf.parameterName}"
					value="${_csrf.token}" />
			</form>
	      <ul class="nav navbar-nav navbar-right">
	       <li class="dropdown">
		     <a class="dropdown-toggle" data-toggle="dropdown">${kid.doctor.user.name}<b class="caret"></b></a>
		        <ul class="dropdown-menu">
		            <li><a href="${pageContext.request.contextPath}/Doctor_ProfileDoctor?username=${pageContext.request.userPrincipal.name}"><i class="icon-arrow-up"></i> <fmt:message key="profile" /> <span class="glyphicon glyphicon-user"></span></a></li>
		            <li role="separator" class="divider"></li>
		            <li><a href="javascript:formSubmit()"><i class="icon-arrow-down"></i> <fmt:message key="signout" /><span class="glyphicon glyphicon-off"></span></a></li>
		        </ul>
		    </li>
	      </ul>
	    </div><!-- /.navbar-collapse -->
	  </div><!-- /.container-fluid -->
	</nav>
	<div id="contentarea">
		<table id="contexttable">
			<tr>
				<td><p><fmt:message key="user.name.title" /></p></td>
				<td><p><fmt:message key="timestamp.createdate.title" /></p></td>
				<td><p><fmt:message key="kid.connect.title" /></p></td>
			</tr>
			<tr>
				<td><p>${kid.user.name}</p></td>
				<td><p>${kid.user.timestamp.createDate.getTime().toString()}</p></td>
				<td><c:choose>
						<c:when test="${kid.connected}">
							<button id="connectedBtn" class=" btn btn-xsm glyphicon glyphicon-ok" style="color: green; background-color: #dff0d8;"></button>
						</c:when>
						<c:otherwise>
							<button  id="connectedBtn" class=" btn btn-xsm glyphicon glyphicon-remove" style="color: red; background-color: #f2dede;"></button>
						</c:otherwise>
					</c:choose>
				</td>
				
			</tr>
			<tr>
				<td><p><fmt:message key="user.surename.title" /></p></td>
				<td><p><fmt:message key="kid.weight.title" /></p></td>
				<td><p><fmt:message key="addtargetbtn" /></p></td>
			</tr>
			<tr>
				<td><p>${kid.user.surename}</p></td>
				<td><p>${kid.weight} kg</p></td>
				<td>
					<a href="${pageContext.request.contextPath}/Target_NewTarget?kid_id=${kid.kidId}" class="btn btn-xsm glyphicon glyphicon-plus" id="addTargetBtn" ></a>
				</td>
			</tr>
			<tr>
				<td><p><fmt:message key="user.sex.title" /></p></td>
				<td><p><fmt:message key="kid.high.title" /></p></td>
				<td></td>
			</tr>
			<tr>
				<td><p>${kid.user.sex}</p></td>
				<td><p>${kid.high} m</p></td>
				<td></td>
			</tr>
			<tr>
				<td><p><fmt:message key="user.telephone.title" /></p></td>
				<td><p><fmt:message key="user.age.title" /></p></td>
				<td></td>
			</tr>
			<tr>
				<td><p>${kid.user.telephone}</p></td>
				<td><p>${kid.user.age}<fmt:message key="years" /></p></td>
				<td></td>
			</tr>
		</table>
		<hr>
		<div id="progressBar_div">
			 
			<div class="title_div"><p><fmt:message key="carbohydrate" />: </p></div>
			<div id="more_detail_div"><a href="${pageContext.request.contextPath}/Target_ListTargets?kid_id=${kid.kidId}" class="btn btn-xsm glyphicon glyphicon-chevron-right" ></a></div>
			<div class="progress progress-success progress-striped" ><!-- active -->
		    	 <div id="progressBarCarbohydrates" class="progress-bar" role="progressbar" aria-valuemin="0" aria-valuemax="100" style="width: 0%"></div>
		 	</div>
		 	
		 	<div class="title_div"><p><fmt:message key="meatfish" />: </p></div>
		     <div class="progress progress-success progress-striped" >
		    	 <div id="progressBarMeatFish" class="progress-bar" role="progressbar" aria-valuemin="0" aria-valuemax="100" style="width: 0%"></div>
		 	</div>
		 	
		 	<div class="title_div"><p><fmt:message key="fruitvegetable" />: </p></div>
		 	<div class="progress progress-success progress-striped" >
		    	 <div id="progressBarFruitVegetable" class="progress-bar" role="progressbar" aria-valuemin="0" aria-valuemax="100" style="width: 0%"></div>
		 	</div>
		 
		 	<div class="title_div"><p><fmt:message key="exercise" />: </p></div>
		     <div class="progress progress-success progress-striped" >
		    	 <div id="progressBarExercise" class="progress-bar" role="progressbar" aria-valuemin="0" aria-valuemax="100" style="width: 0%"></div>
		 	</div>
		
		</div>
		<hr>
		<div id="graph_div">
			 
			<div id="graph_food_div">
				<div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
			</div>
			<hr>
			<div id="graph_exercise_div">
				<div id="container1" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
			</div>
			
		
		</div>
		
	</div><!-- end content area -->
	<script type="text/javascript"> 
	
	$(document).ready (function updateProgress() {  
            document.getElementById('progressBarCarbohydrates').style.width = ${kid.weight}+"%";
            document.getElementById('progressBarMeatFish').style.width = ${kid.weight}+"%";
            document.getElementById('progressBarFruitVegetable').style.width = ${kid.weight}+"%";
            document.getElementById('progressBarExercise').style.width = ${kid.weight}+"%";
        });
	</script>
  	
  	<script>
			function formSubmit() {
				document.getElementById("logoutForm").submit();
			}
			
			$(function () {
			    $('#container').highcharts({
			    	credits: {
			            enabled: false
			        },
			        chart: {
			            type: 'line'
			        },
			        title: {
			            text: '<fmt:message key="food" />'
			        },
			        subtitle: {
			            text: '<fmt:message key="monthlyaverage" />'
			        },
			        xAxis: {
			            categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
			        },
			        yAxis: {
			            title: {
			                text: ''
			            }
			        },
			        plotOptions: {
			            line: {
			                dataLabels: {
			                    enabled: false
			                },
			                enableMouseTracking: true
			            }
			        },
			        series: [{
			            name: '<fmt:message key="carbohydrate" />',
			            color: '#607D8B',
			            data: [7.0, 6.9, 9.5, 0, 0, 21.5, 25.2, 26.5, 18.3, 13.9, 9.6]
			        }, {
			            name: '<fmt:message key="meatfish" />',
			            color: '#6D4C41',
			            data: [3.9, 4.2, 5.7, 0, 0, 15.2, 17.0, 16.6, 14.2, 10.3, 6.6, 4.8]
			        }, {
			            name: '<fmt:message key="fruitvegetable" />',
			            color: '#239957',
			            data: [-1.9, 5.2, 0.7, 8.5, 2.9, 6.2, 1.0, 9.6, 4.2, 5.3, 8.6, 0.8]
			        }]
			    });
			});
			
			
			$(function () {
			    $('#container1').highcharts({
			    	credits: {
			            enabled: false
			        },
			        chart: {
			            type: 'line'
			        },
			        title: {
			            text: '<fmt:message key="exercise" />'
			        },
			        subtitle: {
			            text: '<fmt:message key="monthlyaverage" />'
			        },
			        xAxis: {
			            categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
			        },
			        yAxis: {
			            title: {
			                text: ''
			            }
			        },
			        plotOptions: {
			            line: {
			                dataLabels: {
			                    enabled: false
			                },
			                enableMouseTracking: true
			            }
			        },
			        series: [{
			            name: 'Exercise',
			            color: '#00B0FF',
			            data: [7.0, 6.9, 9.5, 0, 0, 21.5, 25.2, 26.5, 18.3, 13.9, 9.6]
			        }]
			    });
			});
			
			
	</script>
            

</body>


</html>