<!DOCTYPE HTML>
<%@ page language="java" isELIgnored="false" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<fmt:setBundle basename="bundles.user-resources" />

<html>
<head>
<title>List kids</title>
<link href="<c:url value='/resources/bootstrap-3.3.6-dist/css/bootstrap.min.css'/>" rel="stylesheet" type="text/css" />
<link href="<c:url value='/resources/css/kid/listKids.css'/>" rel="stylesheet" type="text/css" />
<link href="<c:url value='/resources/css/header.css'/>" rel="stylesheet" type="text/css" />
<script src="<c:url value='/resources/js/jquery-1.12.3.min.js'/>" /></script>
<script src="<c:url value='/resources/js/bootstrap-paginator.js'/>" /></script>
<script src="<c:url value='/resources/bootstrap-3.3.6-dist/js/bootstrap.min.js'/>" /></script>
</head>
<body>
	
	<nav class="navbar navbar-default">
	  <div class="container-fluid">
	     <!-- Brand and toggle get grouped for better mobile display -->
		    <div class="navbar-header">
		      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
		        <span class="sr-only">Toggle navigation</span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		      </button>
		      <a class="navbar-brand" href="#">e-Health</a>
		    </div>
	
	    <!-- Collect the nav links, forms, and other content for toggling -->
	    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
	      
	       <form class="navbar-form navbar-left" role="search">
		        <div class="input-group">
		            <input type="text" class="form-control search" placeholder="Search" name="srch-term" id="srch-term">
		            <div class="input-group-btn">
		                <button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
		            </div>
		        </div>
		       
	        </form>
	        <c:url value="/logout" var="logoutUrl" />
			<form action="${logoutUrl}" method="post" id="logoutForm">
				<input type="hidden" name="${_csrf.parameterName}"
					value="${_csrf.token}" />
			</form>
	      <ul class="nav navbar-nav navbar-right">
	       <li class="dropdown">
		    <c:forEach items="${kids}" var="kid" end="0">
		     <a class="dropdown-toggle" data-toggle="dropdown">${kid.doctor.user.name}<b class="caret"></b></a>
		
		        </c:forEach>
		        <ul class="dropdown-menu">
		            <li><a href="${pageContext.request.contextPath}/Doctor_ProfileDoctor?username=${pageContext.request.userPrincipal.name}"><i class="icon-arrow-up"></i> <fmt:message key="profile" /> <span class="glyphicon glyphicon-user"></span></a></li>
		            <li role="separator" class="divider"></li>
		            <li><a href="javascript:formSubmit()"><i class="icon-arrow-down"></i> <fmt:message key="signout" /> <span class="glyphicon glyphicon-off"></span></a></li>
		        </ul>
		    </li>
	      </ul>
	    </div><!-- /.navbar-collapse -->
	  </div><!-- /.container-fluid -->
	</nav>
	<div id="contentarea">
		<div id="tablewrapper">
			<table id="listKidsTable" class="table table-hover results">
				<thead>
					<tr>
						<th class="thead">No</th>
						<th class="thead"><fmt:message key="user.name.title" /></th>
						<th class="thead"><fmt:message key="user.surename.title" /></th>
						<th class="thead"><fmt:message key="user.telephone.title" /></th>
						<th class="thead"><fmt:message key="city.title" /></th>
						<th class="thead"><fmt:message key="kid.connect.title" /></th>
					</tr>
				</thead>
				<tbody>
				
					<c:forEach items="${kids}" var="current" varStatus="i">
						<c:choose>
							<c:when test="${(i.count) % 2 == 0}">
								<c:set var="rowclass" value="rowtwo" />
							</c:when>
							<c:otherwise>
								<c:set var="rowclass" value="rowone" />
							</c:otherwise>
						</c:choose>
						<tr class="${rowclass}">
							<td ></td>
							<td ><a href="${pageContext.request.contextPath}/Kid_DetailKid${current.kidId}"> ${current.user.name}&nbsp;</a></td>
							<td >${current.user.surename}&nbsp;</td>
							<td >${current.user.telephone}&nbsp;</td>
							<td >${current.city.name}&nbsp;</td>
							<td >
								<c:choose>
									<c:when test="${current.connected}">
										<span class="glyphicon glyphicon-ok"></span>
									</c:when>
									<c:otherwise>
										<span class="glyphicon glyphicon-remove"></span>
									</c:otherwise>
								</c:choose>
								
							</td>
						</tr>
					</c:forEach>
					
				</tbody>
			</table>
		</div>
		
		<div id="footer">
			
		</div>
	</div>
	
	<script>
	$(document).ready(function() {
		  $(".search").keyup(function () {
		    var searchTerm = $(".search").val();
		    var listItem = $('.results tbody').children('tr');
		    var searchSplit = searchTerm.replace(/ /g, "'):containsi('")
		    
		  $.extend($.expr[':'], {'containsi': function(elem, i, match, array){
		        return (elem.textContent || elem.innerText || '').toLowerCase().indexOf((match[3] || "").toLowerCase()) >= 0;
		    }
		  });
		    
		  $(".results tbody tr").not(":containsi('" + searchSplit + "')").each(function(e){
		    $(this).attr('visible','false');
		  });
	
		  $(".results tbody tr:containsi('" + searchSplit + "')").each(function(e){
		    $(this).attr('visible','true');
		  });
	
		  var jobCount = $('.results tbody tr[visible="true"]').length;
		    $('.counter').text(jobCount + ' item');
	
		  if(jobCount == '0') {$('.no-result').show();}
		    else {$('.no-result').hide();}
				  });
		});
	</script>
	<script type='text/javascript'>
        var options = {
            currentPage: 1,
            totalPages: 2
        }

        $('#footer').bootstrapPaginator(options);
  	</script>
  	
  	<script>
			function formSubmit() {
				document.getElementById("logoutForm").submit();
			}
	</script>
            

</body>


</html>