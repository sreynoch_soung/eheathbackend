<!DOCTYPE HTML>
<%@ page language="java" isELIgnored="false" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %> 

<fmt:setBundle basename="bundles.user-resources" />
<html>
<head>
<title>Profile</title>
<link href="<c:url value='/resources/bootstrap-3.3.6-dist/css/bootstrap.min.css'/>" rel="stylesheet" type="text/css" />
<link href="<c:url value='/resources/css/header.css'/>" rel="stylesheet" type="text/css" />
<link href="<c:url value='/resources/css/doctor/profileDoctor.css'/>" rel="stylesheet" type="text/css" />


<script src="<c:url value='/resources/js/jquery-1.12.3.min.js'/>" /></script>
<script src="<c:url value='/resources/bootstrap-3.3.6-dist/js/bootstrap.min.js'/>" /></script>


</head>
<body>
	
	<nav class="navbar navbar-default">
	  <div class="container-fluid">
	     <!-- Brand and toggle get grouped for better mobile display -->
		    <div class="navbar-header">
		      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
		        <span class="sr-only">Toggle navigation</span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		      </button>
		      <a class="navbar-brand" href="${pageContext.request.contextPath}/Kid_ListKids?username=${pageContext.request.userPrincipal.name}">e-Health <span class="glyphicon glyphicon-home"></span></a>
		    </div>
	
	    <!-- Collect the nav links, forms, and other content for toggling -->
	    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
	      
	        <c:url value="/logout" var="logoutUrl" />
			<form action="${logoutUrl}" method="post" id="logoutForm">
				<input type="hidden" name="${_csrf.parameterName}"
					value="${_csrf.token}" />
			</form>
	      <ul class="nav navbar-nav navbar-right">
	       <li class="dropdown">
		     <a class="dropdown-toggle" data-toggle="dropdown">${user.name}<b class="caret"></b></a>
		        <ul class="dropdown-menu">
		            <li><a href="${pageContext.request.contextPath}/Doctor_ProfileDoctor?username=${pageContext.request.userPrincipal.name}"><i class="icon-arrow-up"></i> <fmt:message key="profile" />  <span class="glyphicon glyphicon-user"></span></a></li>
		            <li role="separator" class="divider"></li>
		            <li><a href="javascript:formSubmit()"><i class="icon-arrow-down"></i> <fmt:message key="signout" /> <span class="glyphicon glyphicon-off"></span></a></li>
		        </ul>
		    </li>
	      </ul>
	    </div><!-- /.navbar-collapse -->
	  </div><!-- /.container-fluid -->
	</nav>
	<div id="contentarea">
	<form:form  name='UpdateProfileForm' action="${pageContext.request.contextPath}/Doctor/UpdateDoctor" method='POST' modelAttribute="user">
		
		<form:hidden id="userId" path="userId"/>
		<form:hidden id="enabled" path="enabled"/>
		<form:hidden id="sex" path="sex"/>
		<%-- <form:hidden id="timestamp" path="timestamp"/>
		<form:hidden id="userRole" path="userRole"/> --%>
		
		<table id="contexttable">
			<tr>
				<td><img src="resources/images/doctor/doctor.png"></td>
				<td><h3>${doctor.hospital}</h3></td>
			</tr>
			<tr>
				<fmt:message key="user.username.title" var="username" />
				<td><font color="red"> (*) </font><form:input type="text" name="username"  required="required" placeholder="${username}" path="username"></form:input></td>
				<td><h5><fmt:message key="newpassword" /></h5></td>
			</tr>
			<tr>
				<fmt:message key="user.surename.title" var="surename"/>
				<td><font color="red"> (*) </font><form:input type="text" name="surename"  required="required" placeholder="${surename}" path="surename"></form:input></td>
				<td><input type="password" placeholder="<fmt:message key="newpassword" />" ></td>
			</tr>
			<tr>
				<fmt:message key="user.name.title" var="name"/>
				<td><font color="red"> (*) </font><form:input type="text" name ="name"  required="required" placeholder="${name}" path="name"></form:input></td>
				<td><input type="password" placeholder="<fmt:message key="confirmpassword" />"  value=""></td>
			</tr>
			<tr>
				<fmt:message key="user.email.title" var="email"/>
				<td><font color="red"> (*) </font><form:input type="text" name="email"  required="required" placeholder="${email}" path="email"></form:input></td>
				<td><h5><fmt:message key="yourcode" /></h5></td>
			</tr>
			<tr>
				<fmt:message key="user.telephone.title" var="telephone"/>
				<td><font color="red"> (*) </font><form:input type="text" name="telephone"  required="required" placeholder="${telephone}" path="telephone"></form:input></td>
				
				<td>
					<form:form action="#">
					<input type="text" name="codekey" id ="codekey" value="${doctor.codeKey}"  disabled>
				
					<a class="btn btn-xsm" href="${pageContext.request.contextPath}/Doctor_NewCodeKey?username=${pageContext.request.userPrincipal.name}"><fmt:message key="newcodebtn" /></a>
					</form:form>
				</td>
				
			</tr>
			<tr>
				<td colspan=2>
				<button  class="btn pull-right" id="updateBtn" type="submit"><fmt:message key="updatebtn" /></button>
				</td>
				
			</tr>
			
		</table>
		<input type="hidden" name="${_csrf.parameterName}"
				value="${_csrf.token}" />
	</form:form>
	</div>
	
  	<script>
			function formSubmit() {
				document.getElementById("logoutForm").submit();
			}
	</script>
            

</body>


</html>