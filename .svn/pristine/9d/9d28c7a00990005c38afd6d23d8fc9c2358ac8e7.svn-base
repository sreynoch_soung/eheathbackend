package ehealth.service;

import ehealth.dao.ChatRecordDAO;
import ehealth.dao.KidChatDAO;

import ehealth.domain.ChatRecord;
import ehealth.domain.KidChat;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;

import org.springframework.transaction.annotation.Transactional;

/**
 * Spring service that handles CRUD requests for ChatRecord entities
 * 
 */

@Service("ChatRecordService")

@Transactional
public class ChatRecordServiceImpl implements ChatRecordService {

	/**
	 * DAO injected by Spring that manages ChatRecord entities
	 * 
	 */
	@Autowired
	private ChatRecordDAO chatRecordDAO;

	/**
	 * DAO injected by Spring that manages KidChat entities
	 * 
	 */
	@Autowired
	private KidChatDAO kidChatDAO;

	/**
	 * Instantiates a new ChatRecordServiceImpl.
	 *
	 */
	public ChatRecordServiceImpl() {
	}

	/**
	 * Save an existing KidChat entity
	 * 
	 */
	@Transactional
	public ChatRecord saveChatRecordKidChats(Integer chatRecordId, KidChat related_kidchats) {
		ChatRecord chatrecord = chatRecordDAO.findChatRecordByPrimaryKey(chatRecordId, -1, -1);
		KidChat existingkidChats = kidChatDAO.findKidChatByPrimaryKey(related_kidchats.getKidChatId());

		// copy into the existing record to preserve existing relationships
		if (existingkidChats != null) {
			existingkidChats.setKidChatId(related_kidchats.getKidChatId());
			related_kidchats = existingkidChats;
		}

		related_kidchats.setChatRecord(chatrecord);
		chatrecord.getKidChats().add(related_kidchats);
		related_kidchats = kidChatDAO.store(related_kidchats);
		kidChatDAO.flush();

		chatrecord = chatRecordDAO.store(chatrecord);
		chatRecordDAO.flush();

		return chatrecord;
	}

	/**
	 * Save an existing ChatRecord entity
	 * 
	 */
	@Transactional
	public ChatRecord saveChatRecord(ChatRecord chatrecord) {
		ChatRecord existingChatRecord = chatRecordDAO.findChatRecordByPrimaryKey(chatrecord.getChatRecordId());

		if (existingChatRecord != null) {
			if (existingChatRecord != chatrecord) {
				existingChatRecord.setChatRecordId(chatrecord.getChatRecordId());
				existingChatRecord.setDate(chatrecord.getDate());
				existingChatRecord.setMessage(chatrecord.getMessage());
			}
			chatrecord = chatRecordDAO.store(existingChatRecord);
		} else {
			chatrecord = chatRecordDAO.store(chatrecord);
		}
		chatRecordDAO.flush();
		return chatrecord;
	}

	/**
	 * Return all ChatRecord entity
	 * 
	 */
	@Transactional
	public List<ChatRecord> findAllChatRecords(Integer startResult, Integer maxRows) {
		return new java.util.ArrayList<ChatRecord>(chatRecordDAO.findAllChatRecords(startResult, maxRows));
	}

	/**
	 */
	@Transactional
	public ChatRecord findChatRecordByPrimaryKey(Integer chatRecordId) {
		return chatRecordDAO.findChatRecordByPrimaryKey(chatRecordId);
	}

	/**
	 * Return a count of all ChatRecord entity
	 * 
	 */
	@Transactional
	public Integer countChatRecords() {
		return ((Long) chatRecordDAO.createQuerySingleResult("select count(o) from ChatRecord o").getSingleResult()).intValue();
	}

	/**
	 * Delete an existing ChatRecord entity
	 * 
	 */
	@Transactional
	public void deleteChatRecord(ChatRecord chatrecord) {
		chatRecordDAO.remove(chatrecord);
		chatRecordDAO.flush();
	}

	/**
	 * Delete an existing KidChat entity
	 * 
	 */
	@Transactional
	public ChatRecord deleteChatRecordKidChats(Integer chatrecord_chatRecordId, Integer related_kidchats_kidChatId) {
		KidChat related_kidchats = kidChatDAO.findKidChatByPrimaryKey(related_kidchats_kidChatId, -1, -1);

		ChatRecord chatrecord = chatRecordDAO.findChatRecordByPrimaryKey(chatrecord_chatRecordId, -1, -1);

		related_kidchats.setChatRecord(null);
		chatrecord.getKidChats().remove(related_kidchats);

		kidChatDAO.remove(related_kidchats);
		kidChatDAO.flush();

		return chatrecord;
	}

	/**
	 * Load an existing ChatRecord entity
	 * 
	 */
	@Transactional
	public Set<ChatRecord> loadChatRecords() {
		return chatRecordDAO.findAllChatRecords();
	}
}
