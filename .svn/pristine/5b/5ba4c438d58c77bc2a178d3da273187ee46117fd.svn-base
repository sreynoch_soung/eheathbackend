
package ehealth.dao;

import ehealth.domain.League;

import java.util.Set;

import org.skyway.spring.util.dao.JpaDao;

import org.springframework.dao.DataAccessException;

/**
 * DAO to manage League entities.
 * 
 */
public interface LeagueDAO extends JpaDao<League> {

	/**
	 * JPQL Query - findLeagueByLeagueName
	 *
	 */
	public Set<League> findLeagueByLeagueName(String leagueName) throws DataAccessException;

	/**
	 * JPQL Query - findLeagueByLeagueName
	 *
	 */
	public Set<League> findLeagueByLeagueName(String leagueName, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findLeagueByLeagueId
	 *
	 */
	public League findLeagueByLeagueId(Integer leagueId) throws DataAccessException;

	/**
	 * JPQL Query - findLeagueByLeagueId
	 *
	 */
	public League findLeagueByLeagueId(Integer leagueId, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findLeagueByLeagueNameContaining
	 *
	 */
	public Set<League> findLeagueByLeagueNameContaining(String leagueName_1) throws DataAccessException;

	/**
	 * JPQL Query - findLeagueByLeagueNameContaining
	 *
	 */
	public Set<League> findLeagueByLeagueNameContaining(String leagueName_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findLeagueByMatchDateContaining
	 *
	 */
	public Set<League> findLeagueByMatchDateContaining(String matchDate) throws DataAccessException;

	/**
	 * JPQL Query - findLeagueByMatchDateContaining
	 *
	 */
	public Set<League> findLeagueByMatchDateContaining(String matchDate, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findLeagueByPoint
	 *
	 */
	public Set<League> findLeagueByPoint(String point) throws DataAccessException;

	/**
	 * JPQL Query - findLeagueByPoint
	 *
	 */
	public Set<League> findLeagueByPoint(String point, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findAllLeagues
	 *
	 */
	public Set<League> findAllLeagues() throws DataAccessException;

	/**
	 * JPQL Query - findAllLeagues
	 *
	 */
	public Set<League> findAllLeagues(int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findLeagueByMatchDate
	 *
	 */
	public Set<League> findLeagueByMatchDate(String matchDate_1) throws DataAccessException;

	/**
	 * JPQL Query - findLeagueByMatchDate
	 *
	 */
	public Set<League> findLeagueByMatchDate(String matchDate_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findLeagueByPrimaryKey
	 *
	 */
	public League findLeagueByPrimaryKey(Integer leagueId_1) throws DataAccessException;

	/**
	 * JPQL Query - findLeagueByPrimaryKey
	 *
	 */
	public League findLeagueByPrimaryKey(Integer leagueId_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findLeagueByPointContaining
	 *
	 */
	public Set<League> findLeagueByPointContaining(String point_1) throws DataAccessException;

	/**
	 * JPQL Query - findLeagueByPointContaining
	 *
	 */
	public Set<League> findLeagueByPointContaining(String point_1, int startResult, int maxRows) throws DataAccessException;

}