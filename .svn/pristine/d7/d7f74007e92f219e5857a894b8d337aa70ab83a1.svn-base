package ehealth.service;

import ehealth.dao.KidDAO;
import ehealth.dao.KidMeasuresDAO;

import ehealth.domain.Kid;
import ehealth.domain.KidMeasures;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;

import org.springframework.transaction.annotation.Transactional;

/**
 * Spring service that handles CRUD requests for KidMeasures entities
 * 
 */

@Service("KidMeasuresService")

@Transactional
public class KidMeasuresServiceImpl implements KidMeasuresService {

	/**
	 * DAO injected by Spring that manages Kid entities
	 * 
	 */
	@Autowired
	private KidDAO kidDAO;

	/**
	 * DAO injected by Spring that manages KidMeasures entities
	 * 
	 */
	@Autowired
	private KidMeasuresDAO kidMeasuresDAO;

	/**
	 * Instantiates a new KidMeasuresServiceImpl.
	 *
	 */
	public KidMeasuresServiceImpl() {
	}

	/**
	 * Delete an existing Kid entity
	 * 
	 */
	@Transactional
	public KidMeasures deleteKidMeasuresKids(Integer kidmeasures_kidMeasuresId, Integer related_kids_kidId) {
		Kid related_kids = kidDAO.findKidByPrimaryKey(related_kids_kidId, -1, -1);

		KidMeasures kidmeasures = kidMeasuresDAO.findKidMeasuresByPrimaryKey(kidmeasures_kidMeasuresId, -1, -1);

		related_kids.setKidMeasures(null);
		kidmeasures.getKids().remove(related_kids);

		kidDAO.remove(related_kids);
		kidDAO.flush();

		return kidmeasures;
	}

	/**
	 * Delete an existing KidMeasures entity
	 * 
	 */
	@Transactional
	public void deleteKidMeasures(KidMeasures kidmeasures) {
		kidMeasuresDAO.remove(kidmeasures);
		kidMeasuresDAO.flush();
	}

	/**
	 * Return all KidMeasures entity
	 * 
	 */
	@Transactional
	public List<KidMeasures> findAllKidMeasuress(Integer startResult, Integer maxRows) {
		return new java.util.ArrayList<KidMeasures>(kidMeasuresDAO.findAllKidMeasuress(startResult, maxRows));
	}

	/**
	 * Save an existing KidMeasures entity
	 * 
	 */
	@Transactional
	public KidMeasures saveKidMeasures(KidMeasures kidmeasures) {
		KidMeasures existingKidMeasures = kidMeasuresDAO.findKidMeasuresByPrimaryKey(kidmeasures.getKidMeasuresId());

		if (existingKidMeasures != null) {
			if (existingKidMeasures != kidmeasures) {
				existingKidMeasures.setKidMeasuresId(kidmeasures.getKidMeasuresId());
				existingKidMeasures.setGlucoseLevel(kidmeasures.getGlucoseLevel());
				existingKidMeasures.setBloodSugarLevel(kidmeasures.getBloodSugarLevel());
				existingKidMeasures.setHabit(kidmeasures.getHabit());
				existingKidMeasures.setMets(kidmeasures.getMets());
			}
			kidmeasures = kidMeasuresDAO.store(existingKidMeasures);
		} else {
			kidmeasures = kidMeasuresDAO.store(kidmeasures);
		}
		kidMeasuresDAO.flush();
		return kidmeasures;
	}

	/**
	 * Return a count of all KidMeasures entity
	 * 
	 */
	@Transactional
	public Integer countKidMeasuress() {
		return ((Long) kidMeasuresDAO.createQuerySingleResult("select count(o) from KidMeasures o").getSingleResult()).intValue();
	}

	/**
	 * Load an existing KidMeasures entity
	 * 
	 */
	@Transactional
	public Set<KidMeasures> loadKidMeasuress() {
		return kidMeasuresDAO.findAllKidMeasuress();
	}

	/**
	 * Save an existing Kid entity
	 * 
	 */
	@Transactional
	public KidMeasures saveKidMeasuresKids(Integer kidMeasuresId, Kid related_kids) {
		KidMeasures kidmeasures = kidMeasuresDAO.findKidMeasuresByPrimaryKey(kidMeasuresId, -1, -1);
		Kid existingkids = kidDAO.findKidByPrimaryKey(related_kids.getKidId());

		// copy into the existing record to preserve existing relationships
		if (existingkids != null) {
			existingkids.setKidId(related_kids.getKidId());
			existingkids.setHigh(related_kids.getHigh());
			existingkids.setWeight(related_kids.getWeight());
			existingkids.setAddress(related_kids.getAddress());
			existingkids.setCodeKey(related_kids.getCodeKey());
			related_kids = existingkids;
		} else {
			related_kids = kidDAO.store(related_kids);
			kidDAO.flush();
		}

		related_kids.setKidMeasures(kidmeasures);
		kidmeasures.getKids().add(related_kids);
		related_kids = kidDAO.store(related_kids);
		kidDAO.flush();

		kidmeasures = kidMeasuresDAO.store(kidmeasures);
		kidMeasuresDAO.flush();

		return kidmeasures;
	}

	/**
	 */
	@Transactional
	public KidMeasures findKidMeasuresByPrimaryKey(Integer kidMeasuresId) {
		return kidMeasuresDAO.findKidMeasuresByPrimaryKey(kidMeasuresId);
	}
}
